<!DOCTYPE html>
<html>
	<head>
		<title>Xpress Leader</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="apple-touch-icon" sizes="57x57" href="/images/logos/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/images/logos/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/logos/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/images/logos/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/logos/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/images/logos/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/logos/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/images/logos/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/images/logos/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/images/logos/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/logos/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/images/logos/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/logos/favicon/favicon-16x16.png">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/images/logos/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		
		<!-- Bootstrap -->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" media="screen" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		
		<link href="/style/v4.site.css" rel="stylesheet">
		<link href="/assets/timepicker/timepicker.min.css" media="screen" rel="stylesheet">
		<link href="/assets/datepicker/css/datepicker.css" media="screen" rel="stylesheet">
		
		<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Raleway:300,500,600' rel='stylesheet' type='text/css'>
	</head>
	<body class="homepage">
	    <?php
	  	if($validkey){
	  	    
	  		if(isset($activationexpire)&&$activationexpire) include_once SITEPATH."/views/public/activation/view.expired.php";
	  		else{

				?>
				  	<nav class="navbar navbar-xpress" role="navigation">
			  		<!-- Brand and toggle get grouped for better mobile display -->
			  		<div class="container">
					<div class="navbar-header">
		            	<span class="navbar-brand ease nopadding"><img src="/images/logos/xpressleader_black.svg"/></span>
		            	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-xpress-collapse">
		            		<span class="sr-only">Toggle navigation</span>
		            		<span class="icon-bar"></span>
		            		<span class="icon-bar"></span>
		            		<span class="icon-bar"></span>
		            	</button>
		            </div>
				  	</nav>
				  	<?php 
				  	if(_ID<5){
				  	?>
				  	<div class="navbar-nav-xpress">
					  	<div class="container">
						  	<ul class="ease nav navbar-nav">
						  		<li class="<?=(_ID=='')?'active':'';?>"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>">1- <?=(_LANG=='en')?'Activation':'Activation';?></a></li>
								<li class="<?=(_ID=='2')?'active':'';?> <?=(intval(_ID)>=2)?'':'disabled';?>"><a href="<?=(intval(_ID)>=2)?'/'._LANG.'/'._CONTROLLER.'/'._VIEW.'/2':'#';?>">2- <?=(_LANG=='en')?'Create your profile':'Création du profil';?></a></li>
								<li class="<?=(_ID=='3')?'active':'';?> <?=(intval(_ID)>=3)?'':'disabled';?>"><a href="<?=(intval(_ID)>=3)?'/'._LANG.'/'._CONTROLLER.'/'._VIEW.'/3':'#';?>">3- <?=(_LANG=='en')?'Payment information':'Information de paiement';?></a></li>
						  	</ul>
					  	</div>
				  	</div>
				<?php
				  	}
				if($vdnerror)   echo '<div class="container"><div class="alert alert-danger" role="alert">'.$vdnmsg.'</div></div>';
	  			if($vdnsuccess) echo '<div class="container"><div class="alert alert-success" role="alert">'.$vdnmsg.'</div></div>';
				?>
				<div class="container">
					<div class="whiteboard">
						<div class="row">
				  			<div class="col-lg-12">
								<?php
								if(_ID=='') include SITEPATH.'/views/activation/view.step1.php';
								elseif(_ID=='1') include SITEPATH.'/views/activation/view.step1.php';
								elseif(_ID=='2') include SITEPATH.'/views/activation/view.step2.php';
								elseif(_ID=='3') include SITEPATH.'/views/activation/view.step4.php';
								elseif(_ID=='5') include SITEPATH.'/views/activation/view.step5.php';
								else include SITEPATH.'/views/activation/view.step1.php';
								?>
							</div>
						</div>
					</div>
				</div>
	    <?php
	  		}
	  	}else{
	  		
	  		include_once SITEPATH."/views/view.navbar.php";
	  		include_once SITEPATH."/views/activation/view.invalidkey.php";
	  	}
	  	?>
	    
	    
	    
	    
	    <footer class="footer">
			<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<p class="visible-lg"><br>Une plateforme unique de prospections et présentations ainsi qu'une foules d'outils afin de promouvoir votre entreprise.</p>
					<p style="color: #ccc;font-weight: 400;">
						<small>Made with <i class="fa fa-heart"></i> in Canada</small>
					</p>
				</div>
				<div class="col-lg-3">
					<h3>Navigation</h3>
					<ul>
						<li><a href="/<?=_LANG;?>/login" class="ease">Connexion</a></li>
						<li><a href="/<?=_LANG;?>/forgotpassword" class="ease">Mot de passe oublié</a></li>
						<li><a href="/<?=_LANG;?>/conditions" class="ease">Conditions d'utilisation</a></li>
					</ul>	
				</div>
				<div class="col-lg-2">
					<h3>&nbsp;</h3>
				</div>
				<div class="col-lg-3">
					<h3><img src="/images/logos/xpressleader_white.svg"/></h3>
					<small>
						2915 Ogletown road, # 1972 <br>
						Newark, DE 19713, U.S.A.<br>
						<a href="mailto:hello@xpressleader.com">hello@xpressleader.com</a>
					</small>
				</div>
	  		</div>
	  		
		  	<div class="row">
	  			<div class="row">
					<div class="col-lg-12 text-center" style="border-top: solid 1px #333;line-height: 40px;">
						 <?php print(date("Y")); ?> Xpress Leader LLC &copy; All rights reserved
					</div>
				</div>
	  		</div>
	  		</div>
  		</footer>
	    
	    
	    <!-- JavaScript plugins (requires jQuery) -->
	    <script src="//code.jquery.com/jquery.js"></script>
	    
	    <!-- Latest compiled and minified JavaScript -->
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js?v=876"></script>
	
	    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) -->
	    <script src="/assets/bootstrap/js/respond.min.js"></script>
	    
	    <script src="/assets/validate/bootstrap.validate.js"></script>
	    <script src="/assets/validate/validate.<?=_LANG;?>.js"></script>
	    <script src="/assets/jquerymask/jquery.mask.min.js"></script>
	    <script src="/assets/timepicker/timepicker.min.js"></script>
	    <script src="/assets/datepicker/js/bootstrap-datepicker.js"></script>
	    <script src="/assets/spinner/spin.min.js"></script>
		<script src="/v2/application/scripts/jquery.ready.js"></script>
		
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
		<script type="text/javascript">
		    $(document).ready(function(){
		    	
		    	var LANG = "<?=_LANG;?>";
		    	
		    	Stripe.setPublishableKey('<?=PUBLICSTRIPEKEY;?>');
		    	
			    $('#frmactive').bt_validate();
				
				$('.phone_us').mask('(999) 999-9999');
				$('.postal').mask('S9S 9S9');
				
	
				var opts = {
					lines: 13, // The number of lines to draw
					length: 14, // The length of each line
					width: 7, // The line thickness
					radius: 23, // The radius of the inner circle
					corners: 1, // Corner roundness (0..1)
					rotate: 60, // The rotation offset
					direction: 1, // 1: clockwise, -1: counterclockwise
					color: '#56AD56', // #rgb or #rrggbb
					speed: 1.5, // Rounds per second
					trail: 64, // Afterglow percentage
					shadow: true, // Whether to render a shadow
					hwaccel: true, // Whether to use hardware acceleration
					className: 'spinner', // The CSS class to assign to the spinner
					zIndex: 2e9, // The z-index (defaults to 2000000000)
					top: 100, // Top position relative to parent in px
					left: 'auto' // Left position relative to parent in px
				};
	
			    $.fn.bindDelay = function(eventType, eventData, handler, timer ) {
					if($.isFunction(eventData) ) {
						timer = handler;
						handler = eventData;
					}
					timer = (typeof timer === "number") ? timer : 300;
					var timeouts;
					$(this).bind(eventType, function(event) {
						$('#CustomerAjax').empty();
						var spinner = new Spinner(opts).spin(document.getElementById('CustomerAjax'));
						var that = this;
						clearTimeout(timeouts);
						timeouts = setTimeout(function() {
						handler.call(that, event);
					}, timer);
					});
				};
	

				var $form = $('#frmpayment');
				$form.submit(function(event) {
					// Disable the submit button to prevent repeated clicks:
					$('#subpaynow').prop('disabled', true);
					
					// Request a token from Stripe:
					Stripe.card.createToken({
					  number: $('#cc_number').val(),
					  cvc: $('#cc_cvc').val(),
					  exp_month: $('#cc_exp_month').val(),
					  exp_year: $('#cc_exp_year').val()
					}, stripeResponseHandler);
					
					// Prevent the form from being submitted:
					return false;
				});
					
				
				function stripeResponseHandler(status, response) {
					// Grab the form:
					var $form = $('#frmpayment');
					
					if (response.error) { // Problem!
						// Show the errors on the form:
						$('.payment-errors').removeClass("hide");
						$('.payment-errors').text(response.error.message);

						$('#subpaynow').prop('disabled', false); // Re-enable submission
						
					} else { // Token was created!

						// Get the token ID:
						var token = response.id;
						
						// Insert the token ID into the form so it gets submitted to the server:
						$form.append($('<input type="hidden" name="stripeToken">').val(token));
						
						// Submit the form:
						$form.get(0).submit();
					}
				};
			});

	    </script>
	</body>
</html>