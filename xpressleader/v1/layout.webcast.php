<?php 
$lbljoinmeeting     = array("fr"=>"JOINDRE LE MEETING", "en"=>"JOIN MEETING");
$lbltitle           = array("fr"=>"Identification du visiteur", "en"=>"Guest identification");
$lblname            = array("fr"=>"Votre nom", "en"=>"Your name");
$lblemail           = array("fr"=>"Votre adresse courriel", "en"=>"Email address");
$lblpassword        = array("fr"=>"Ce meeting est protégé, entrez le mot de passe", "en"=>"This room is locked, enter the password");

$islogin    = false;
$leaderid   = "";

if($_POST){
    extract($_POST);
    
    if(isset($meet_token)){
        $islogin    = (isset($meet_leaderid)) ? true : false;
        $leaderid   = (isset($meet_leaderid)) ? $meet_leaderid : "";
        
        $_SESSION['leaderid'] = ($islogin) ? $leaderid : null;
        
    }elseif(isset($guest_name)&&isset($guest_email)){
        
        if($guest_name=="" || $guest_email==""){
            
        }
    }
}

$leaderid = (isset($_SESSION['leaderid'])) ? $_SESSION['leaderid'] : "";
?>
<!DOCTYPE html>
<html lang="<?=_LANG;?>">
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"/>
    <title>Xpress Leader</title>
    <meta charset="utf-8"/>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    
    
    <link rel="apple-touch-icon" sizes="57x57" href="/images/logos/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/images/logos/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/logos/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/logos/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/logos/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/logos/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/logos/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/logos/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/images/logos/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/images/logos/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/logos/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/logos/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/logos/favicon/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/images/logos/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	
	<style>
	    html, body, iframe{
        	width: 100%;
        	height: 100%;
        	padding: 0;
        	margin: 0;
        	background-color: #e5e5e5;
        	background : #e5e5e5 url('/images/bgs/white_bed_sheet.png') top left repeat;
        	font-family: 'Open Sans', sans-serif;
        }
        
        body {
        	padding-bottom: 20px;
        }
        
        iframe{
            z-index: -9999;
        }
        
        h1 {
            display: inline-block
            font-size: 3em;
            color: #8a8a8a;
            line-height: 20px;
        }
        
        h2 {
            display: inline-block
            font-size: 2.5em;
            color: #8a8a8a;
            line-height: 30px;
        }
        
        .loginbox {
            position: absolute;
            width: 100%;
            background-color: #ffffff;
            line-height: 30px;
            padding: 10px;
            margin: 0;
            -webkit-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            border-left: solid 20px #818181;
        }
        

        
        label {
            font-size: 1em;
            color: #535353;
            font-weight: 300;
        }
        input[type=text] {
            border-radius: 0;
        }
        
        .loadingbox {
            display: none;
            position: absolute;
            min-height: 300px;
            width: 100%;
            top: 50%;
            background-color: #ffffff;
            border-left: solid 20px #818181;
            padding-top: 100px;
            margin-top: -200px;
            -webkit-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
        }
        
        .hints {
            display: inline-block
            font-size: 1.3em;
            font-weight: bold;
            color: #0091f0;
            padding-left: 5px;
        }
        .wrapper {
            width: 100%;
            margin-top : 0;
        }
        
         /* Small devices (tablets, 768px and up) */
     
        #loader-wrapper {
            position: relative;
            top: 90px;
            z-index: 1000;
            float:left;
        }
        #loader {
            position: relative;
            width: 70px;
            height: 70px;
            margin: -75px 0 0 -75px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #001c2f;
            -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
            animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }
         
        #loader:before {
            content: "";
            position: absolute;
            top: 5px;
            left: 5px;
            right: 5px;
            bottom: 5px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #0071BC;
            -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
              animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }
         
        #loader:after {
            content: "";
            position: absolute;
            top: 15px;
            left: 15px;
            right: 15px;
            bottom: 15px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #d7efff;
            -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
              animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
        }
        #attmessage {
            height: 20px;
        }
        #player_wrapper {
            padding:0;
            margin-top 50px;
            
        }
        
        .modal.fade .modal-dialog{transform:none !important;}
        
        
        
@media (min-width: 768px) { 
    
    .loginbox {
            position: absolute;
            min-height: 350px;
            width: 600px;
            top: 50%;
            left:50%;
            background-color: #ffffff;
            line-height: 30px;
            padding: 10px;
            margin: -200px -300px;
            -webkit-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            border-left: solid 20px #818181;
        }
    
    .loadingbox {
            display: none;
            position: absolute;
            height: 300px;
            width: 100%;
            top: 50%;
            background-color: #ffffff;
            border-left: solid 20px #818181;
            padding-top: 100px;
            margin-top: -200px;
            -webkit-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 21px -10px rgba(0,0,0,0.75);
        }
        
        .wrapper {
            max-width: 1000px;
            margin-top : 30px;
            box-shadow: -60px 0px 100px -80px #000000, 60px 0px 100px -80px #000000;
        }
        
        #player_wrapper {
            padding:0;
            margin-top 0;
            
        }

}  
        
        .btn-xpress-black { 
          color: #EEEEEE; 
          background-color: rgba(0,0,0,0.75);
          border-color: #333; 
        } 
         
        .btn-xpress-black:hover, 
        .btn-xpress-black:focus, 
        .btn-xpress-black:active, 
        .btn-xpress-black.active, 
        .open .dropdown-toggle.btn-xpress-black { 
          color: #EEEEEE; 
          background-color: #4D4D4D; 
          border-color: #333; 
        } 
         
        .btn-xpress-black:active, 
        .btn-xpress-black.active, 
        .open .dropdown-toggle.btn-xpress-black { 
          background-image: none; 
        } 
         
        .btn-xpress-black.disabled, 
        .btn-xpress-black[disabled], 
        fieldset[disabled] .btn-xpress-black, 
        .btn-xpress-black.disabled:hover, 
        .btn-xpress-black[disabled]:hover, 
        fieldset[disabled] .btn-xpress-black:hover, 
        .btn-xpress-black.disabled:focus, 
        .btn-xpress-black[disabled]:focus, 
        fieldset[disabled] .btn-xpress-black:focus, 
        .btn-xpress-black.disabled:active, 
        .btn-xpress-black[disabled]:active, 
        fieldset[disabled] .btn-xpress-black:active, 
        .btn-xpress-black.disabled.active, 
        .btn-xpress-black[disabled].active, 
        fieldset[disabled] .btn-xpress-black.active { 
          background-color: #1C1C1C; 
          border-color: #333; 
        } 
         
        .btn-xpress-black .badge { 
          color: #1C1C1C; 
          background-color: #EEEEEE; 
        }
        
        .alert {
        	border:solid 1px #333;
        	margin:0;
        	padding:0;
        	line-height: 30px;
        }
        
        .alert-danger {
            background-color: #d83034;
            color: #ffffff;
            border-radius: 0;
            border:0;
        }
        
        .alert-success {
            background-color: #299236;
            color: #ffffff;
            border-radius: 0;
            border:0;
        }
        
        /* NAVBAR XPRESS */
        .navbar-xpress {
        	border-bottom: solid 1px #000;
        	border-radius: 0;
        	margin: 0;
        	z-index: 99;
        	font-family: 'Open Sans', sans-serif;
        	font-weight: 400;
        	background-color: rgba(0,0,0,0.75);
        }
        
        .navbar-dark {
        	background-color: rgba(0,0,0,0.95);
        }
        
        .navbar-xpress .navbar-header {
        	min-height: 50px;	
        	text-shadow: none;
        }
        
        .navbar-xpress a.navbar-brand {
        	padding-top: 0;
        	padding-bottom: 0;
        	color: #ffffff;
        }
        
        .navbar-xpress .navbar-nav li a {
        	line-height: 50px;
        	padding-top: 0;
        	padding-bottom: 0;
        	color: #eeeeee;
        	font-family: 'Open Sans', sans-serif;
        	font-weight: 400;
        	text-shadow: none;
        }
        
        .navbar-xpress .navbar-nav li a:hover, .navbar-xpress .navbar-nav li a:focus, .navbar-xpress .navbar-nav li a:active {
        	color: #79bde1;
        	background-color: transparent;
        }
        
        .navbar-xpress .navbar-nav li.active a {
        	color: #2d92c8;
        	background-color: transparent;
        }
        
        .navbar-brand {
            color:#ffffff;
            padding:0 15px;
        }
        
        .navbar-brand:hover, .navbar-brand:active, .navbar-brand:visited  {
            color: #f8f8f8;
        }
        
        .navbar-xpress .dropdown-menu {
            background-color: #2d92c8;
            color: #fff;
            border-radius: 0;
            border:0;
        }
        
        .navbar-xpress .dropdown-menu > li a {
            line-height: 25px;
            color: #fff;
            border-bottom: 1px #eee;
        }
        
        .navbar-xpress-collapse {
        	color: #fff !important;
        }
        
        .navbar-nav > li a{
            background-color: transparent;
        }

        
        .navbar-xpress .dropdown-menu {
            background-color: rgba(0,0,0,0.75);
            color: #eee;
            border-radius: 0;
            border:0;
        }
        
        .navbar-xpress .dropdown-menu > li a {
            line-height: 30px;
            color: #fff;
            border-bottom: 1px #eee;
        }
        /* ####################### */
        .modal.fade .modal-dialog{transform:none !important;}
        .modal-xpress { margin-top: 20px; width: 720px !important; overflow: hidden !important;}
        .modal-xpressmod { margin-top: 20px; overflow: hidden !important;}
        div#videomodal { overflow: hidden !important; }
        div#videomodmodal { overflow: hidden !important; }

        
        .box-shadow--2dp {
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12)
        }
        .box-shadow--3dp {
            box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12)
        }
        .box-shadow--4dp {
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2)
        }
        .box-shadow--6dp {
            box-shadow: 0 6px 10px 0 rgba(0, 0, 0, .14), 0 1px 18px 0 rgba(0, 0, 0, .12), 0 3px 5px -1px rgba(0, 0, 0, .2)
        }
        .box-shadow--8dp {
            box-shadow: 0 8px 10px 1px rgba(0, 0, 0, .14), 0 3px 14px 2px rgba(0, 0, 0, .12), 0 5px 5px -3px rgba(0, 0, 0, .2)
        }
        .box-shadow--16dp {
            box-shadow: 0 16px 24px 2px rgba(0, 0, 0, .14), 0 6px 30px 5px rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(0, 0, 0, .2)
        }

         
        @-webkit-keyframes spin {
            0%   {
                -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(0deg);  /* IE 9 */
                transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
            }
            100% {
                -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(360deg);  /* IE 9 */
                transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
            }
        }
        @keyframes spin {
            0%   {
                -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(0deg);  /* IE 9 */
                transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
            }
            100% {
                -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                -ms-transform: rotate(360deg);  /* IE 9 */
                transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
            }
        }
	</style>
</head>
<body <?=($ismod)?'style="padding-top:50px;"':'';?>>

    <?php
    if($ismod){
    ?>
    <nav class="navbar navbar-xpress navbar-fixed-top" role="navigation">
        
        <div class="navbar-header">
        	<span class="navbar-brand"><img src="/images/logos/xpressleader_white.svg"/></span>
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-xpress-collapse">
        		<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        	</button>
        </div>
    
        <div class="collapse navbar-collapse navbar-xpress-collapse">
          	<div class="navbar-form navbar-right">
          	    
          	    <a href="#" id="btnbroadvideo" class="btn btn-primary" data-toggle="modal" data-target="#videomodmodal"><span class="glyphicon glyphicon-play"></span> <?=(_LANG=='fr')?'Diffuser une vidéo':'Broadcast video';?></a>
          	    <a href="#" id="toggleform" class="btn btn-xpress-black" data-toggle="modal" data-target="#closingform"><i class="fa fa-user-plus"></i> Closing</a>
          	</div>
        </div>
    </nav>
    
    <div id="videomodmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="true" data-keyboard="true">
      <div class="modal-dialog modal-xpressmod">
        <div class="panel panel-primary">
            <div class="panel-heading panel-heading-blue text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Manage videos broadcast</h4>
            </div>
            <div class="row" style="background-color: #000;border-radius:10px;">
                
                <div class="col-lg-6 text-left" style="height: 150px;">
                    <div id="jwdynmodvideo"></div>
                </div>
                <div class="col-lg-6 text-center" style="height: 150px;">
                    <div id="jwdynmodmsg" style="font-size: 1em; color: #f4f4f4;font-weight:bold;padding-top:30px;display:none;">
                        Currently Broadcasting...<br>
                        <div id="duration" style="font-size: 1.5em; color: #ec0006;font-weight:bold;padding-top:10px;">
                            <img src="https://cdn.lukej.me/wp-advanced-ajax-page-loader/2.5.12/loaders/Grey%20Squares%20Loading%20Text.gif" style="width: 50px;" alt=""/>
                        </div>
                    </div>
                </div>
             </div>
            <div class="row">
                <div class="col-lg-12" style="padding: 20px;">

                        <div>
                          <ul class="nav nav-tabs" role="tablist">
                              <?php
                                $mediaGroups = $corporate->getMediaGroup();
                                foreach($mediaGroups as $group){
                                    $text = ($group->lang=="frca") ? "Français Canada" : $group->lang;
                                    $text = ($group->lang=="enca") ? "English Canada" : $text;
                                    $text = ($group->lang=="enus") ? "English USA" : $text;
                                    ?>
                                    <li role="presentation"><a href="#<?=$group->lang;?>" aria-controls="<?=$group->lang;?>" role="tab" data-toggle="tab"><?=$text;?></a></li>
                                    <?php
                                }
                                ?>
                                <li role="presentation"><a href="#music" aria-controls="music" role="tab" data-toggle="tab">Audios</a></li>
                          </ul>
                        
                          <!-- Tab panes -->
                          <div class="tab-content" style="height: 200px;overflow-y:scroll;">
                              <?php
                                $mediaGroups = $corporate->getMediaGroup();
                                foreach($mediaGroups as $group){
                                    ?>
                                    <div role="tabpanel" class="tab-pane fade" id="<?=$group->lang;?>">
                                    <table class="table table-striped">
                                        <?php
                                        $medialist = $corporate->getMedialist($group->lang);
                            			foreach($medialist as $media){
                            				echo '<tr>';
                            				echo  '<td class="text-left"><strong>'.$media->name.'</strong></td>';
                            			    echo  '<td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="'.$media->url.'"><span class="glyphicon glyphicon-play"></span> Play</a></td>';
                            			    echo '<tr>';
                            			}
                            			?>
                                    </table>
                                    </div>
                                    <?php
                                }
                                ?>
                                
                                
                                
                                <div role="tabpanel" class="tab-pane fade" id="music">
                                    <table class="table table-striped">
                                        <tr>
                            				<td class="text-left"><strong> 9 to 5</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/9to5.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                                        <tr>
                            				<td class="text-left"><strong> Amène toi chez nous</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/amenetoicheznous.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> FootLoose</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/footloose.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> I'm Alive</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/imalive.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> La beauté des petites choses</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/beautepetitechose.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> Let's get ready to rumble</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/rumble.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> Millionnaire</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/millionnaire.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                                        <tr>
                            				<td class="text-left"><strong> Star Academie</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/staracademie.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> Sweet Caroline</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/sweetcaroline.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> Simply the best</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/simplythebest.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			<tr>
                            				<td class="text-left"><strong> Wake me up, before you go go</strong></td>
                            			    <td class="text-right"><a href="#" class="btn btn-primary btn-sm playvideo" data-url="/audio/wakemeup.mp3"><span class="glyphicon glyphicon-play"></span> Play</a></td>
                            			</tr>
                            			
                                    </table>
                                    </div>
                            </div>
                          </div>

                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Back to meeting</button>
            </div>  
        </div>
      </div>
    </div>
    <?php
    }else{
        echo '<div id="videomodal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog modal-lg modal-xpress">
                    <div id="jwdynvideo"></div>
                  </div>
                </div>';
    }


    if($validEvent){
        
        if($joinUrl!=""){
            echo '<iframe frameborder="0" src="'.$joinUrl.'" id="castframe" class=""></iframe>';
        }else{
    ?>
    
            <div class="loginbox  box-shadow--6dp center-block">
                <div id="attmessage" class="nopadding attmessage text-center"></div>
                <h2 class="nopadding"><?=$lbltitle[_LANG];?></h2>
                <div class="form-group">
                    <label for=""><?=$lblname[_LANG];?></label>
                    <input type="text" class="form-control" id="attName">
                </div>
                <div class="form-group">
                    <label for=""><?=$lblemail[_LANG];?></label>
                    <input type="text" class="form-control" id="attEmail">
                </div>
                <?php 
                if($curevent->password != ""){
                ?>
                <div class="form-group">
                    <label for=""><?=$lblpassword[_LANG];?></label>
                    <input type="text" class="form-control" id="attPassword">
                </div>
                <?php 
                }else echo '<input type="hidden" class="form-control" value="" id="attPassword">'
                ?>
                <input type="hidden" id="token" value="<?=sha1(_VIEW."AttendT3HSuper3V3nt!");?>"/>
                <input type="hidden" id="eventid" value="<?=_VIEW;?>"/>
                <input type="hidden" id="leaderid" value="<?=_ID;?>"/>
                <button type="button"  data-lang="<?=_LANG;?>" class="btn btn-danger btn-md login pull-right"><?=$lbljoinmeeting[_LANG];?> <i class="fa fa-play-circle" aria-hidden="true"></i></button>
            </div>
            
                
            <div class="loadingbox loadfr box-shadow--6dp">
            <div class="container">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <div id="loader-wrapper" class="">
                            <div id="loader"></div>
                        </div>
                        <h1>Chargement de votre meeting...</h1>
                        <div class="hints">Veuillez patienter pendant la préparation de la salle.</div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="loadingbox loaden box-shadow--6dp">
            <div class="container">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <div id="loader-wrapper" class="">
                            <div id="loader"></div>
                        </div>
                        <h1>Your meeting is loading...</h1>
                        <div class="hints">Please wait while the meeting is starting up.</div>
                    </div>
                </div>
            </div>
        </div>
            
            <iframe frameborder="0" src="" id="castframe" class="hide"></iframe>
            
            
            <div id="videomodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
              <div class="modal-dialog modal-lg modal-xpress">
                <div id="jwdynvideo"></div>
              </div>
            </div>

    
    <?php 
        }
    }else{
        
        if(isset($cast)){
        ?>
        <div id="login" class="loginbox  box-shadow--6dp center-block">
            <div class="nopadding attmessage text-center"></div>
            <h2>BIENVENUE! /  WELCOME!</h2>
            <div class="form-group">
                <label for="">Votre nom / Your name</label>
                <input type="text" class="form-control" id="attName" readonly="true" value="<?=$cast->name;?>">
            </div>
            <div class="form-group">
                <label for="">Votre adresse courriel / Email Address</label>
                <input type="text" class="form-control" id="attEmail" value="<?=(isset($iscampaign))?$cast->mail:'';?>">
            </div>
            
            <input type="hidden" id="token" value="<?=sha1(_VIEW."AttendT3HSuper3V3nt!");?>"/>
            <input type="hidden" id="key" value="<?=_VIEW;?>" />
            <input type="hidden" id="leaderid" value="<?=$cast->leaderid;?>"/>
            
            <div class="row">
                <div class="col-sm-6">
                    <button type="button" data-lang="en" data-url="https://player.vimeo.com/external/173271444.sd.mp4?s=5f8ea0512d131ee263de55733dfe4ab753e8114d&profile_id=165" class="btn btn-danger btn-md startwebcast">BEGIN THE OVERVIEW <i class="fa fa-play-circle" aria-hidden="true"></i></button>
                </div>
                <div class="col-sm-6">
                    <button type="button" data-lang="fr" data-url="https://player.vimeo.com/external/171692177.sd.mp4?s=3ca94edddde3489f3e01c47ab807e698825615d0&profile_id=165" class="btn btn-danger btn-md startwebcast pull-right">DÉMARRER LE SURVOL <i class="fa fa-play-circle" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
        
        <div class="loadingbox loadfr box-shadow--6dp">
            <div class="container">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <div id="loader-wrapper" class="">
                            <div id="loader"></div>
                        </div>
                        <h1>Chargement de votre survol...</h1>
                        <div class="hints">Veuillez patienter pendant la préparation du vidéo.</div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="loadingbox loaden box-shadow--6dp">
            <div class="container">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <div id="loader-wrapper" class="">
                            <div id="loader"></div>
                        </div>
                        <h1>Your business overview is loading...</h1>
                        <div class="hints">Please wait while your video is starting up.</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container wrapper">
        <div class="row">
            <div class="col-md-12 text-center" id="player_wrapper">
                <div id="jwoverview" class="center-block"></div>
                <input type="hidden" id="overview_position" value=""/>
            </div>
        </div>
        <div id="castfr" class="hide">
            <div class="row">
                <div class="col-md-12" style="padding: 0;">
                    <div class="panel panel-default" style="margin-top: 0;">
                        <div class="panel-body">
                            <div class="attmessage nopadding text-center"></div>
                            <div class="text-center addprospectbox">
                                
                                <div class="row">
                                    <div class="col-lg-12" style="padding: 10px 0;">
                                        <h3 style="line-height: 20px;margin:0;">Vous êtes intéressé ? Vous avez des questions ? </h3>
                                        <small>Recevez par courriel un document explicatif qui résume ce vidéo.</small>
                                    </div>
                                </div>
                                <div class="row" style="background-color: #eeeeee;">
                                    <div class="col-lg-3 text-left">
                                        <div class="form-group">
                                            <label style="color: #000;">Votre nom</label>
                                            <input type="text" name="txtinvitename" id="txtinvitename" value="<?=(!isset($iscampaign))?$cast->name:'';?>" class="form-control" required="true" <?=(!isset($iscampaign))?'readonly="true"':'';?>/> 
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 text-left">
                                        <div class="form-group">
                                            <label style="color: #000;">Adresse courriel</label>
                                            <input type="email" name="txtinvitemail" id="txtinvitemail" value="<?=$cast->mail;?>" class="form-control" required="true" readonly="true" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 text-left">
                                        <div class="form-group">
                                            <label style="color: #000;">Téléphone</label>
                                            <input type="text" name="txtinvitephone" id="txtinvitephone" value="" class="form-control" required="true"/>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3">
                                        <label>&nbsp;</label>
                                        <input type="hidden" id="interesttoken" value="<?=sha1(_VIEW."AddT3HSuperPr0sp3ct!");?>"/>
                                        <input type="hidden" id="key" value="<?=_VIEW;?>" />
                                        <input type="hidden" id="leaderid" value="<?=$cast->leaderid;?>"/>
                                        <button type="button" data-lang="fr" class="btn btn-danger btn-block addinterest">PLUS D'INFOS !</button>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div id="casten" class="hide">
            <div class="row">
                <div class="col-md-12" style="padding: 0;">
                    <div class="panel panel-default" style="margin-top: 0;">
                        <div class="panel-body">
                            <div class="nopadding attmessage text-center"></div>
                            <div class="form-inline text-center addprospectbox">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Your name</label>
                                        <input type="text" name="txtinvitename" id="txtinvitename"  value="<?=$cast->name;?>" class="form-control" required="true" readonly="true"/> 
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" name="txtinvitemail" id="txtinvitemail"  value="<?=$cast->mail;?>" class="form-control" required="true" readonly="true"/>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Phone number</label>
                                        <input type="text" name="txtinvitephone" id="txtinvitephone"  value="" class="form-control" required="true"/>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <input type="hidden" id="interesttoken" value="<?=sha1(_VIEW."AddT3HSuperPr0sp3ct!");?>"/>
                                    <input type="hidden" id="key" value="<?=_VIEW;?>" />
                                    <input type="hidden" id="leaderid" value="<?=$cast->leaderid;?>"/>
                                    <label><strong>I'm interrested!</strong></label>
                                    <button type="button" data-lang="en" class="btn btn-danger btn-block addinterest">CONTACT ME!</button>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="padding: 0;">
                    <center><small>powered by Xpress Leader LLC &copy; All rights reserved 2016</small></center>
                </div>
            </div>
        </div>
        
        </div>
        
        <?php
        }else{
        ?>
        <div class="loginbox  box-shadow--6dp center-block">
            <h2>DÉSOLÉ ! / SORRY !</h2>
            <p class="text-default">Cette page n'est pas disponible, le lien est peut-être expiré ou invalide.</p>
            
            <p class="text-default">This page does'nt exist, could be expired or unavailable.</p>
        </div>
        <?php
        }
    }
    ?>
 
    <input type="hidden" value="<?=_VIEW;?>" id="eventkey"/>
  	<input type="hidden" value="" id="videourl"/>
  	<input type="hidden" value="" id="playingvideourl"/>
  	<input type="hidden" value="" id="offset"/>
  	<input type="hidden" value="<?=$ismod;?>" id="ispresenter"/>
    <input type="hidden" value="" id="isopen"/>
    
    <script>
	    var _LANG = "<?=_LANG;?>";
	    var _iscampaign = "<?=(isset($iscampaign))?"TRUE":"FALSE";?>"
	</script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="/assets/jwplayer/jwplayer.js"></script>
    <script src="/scripts/meeting.js?v=321654"></script>
    
    <?php 
    if($account->islogin){
    ?>
    <script>
  		autovideo(jwplayer);
  		checkform();
	</script>
	<?php 
    }
    ?>
</body>
</html>