<?php
$lblsuspended     = array("fr"=>"Votre compte est suspendu, mettez à jour dès maintenant vos informations de paiements.","en"=>"Your account is suspended, please update your billing informations.");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Xpress Leader</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="apple-touch-icon" sizes="57x57" href=" /images/logos/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href=" /images/logos/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href=" /images/logos/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href=" /images/logos/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href=" /images/logos/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href=" /images/logos/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href=" /images/logos/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href=" /images/logos/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href=" /images/logos/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href=" /images/logos/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href=" /images/logos/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href=" /images/logos/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href=" /images/logos/favicon/favicon-16x16.png">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content=" /images/logos/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" media="screen" rel="stylesheet">
		<link href="//opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css" integrity="sha384-Wrgq82RsEean5tP3NK3zWAemiNEXofJsTwTyHmNb/iL3dP/sZJ4+7sOld1uqYJtE" crossorigin="anonymous" rel="stylesheet" >
		<link href=" /style/bootstrap-switch.min.css" rel="stylesheet">
		<link href=" /style/bootstrap-datetimepicker.min.css" rel="stylesheet">
		<link href=" /style/v4.site.css" rel="stylesheet">

		<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Raleway:300,500,600' rel='stylesheet' type='text/css'>
	</head>
	<body class="homepage">
	    <?php


	    if(isset($cleader) && $cleader->chargetry==2 && $dtncharge->format("Y-m-d") < $dtime->format("Y-m-d")) echo '<div class="alert alert-danger text-center" role="alert">'.$lblsuspended[_LANG].'</div>';

	    include_once "views/view.navbar.php";

        if($vdnerror)   echo '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-danger" role="alert">'.$vdnmsg[_LANG].'</div></div></div>';
        if($vdnsuccess) echo '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-success" role="alert">'.$vdnmsg[_LANG].'</div></div></div>';

        if($account->islogin && _CONTROLLER!="corporate" && _CONTROLLER!="pro" && _CONTROLLER!="overview" && ($cleader->chargetry!=2 || $dtncharge->format("Y-m-d") >= $dtime->format("Y-m-d"))){

        	$stats = $prospect->getSummaries($cleader->leaderid);
        ?>

        <div class="container">
    	  	<div class="row">
    			<div class="col-lg-3">
    				<div class="panel panel-xpress panel-default">
    					<div class="panel-heading panel-heading-orange">
    						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"VISITEURS":"VISITORS";?></h3>
    					</div>
    					<div class="panel-body text-center">
    						<h2 style="line-height: 10px;">0</h2>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 ">
    				<div class="panel panel-xpress panel-primary">
    					<div class="panel-heading panel-heading-blue">
    						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"TEST":"TEST";?></h3>
    					</div>
    					<div class="panel-body text-center">
    						<h2 style="line-height: 10px;"><?=$stats['meeting'];?></h2>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 ">
    				<div class="panel panel-xpress panel-success">
    					<div class="panel-heading panel-heading-green">
    						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"INTÉRÊTS":"INTERESTS";?></h3>
    					</div>
    					<div class="panel-body text-center">
    						<h2 style="line-height: 10px;"><?=$stats['interest'];?></h2>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 ">
    				<div class="panel panel-xpress panel-danger">
    					<div class="panel-heading panel-heading-red">
    						<h3 class="panel-title text-center">CLIENTS</h3>
    					</div>
    					<div class="panel-body text-center">
    						<h2 style="line-height: 10px;"><?=$stats['client'];?></h2>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<?php
	  	}


	  	if($account->islogin){

		  		if(_CONTROLLER==""||_CONTROLLER=="home"){
		  			include_once SITEPATH."/views/view.home.php";
		  		}elseif(_CONTROLLER=="account"){
		  			include_once SITEPATH."/views/account/view.account.php";
	    	  	}elseif(_CONTROLLER=="office"){
	    	  	    if(_VIEW=="" || _VIEW=="prospect") include_once SITEPATH."/views/office/view.prospects.php";
	    	  	    if(_VIEW=="client") include_once SITEPATH."/views/office/view.clients.php";
	    	  	    if(_VIEW=="contacts") include_once SITEPATH."/views/office/view.contacts.php";
	    	  	}elseif(_CONTROLLER=="meeting"){
	    	  	    if(_VIEW=="" || _VIEW=="events") include_once SITEPATH."/views/meeting/view.events.php";
	    	  	}elseif(_CONTROLLER=="pro" && $cleader->ispro){
	    	  	    include_once SITEPATH."/views/pro/pro.php";
	    	  	}elseif(_CONTROLLER=="corporate" && $iscorpo){
	    	  	    include_once SITEPATH."/views/corporate/corporate.php";
	    	  	}elseif(_CONTROLLER=="overview") {
				    include_once SITEPATH."/views/view.overview.php";
	    	  	}elseif(_CONTROLLER=="conditions"){
					include_once SITEPATH."/views/view.terms.php";
	    	  	}elseif(_CONTROLLER=="signup"){
					include_once SITEPATH."/views/view.signup.php";
	    	  	}elseif(_CONTROLLER=="fasttrack"){
					include_once SITEPATH."/views/fasttrack/view.fasttrack.php";
	    	  	}

	  	}else{
	  	    if(_CONTROLLER=="home" || _CONTROLLER=="login" || _CONTROLLER==""){
				include_once SITEPATH."/views/view.login.php";
			}elseif(_CONTROLLER=="signup"){
				include_once SITEPATH."/views/view.signup.php";
			}elseif(_CONTROLLER=="forgotpassword"){
				include_once SITEPATH."/views/view.forgotpassword.php";
			}elseif(_CONTROLLER=="conditions"){
				include_once SITEPATH."/views/view.terms.php";
			}elseif(_CONTROLLER=="thanks") {
				include_once SITEPATH."/views/view.thanks.php";
			}elseif(_CONTROLLER=="overview") {
				include_once SITEPATH."/views/view.overview.php";
			}else{
				include_once SITEPATH."/views/view.login.php";
			}
	  	}
	  	?>




	    <?php
	    if(_CONTROLLER != "overview") include_once SITEPATH."/views/view.footer.php";
	    ?>

  		<script type="text/javascript">
  			var LANG = "<?=_LANG;?>";
  			var _LANG = "<?=_LANG;?>";
  		</script>

  		<!-- JavaScript plugins (requires jQuery) -->
	    <script src="//code.jquery.com/jquery.js"></script>

	    <!-- Latest compiled and minified JavaScript -->
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js?v=876"></script>

	    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) -->
	    <script src="/assets/bootstrap/js/respond.min.js"></script>

	    <script src="/assets/validate/bootstrap.validate.js"></script>
	    <script src="/assets/validate/validate.<?=_LANG;?>.js"></script>
	    <script src="/assets/jquerymask/jquery.mask.min.js"></script>

	    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/locale/fr-ca.js"></script>
	    <script src=" /scripts/bootstrap-datetimepicker.min.js"></script>
	    <?php
	    if(_LANG != "en") echo '<script src=" /scripts/locales/bootstrap-datetimepicker.'._LANG.'.js"></script>';
	    ?>
	    <script src="/scripts/bootstrap-switch.min.js"></script>
	    <script src="/assets/jwplayer/jwplayer.js"></script>
		<script src="/scripts/jquery.ready.js?v=879"></script>

		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
		<script type="text/javascript">
			<?=$jscript;?>
		</script>
	</body>
</html>
