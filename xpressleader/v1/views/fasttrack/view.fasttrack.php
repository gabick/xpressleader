<?php
$lbltitle      		= array("fr"=>'Gestion Fast<span class="text-primary">Track</span>',"en"=>'Fast<span class="text-primary">Track</span> management');
$myteam 		    	= $fasttrack->GetMyTeam($cleader->leaderid);
?>
<div class="container">
	<div class="whiteboard">
	    
	    <div class="row">
            <div class="col-md-9">
                <div class="page-title">
            		<?=$lbltitle[_LANG];?>
            	</div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" style="font-size: 0.9em;">
                    <div class="panel-heading pull-left">Sessions en banque</div>
                    <div class="panel-body pull-right">
                        <div style="line-height: 10px;font-weight: 600;text-align: center;font-size: 2em">1</div>
                    </div>
                        <!--<button type="button" class="btn btn-block btn-danger">Acheter des sessions...</button>-->
                </div>
    	    </div>
        </div>
    	
    	<div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills  nav-justified">
                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Former les équipes</a></li>
                  <li role="presentation"><a href="#addclient" aria-controls="addclient" role="tab" data-toggle="tab">Gestion des clients</a></li>
                  <li role="presentation"><a href="#rankings" aria-controls="rankings" role="tab" data-toggle="tab">Classements</a></li>
                  <li role="presentation"><a href="#rules" aria-controls="rules" role="tab" data-toggle="tab">Règlements</a></li>
                </ul>
                
            </div>
        </div>
        
        <!-- Tab panes -->
        <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="home">
            
            <div class="row">
                <div class="col-md-12">
                    
                    <h3 style="font-weight: 600;line-height:80px;">FORMER VOTRE ÉQUIPE</h3>
                    
                    <h4 style="border-bottom: solid 1px #ccc;">Liste des équipes en préparation</h4>
                    <?php if(!$myteam) echo '<button type="button"  data-toggle="modal" data-target="#modCreateTeam" class="btn btn-primary pull-right center-block">Créer une équipe</button>'; ?>
                    <?php
                    $teams = $fasttrack->GetTeamsList();
                    if($teams){
                        echo '<table class="table table-hover">';
                        echo '<tr>';
                        echo '<th>Nom</th>';
                        echo '<th>Capitaine</th>';
                        echo '<th>Membres</th>';
                        echo '<th></th>';
                        echo '</tr>';
                        
                        foreach($teams as $team){
                            $btnManage = ($myteam && $myteam->id == $team->id) ? '<button type="button" class="btn btn-default">Mon équipe</button>' : "";
                            $btnrequest = (!$myteam) ? '<button type="button"  data-toggle="modal" data-target="#modRequest'.$team->id.'" class="btn btn-default">Joindre l\'équipe</button>':'';
                            echo '<tr>';
                            echo '<td>'.$team->name.'</td>';
                            echo '<td>'.$team->cpt_name.'</td>';
                            echo '<td>'.$fasttrack->getCountByTeam($team->id).'</td>';
                            echo '<td class="text-right">'.$btnManage.$btnrequest.'</td>';
                            echo '</tr>';
                            
                            echo '
<div class="modal fade" id="modRequest'.$team->id.'" tabindex="-1" role="dialog" aria-labelledby="modRequest'.$team->id.'Label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Faites une demande à l\'équipe '.$team->name.'</h4>
      </div>
      <form method="post" action="" enctype="multipart/form-data" class="form">
      <div class="modal-body">
        <p>Faites une demande à '.$team->cpt_name.' afin de joindre l\'équipe '.$team->name.'. Votre demande peut-être annulé en tout temps et vous pouvez faire une demande par équipe disponible.</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="leaderid" value="'.$cleader->leaderid.'"/>
        <input type="hidden" name="teamid" value="'.$team->id.'"/>
    	    <input type="hidden" name="validate" value="true"/>
            <input type="hidden" name="subform" value="ftsetrequest"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger">Envoyer ma demande</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
                            ';
                            
                        }
                        echo '</table>';
                    }else{
                        echo '<div style="text-align: center;line-height: 80px;">Aucune équipe n\'a été crée pour le moment.</div>';
                    }
                    
                    
                    ?>
                    
                </div>
            </div>
            
            <?php 
            if($myteam){
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3><?=$myteam->name;?></h3></div>
                      <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>Membres participants</th>
                                <th></th>
                            </tr>
                            <?php 
                            $players = $fasttrack->getAcceptedPlayers($myteam->teamid);
                            if($players){
                                foreach($players as $player){
                                    echo '<tr>';
                                    echo '<td>'.$player->leadername.'</td>';
                                    echo '<td class="text-right"></td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </table>
                        
                        
                        <?php
                        if($fasttrack->isCpt($myteam->teamid, $myteam->leaderid)){
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table">    
                                    <tr>
                                        <th>Membres en attentes</th>
                                        <th></th>
                                    </tr>
                                    <?php 
                                    $players = $fasttrack->getCptPlayerRequests($myteam->teamid);
                                    if($players){
                                        foreach($players as $player){
                                            echo '<tr>';
                                            echo '<td>'.$player->leadername.'</td>';
                                            echo '<td class="text-right"><button type="button"  data-toggle="modal" data-target="#modAcceptRequest'.$player->leaderid.'" class="btn btn-success">Accepter</button></td>';
                                            echo '</tr>';
                                            
                                            
                            echo '
                            <div class="modal fade" id="modAcceptRequest'.$player->leaderid.'" tabindex="-1" role="dialog" aria-labelledby="modAcceptRequest'.$player->leaderid.'Label">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Vous avez reçu une demande de '.$player->leadername.'</h4>
                                  </div>
                                  <form method="post" action="" enctype="multipart/form-data" class="form">
                                  <div class="modal-body">
                                    <p>Voulez-vous accepter la demande de '.$player->leadername.' à joindre votre équipe ?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <input type="hidden" name="leaderid" value="'.$cleader->leaderid.'"/>
                                    <input type="hidden" name="teamid" value="'.$myteam->teamid.'"/>
                                    <input type="hidden" name="playerid" value="'.$player->leaderid.'"/>
                                    <input type="hidden" name="response" value="true"/>
                                	    <input type="hidden" name="validate" value="true"/>
                                        <input type="hidden" name="subform" value="ftresponserequest"/>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-danger">J\'ACCEPTE !</button>
                                  </div>
                                  </form>
                                </div><!-- /.modal-content -->
                              </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->';
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                            
                            <div class="col-md-6">
                                
                                <table class="table">    
                                    <tr>
                                        <th>Offres en attentes</th>
                                        <th></th>
                                    </tr>
                                    <?php 
                                    /*
                                    $players = $fasttrack->getCptTeamOffers($myteam->teamid);
                                    if($players){
                                        foreach($players as $player){
                                            echo '<tr>';
                                            echo '<td>'.$player->leadername.'</td>';
                                            echo '<td class="text-right"><button type="button"  data-toggle="modal" data-target="#modAcceptRequest'.$player->leaderid.'" class="btn btn-success">Accepter</button></td>';
                                            echo '</tr>';
                                        }
                                    }*/
                                    ?>
                                </table>
                                <!--
                                <form action="" class="form">
                                    <div class="form-group">
                                        <label for="mailoffer">Envoyé une offre à un joueur</label>
                                        <input type="text" class="form-control" value=""/>
                                        <button type="button" class="btn btn-primary">Envoyer!</button>
                                    </div>
                                </form>
                                -->
                            </div>
                        </div>
                        
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                </div>
            </div>
            <?php 
            }else{
            ?>
            <div class="row">
                <div class="col-md-6">
                    <h4 style="border-bottom: solid 1px #ccc;">Offres reçues</h4>
                    
                    <table class="table">
                        <?php
                        $offers = $fasttrack->GetTeamOffers($cleader->leaderid);
                        if($offers){
                            foreach($offers as $offer){
                                echo '<tr>';
                                echo '<td>'.$offer->name.'</td>';
                                echo '<td class="text-right"><strong>En attente...</strong></td>';
                                echo '</tr>';
                            }
                        }else echo '<div style="text-align: center;line-height: 80px;">Vous n\'avez recu aucune offre</div>';
                        ?>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4 style="border-bottom: solid 1px #ccc;">Demandes envoyées</h4>
                    <table class="table">
                        <?php 
                        $requests = $fasttrack->GetPlayerRequests($cleader->leaderid);
                        if($requests){
                            foreach($requests as $request){
                                echo '<tr>';
                                echo '<td>'.$request->name.'</td>';
                                echo '<td class="text-right"><strong>En attente...</strong></td>';
                                echo '</tr>';
                            }
                        }else echo '<div style="text-align: center;line-height: 80px;">Vous n\'avez envoyé aucune demande</div>';
                        ?>
                    </table>
                    
                </div>
            </div>
            <?php
            }
            ?>
            
        </div>
        <div role="tabpanel" class="tab-pane fade" id="addclient">
            
            <div class="row">
                <div class="col-md-12">
                    <h3 style="font-weight: 600;line-height:80px;">GESTIONS DES CLIENTS</h3>
                    
                    <table class="table">
                        <tr>
                            <th>Client</th>
                            <th>Ensemble valeur</th>
                            <th>Avancement</th>
                            <th>Supprimer</th>
                        </tr>
                    </table>
                    <h3 class="text-center" style="font-weight: 600;line-height:80px;">La session FastTrack Août 2017 n'est pas encore débutée.</h3>
                </div>
            </div>
            
        </div>
        <div role="tabpanel" class="tab-pane fade" id="rankings">
            
            <div class="row">
                <div class="col-md-12">
                    <h3 style="font-weight: 600;line-height:80px;">CLASSEMENT AOÛT 2017</h3>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Équipes</th>
                            <th>Points</th>
                        </tr>
                    </table>
                    <h3 class="text-center" style="font-weight: 600;line-height:80px;">La session FastTrack Août 2017 n'est pas encore débutée.</h3>
                </div>
            </div>
        </div>
        
        
        
        
        <div role="tabpanel" class="tab-pane fade" id="rules">
            
            <div class="row">
                <div class="col-md-12">
                    <h3 style="font-weight: 600;line-height:80px;">RÈGLEMENTS FASTTRACK</h3>

                    <p>Le but du FastTrack est de conserver l'intérêt et l'enthousiasme de chacun envers cette compétition saine et aussi, afin de garder votre détermination actuelle envers VOS objectifs du mois.</p>
                
                
                    <p>
                        <strong>1. Vous avez jusqu'au dernier jour du mois pour inscrire vos résultats sur le site de XpressLeader dans l’onglet Fast Track</strong><br/>
                        Le classement en direct de chaque équipe sera visible sur le site XpressLeader jusqu’à 2 ou 3 jours avant la fin. Un membre de l’équipe administrative de XpressLeader pourra, vers la fin du mois, voiler le classement en direct afin de pouvoir faire la vérification des résultats auprès de Melaleuca et, par la suite, révéler le nom de l’équipe gagnante en temps et lieu.
                        Donc, il est très important que, comme membre d'une équipe, vous vous engagiez à inscrire les données à chaque inscription de clients ou de directeurs personnel afin que les résultats soient compilés.
                    </p>
                    
                    
                    
                    <p>
                        <strong>2. Seulement les abonnés XpressLeader peuvent s'inscrire dans une équipe de Fast Track</strong><br/>
                        Pour participer au Fast Track vous devez être abonné à XpressLeader afin de participer aux prix distribués et au frais d’entretien du site pour un montant de:
                    </p>
                    
                    <p>
                        Forfait Mensuel : $10.00 + $ 15.00 : $ 25.00 US<br>
                        Forfait 3 Mois : $25.00 + $ 40.00 : $ 65.00 US (Économisez $40/an)<br>
                        Forfait Annuel : $80.00 + $120.00: $ 200.00 US (Économisez $100/an)<br>
                    </p>
                    
                    <p>P.S. Les abonnements peuvent être annulés en tout temps mais, les forfaits de 3 mois ou annuels ne sont pas remboursables.</p>
                    
                    <p>
                        <strong>3. Pour l'équité de chaque équipe aucun changement, annulation, rajout de membre ou de client dans une équipe formée ne sera permise après le début ou la fin d'un Fast Track pour un mois donné.</strong><br>
                        Prenez note qu'il est permis de regrouper les membres des organisations différentes, afin de former une équipe.
                    </p>
                    
                    
                    <p>
                        <strong>4. Attribution des points Fast Track:</strong><br>
                        Une adhésion: <strong>1 point</strong> <br>
                        Ensemble Valeur ou Carrière: <strong>1 point</strong> <br>
                        Nouveau Directeur personnel: <strong>5 points</strong>
                    </p>
                    
                    <p>
                        <strong>Les points de l'équipe sont calculés au prorata du nombre de membres dans l'équipe.</strong><br>
                        Afin d'équilibrer les chances de gagner pour tous, les points totaux gagnés par chaque équipe seront re-divisés par le nombre de membres inscrits dans l'équipe.
                    </p>
                    
                    <p>
                        Par exemple:<br> Si une équipe de 6 membres gagnent 24 points durant le mois; les points seront divisés par 6 ce qui donnera une moyenne de 4 points par membre dans l'équipe. Comme dans de nombreuses compétitions sportives, nous tiendrons compte de trois décimales pour déterminer les gagnants en cas de résultats similaires d'un nombre entier.
                    </p>
                    
                    <p>
                        Une équipe de 7 membres atteignent 22 points; 22 divisé par 7 : 3.142 <br>
                        Une équipe de 8 membres atteignent 25 points; 25 divisé par 8 : 3.125
                    </p>
                    
                    <p>
                        <strong>IMPORTANT:</strong> <br>
                        Les membres d'une équipe gagnante devront avoir produit au moins 2 point Fast Track personnellement pour se mériter le prix attribué à l’équipe gagnante. Ce minimum de points exigés a pour but de récompenser uniquement les membres qui ont contribués au succès de l'équipe. Les argents ainsi non-distribués seront remis dans la cagnote pour les prochains Fast Track.
                    </p>
                
                </div>
            </div>
        </div>
        </div>
        
        
        
	</div>
</div>


<div class="modal fade" id="modCreateTeam" tabindex="-1" role="dialog" aria-labelledby="modCreateTeamLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Création d'une équipe FastTrack</h4>
      </div>
      <form method="post" action="" enctype="multipart/form-data" class="form">
      <div class="modal-body">
        <p>Entrez le nom de votre équipe</p>
            <input type="text" name="txtteamname" maxlength="50" class="form-control" value=""/>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
    	    <input type="hidden" name="validate" value="true"/>
            <input type="hidden" name="subform" value="ftaddteam"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-danger">Créer mon équipe</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->