<?php 
$lbldesc        = array("fr"=>"Une plateforme unique de prospections et formations ainsi qu'une foules d'outils afin de promouvoir votre entreprise.", "en"=>"A unique platform for prospecting and training including many tools to promote your business.");
$lblsignin      = array("fr"=>"Connexion", "en"=>"Connection");
$lblforgotpw    = array("fr"=>"Mot de passe oublié", "en"=>"Forgot your password");
$lblterms       = array("fr"=>"Conditions d'utilisation", "en"=>"Terms of use");
?>
<footer class="footer">
<div class="container">
    <div class="row">
        <div class="col-lg-4">
        	<p class="visible-lg"><?=$lbldesc[_LANG];?></p>
        	<p style="color: #ccc;font-weight: 400;">
        		<small>Made with <i class="fa fa-heart"></i> in Canada</small>
        	</p>
        </div>
        <div class="col-lg-3">
        	<h3>Navigation</h3>
        	<ul>
        		<li><a href="/<?=_LANG;?>/login" class="ease"><?=$lblsignin[_LANG];?></a></li>
        		<li><a href="/<?=_LANG;?>/forgotpassword" class="ease"><?=$lblforgotpw[_LANG];?></a></li>
        		<li><a href="/<?=_LANG;?>/conditions" class="ease"><?=$lblterms[_LANG];?></a></li>
        		<li><a href="https://facebook.com/xpressleader" class="ease">Facebook</a></li>
        	</ul>	
        </div>
        <div class="col-lg-2">
        	<h3>&nbsp;</h3>
        </div>
        <div class="col-lg-3">
        	<h3><img src="/images/logos/xpressleader_white.svg"/></h3>
        	<small>
        		2915 Ogletown road, # 1972 <br>
        		Newark, DE 19713, U.S.A.<br>
        		<a href="mailto:hello@xpressleader.com">hello@xpressleader.com</a>
        	</small>
        </div>
    </div>
  
  <div class="row">
  	<div class="row">
		<div class="col-lg-12 text-center" style="border-top: solid 1px #333;line-height: 40px;">
			 <?php print(date("Y")); ?> Xpress Leader LLC &copy; All rights reserved
		</div>
	</div>
  </div>
  </div>
</footer>