<?php
$lbltitle           = (_LANG=="fr") ? 'Session d\'activation de compte <span class="text-primary">expiré</span>' : '';
?>
<div class="whiteboard">
<div class="page-title">
	<?=$lbltitle;?>
</div>

<div id="pagewrap">
	<div class="row">
		<div class="col-lg-12">
			<p>Dû à un trop long moment d'activité de votre part, la session d'activation de votre compte est maintenant expiré, vous devez recommencer votre activation.</p>
						
			<div class="text-center"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/1" class="btn btn-lg btn-danger" type="button">Recommencer</a></div>
			
			<p>Si vous avez des questions concernant notre système d'activation de compte, communiqué avec notre service à la clientèle par courriel à 
			<a href="mailto:support@xpressleader.com">support@xpressleader.com</a>.</p>
		</div>
	</div>
</div>
</div>