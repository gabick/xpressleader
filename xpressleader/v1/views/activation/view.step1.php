<?php
$lbltitle           = (_LANG=="fr") ? '<span class="text-primary">Activation</span> de votre nouveau compte' : 'New account <span class="text-primary">activation</span>';
$lbldesc			= (_LANG=="fr") ? 'Merci de votre intérêt pour notre plateforme XpressLeader, afin d\'activer votre compte vous devez compléter la configuration de celui-ci.  Notez que vous pourrez modifier vos informations en tout temps une fois l\'activation terminée.' : 'Thank you for your interest in our XpressLeader platform to activate your account, you must complete the configuration of it. Note that you can change your information at any time after the activation completed.';
$lblterms			= (_LANG=="fr") ? "Termes et conditions d'utilisation" : "Terms and conditions of use";
$lblaccept			= (_LANG=="fr") ? "J'accepte les termes et conditions et je désires poursuivre l'activation de mon compte." : "I accept the terms and conditions of use";
$lblnext			= (_LANG=="fr") ? "Suivant" : "Next step";

$input['termsaccept'] = (isset($_SESSION["activation"]["termsaccept"])) ? 'checked="checked"' : '';
?>
<div class="page-title">
	<?=$lbltitle;?>
</div>

<div id="pagewrap">
	<div class="row">
		<div class="col-lg-12">
			<p><?=$lbldesc;?></p>
		</div>
	</div>
	
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<h4><?=$lblterms;?></h4>
				<textarea readonly="readonly" disabled="disabled" rows="16" class="form-control" style="width: 100%; font-size: 12px;font-weight:bold;"><? include(SITEPATH.'/views/activation/view.termsconditions.php'); ?></textarea>
			</div>
		</div>
		<br/>
		<div class="checkbox text-right">
			<label for="termsaccept" class="pull-right">
				<input type="checkbox" name="termsaccept" id="termsaccept" <?=$input['termsaccept'];?> value="true"> <strong><?=$lblaccept;?></strong>
			</label>
		</div>
		<br/><br/>
		
		
		<div class="row">
			<div class="col-lg-6"></div>
			<div class="col-lg-6 text-right"><button type="submit" name="activationstep1" class="btn btn-lg btn-primary"><?=$lblnext;?></button></div>
		</div>
	</form>
</div>