<?php
$lbltitle           = (_LANG=="fr") ? '<span class="text-primary">Clé d\'activation</span> invalide' : 'Invalid <span class="text-primary">activation key</span>';
?>
<div class="container">
	<div class="whiteboard">
		<div class="page-title">
			<?=$lbltitle;?>
		</div>
		
		<div id="pagewrap">
			<div class="row">
				<div class="col-lg-12">
		            <?php 
		            if(_LANG=="en"){
		            ?>
		                <p>To activate your account Xpress Leader, you must have a valid activation key. This code is sent to you via email after your subscription.</p>
						
						<p>If you have any questions about our account activation system, contacted our customer service by email at
						<a href="mailto:support@xpressleader.com">support@xpressleader.com</a>.</p>
						<br/><br/>
		            <?php 
		            }else{
		            ?>
		                <p>Afin d'activer votre compte Xpress Leader, vous devez détenir une clé d'activation valide. Ce code vous est envoyé par courriel suite à votre abonnement.</p>
						
						<p>Si vous avez des questions concernant notre système d'activation de compte, communiqué avec notre service à la clientèle par courriel à 
						<a href="mailto:support@xpressleader.com">support@xpressleader.com</a>.</p>
						<br/><br/>
		            <?php 
		            }
		            ?>
				</div>
			</div>
		</div>
	</div>
</div>