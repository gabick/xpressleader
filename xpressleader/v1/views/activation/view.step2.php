<?php
$lbltitle           = (_LANG=="fr") ? '<span class="text-primary">Profil</span> du nouveau membre' : 'New leader <span class="text-primary">profile</span>';
$lbldesc			= (_LANG=="fr") ? 'Assurez-vous que ce formulaire soit dûment rempli, tous les champs avec un asterix(*) sont obligatoires.' : 'Make sure the form below is duly completed, all fields followed by (*) are required.';
$lblfirstname		= (_LANG=="fr") ? "Prénom" : "Firstname";
$lbllastname		= (_LANG=="fr") ? "Nom" : "Lastname";
$lblpassword		= (_LANG=="fr") ? "Mot de passe (Minimum 6 caractères)" : "Password ( At least 6 characters )";
$lblphone			= (_LANG=="fr") ? "Téléphone" : "Phone Number";
$lblbday			= (_LANG=="fr") ? "Date de naissance" : "Date of birth";
$lblmonth			= (_LANG=="fr") ? "Mois" : "Month";
$lbladdress1		= (_LANG=="fr") ? "Adresse 1" : "Address 1";
$lbladdress2		= (_LANG=="fr") ? "Adresse 2" : "Address 2";
$lbladdress2d		= (_LANG=="fr") ? "( Appartement, Numéro buzzer etc )" : "( Apartment, Buzzer code etc)";
$lblcity			= (_LANG=="fr") ? "Ville" : "City";
$lblpostal			= (_LANG=="fr") ? "Code postal / Zip code" : "Postal / Zip code";
$lblregion			= (_LANG=="fr") ? "Région" : "Region";
$lblregionCA		= (_LANG=="fr") ? "PROVINCES DU CANADA" : "PROVINCES OF CANADA";
$lblregionUS		= (_LANG=="fr") ? "ÉTATS AMÉRICAIN" : "STATES OF AMERICA";
$lblcountry			= (_LANG=="fr") ? "Pays" : "Country";
$lblusa				= (_LANG=="fr") ? "États-Unis" : "United States";
$lblbtnnext			= (_LANG=="fr") ? "Suivant" : "Next step";
$lblbtnback			= (_LANG=="fr") ? "Retour" : "Previous";

$input['firstname'] = (isset($_SESSION["activation"]["firstname"])) ? $_SESSION["activation"]["firstname"] : '';
$input['lastname']  = (isset($_SESSION["activation"]["lastname"])) ? $_SESSION["activation"]["lastname"] : '';
$input['password'] 	= (isset($_SESSION["activation"]["password"])) ? $_SESSION["activation"]["password"] : '';
$input['phone'] 	= (isset($_SESSION["activation"]["phone"])) ? $_SESSION["activation"]["phone"] : '';
$input['address1'] 	= (isset($_SESSION["activation"]["address1"])) ? $_SESSION["activation"]["address1"] : '';
$input['address2'] 	= (isset($_SESSION["activation"]["address2"])) ? $_SESSION["activation"]["address2"] : '';
$input['city'] 		= (isset($_SESSION["activation"]["city"])) ? $_SESSION["activation"]["city"] : '';
$input['postal'] 	= (isset($_SESSION["activation"]["postal"])) ? $_SESSION["activation"]["postal"] : '';
$input['region'] 	= (isset($_SESSION["activation"]["city"])) ? $_SESSION["activation"]["region"] : '';
$input['country'] 	= (isset($_SESSION["activation"]["country"])) ? $_SESSION["activation"]["country"] : '';
$input['bday'] 		= (isset($_SESSION["activation"]["bday"])) ? $_SESSION["activation"]["bday"] : '';
$input['bmonth'] 	= (isset($_SESSION["activation"]["bmonth"])) ? $_SESSION["activation"]["bmonth"] : '';
$input['byear'] 	= (isset($_SESSION["activation"]["byear"])) ? $_SESSION["activation"]["byear"] : '';
?>

<div class="page-title">
	<?=$lbltitle;?>
</div>

<div id="pagewrap">

<p><?=$lbldesc;?></p>

<form action="" method="post" enctype="multipart/form-data" id="frmactive">
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputFirstname"><?=$lblfirstname;?>*</label>
				<input type="text" class="form-control" name="InputFirstname" id="InputFirstname" value="<?=$input['firstname'];?>" placeholder="" validate="required">
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputLastname"><?=$lbllastname;?>*</label>
				<input type="text" class="form-control" name="InputLastname" id="InputLastname" value="<?=$input['lastname'];?>" placeholder="" validate="required">
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="InputPassword"><?=$lblpassword;?>*</label>
		<input type="text" class="form-control" name="InputPassword" id="InputPassword" value="<?=$input['password'];?>" maxlength="12" placeholder="" validate="required">
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputPhone"><?=$lblphone;?>*</label>
				<input type="text" class="form-control phone_us" name="InputPhone" id="InputPhone" value="<?=$input['phone'];?>" placeholder="" validate="required">
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputAssociate"><?=$lblbday;?>*</label>
				<div class="row">
					<div class="col-lg-4"><input type="text" class="form-control text-center" maxlength="2" name="InputBday" id="InputBday" value="<?=$input['bday'];?>" placeholder="dd" validate="required|number"></div>
					<div class="col-lg-4">
						<?php 
						$dt = new datetimefrench();
						?>
						<select class="form-control" name="InputBmonth" id="InputBmonth" placeholder="<?=$lblmonth;?>" validate="required">
							<option value=""><?=$lblmonth;?></option>
							<option value="1" <?=($input['bmonth']=='1')?'selected=selected':'';?>><?=$dt->getStringmonth(1,_LANG);?></option>
							<option value="2" <?=($input['bmonth']=='2')?'selected=selected':'';?>><?=$dt->getStringmonth(2,_LANG);?></option>
							<option value="3" <?=($input['bmonth']=='3')?'selected=selected':'';?>><?=$dt->getStringmonth(3,_LANG);?></option>
							<option value="4" <?=($input['bmonth']=='4')?'selected=selected':'';?>><?=$dt->getStringmonth(4,_LANG);?></option>
							<option value="5" <?=($input['bmonth']=='5')?'selected=selected':'';?>><?=$dt->getStringmonth(5,_LANG);?></option>
							<option value="6" <?=($input['bmonth']=='6')?'selected=selected':'';?>><?=$dt->getStringmonth(6,_LANG);?></option>
							<option value="7" <?=($input['bmonth']=='7')?'selected=selected':'';?>><?=$dt->getStringmonth(7,_LANG);?></option>
							<option value="8" <?=($input['bmonth']=='8')?'selected=selected':'';?>><?=$dt->getStringmonth(8,_LANG);?></option>
							<option value="9" <?=($input['bmonth']=='9')?'selected=selected':'';?>><?=$dt->getStringmonth(9,_LANG);?></option>
							<option value="10" <?=($input['bmonth']=='10')?'selected=selected':'';?>><?=$dt->getStringmonth(10,_LANG);?></option>
							<option value="11" <?=($input['bmonth']=='11')?'selected=selected':'';?>><?=$dt->getStringmonth(11,_LANG);?></option>
							<option value="12" <?=($input['bmonth']=='12')?'selected=selected':'';?>><?=$dt->getStringmonth(12,_LANG);?></option>
						</select>
					</div>
					<div class="col-lg-4"><input type="text" class="form-control text-center" maxlength="4" name="InputByear" id="InputByear" value="<?=$input['byear'];?>" placeholder="yyyy" validate="required|number"></div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="form-group">
		<label for="InputAddress1"><?=$lbladdress1;?>*</label>
		<input type="text" class="form-control" name="InputAddress1" id="InputAddress1" value="<?=$input['address1'];?>" placeholder="" validate="required">
	</div>
	
	<div class="form-group">
		<label for="InputAddress2"><?=$lbladdress2;?> <small><?=$lbladdress2d;?></small></label>
		<input type="text" class="form-control" name="InputAddress2" id="InputAddress2" value="<?=$input['address2'];?>" placeholder="">
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputCity"><?=$lblcity;?>*</label>
				<input type="text" class="form-control" name="InputCity" id="InputCity" value="<?=$input['city'];?>" placeholder="" validate="required">
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputPostal"><?=$lblpostal;?>*</label>
				<input type="text" class="form-control" style="text-transform: uppercase;" name="InputPostal" id="InputPostal" value="<?=$input['postal'];?>" placeholder="" validate="required">
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputRegion"><?=$lblregion;?>*</label>
				<select class="form-control" name="InputRegion" validate="required">
					<option value=""></option>
					<option value=""><?=$lblregionCA;?></option>
					<option value="AB" <?=($input['region']=='AB')?'selected=selected':'';?>> &nbsp; Alberta</option>
					<option value="BC" <?=($input['region']=='BC')?'selected=selected':'';?>> &nbsp; British Columbia</option>
					<option value="MB" <?=($input['region']=='MB')?'selected=selected':'';?>> &nbsp; Manitoba</option>
					<option value="NB" <?=($input['region']=='NB')?'selected=selected':'';?>> &nbsp; New Brunswick</option>
					<option value="NL" <?=($input['region']=='NL')?'selected=selected':'';?>> &nbsp; Newfoundland and Labrador</option>
					<option value="NS" <?=($input['region']=='NS')?'selected=selected':'';?>> &nbsp; Nova Scotia</option>
					<option value="ON" <?=($input['region']=='ON')?'selected=selected':'';?>> &nbsp; Ontario</option>
					<option value="PE" <?=($input['region']=='PE')?'selected=selected':'';?>> &nbsp; Prince Edward Island</option>
					<option value="QC" <?=($input['region']=='QC')?'selected=selected':'';?>> &nbsp; Quebec</option>
					<option value="SK" <?=($input['region']=='SK')?'selected=selected':'';?>> &nbsp; Saskatchewan</option>
					<option value="NT" <?=($input['region']=='NT')?'selected=selected':'';?>> &nbsp; Northwest Territories</option>
					<option value="NU" <?=($input['region']=='NU')?'selected=selected':'';?>> &nbsp; Nunavut</option>
					<option value="YT" <?=($input['region']=='YT')?'selected=selected':'';?>> &nbsp; Yukon</option>
					<option value=""><?=$lblregionUS;?></option>
					<option value="AL" <?=($input['region']=='AL')?'selected=selected':'';?>> &nbsp; Alabama</option> 
					<option value="AK" <?=($input['region']=='AK')?'selected=selected':'';?>> &nbsp; Alaska</option> 
					<option value="AZ" <?=($input['region']=='AZ')?'selected=selected':'';?>> &nbsp; Arizona</option> 
					<option value="AR" <?=($input['region']=='AR')?'selected=selected':'';?>> &nbsp; Arkansas</option> 
					<option value="CA" <?=($input['region']=='CA')?'selected=selected':'';?>> &nbsp; California</option> 
					<option value="CO" <?=($input['region']=='CO')?'selected=selected':'';?>> &nbsp; Colorado</option> 
					<option value="CT" <?=($input['region']=='CT')?'selected=selected':'';?>> &nbsp; Connecticut</option> 
					<option value="DE" <?=($input['region']=='DE')?'selected=selected':'';?>> &nbsp; Delaware</option> 
					<option value="DC" <?=($input['region']=='DC')?'selected=selected':'';?>> &nbsp; District Of Columbia</option> 
					<option value="FL" <?=($input['region']=='FL')?'selected=selected':'';?>> &nbsp; Florida</option> 
					<option value="GA" <?=($input['region']=='GA')?'selected=selected':'';?>> &nbsp; Georgia</option> 
					<option value="HI" <?=($input['region']=='HI')?'selected=selected':'';?>> &nbsp; Hawaii</option> 
					<option value="ID" <?=($input['region']=='ID')?'selected=selected':'';?>> &nbsp; Idaho</option> 
					<option value="IL" <?=($input['region']=='IL')?'selected=selected':'';?>> &nbsp; Illinois</option> 
					<option value="IN" <?=($input['region']=='IN')?'selected=selected':'';?>> &nbsp; Indiana</option> 
					<option value="IA" <?=($input['region']=='IA')?'selected=selected':'';?>> &nbsp; Iowa</option> 
					<option value="KS" <?=($input['region']=='KS')?'selected=selected':'';?>> &nbsp; Kansas</option> 
					<option value="KY" <?=($input['region']=='KY')?'selected=selected':'';?>> &nbsp; Kentucky</option> 
					<option value="LA" <?=($input['region']=='LA')?'selected=selected':'';?>> &nbsp; Louisiana</option> 
					<option value="ME" <?=($input['region']=='ME')?'selected=selected':'';?>> &nbsp; Maine</option> 
					<option value="MD" <?=($input['region']=='MD')?'selected=selected':'';?>> &nbsp; Maryland</option> 
					<option value="MA" <?=($input['region']=='MA')?'selected=selected':'';?>> &nbsp; Massachusetts</option> 
					<option value="MI" <?=($input['region']=='MI')?'selected=selected':'';?>> &nbsp; Michigan</option> 
					<option value="MN" <?=($input['region']=='MN')?'selected=selected':'';?>> &nbsp; Minnesota</option> 
					<option value="MS" <?=($input['region']=='MS')?'selected=selected':'';?>> &nbsp; Mississippi</option> 
					<option value="MO" <?=($input['region']=='MO')?'selected=selected':'';?>> &nbsp; Missouri</option> 
					<option value="MT" <?=($input['region']=='MT')?'selected=selected':'';?>> &nbsp; Montana</option> 
					<option value="NE" <?=($input['region']=='NE')?'selected=selected':'';?>> &nbsp; Nebraska</option> 
					<option value="NV" <?=($input['region']=='NV')?'selected=selected':'';?>> &nbsp; Nevada</option> 
					<option value="NH" <?=($input['region']=='NH')?'selected=selected':'';?>> &nbsp; New Hampshire</option> 
					<option value="NJ" <?=($input['region']=='NJ')?'selected=selected':'';?>> &nbsp; New Jersey</option> 
					<option value="NM" <?=($input['region']=='NM')?'selected=selected':'';?>> &nbsp; New Mexico</option> 
					<option value="NY" <?=($input['region']=='NY')?'selected=selected':'';?>> &nbsp; New York</option> 
					<option value="NC" <?=($input['region']=='NC')?'selected=selected':'';?>> &nbsp; North Carolina</option> 
					<option value="ND" <?=($input['region']=='ND')?'selected=selected':'';?>> &nbsp; North Dakota</option> 
					<option value="OH" <?=($input['region']=='OH')?'selected=selected':'';?>> &nbsp; Ohio</option> 
					<option value="OK" <?=($input['region']=='OK')?'selected=selected':'';?>> &nbsp; Oklahoma</option> 
					<option value="OR" <?=($input['region']=='OR')?'selected=selected':'';?>> &nbsp; Oregon</option> 
					<option value="PA" <?=($input['region']=='PA')?'selected=selected':'';?>> &nbsp; Pennsylvania</option> 
					<option value="RI" <?=($input['region']=='RI')?'selected=selected':'';?>> &nbsp; Rhode Island</option> 
					<option value="SC" <?=($input['region']=='SC')?'selected=selected':'';?>> &nbsp; South Carolina</option> 
					<option value="SD" <?=($input['region']=='SD')?'selected=selected':'';?>> &nbsp; South Dakota</option> 
					<option value="TN" <?=($input['region']=='TN')?'selected=selected':'';?>> &nbsp; Tennessee</option> 
					<option value="TX" <?=($input['region']=='TX')?'selected=selected':'';?>> &nbsp; Texas</option> 
					<option value="UT" <?=($input['region']=='UT')?'selected=selected':'';?>> &nbsp; Utah</option> 
					<option value="VT" <?=($input['region']=='VT')?'selected=selected':'';?>> &nbsp; Vermont</option> 
					<option value="VA" <?=($input['region']=='VA')?'selected=selected':'';?>> &nbsp; Virginia</option> 
					<option value="WA" <?=($input['region']=='WA')?'selected=selected':'';?>> &nbsp; Washington</option> 
					<option value="WV" <?=($input['region']=='WV')?'selected=selected':'';?>> &nbsp; West Virginia</option> 
					<option value="WI" <?=($input['region']=='WI')?'selected=selected':'';?>> &nbsp; Wisconsin</option> 
					<option value="WY" <?=($input['region']=='WY')?'selected=selected':'';?>> &nbsp; Wyoming</option>
				</select>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label for="InputCountry"><?=$lblcountry;?>*</label>
				<select class="form-control" validate="required">
					<option value=""></option>
					<option value="CA" <?=($input['country']=='CA')?'selected=selected':'';?>>Canada</option>
					<option value="USA" <?=($input['country']=='USA')?'selected=selected':'';?>><?=$lblusa;?></option>
				</select>
			</div>
		</div>
	</div>
	
	<br/><br/>
	<div class="row">
		<div class="col-lg-6"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/" class="btn btn-lg btn-danger pull-left" type="button"><?=$lblbtnback;?></a></div>
		<div class="col-lg-6"><button type="submit" name="activationstep2" class="btn btn-lg btn-primary pull-right"><?=$lblbtnnext;?></button></div>
	</div>
</form>
</div>