<?php
$lbltitle           = (_LANG=="fr") ? '<span class="text-primary">Sommaire</span> de l\'abonnement et paiement' : 'Account <span class="text-primary">summary</span> & payment';
$lblleadpro			= (_LANG=="fr") ? "PROFIL DU LEADER" : "LEADER PROFILE";
$lblbtnmodify		= (_LANG=="fr") ? "Modifier" : "Edit";
$lblpaymentinfo		= (_LANG=="fr") ? "Informations de paiement" : "Payment information";
$lblselplan			= (_LANG=="fr") ? "Sélectionnez votre forfait" : "Select a plan";
$lblmonthly			= (_LANG=="fr") ? "MENSUEL - 15$" : "MONTHLY - 15$";
$lblyearly			= (_LANG=="fr") ? "ANNUEL - 120$" : "YEARLY - 120$";
$lblccholder		= (_LANG=="fr") ? "Détenteur de la carte" : "Card holder name";
$lblccnumber		= (_LANG=="fr") ? "Numéro de la carte" : "Card number";
$lblccexp			= (_LANG=="fr") ? "Date d'expiration" : "Expiration date";
$lblccmonth			= (_LANG=="fr") ? "Mois" : "Month";
$lblccyear			= (_LANG=="fr") ? "Année" : "Year";
$lblcccvc			= (_LANG=="fr") ? "Code de sécurité" : "Security code";
$lblimportant		= (_LANG=="fr") ? '<strong>Important</strong> Par mesure de sécurité, aucune information de paiement n\'est conservée chez XpressLeader.' : '<strong>Important</strong> To protect yourself, no payment information is stored in XpressLeader.';
$lbltitledesc		= (_LANG=="fr") ? "Tous les membres XpressLeader ont accès à une foule de produits et services exceptionnels:" : "XpressLeader All subscribers have access to a wealth of outstanding products and services:";
$lblbtnback			= (_LANG=="fr") ? "Retour" : "Previous";
$lblbtnactivate		= (_LANG=="fr") ? "PAYER ET ACTIVER MON COMPTE" : "PAY & ACTIVATE MY ACCOUNT";

$lblselplan     = array("fr"=>"Choisissez la fréquence de paiement","en"=>"Choose a payment frequency");
$lblmonthly     = array("fr"=>"MENSUEL","en"=>"MONTHLY");
$lblyearly      = array("fr"=>"ANNUEL","en"=>"YEARLY");
$lblccnumber    = array("fr"=>"Numéro de carte de crédit","en"=>"Card number");
$lblccmonth     = array("fr"=>"Mois","en"=>"Month");
$lblccyear      = array("fr"=>"Année","en"=>"Year");
$lblccexp       = array("fr"=>"Date d'expiration","en"=>"Expiration date");

$input['plan'] 			= (isset($_SESSION["activation"]["plan"])) ? $_SESSION["activation"]["plan"] : '';
$input['ccholder'] 		= (isset($_SESSION["activation"]["ccholder"])) ? $_SESSION["activation"]["ccholder"] : '';
?>

<div class="page-title">
	<?=$lbltitle;?>
</div>

<form action="" method="post" enctype="multipart/form-data" id="frmpayment">
<div id="pagewrap">
<div class="row">
	<div class="col-lg-4" style="background-color: #eeeeee;">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title"><?=$lblleadpro;?></h2>
			</div>
			<div class="panel-body">
				<strong><?=$_SESSION["activation"]["firstname"];?> <?=$_SESSION["activation"]["lastname"];?></strong> <br/>
				<?=$_SESSION["activation"]["address1"];?> <br/>
				<?=($_SESSION["activation"]["address2"]<>"")?$_SESSION["activation"]["address2"].'<br>': '';?>
				<?=$_SESSION["activation"]["city"];?>, <?=$_SESSION["activation"]["region"];?>, <?=$_SESSION["activation"]["country"];?> <br/>
				<?=$_SESSION["activation"]["postal"];?> <br/>
				<?=$_SESSION["activation"]["phone"];?> <br/>
				<div class="text-right">
					<a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/2" class="btn btn-default" type="button"><?=$lblbtnmodify;?></a>
				</div>
			</div>
			
		</div>
		
	</div>
	<div class="col-lg-8">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title"><?=$lblpaymentinfo;?></h2>
			</div>
			<div class="panel-body">
				<div class="alert alert-danger payment-errors text-center hide" role="alert"></div>
				<div class="form-group">
					<label for="InputName"><?=$lblselplan[_LANG];?></label><br/>
					<div class="btn-group btn-group-justified" data-toggle="buttons">
						<label id="plan1" for="selplan1" class="btn btn-lg <?=($input['plan'] =="1")?'btn-primary':'btn-default';?>">
							<input type="radio" name="selplan" class="selplan" id="selplan1" value="1" <?=($input['plan']=="1")?'checked="checked" class="checked"':'';?>> <?=$lblmonthly[_LANG];?> - 15$
						</label>
						<label id="plan2" for="selplan2" class="btn btn-lg <?=($input['plan']=="2")?'btn-primary':'btn-default';?>">
							<input type="radio" name="selplan" class="selplan" id="selplan2" value="2" <?=($input['plan']=="2")?'checked="checked" class="checked"':'';?>> <?=$lblyearly[_LANG];?> - 120$
						</label>
					</div>
				</div>
		
				<div class="form-group">
					<label for=""><?=$lblccnumber[_LANG];?></label>
					<input type="text" class="form-control" maxlength="16" id="cc_number" placeholder="" validate="required|number|length_min,16">
				</div>
		
				<div class="row">
					<div class="col-lg-4">
					    <div class="form-group">
					        <label for=""><?=$lblccexp[_LANG];?></label>
							<select id="cc_exp_month" class="form-control" validate="required">
								<option value=""><?=$lblccmonth[_LANG];?></option>
								<?php
								$d = 1;
								while ($d <= 12) {
								    echo '<option value="'.$d.'">'.str_pad($d,2,"0",STR_PAD_LEFT).'</option>'; 
								    $d += 1;
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="form-group">
					        <label>&nbsp;</label>
							<select id="cc_exp_year" class="form-control" validate="required">
								<option value=""><?=$lblccyear[_LANG];?></option>
								<?php
								$today = getdate();
								$y = $today['year'];
								$i = 1;
								while ($i <= 15) {
								    echo '<option value="'.$y.'">'.$y.'</option>';  
								    $y += 1;
								    $i += 1;
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="form-group">
							<label for="">CVC</label>
							<input type="text" class="form-control" maxlength="4" id="cc_cvc" placeholder="999" validate="required|number|length_min,3">
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-info"><?=$lblimportant;?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<br/><br/>
	</div>
</div>

<div class="row">
	<input type="hidden" name="activationstep3" value="true"/>
	<div class="col-lg-6"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/2" class="btn btn-lg btn-danger pull-left" type="button"><?=$lblbtnback;?></a></div>
	<div class="col-lg-6"><button type="submit" id="subpaynow" class="btn btn-lg btn-primary pull-right submit"><?=$lblbtnactivate;?></button></div>
</div>
</form>
</div>
</div>