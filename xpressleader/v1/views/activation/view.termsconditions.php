SI VOUS N'ÊTES PAS D'ACCORD AVEC L'UN OU L’AUTRE DE CES TERMES ET CONDITIONS, NE PAS UTILISER NOTRE SITE WEB.

MINEURS
Nous n'offrons pas de services aux enfants. Si vous êtes âgé de moins de 18 ans, vous pouvez utiliser notre site Web seulement avec la permission et la participation active d'un parent ou d'un tuteur légal.

FACTURATION
Les membres qui obtiennent un essai gratuit, comprennent qu'ils seront automatiquement mis à jour et facturé au prix convenu à la fin de ce délai à moins d’avoir annulé avant le délai.
Les annulations doivent être demandées par le membre dans la zone des membres via le site dans la section aide.

POLITIQUE DE CONFIDENTIALITÉ
Notre politique de confidentialité fait partie, et sous réserve des présentes conditions générales d'utilisation. Vous pouvez consulter notre politique de confidentialité sur XpressLeader.com

LA POLITIQUE ANTI-SPAM FAIT PARTIE DE CES TERMES ET CONDITIONS
Notre politique anti-Spam fait partie des présentes conditions générales d'utilisation. Vous pouvez consulter notre politique anti-spam sur XpressLeader.com

MODIFICATIONS DES POLITIQUES
Ces termes et conditions peuvent changer de temps en temps. Si ces modifications sont apportées, elles seront en vigueur immédiatement, et nous allons vous informer par un avis affiché sur la page d'accueil de notre site Web des changements qui ont été apportés.
Si vous êtes en désaccord avec les changements qui ont été faits, vous ne devez pas utiliser notre site Web.
Nous pouvons mettre fin à ces termes et conditions d'utilisation pour toutes raisons et à tout moment sans préavis.
Si vous êtes préoccupé(e) par ces termes et conditions d'utilisation, vous devriez les lire chaque fois que vous utilisez notre site Web.
Si vous avez des questions ou des préoccupations qui devraient être porté à notre attention
envoyez-nous un courriel à support@XpressLeader.com, et nous vous fournirons des
informations relatives à votre question ou préoccupation.

ÉTAT DE LICENCE
Vous comprenez et acceptez que votre utilisation de notre site Web est limité et non-exclusif en
tant que licencié révocable non transférable. Nous pouvons résilier votre licence d'utilisation de
notre site, et vous empêcher d’accéder à notre site Web, pour une raison quelconque, et sans
vous donner un avis.

LA PROPRIÉTÉ DU CONTENU
Tout le contenu de notre site Internet est détenu par nous ou nos fournisseurs de contenu. Au
nom de nous-mêmes et de nos fournisseurs de contenu, nous revendiquons tous les droits de
propriété, y compris les droits de propriété intellectuelle, pour ce contenu et vous n'êtes pas
autorisé à porter atteinte à ces droits. Une poursuite selon la loi pourrait-être intentée à toute
personne qui tente de s’approprier ou de voler notre propriété.

Vous vous engagez à ne pas copier le contenu de notre site Web sans notre permission.
Toutes demandes d'utiliser notre contenu doivent être soumises à nous par courriel à :
support@XpressLeader.com

Si vous croyez que vos droits de propriété intellectuelle ont été violés par le contenu de notre
site Web, s'il vous plaît nous en informer en envoyant un e-mail à support@XpressLeader.com.
S'il vous plaît décrire en détail l’infraction présumée, dont le fondement actuel et juridique de
votre revendication de la propriété.

EXCLUSIONS ET LIMITATIONS DE RESPONSABILITÉ
L'information sur notre site Web est fourni «tel quel», «tel que disponible".
Vous acceptez que votre utilisation de notre site Web est à vos risques et périls.
Nous déclinons toute garantie de toute nature, y compris mais non limité à toute garantie
expresse, les garanties statutaires, notamment les garanties implicites de qualité marchande,
d'adéquation à un usage particulier et de non-contrefaçon.

Nous ne garantissons pas que notre site sera toujours disponible, ou que l'accès sera
ininterrompu, ni exempt d'erreurs.

Nous essaierons de répondre à vos besoins pour que toutes les erreurs de notre site soient
corrigées.

Les informations sur notre site web ne doivent pas nécessairement être invoquées et ne
devraient pas être interprétés comme des conseils professionnels de nous.

Nous ne garantissons pas l'exactitude ou l'exhaustivité de l'information fournie, et nous ne
sommes pas responsable de toutes pertes résultant de votre utilisation de cette information.

Si votre pays ne permet pas les limitations de garanties, cette limitation peut ne pas s'appliquer à vous. Votre seul et unique recours concernant votre utilisation du site est de cesser d'utiliser le site.
En aucun cas nous ne pourrons être tenu responsable pour tout dommage direct, indirect, accessoire, consécutif (y compris les dommages causés par la perte d'affaires, pertes de profits, les litiges ou autres), spéciaux, exemplaires, punitifs ou d'autres dommages et intérêts, sous quelque théorie juridique, découlant de, ou lié de quelque façon à notre site, votre utilisation du site Web ou du contenu, même si conseillé, peux avoir la possibilité de tels dommages.

Notre responsabilité totale pour toute réclamation découlant de, ou lié à notre site Web ne doit pas excéder cent (100) dollars et ce montant doit être à la place de tous les autres recours que vous pouvez avoir contre nous ou nos sociétés affiliées. Toute réclamation doit être soumise à l'arbitrage exécutoire confidentiel comme décrit plus loin dans ces termes et conditions d'utilisation.
Contenus obscènes et offensants

Nous ne sommes pas responsable de tout contenu obscène ou offensant que vous recevez ou visualiser des autres tout en utilisant notre site Web.
Toutefois, si vous recevez ou afficher ce contenu, s'il vous plaît nous contacter par e-mail à admin@XPressLeader.com afin que nous puissions enquêter sur l'affaire. Bien que nous ne sommes pas obligés de le faire, nous nous réservons le droit de surveiller, d'enquêter et de retirer du matériel obscène ou offensant affiché sur notre site Web.

INDEMNISATION
Vous comprenez et acceptez que vous vous engagez à indemniser, défendre et nous tenir au courant à nous et à nos affiliés de toute responsabilité, perte, réclamation et dépenses, y compris les honoraires raisonnables d'avocat, résultant de votre utilisation de notre site Web ou votre violation de ces termes et conditions.

RESPECT DES LOIS APPLICABLES ET LITIGES
Vous vous engagez à respecter toutes les lois applicables lorsque vous utilisez notre site.
Vous acceptez que les lois du Canada régissent ces termes et conditions d'utilisation, sans égard aux conflits de lois.
Vous acceptez également que tout litige entre vous et nous, à l'exclusion de toute violation du droit de propriété intellectuelle prétendu, doit être réglée uniquement par arbitrage exécutoire confidentiel par les règles d'arbitrage commercial du Canada. Toutes les réclamations doivent être arbitrer sur une base individuelle, et ne peuvent pas être incorporée à un arbitrage à toute réclamation ou controverse de quelqu'un d'autre.
Tout arbitrage doit avoir lieu à Montréal, Québec, Canada. Chaque partie supportera la moitié des frais d'arbitrage et les coûts encourus, et chaque partie est responsable de ses propres frais d'avocat.

AUTONOMIE DE CES TERMES ET CONDITIONS
Si une partie de ces termes et conditions d'utilisation sont déterminés par un tribunal de juridiction compétente comme invalide ou inapplicable, cette partie sera limitée ou éliminée dans la mesure minimale nécessaire pour que le reste de ces termes et conditions sont pleinement exécutoires et légalement contraignant.

COMMENT NOUS CONTACTER
Si vous avez des questions ou des préoccupations au sujet de ces termes et conditions d'utilisation elles doivent être portées à notre attention par courriel à support@XpressLeader.com, et vous devrez nous fournir des informations relatives à votre préoccupation.

INTÉGRALITÉ DE L'ENTENTE
Ces termes et conditions, y compris les politiques qui y sont intégrés par renvoi exprès, constituent l'intégralité de votre accord avec nous à l'égard de votre utilisation de notre site Web.
Cette Conditions générales d'utilisation a été mis à jour le 08-08-2013.
Droit d'auteur © XPressLeader.com, et utilisées sous licence par le propriétaire de ce site à http://XpressLeader.com
Tous droits réservés. Aucune partie de ce document ne peut être copié ou utilisé par une autre personne que le titulaire de permis sans l'autorisation expresse écrite du propriétaire du droit d'auteur.