<?php
if($activationerror){
	echo '<div class="alert alert-danger">'.$activationmsg.'</div>';
}

$input['code'] 		= (isset($_SESSION["activation"]["code"])) ? $_SESSION["activation"]["code"] : '';
$input['order'] 	= (isset($_SESSION["activation"]["bcorder"])) ? $_SESSION["activation"]["bcorder"] : '';
?>

<div class="page-title">
	<?php 
	if(_LANG=="fr"){
		echo '<span class="text-primary">Configuration</span> de votre compte';
	}else{
		echo 'Account <span class="text-primary">configuration</span>';
	}
	?>
</div>

<div id="pagewrap">
<?php 
if(_LANG=='en'){
?>
<form action="" method="post" enctype="multipart/form-data" id="frmactive">
<div class="row">
	<div class="col-lg-12">
		<h3>Create your alias code</h3>
		
		<p>The products and services are customized with a unique alias code for each subscriber. This will allow for example to access your page XpressLook
		or XpressMeeting room. You must now determine your unique alias code for your account.</p>

	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<p>Some rules to follow regarding your code:</p>
		<ul>
			<li>It must be between 3 and 10 characters.</li>
			<li>It must be alphanumeric.</li>
			<li>It must contain no accent or special character </li>
			<li>Code of nature racist, sexist or homophobic will not be tolerated.</li>
		</ul>
	</div><!-- /.col-lg-6 -->
	<div class="col-lg-6">
		<label for="InputCode">Enter your alias code</label>
		<div class="form-group">
			<input type="text" name="InputCode" id="InputCode" maxlength="10" value="<?=$input['code'];?>" class="form-control col-lg-12" validate="required|length_between,3,10">
		</div><!-- /form-group -->
		
		<p>
			<small>http://xpresslook.com/</small><strong class="previewcode"></strong><br/>
			<small>http://xpressmeeting.com/</small><strong class="previewcode"></strong><br/>
		</p>
	</div>
</div><!-- /.row -->

<div class="row">
	<div class="col-lg-12">
		<br/><br/>
	</div>
</div>

<div class="row">
	<input type="hidden" name="InputBcOrder" value="0"/>
	<div class="col-lg-6"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/2" class="btn btn-lg btn-danger pull-left" type="button">Previous</a></div>
	<div class="col-lg-6"><button type="submit" name="activationstep3" class="btn btn-lg btn-primary pull-right">Next step</button></div>
</div>
</form>



<?php
}else{
?>

<form action="" method="post" enctype="multipart/form-data" id="frmactive">
<div class="row">
	<div class="col-lg-12">
		<h3>Définir votre lien unique</h3>
		
		<p>Les produits et services d'xpressleader sont personnalisés grâce à un lien unique pour chaque abonné. Celui-ci permettra par exemple d'accéder à votre page XpressLook
		ou votre salle virtuelle XpressMeeting. Vous devez dès maintenant déterminer votre lien unique pour votre compte.</p>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<p>Quelques règles à respecter concernant votre lien:</p>
		<ul>
			<li>Il doit être entre 3 et 10 caractères.</li>
			<li>Il doit être alphanumérique.</li>
			<li>Il ne doit contenir aucun accent ni caractère spécial (&?%éàê).</li>
			<li>Les liens à caractère raciste, sexiste ou homophobe ne seront pas tolérés.</li>
		</ul>
	</div><!-- /.col-lg-6 -->
	<div class="col-lg-6">
		<label for="InputCode">Définissez votre lien</label>
		<div class="form-group">
			<input type="text" name="InputCode" id="InputCode" maxlength="10" value="<?=$input['code'];?>" class="form-control" validate="required|length_between,3,10">
		</div><!-- /form-group -->
		
		<p>
			<small>http://xpresslook.com/</small><strong class="previewcode"></strong><br/>
			<small>http://xpressmeeting.com/</small><strong class="previewcode"></strong><br/>
		</p>
	</div>
</div><!-- /.row -->

<div class="row">
	<div class="col-lg-12">
		<br/><br/>
	</div>
</div>

<div class="row">
	<input type="hidden" name="InputBcOrder" value="0"/>
	<div class="col-lg-6"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/2" class="btn btn-lg btn-danger pull-left" type="button">Retour</a></div>
	<div class="col-lg-6"><button type="submit" name="activationstep3" class="btn btn-lg btn-primary pull-right">Suivant</button></div>
</div>
</form>




<?php
}
?>

</div>