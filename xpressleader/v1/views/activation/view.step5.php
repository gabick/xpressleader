<div class="page-title">
	<?php 
	if(_LANG=="fr"){
		echo 'Votre abonnement a été complété <span class="text-success">avec succès</span> !';
	}else{
		echo 'Your subscription has been <span class="text-success">successfully completed!</span>';
	}
	?>
</div>

<div id="pagewrap">
<?php
if(_LANG=='en'){
?>

<p>In a few minutes you will receive an email with your credential as well as all the details of your new subscription.</p>

<p>If you have any questions / comments regarding your subscription, please contact us <a href="mailto:billing@xpressleader.com">billing@xpressleader.com</a>.</p>

<p>Welcome to the team!</p>

<div class="text-center"><a href="/en" class="btn btn-danger btn-lg">LOGIN TO YOUR ACCOUNT</a></div>
<?php
}else{
?>

<p>Dans quelques minutes, vous recevrez un courriel de confirmation d'abonnement ainsi que tous les détails de votre nouvel abonnement.</p>

<p>Si vous avez des questions/commentaires concernant votre abonnement, communiquez avec nous à <a href="mailto:billing@xpressleader.com">billing@xpressleader.com</a>.</p>

<p>Bienvenue dans l'équipe !</p>

<div class="text-center"><a href="/fr" class="btn btn-danger btn-lg">CONNECTEZ-VOUS À VOTRE COMPTE</a></div>
<?php
}
?>
</div>