<?php
$client = $account->GetClient($cleader->code,_ID);

if(!$client){
	if(_LANG=="en"){
?>
		<div class="row">
			<div class="col-lg-12 text-center">
				<br/><br/><br/>
				<h3>We cannot find this client, please try again</h3>
				<a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>" class="btn btn-primary">Back to my clients list</a>
				<br/><br/><br/>
			</div>
		</div>
<?php
	}else{
?>

	<div class="row">
			<div class="col-lg-12 text-center">
				<br/><br/><br/>
				<h3>Aucun client n'a été trouvé, s'il vous plaît, réessayez de nouveau.</h3>
				<a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>" class="btn btn-primary">Retour à la liste des clients</a>
				<br/><br/><br/>
			</div>
		</div>

<?php
	}
}else{
		
$input['name'] 			= (isset($client->name)) ? $client->name : '';
$input['associate'] 	= (isset($client->associate)) ? $client->associate : '';
$input['phone'] 		= (isset($client->phone)) ? $client->phone : '';
$input['mail'] 			= (isset($client->mail)) ? $client->mail : '';
$input['birthday'] 		= (isset($client->bday)) ? $client->bday : '';
$input['birthmonth'] 	= (isset($client->bmonth)) ? $client->bmonth : '';
$input['birthyear'] 	= (isset($client->byear)) ? $client->byear : '';
$input['chkinterest'] 	= (isset($client->interest)) ? $client->interest : '';
$input['chkvaluepack'] 	= (isset($client->valuepack)) ? $client->valuepack : '';
$input['chksecurity'] 	= (isset($client->security)) ? $client->security : '';
$input['comment'] 		= (isset($client->message)) ? $client->message : '';

if($officeerror){
	echo '<div class="alert alert-danger alert-block">'.$officemsg.'</div>';
}

if($officesuccess){
	echo '<div class="alert alert-success alert-block">'.$officemsg.'</div>';
}



if(_LANG=="en"){
?>

<div class="page-header">
	<h1><span class="text-primary">Client</span> informations <small><?=$client->name;?> ( <?=$client->mail;?> )</small></h1>
</div>



	<div class="row">
		
		<div class="col-lg-6">
			
			<table class="table table-condensed">
				<tr>
					<td valign="top">Prefered language</td>
					<td style="background-color: #eee;">
						<?php
						if($client->lang=='fr') echo 'French'; else echo 'English';
						?>
					</td>
				</tr>
				<tr>
					<td valign="top">Last update</td>
					<td style="background-color: #eee;">
						<?=$client->dateupdate;?>
					</td>
				</tr>
				<?php
				if($client->presname<>""){		
				?>
				<tr>
					<td valign="top">Presenter</td>
					<td style="background-color: #eee;">
						<?=$client->presname;?><br>
						<?=$client->presphone;?>
					</td>
				</tr>
				<?php
				}		
				?>
				
				<?php
				if($client->unlook==1){		
				?>
				<tr>
					<td valign="top" class="text-center" colspan="2"><i>Prospect have been removed from our sent list</i></td>
				</tr>
				<?php
				}		
				?>
			</table>
			
		</div>
		
		<div class="col-lg-3">
		
		    <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change client status</h3>
                </div>
                <div class="panel-body text-center">
                    <button data-toggle="modal" href="#chclientlook" type="button" class="btn <?=($client->status=="0")?'disabled':'btn-danger';?>">Look</button>
        		    <button data-toggle="modal" href="#chclientmeeting" type="button" class="btn <?=($client->status=="1")?'disabled':'btn-primary';?>">Meeting</button>
        		    <button data-toggle="modal" href="#chclientclient" type="button" class="btn btn-success">Client</button>
                </div>
                
                
                <div class="modal fade" id="chclientlook" tabindex="-1" role="dialog" aria-labelledby="chclientlook" aria-hidden="true">
    		    	<div class="modal-dialog">
    		    	    <form action="" method="post" enctype="multipart/form-data">
    			    	<div class="panel panel-danger">
    			    		<div class="panel-heading panel-heading-red text-center"><h3>Change client status to <strong>LOOK</strong></h3></div>
    			    		<div class="panel-body">
    							 From the moment you switch a client back to LOOK status, he will receives your calendar everyday. Are you sure that you want change this status to Look ?
    						</div>
    						<div class="panel-footer text-right">
								<input type="hidden" name="clientid" value="<?=$client->id;?>"/>
								<input type="hidden" name="presenterid" value="<?=$client->presenterid;?>"/>
								<input type="hidden" name="status" value="0"/>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" name="subupdatestatus" class="btn btn-danger">CONFIRM!</button>
    						</div>
    			    	</div>
    			    	</form>
    		    	</div>
    		    </div>
    		    
    		    <div class="modal fade" id="chclientmeeting" tabindex="-1" role="dialog" aria-labelledby="chclientmeeting" aria-hidden="true">
    		    	<div class="modal-dialog">
    		    	    <form action="" method="post" enctype="multipart/form-data">
    			    	<div class="panel panel-primary">
    			    		<div class="panel-heading text-center"><h3>Change client status to <strong>MEETING</strong></h3></div>
    			    		<div class="panel-body">
    							<p>To switch a client to Meeting status, you need to specify the presenter, who makes the business overview for this client.</p>
    							 
							     <div class="form-group">
							         <label>Presenter</label>
							         <select name="presenterid" class="form-control">
							             <option value="<?=$cleader->code;?>">I am the presenter</option>
							             <option value="">Someone else, not on this list.</option>
							             <?php
							                $sql = "SELECT * FROM leaders WHERE xmeetleader=1";
						                    $res	= $db->datasource->query($sql);
                                	  		$res->setFetchMode(PDO::FETCH_OBJ);
                                			
                                			if(($res->rowCount()>0)){
                                				while( $ligne = $res->fetch() ) {
                                					echo '<option value="'.$ligne->code.'">'.$ligne->name.'</option>';
                                				}
                                			}
							             ?>
							         </select>
							     </div>
    							 
    						</div>
    						<div class="panel-footer text-right">
								<input type="hidden" name="clientid" value="<?=$client->id;?>"/>
								<input type="hidden" name="status" value="1"/>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" name="subupdatestatus" class="btn btn-primary">SAVE!</button>
    						</div>
    			    	</div>
    			    	</form>
    		    	</div>
    		    </div>
    		    
    		    
    		    
    		    
    		    <div class="modal fade" id="chclientclient" tabindex="-1" role="dialog" aria-labelledby="chclientclient" aria-hidden="true">
    		    	<div class="modal-dialog">
    		    	    <form action="" method="post" enctype="multipart/form-data">
    			    	<div class="panel panel-success">
    			    		<div class="panel-heading panel-heading-green text-center"><h3>Change client status to <strong>CLIENT</strong></h3></div>
    			    		<div class="panel-body text-center">
    							 <h2>Coming soon!</h2>
    						</div>
    						<div class="panel-footer text-right">
								<input type="hidden" name="inputid" value="<?=$client->id;?>"/>
								<input type="hidden" name="inputleaderid" value="<?=$cleader->code;?>"/>
    						</div>
    			    	</div>
    			    	</form>
    		    	</div>
    		    </div>
            </div>
		    
		</div>
		
		
		<div class="col-lg-3">
            <div class="panel panel-danger">
                <div class="panel-heading panel-heading-red">
                    <h3 class="panel-title">Not interested ?</h3>
                </div>
                <div class="panel-body text-center">
                    <button data-toggle="modal" href="#delclient<?=$client->id;?>" type="button" class="btn btn-default">Delete client</button>
                </div>
            </div>
            
            <div class="modal fade" id="delclient<?=$client->id;?>" tabindex="-1" role="dialog" aria-labelledby="delclient<?=$client->id;?>" aria-hidden="true">
		    	<div class="modal-dialog">
			    	<div class="panel panel-danger">
			    		<div class="panel-heading panel-heading-red text-center"><h3>Remove client <i><?=$client->name;?></i></h3></div>
			    		<div class="panel-body">
							 <strong><?=$client->name;?> - <?=$client->mail;?> is not interested and i want to remove this client.</strong>
						</div>
						<div class="panel-footer text-right">
							<form action="" method="post" enctype="multipart/form-data">
								<input type="hidden" name="inputid" value="<?=$client->id;?>"/>
								<input type="hidden" name="inputleaderid" value="<?=$cleader->code;?>"/>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" name="subdelclient" class="btn btn-danger">DELETE</button>
							</form>
						</div>
			    	</div>
		    	</div>
		    </div>
		</div>
		
		
		
	</div>

	<div class="row">
	
	    <div class="col-lg-4">
		    <br>
		    <form action="" method="post" role="form" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">FOLLOW UP</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="selfustatus">Status</label>
                        <select name="fustatus" id="selfustatus" class="form-control">
                            <option <?=($client->status==0)?'selected=selected':'';?> value="0"></option>
                            <option <?=($client->status==1)?'selected=selected':'';?> value="1">Directeur</option>
                            <option <?=($client->status==2)?'selected=selected':'';?> value="2">Emailed</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputfunotes">Notes</label>
                        <textarea name="funotes" id="inputfunotes" class="form-control" rows="4"><?=$client->notes;?></textarea>
                    </div>
                    
                    <input type="hidden" name="fuProspectid" value="<?=$client->id;?>"/>
                    
                    <button type="submit" name="subfuupdate" class="btn btn-primary btn-block">Update follow up</button>
                </div>
            </div>
            </form>
            
            
            <form action="" method="post" role="form" enctype="multipart/form-data">
            <div class="panel panel-danger">
                <div class="panel-heading panel-heading-red">
                    <h3 class="panel-title">Reminder & Alerts</h3>
                </div>
                <div class="panel-body text-center">
                    Coming soon
                    <input type="hidden" name="fuProspectid" value="<?=$client->id;?>"/>
                </div>
            </div>
            </form>
            
            
    		
		</div>
		<div class="col-lg-8">
			<div class="form-group">
				<label for="InputName">Prospect name</label><br>
				<input type="text" class="form-control" name="InputName" id="InputName" value="<?=$input['name'];?>" placeholder="" validate="required">
			</div>
			
			<div class="form-group">
				<label for="InputAssociate">Associate/Spouse</label>
				<input type="text" class="form-control" name="InputAssociate" id="InputAssociate" value="<?=$input['associate'];?>" placeholder="">
			</div>
			
			<div class="form-group">
				<label for="inputmail">Mail</label>
				<input type="text" class="form-control" name="InputMail" id="inputmail" value="<?=$input['mail'];?>" placeholder="" validate="email">
			</div>
			
			<div class="form-group">
				<label for="InputPhone">Phone</label>
				<input type="text" class="form-control" name="InputPhone" maxlength="10" id="InputPhone" value="<?=$input['phone'];?>" placeholder="">
			</div>
            
            <div class="form-group">
    			<label for="InputBirthDay">Date of birth</label>
    			<div class="form-inline">
    				<div class="row">
    					<div class="col-lg-4"><input type="text" class="form-control" name="InputBirthday" id="InputBirthDay" value="<?=$input['birthday'];?>" placeholder="JJ" validate="required"></div>
    					<div class="col-lg-4">
    						<select class="form-control" name="InputBirthmonth" name="InputBirthmonth" validate="required">>
        						<option <?=($input['birthmonth']=='')?'selected=selected':'';?> value="">Month</option>
    							<option value="1" <?=($input['birthmonth']=='1')?'selected=selected':'';?>>January</option>
    							<option value="2" <?=($input['birthmonth']=='2')?'selected=selected':'';?>>February</option>
    							<option value="3" <?=($input['birthmonth']=='3')?'selected=selected':'';?>>March</option>
    							<option value="4" <?=($input['birthmonth']=='4')?'selected=selected':'';?>>April</option>
    							<option value="5" <?=($input['birthmonth']=='5')?'selected=selected':'';?>>May</option>
    							<option value="6" <?=($input['birthmonth']=='6')?'selected=selected':'';?>>June</option>
    							<option value="7" <?=($input['birthmonth']=='7')?'selected=selected':'';?>>July</option>
    							<option value="8" <?=($input['birthmonth']=='8')?'selected=selected':'';?>>August</option>
    							<option value="9" <?=($input['birthmonth']=='9')?'selected=selected':'';?>>September</option>
    							<option value="10" <?=($input['birthmonth']=='10')?'selected=selected':'';?>>October</option>
    							<option value="11" <?=($input['birthmonth']=='11')?'selected=selected':'';?>>November</option>
    							<option value="12" <?=($input['birthmonth']=='12')?'selected=selected':'';?>>December</option>
    						</select>
    					</div>
    					<div class="col-lg-4"><input type="text" class="form-control" name="InputBirthyear" id="InputBirthyear" value="<?=$input['birthyear'];?>" placeholder="YYYY" validate="required"></div>
    				</div>
    			</div>
			</div>
			
			<div class="form-group">
			    <label for="InputBirthDay">Questions / Comments</label>
			    <textarea class="form-control" rows="5"><?=$input['comment'];?></textarea>
            </div>
		</div>

	</div>
	
	
	<?php
	if($input['chkinterest']<>''){
	?>
	<h3>Interest level</h3>
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-success">
				<div class="panel-heading"><label for="chkinterest1"><input type="radio" name="chkinterest" id="chkinterest1" value="1" <?=($input['chkinterest']=="1")? 'checked="checked"' : '';?> /> <strong> Client</strong></label></div>
				<div class="panel-body">
					Shop and save 30% - 40%
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel panel-warning">
				<div class="panel-heading"><label for="chkinterest2"><input type="radio" name="chkinterest" id="chkinterest2" value="2" <?=($input['chkinterest']=="2")? 'checked="checked"' : '';?> /> <strong> Business builder (Part time)</strong></label></div>
				<div class="panel-body">
					Invest 5-15 hours a week
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel panel-danger">
				<div class="panel-heading"><label for="chkinterest3"><input type="radio" name="chkinterest" id="chkinterest3" value="3" <?=($input['chkinterest']=="3")? 'checked="checked"' : '';?> /> <strong> Business builder (Full time)</strong></label></div>
				<div class="panel-body">
				    Put in a serious effort - 20+ hours each week
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	
	if($input['chkvaluepack']<>''){
	?>
	
	<h3>Value pack</h3>
	<div class="row">
	
		<div class="col-lg-12">
			<div class="list-group">
				<div class="list-group-item <?=($input['chkvaluepack']=="1")? 'active' : '';?>">
					
					<h4 class="list-group-item-heading">
					<input type="radio" name="chkvaluepack" value="1" <?=($input['chkvaluepack']=="1")? 'checked="checked"' : '';?> /> <strong>- Ensemble Essentiel</strong></h4>
					<p class="list-group-item-text"></p>
				</div>
				
				<div class="list-group-item <?=($input['chkvaluepack']=="2")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chkvaluepack" value="2" <?=($input['chkvaluepack']=="2")? 'checked="checked"' : '';?> /> <strong>- Ensemble Valeur</strong></h4>
					<p class="list-group-item-text"></strong></p>
				</div>
				
				<div class="list-group-item <?=($input['chkvaluepack']=="3")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chkvaluepack" value="3" <?=($input['chkvaluepack']=="3")? 'checked="checked"' : '';?> /> <strong>- Aucun ensemble</strong></h4>
					<p class="list-group-item-text"> </p>
				</div>
			</div>
		</div>
	</div>
	
	<?php
	}
	
	if($input['chksecurity']<>''){
	?>
	<h3>Security order</h3>
	
	<div class="row">
	
		<div class="col-lg-12">
			<div class="list-group">
				<div class="list-group-item <?=($input['chksecurity']=="1")? 'active' : '';?>">
					
					<h4 class="list-group-item-heading">
					<input type="radio" name="chksecurity" value="1" <?=($input['chksecurity']=="1")? 'checked="checked"' : '';?> /> <strong>- Vitality pack for woman</strong></h4>
					<p class="list-group-item-text"></p>
				</div>
				
				<div class="list-group-item <?=($input['chksecurity']=="2")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chksecurity" value="2" <?=($input['chksecurity']=="2")? 'checked="checked"' : '';?> /> <strong>- Vitality pack for men</strong></h4>
					<p class="list-group-item-text"></strong></p>
				</div>
				
				<div class="list-group-item <?=($input['chksecurity']=="3")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chksecurity" value="3" <?=($input['chksecurity']=="3")? 'checked="checked"' : '';?> /> <strong>- No vitality pack</strong></h4>
					<p class="list-group-item-text"> </p>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
	<br/><br/>

<?php
}else{
?>


<div class="page-header">
	<h1>Profil du <span class="text-primary">client</span> <small><?=$client->name;?> ( <?=$client->mail;?> )</small></h1>
</div>


	<div class="row">
		
		<div class="col-lg-6">
			
		</div>
		
		<div class="col-lg-3">
		
		    <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Date de l'adhésion</h3>
                </div>
                <div class="panel-body text-center">
                    <?=(date("d m Y", strtotime("now")));?>
                </div>
                
                
                <div class="modal fade" id="chclientlook" tabindex="-1" role="dialog" aria-labelledby="chclientlook" aria-hidden="true">
    		    	<div class="modal-dialog">
    		    	    <form action="" method="post" enctype="multipart/form-data">
    			    	<div class="panel panel-danger">
    			    		<div class="panel-heading panel-heading-red text-center"><h3>Convertir le client en <strong>LOOK</strong></h3></div>
    			    		<div class="panel-body">
    			    		    Dès l'instant où le client sera LOOK, un courriel quotidien incluant l'horaire de vos survols lui sera envoyé. Êtes-vous certain de vouloir modifier ce statut ?
    						</div>
    						<div class="panel-footer text-right">
								<input type="hidden" name="clientid" value="<?=$client->id;?>"/>
								<input type="hidden" name="presenterid" value="<?=$client->presenterid;?>"/>
								<input type="hidden" name="status" value="0"/>
								<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
								<button type="submit" name="subupdatestatus" class="btn btn-danger">CONFIRMER!</button>
    						</div>
    			    	</div>
    			    	</form>
    		    	</div>
    		    </div>
    		    
            </div>
		    
		</div>
		
		
		<div class="col-lg-3">
            <div class="panel panel-danger">
                <div class="panel-heading panel-heading-red">
                    <h3 class="panel-title">Pas intéressé(e) ?</h3>
                </div>
                <div class="panel-body text-center">
                    <button data-toggle="modal" href="#delclient<?=$client->id;?>" type="button" class="btn btn-default">Supprimer le client</button>
                </div>
            </div>
            
            <div class="modal fade" id="delclient<?=$client->id;?>" tabindex="-1" role="dialog" aria-labelledby="delclient<?=$client->id;?>" aria-hidden="true">
		    	<div class="modal-dialog">
			    	<div class="panel panel-danger">
			    		<div class="panel-heading panel-heading-red text-center"><h3>Remove client <i><?=$client->name;?></i></h3></div>
			    		<div class="panel-body">
							 <strong><?=$client->name;?> - <?=$client->mail;?> is not interested and i want to remove this client.</strong>
						</div>
						<div class="panel-footer text-right">
							<form action="" method="post" enctype="multipart/form-data">
								<input type="hidden" name="inputid" value="<?=$client->id;?>"/>
								<input type="hidden" name="inputleaderid" value="<?=$cleader->code;?>"/>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" name="subdelclient" class="btn btn-danger">DELETE</button>
							</form>
						</div>
			    	</div>
		    	</div>
		    </div>
		</div>
		
		
		
	</div>

	<div class="row">
	
	    <div class="col-lg-4">
		    <br>
		    <form action="" method="post" role="form" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">SUIVI DU CLIENT</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="selfustatus">Statut</label>
                        <select name="fustatus" id="selfustatus" class="form-control">
                            <option <?=($client->status==0)?'selected=selected':'';?> value="0"></option>
                            <option <?=($client->status==1)?'selected=selected':'';?> value="1">Directeur</option>
                            <option <?=($client->status==2)?'selected=selected':'';?> value="2">Club 20/20</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputfunotes">Notes</label>
                        <textarea name="funotes" id="inputfunotes" class="form-control" rows="4"><?=$client->notes;?></textarea>
                    </div>
                    
                    <input type="hidden" name="fuProspectid" value="<?=$client->id;?>"/>
                    
                    <button type="submit" name="subfuupdate" class="btn btn-primary btn-block">Mise à jour</button>
                </div>
            </div>
            </form>
            
            
            <form action="" method="post" role="form" enctype="multipart/form-data">
            <div class="panel panel-danger">
                <div class="panel-heading panel-heading-red">
                    <h3 class="panel-title">Rappels et Alertes</h3>
                </div>
                <div class="panel-body text-center">
                    Bientôt disponible
                    <input type="hidden" name="fuProspectid" value="<?=$client->id;?>"/>
                </div>
            </div>
            </form>
            
            
    		
		</div>
		
		<div class="col-lg-8">
		    <div class="form-group">
				<label for="InputName">Nom du client</label><br>
				<input type="text" class="form-control" name="InputName" id="InputName" value="<?=$input['name'];?>" placeholder="" validate="required">
			</div>
			
			<div class="form-group">
				<label for="InputAssociate">Nom du conjoint(e) (S'il y a lieu)</label>
				<input type="text" class="form-control" name="InputAssociate" id="InputAssociate" value="<?=$input['associate'];?>" placeholder="">
			</div>
			
			<div class="form-group">
				<label for="inputmail">Courriel</label>
				<input type="text" class="form-control" name="InputMail" id="inputmail" value="<?=$input['mail'];?>" placeholder="" validate="email">
			</div>
			
			<div class="form-group">
				<label for="InputPhone">Téléphone</label>
				<input type="text" class="form-control" name="InputPhone" maxlength="10" id="InputPhone" value="<?=$input['phone'];?>" placeholder="">
			</div>
			
			<div class="form-group">
    			<label for="InputBirthDay">Date de naissance</label>
    			<div class="form-inline">
    				<div class="row">
    					<div class="col-lg-4"><input type="text" class="form-control" name="InputBirthday" id="InputBirthDay" value="<?=$input['birthday'];?>" placeholder="JJ" validate="required"></div>
    					<div class="col-lg-4">
    						<select class="form-control" name="InputBirthmonth" name="InputBirthmonth" validate="required">>
    							<option <?=($input['birthmonth']=='')?'selected=selected':'';?> value=""></option>
    							<option <?=($input['birthmonth']=='01')?'selected=selected':'';?> value="01">Janvier</option>
    							<option <?=($input['birthmonth']=='02')?'selected=selected':'';?> value="02">Février</option>
    							<option <?=($input['birthmonth']=='03')?'selected=selected':'';?> value="03">Mars</option>
    							<option <?=($input['birthmonth']=='04')?'selected=selected':'';?> value="04">Avril</option>
    							<option <?=($input['birthmonth']=='05')?'selected=selected':'';?> value="05">Mai</option>
    							<option <?=($input['birthmonth']=='06')?'selected=selected':'';?> value="06">Juin</option>
    							<option <?=($input['birthmonth']=='07')?'selected=selected':'';?> value="07">Juillet</option>
    							<option <?=($input['birthmonth']=='08')?'selected=selected':'';?> value="08">Août</option>
    							<option <?=($input['birthmonth']=='09')?'selected=selected':'';?> value="09">Septembre</option>
    							<option <?=($input['birthmonth']=='10')?'selected=selected':'';?> value="10">Octobre</option>
    							<option <?=($input['birthmonth']=='11')?'selected=selected':'';?> value="11">Novembre</option>
    							<option <?=($input['birthmonth']=='12')?'selected=selected':'';?> value="12">Décembre</option>
    						</select>
    					</div>
    					<div class="col-lg-4"><input type="text" class="form-control" name="InputBirthyear" id="InputBirthyear" value="<?=$input['birthyear'];?>" placeholder="YYYY" validate="required"></div>
    				</div>
    			</div>
			</div>
			
		</div>

	</div>
	
	<?php
	if($input['chkinterest']<>''){
	?>
	<h3>Intérêt pour l'entreprise</h3>
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-success">
				<div class="panel-heading"><label for="chkinterest1"><input type="radio" name="chkinterest" id="chkinterest1" value="1" <?=($input['chkinterest']=="1")? 'checked="checked"' : '';?> /> <strong> Client</strong></label></div>
				<div class="panel-body">
					Référez un ami de temps en temps
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel panel-warning">
				<div class="panel-heading"><label for="chkinterest2"><input type="radio" name="chkinterest" id="chkinterest2" value="2" <?=($input['chkinterest']=="2")? 'checked="checked"' : '';?> /> <strong> Client bâtisseur (Temps partiel)</strong></label></div>
				<div class="panel-body">
					Créer un revenu supplémentaire
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel panel-danger">
				<div class="panel-heading"><label for="chkinterest3"><input type="radio" name="chkinterest" id="chkinterest3" value="3" <?=($input['chkinterest']=="3")? 'checked="checked"' : '';?> /> <strong> Client bâtisseur (Temps Plein)</strong></label></div>
				<div class="panel-body">
					Remplacer mon revenu
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	
	
	if($input['chkvaluepack']<>''){
	?>
	
	<h3>Ensembles essentiels</h3>
	<div class="row">
	
		<div class="col-lg-12">
			<div class="list-group">
				<div class="list-group-item <?=($input['chkvaluepack']=="1")? 'active' : '';?>">
					
					<h4 class="list-group-item-heading">
					<input type="radio" name="chkvaluepack" value="1" <?=($input['chkvaluepack']=="1")? 'checked="checked"' : '';?> /> <strong>- Ensemble Essentiel</strong></h4>
					<p class="list-group-item-text">Comprend plus de 60 produits ainsi que des accessoires et des outils de développement d'entreprise. Économisez 47% sur le prix déjà réduit dont bénéficient les Clients privilégiés. Seulement offert aux nouveaux clients au cours des deux mois qui suivent leur adhésion.<br><strong>Seulement 299$</strong></p>
				</div>
				
				<div class="list-group-item <?=($input['chkvaluepack']=="2")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chkvaluepack" value="2" <?=($input['chkvaluepack']=="2")? 'checked="checked"' : '';?> /> <strong>- Ensemble Valeur</strong></h4>
					<p class="list-group-item-text">Économisez 49% sur le prix courant. 49% sur le prix déjà réduit dont bénéficient les Clients privilégiés. Comprend plus de 30 produits ainsi que des accessoires et des outils de développement d’entreprise. Seulement offert aux nouveaux clients au cours des deux mois qui suivent leur adhésion.<br><strong>Seulement 199$</strong></p>
				</div>
				
				<div class="list-group-item <?=($input['chkvaluepack']=="3")? 'active' : '';?>">
					<h4 class="list-group-item-heading"><input type="radio" name="chkvaluepack" value="3" <?=($input['chkvaluepack']=="3")? 'checked="checked"' : '';?> /> <strong>- Aucun ensemble</strong></h4>
					<p class="list-group-item-text">Vous devrez faire vous-même votre commande de 35 points. </p>
				</div>
			</div>
		</div>
	</div>
	
	<?php
	}
	
	if($input['chksecurity']<>''){
	?>
	<h3>Commande de sécurité</h3>
	
	<div class="row">
		<div class="col-lg-4">
			<div class="panel <?=($input['chksecurity']=="1")? 'panel-primary' : 'panel-default';?>">
				<div class="panel-heading"><label for="chksecurity1"><input type="radio" name="chksecurity" id="chksecurity1" value="1" <?=($input['chksecurity']=="1")? 'checked="checked"' : '';?> /> <strong> Ensemble vitalité Femme</strong></label></div>
				<div class="panel-body">
					Inclut l'ensemble vitalité pour femmes avec un choix variable de produits. 35pts | 69.49$
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel <?=($input['chksecurity']=="2")? 'panel-primary' : 'panel-default';?>">
				<div class="panel-heading"><label for="chksecurity2"><input type="radio" name="chksecurity" id="chksecurity2" value="2" <?=($input['chksecurity']=="2")? 'checked="checked"' : '';?> /> <strong> Ensemble vitalité Homme</strong></label></div>
				<div class="panel-body">
					Inclut l'ensemble vitalité pour hommes avec un choix variable de produits. 35pts | 69.49$
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel <?=($input['chksecurity']=="3")? 'panel-primary' : 'panel-default';?>">
				<div class="panel-heading"><label for="chksecurity3"><input type="radio" name="chksecurity" id="chksecurity3" value="3" <?=($input['chksecurity']=="3")? 'checked="checked"' : '';?> /> <strong> Pack produit sans vitalité</strong></label></div>
				<div class="panel-body">
					Pas d'ensemble vitalité. Un choix variable de produits. 35pts | 69.49$
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
	<br/><br/>
	<!--
	<div class="row">
		<div class="col-lg-2 text-left"><a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>" class="btn btn-lg col-lg-12 btn-danger" type="button">Retour</a></div>
		<div class="col-lg-8"></div>
		<div class="col-lg-2 text-right"><button type="submit" name="orderstep2" class="btn btn-lg col-lg-12 btn-primary">Sauvegarder</button></div>
	</div>
	-->
	
	
</form>
<?php
}
}
?>