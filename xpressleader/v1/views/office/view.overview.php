<?php
$lbltitle           = (_LANG=="fr") ? 'Mon <span class="text-primary">bureau</span>' : 'My <span class="text-primary">office</span>';

$stats = $prospect->getStats($cleader->leaderid, _LANG);
?>
<div class="container">
<div class="page-title">
	<?=$lbltitle;?>
</div>

<div id="pagewrap">
	<div class="row">
		<div class="col-lg-12">

			<div class="row">
			<div class="col-lg-5">
                <form action="" method="get" enctype="multipart/form-data">
                	<div class="form-group">
	                	<label for="key">Rechercher un prospect</label>
						<div class="input-group">
							<input type="text" name="key" id="key" class="form-control" placeholder="Name or Email address">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span> Rechercher</button>
							</span>
						</div>
                    </div>
                    <input type="hidden" name="page" value="1"/>
				</form>
			</div>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<ul class="nav nav-pills" role="tablist">
						<li role="presentation" class="active"><a href="#events" aria-controls="events" role="tab" data-toggle="pill">Tous</a></li>
						<li role="presentation"><a href="#evlook" aria-controls="evlook" role="tab" data-toggle="pill">Look <span class="badge"><?=$stats["interest"];?></span></a></li>
						<li role="presentation"><a href="#evmeeting" aria-controls="evmeeting" role="tab" data-toggle="pill">Meeting <span class="badge"><?=$stats["meeting"];?></span></a></li>
						<li role="presentation"><a href="#evinterest" aria-controls="evinterest" role="tab" data-toggle="pill">Intérêt <span class="badge"><?=$stats["order"];?></span></a></li>
					</ul>
					
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="events">
							<table class="table table-responsive table-striped">
								<?php 
								if(isset($_GET['key'])&&$_GET['key']<>""){
									$stats = $prospect->getList($cleader->leaderid, $_GET['key']);
									if($stats){
									foreach($stats as $xlook){
										if($xlook->status == 0) {
							  		        echo (_LANG=='fr') ? '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> veut en savoir plus.</td></tr>': '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> wants to know more.</td></tr>';
							  		    }elseif($xlook->status == 1) {
							  				$presname = '';
							  				$presname = ($xlook->presname<>'' && _LANG=='fr') ? 'par <strong>'.$xlook->presname.'</strong>' : $presname;
							  				$presname = ($xlook->presname<>'' && _LANG<>'fr') ? 'by <strong>'.$xlook->presname.'</strong>' : $presname;
							  				echo (_LANG=='fr') ? '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> a vu une présentation '.$presname.'</td></tr>': '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> viewed a business overview '.$presname.'</td></tr>';
							  				
							  			}elseif($xlook->status == 2) {
							  				echo (_LANG=='fr') ? '<tr><td class="text-center"><div class="status-order">INTÉRÊT</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> est intéressé par l\'opportunité!</td></tr>': '<tr><td class="text-center"><div class="status-order">INTERESTED</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> is interested by your opportunity!</td></tr></li>';
							  			}
									}
									}
								}else echo $stats["events"];
								?>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="evlook">
							<table class="table table-responsive table-striped">
								<?=$stats["evlooks"];?>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="evmeeting">
							<table class="table table-responsive table-striped">
								<?=$stats["evmeetings"];?>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="evinterest">
							<table class="table table-responsive table-striped">
								<?=$stats["evinterests"];?>
							</table>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
</div>
