<?php
$lbltitle      		= array("fr"=>'Gestion des <span class="text-primary">clients</span>',"en"=>'Manage my  <span class="text-primary">clients</span>');
$lblsrchclient    = array("fr"=>"Rechercher un client","en"=>"Find a client");
$lblkeysearch    	= array("fr"=>"Mots clés","en"=>"Key search");
$lblphkeysearch    	= array("fr"=>"Nom, courriel","en"=>"Name or email address");

$lblstatus    		= array("fr"=>"Statut","en"=>"Status");
$lblmeeting    		= array("fr"=>"Survol","en"=>"Meeting");
$lblmeetingdesc    	= array("fr"=>"Le client a join un meeting depuis votre calendrier.","en"=>"The client has join a meeting from your calendar.");
$lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
$lblinterestdesc    = array("fr"=>"Le client est intéressé à joindre votre entreprise.","en"=>"The client is interested to join your company!");
$lblnote    		= array("fr"=>"Notez que les status de vos clients sont à titre informatifs uniquement. Il n'y a aucune garantie qu'il sont juste.","en"=>"Note that the status of the clients are for informational purposes only. There is no guarantee that the information entered by them is accurate.");

$lblfollowup    	= array("fr"=>"Suivi","en"=>"Follow up");
$lblcalled    		= array("fr"=>"Appel","en"=>"Called");
$lblemailed    		= array("fr"=>"Courriel","en"=>"Emailed");
$lblwaiting    		= array("fr"=>"En attente","en"=>"Waiting for response");

$lblbtnsearch   	= array("fr"=>"RECHERCHER","en"=>"SEARCH");

$lblshowall    		= array("fr"=>"Afficher Tout","en"=>"Show All");

$lblname    	= array("fr"=>"Nom","en"=>"Name");
$lblmail    	= array("fr"=>"Courriel","en"=>"Email");
$lblphone    	= array("fr"=>"Téléphone","en"=>"Phone");

$page       = (isset($_GET["page"]) && $_GET["page"]<>"") ? $_GET["page"] : 1;
$key        = (isset($_GET["key"]) && $_GET["key"]<>"") ? $_GET["key"] : "";

$xlooklist 	= $client->_list($cleader->leaderid, $key);
$pages 		= ceil(count($xlooklist)/50);
if($page > $pages || $page < 1) $page = 1;
?>


<div class="container">
	<div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-tabs nav-justified nav-xpress" role="tablist">
                <li role="presentation"><a href="/<?=_LANG;?>/office/prospect">PROSPECT</a></li>
				<li role="presentation" class="active"><a href="/<?=_LANG;?>/office/client">CLIENT</a></li>
            </ul>
        </div>
    </div>
	<div class="whiteboard">
	<div class="page-title">
		<?=$lbltitle[_LANG];?>
	</div>
	
	<div id="pagewrap">
		
		
		
		<div class="row">
			<div class="col-lg-12">

                <form action="" method="get" enctype="multipart/form-data">
				    <div class="form-group">
				    	<label for="key"><?=$lblsrchclient[_LANG];?></label>
				    	<div class="input-group">
							<input type="text" class="form-control" name="key" id="key" value="" placeholder="<?=$lblphkeysearch[_LANG];?>">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i> <?=$lblbtnsearch[_LANG];?></button>
							</span>
						</div>
                    </div>
                    <input type="hidden" name="page" value="1"/>
				</form>
	 
			</div>
		</div>
		

		
		
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table class="table table-striped table-condensed">
						<tr>
							<th>Status</th>
							<th>Date</th>
							<th><?=$lblname[_LANG];?></th>
							<th><?=$lblmail[_LANG];?></th>
							<th><?=$lblphone[_LANG];?></th>
							<th></th>
						</tr>
						
						<?php
						
						if($xlooklist){
							foreach($xlooklist as $xlook){
								
								if($xlook->category==1) {
									$status = "<span class=\"label label-info\" style=\"width:100%;line-height:20px;display:block;\">CLIENT</span>";
								}elseif($xlook->category==2 || $xlook->category==3) {
									$status = "<span class=\"label label-warning\" style=\"width:100%;line-height:20px;display:block;\">BÂTISSEUR</span>";
								}else $status = "";
							
								echo '<tr>';
								echo '<td>'.$status.'</td>';
								echo '<td>'.$xlook->datecreation.'</td>';
								echo '<td><span class="label label-primary">'.mb_strtoupper($xlook->lang).'</span> '.$xlook->name.'</td>';
								echo '<td>'.$xlook->mail.'</td>';
								echo '<td>'.$xlook->phone.'</td>';
								echo '<td class="text-right">
								<a data-toggle="modal" href="#delprospect'.$xlook->id.'" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>';
								echo '</tr>';
								
								?>
							    
							    
							    <div class="modal fade" id="delprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="delprospect<?=$xlook->id;?>" aria-hidden="true">
							    	<div class="modal-dialog">
								    	<div class="panel panel-danger">
								    		<div class="panel-heading panel-heading-red text-center"><h3>Supprimer le client <i><?=$xlook->name;?></i></h3></div>
								    		<div class="panel-body">
												 <strong><?=$xlook->name;?> - <?=$xlook->mail;?></strong> n'est plus client et je veux le supprimer définitivement.
											</div>
											<div class="panel-footer text-right">
												<form action="" method="post" enctype="multipart/form-data">
													<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
													<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
								    			    <input type="hidden" name="validate" value="true"/>
										            <input type="hidden" name="subform" value="removeprospect"/>
													
													<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
													<button type="submit" name="subdelprospect" class="btn btn-danger">SUPPRIMER</button>
												</form>
											</div>
								    	</div>
							    	</div>
							    </div>
							    <?php
							}
						}else{
						    if(isset($_GET["search"])){
						        
						        echo '<tr>';
			    				echo '<td colspan="6" class="text-center">Aucun client n\'a été trouvé, ajustez votre recherche.</td>';
			    				echo '</tr>';
			    				
						    }else{
			    				echo '<tr>';
			    				echo '<td colspan="6" class="text-center">Vous n\'avez actuellement aucun clients.</td>';
			    				echo '</tr>';
						    }
						}
						?>
					</table>
					</div>
			</div>
		</div>

	</div>
	</div>
</div>