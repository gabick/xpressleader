<?php
$lbltitle      		= array("fr"=>'Mon bureau',"en"=>'My office');
$lblloading    		= array("fr"=>"Recherche de contacts...","en"=>"Searching for contacts...");
$lblgetloading    	= array("fr"=>"Chargement...","en"=>"Loading...");

$lblsrchprospect    = array("fr"=>"Rechercher un prospect","en"=>"Find a contact");
$lblkeysearch    	= array("fr"=>"Mots clés","en"=>"Key search");
$lblphkeysearch    	= array("fr"=>"Nom, courriel","en"=>"Name or email address");

$lblnewprospect    	= array("fr"=>"Nouveau contact","en"=>"Add contact");
$lblrefresh    		= array("fr"=>"Rafraîchir","en"=>"Refresh");

$lblall    			= array("fr"=>"Tous","en"=>"All");
$lblarchived    	= array("fr"=>"Archive","en"=>"Archived");
$lblstatus    		= array("fr"=>"Statut","en"=>"Status");
$lblmeeting    		= array("fr"=>"Survol","en"=>"Meeting");
$lblmeetingdesc    	= array("fr"=>"Le prospect a regardé le test.","en"=>"The prospect watched the test.");
$lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
$lblpending   		= array("fr"=>"À contacter","en"=>"To contact");
$lblinvited   		= array("fr"=>"Invité(e)","en"=>"Invited");
$lblexpired   		= array("fr"=>"Invitation expirée","en"=>"Expired invite");
$lblclient    		= array("fr"=>"Client","en"=>"Client");
$lblbuilder    		= array("fr"=>"Bâtisseur","en"=>"Builder");
$lblpendingdesc    	= array("fr"=>"Une invitation a été envoyé au prospect.","en"=>"An invitation has been sent to the prospect.");
$lblinterestdesc    = array("fr"=>"Le prospect est intéressé à voir plus d'infos.","en"=>"The prospect is interested in seeing more infos!");
$lblnote    		= array("fr"=>"Notez que les statuts de vos prospects sont à titre informatifs uniquement. Il n'y a aucune garantie qu'ils soient justes ou précis.","en"=>"Note that the status of the prospects are for informational purposes only. There is no guarantee that the information entered by them is accurate.");

$lblfollowup    	= array("fr"=>"Suivi","en"=>"Follow up");
$lblcalled    		= array("fr"=>"Appel","en"=>"Called");
$lblemailed    		= array("fr"=>"Courriel","en"=>"Emailed");
$lblwaiting    		= array("fr"=>"En attente","en"=>"Waiting for response");

$lblbtnsearch   	= array("fr"=>"RECHERCHER","en"=>"SEARCH");

$lblshowall    		= array("fr"=>"Afficher Tout","en"=>"Show All");

$lblname    		= array("fr"=>"Nom","en"=>"Name");
$lblmail    		= array("fr"=>"Courriel","en"=>"Email");
$lblphone    		= array("fr"=>"Téléphone","en"=>"Phone");
?>


<div class="container">
	<div style="background-color: rgba(0,0,0,0.6);width:100%;padding:10px;">
	<div class="row">
	    <div class="col-md-12 text-left">
	    </div>
	</div>
	<div class="row">
	    <div class="col-md-3">
			
	    </div>
	    <div class="col-md-9 text-left">
	    	<div class="row">
	    		<div class="col-lg-9">
	    			<div class="form-group">
		    			<div class="input-group">
					    	<input type="text" class="form-control" name="key" id="key" value="" placeholder="<?=$lblphkeysearch[_LANG];?>">
							<span class="input-group-btn">
								<button class="btn btn-xp-black" style="min-width: 90px;" type="button" id="srchprospects"><i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
						</div>
					</div>
	    		</div>
	    		<div class="col-lg-3 text-right">
	    			<button class="btn btn-success" type="button" id="addContact" data-lang="<?=_LANG;?>" data-leaderid="<?=$cleader->leaderid;?>" data-token="<?=sha1($cleader->leaderid.'g3Tl1stPr0sp3cts');?>"><i class="fa fa-plus" aria-hidden="true"></i> <?=$lblnewprospect[_LANG];?></button>
	    		</div>
	    	</div>
	    </div>
	</div>
	</div>
</div>



<div class="container">
	<div class="whiteboard" style="padding:0 15px;">

		<div class="row">
			<div class="col-lg-3 nopadding" style="border-right: solid 1px #ccc;">
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" class="<?=(_ID==""||_ID=="all")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/all"><span class="label" style="width:45px;line-height: 25px;border-radius: 2px;color:#333333;background-color: #ffffff;border:solid 1px #333333;display: inline-block;font-size:1.1em;"><?=$contact->statusCount($cleader->leaderid, "all");?></span> <?=$lblall[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="archived")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/archived"><span class="label" style="width:45px;line-height: 25px;border-radius: 2px;color:#333333;background-color: #f5f5f5;display: inline-block;font-size:1.1em;">-</span> <?=$lblarchived[_LANG];?></a></li>
				</ul>
				<h4 style="width: 100%;background-color: #eeeeee;line-height: 42px;margin:0;padding-left:10px;">Prospects</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" class="<?=(_ID=="pending")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/pending"><span class="label label-default" style="width:45px;line-height: 30px;border-radius: 2px;display: inline-block;"><?=$contact->statusCount($cleader->leaderid, "pending");?></span> <?=$lblpending[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="invited")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/invited"><span class="label label-warning" style="width:45px;line-height: 30px;border-radius: 2px;display: inline-block;"><?=$contact->statusCount($cleader->leaderid, "invited");?></span> <?=$lblinvited[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="expired")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/expired"><span class="label label-danger" style="width:45px;line-height: 30px;border-radius: 2px;display: inline-block;"><?=$contact->statusCount($cleader->leaderid, "expired");?></span> <?=$lblexpired[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="meeting")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/meeting"><span class="label label-primary" style="width:45px;line-height: 30px;border-radius: 2px;display: inline-block;"><?=$contact->statusCount($cleader->leaderid, "meeting");?></span> <?=$lblmeeting[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="interest")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/interest"><span class="label label-success" style="width:45px;line-height: 30px;border-radius: 2px;display: inline-block;"><?=$contact->statusCount($cleader->leaderid, "interest");?></span> <?=$lblinterest[_LANG];?></a></li>
				</ul>
				<h4 style="width: 100%;background-color: #eeeeee;line-height: 42px;margin:0;padding-left:10px;">Clients</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" class="<?=(_ID=="builder")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/builder"><span class="label" style="width:45px;line-height: 25px;border-radius: 2px;color:#e22c2c;background-color: #ffffff;border:solid 2px #e22c2c;display: inline-block;font-size:1.1em;"><?=$contact->statusCount($cleader->leaderid, "builder");?></span> <?=$lblbuilder[_LANG];?></a></li>
					<li role="presentation" class="<?=(_ID=="client")?"active":"";?>"><a href="/<?=_LANG;?>/office/contacts/client"><span class="label" style="width:45px;line-height: 25px;border-radius: 2px;color:#7aa668;background-color: #ffffff;border:solid 2px #7aa668;display: inline-block;font-size:1.1em;"><?=$contact->statusCount($cleader->leaderid, "client");?></span> <?=$lblclient[_LANG];?></a></li>
				</ul>
			</div>
			<div class="col-lg-9">
				
				<div class="row">
					<div class="col-md-12 nopadding">
						
						<div id="prospects-box">
							<input type="hidden" id="leaderid" value="<?=$cleader->leaderid;?>"/>
							<input type="hidden" id="token" value="<?=(sha1($cleader->leaderid.'g3Tl1stPr0sp3cts'));?>"/>
							<input type="hidden" id="lang" value="<?=_LANG;?>"/>
							<input type="hidden" id="page" value="1"/>
							<input type="hidden" id="perpage" value="<?=$cleader->ctcperpage;?>"/>
							<input type="hidden" id="orderby" value="name"/>
							<input type="hidden" id="sort" value="ASC"/>
							<input type="hidden" id="statusid" value="<?=_ID;?>"/>
							<input type="hidden" id="ctcid" value="<?=(isset($_GET['ctcid']))? $_GET['ctcid']:'';?>"/>
							<input type="hidden" id="ctctoken" value="<?=(isset($_GET['ctcid']))? sha1($cleader->leaderid.$_GET['ctcid'].'g3Tl1stPr0sp3cts'):'';?>"/>
						</div>
						<div class="table-responsive">
							<div id="prosloader" class="loading-block"><span style=""><i class="fa fa-spin fa-spinner"></i> <?=$lblgetloading[_LANG];?></span></div>
							<div id="proslist" class="text-center"></div>
						</div>
						<div id="prosget" class="text-center"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>