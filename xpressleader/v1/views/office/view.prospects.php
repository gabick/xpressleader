<?php
$lbltitle      		= array("fr"=>'Gestion des <span class="text-primary">prospects</span>',"en"=>'Manage my  <span class="text-primary">prospects</span>');
$lblsrchprospect    = array("fr"=>"Rechercher un prospect","en"=>"Find a prospect");
$lblkeysearch    	= array("fr"=>"Mots clés","en"=>"Key search");
$lblphkeysearch    	= array("fr"=>"Nom, courriel","en"=>"Name or email address");

$lblstatus    		= array("fr"=>"Statut","en"=>"Status");
$lblmeeting    		= array("fr"=>"Test","en"=>"Meeting");
$lblmeetingdesc    	= array("fr"=>"Le prospect a regardé le test.","en"=>"The prospect watched the test.");
$lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
$lblpending   		= array("fr"=>"En attente","en"=>"Pending");
$lblexpired   		= array("fr"=>"Lien Expiré","en"=>"Link Expired");
$lblpendingdesc    	= array("fr"=>"Une invitation a été envoyé au prospect.","en"=>"An invitation has been sent to the prospect.");
$lblinterestdesc    = array("fr"=>"Le prospect est intéressé à voir plus d'infos.","en"=>"The prospect is interested in seeing more infos.");
$lblnote    		= array("fr"=>"Notez que les statuts de vos prospects sont à titre informatifs uniquement. Il n'y a aucune garantie qu'ils soient justes ou précis.","en"=>"Note that the status of the prospects are for informational purposes only. There is no guarantee that the information entered by them is accurate.");

$lblfollowup    	= array("fr"=>"Suivi","en"=>"Follow up");
$lblcalled    		= array("fr"=>"Appel","en"=>"Called");
$lblemailed    		= array("fr"=>"Courriel","en"=>"Emailed");
$lblwaiting    		= array("fr"=>"En attente","en"=>"Waiting for response");

$lblbtnsearch   	= array("fr"=>"RECHERCHER","en"=>"SEARCH");

$lblshowall    		= array("fr"=>"Afficher Tout","en"=>"Show All");

$lblname    		= array("fr"=>"Nom","en"=>"Name");
$lblmail    		= array("fr"=>"Courriel","en"=>"Email");
$lblphone    		= array("fr"=>"Téléphone","en"=>"Phone");

$page       = (isset($_GET["page"]) && $_GET["page"]<>"") ? $_GET["page"] : 1;
$key        = (isset($_GET["key"]) && $_GET["key"]<>"") ? $_GET["key"] : "";
$status     = (isset($_GET["status"]) && $_GET["status"]<>"") ? $_GET["status"] : -1;

$xlooklist 	= $prospect->_list($cleader->leaderid, 0, $key, $status);
$pages 		= ceil(count($xlooklist)/50);
if($page > $pages || $page < 1) $page = 1;
?>
<div class="container">
	<div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-tabs nav-justified nav-xpress" role="tablist">
                <li role="presentation" class="active"><a href="/<?=_LANG;?>/office/prospect">PROSPECT</a></li>
				<li role="presentation"><a href="/<?=_LANG;?>/office/client">CLIENT</a></li>
            </ul>
        </div>
    </div>
	<div class="whiteboard">
		
		<div class="page-title">
			<?=$lbltitle[_LANG];?>
		</div>
	
	<div id="pagewrap">
		<div class="row">
			<div class="col-md-12">
				<p><?=$lblnote[_LANG];?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<form action="" method="get" enctype="multipart/form-data">
                    <div class="row">
			            <div class="col-lg-12">
			                <div class="form-group">
			                	<label for="key"><?=$lblkeysearch[_LANG];?></label>
                        		<input type="text" class="form-control" name="key" id="key" value="" placeholder="<?=$lblphkeysearch[_LANG];?>">
                            </div>
			            </div>
			            <div class="col-lg-6">
			                <div class="form-group">
                                <label for="status"><?=$lblstatus[_LANG];?></label>
                                <select name="status" id="status" class="form-control">
                                    <option value=""></option>
                                    <option value="0"><?=$lblpending[_LANG];?></option>
                                    <option value="1"><?=$lblmeeting[_LANG];?></option>
                                    <option value="2"><?=$lblinterest[_LANG];?></option>
                                </select>
                            </div>
			            </div>
			            <div class="col-lg-6">
			                <div class="form-group">
                                <input type="hidden" name="page" value="1"/>
                                <label>&nbsp;</label>
                				<button type="submit" name="search" class="btn btn-default btn-block"><i class="fa fa-search" aria-hidden="true"></i> <?=$lblbtnsearch[_LANG];?></button>
                            </div>
			            </div>
                    </div>
				</form>
				
			</div>
			<div class="col-md-5">
				<table class="table table-striped">
					<tr>
						<td><div class="status-pending text-left" style="display:block;width: 100px;"><?="<span class=\"label label-default\" style=\"width:100%;line-height:20px;display:block;\">".mb_strtoupper($lblpending[_LANG])."</span>";?></div></td>
						<td style="font-size: 0.9em;"><?=$lblpendingdesc[_LANG];?></td>
					</tr>
					<tr>
						<td><div class="status-meeting text-left" style="display:block;width: 100px;"><?="<span class=\"label label-primary\" style=\"width:100%;line-height:20px;display:block;\">".mb_strtoupper($lblmeeting[_LANG])."</span>";?></div></td>
						<td style="font-size: 0.9em;"><?=$lblmeetingdesc[_LANG];?></td>
					</tr>
					<tr>
						<td><div class="status-interest text-left" style="display:block;width: 100px;"><?="<span class=\"label label-success\" style=\"width:100%;line-height:20px;display:block;\">".mb_strtoupper($lblinterest[_LANG])."</span>";;?></div></td>
						<td style="font-size: 0.9em;"><?=$lblinterestdesc[_LANG];?></td>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<hr>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2"><a href="/<?=_LANG;?>/office/prospect" class="btn btn-default btn-block btn-sm"><?=$lblshowall[_LANG];?></a></div>
			<div class="col-md-7">
			    <ul class="pagination pagination-sm" style="margin:0;">
		            <?php
		            $disabled = ($page<=1) ? 'disabled' : '';
		            echo '<li class="'.$disabled.'"><a href="?key='.$key.'&status='.$status.'&page='.($page-1).'">&laquo;</a></li>';
		            $i=1;
		            while ($i <= $pages) {
		                $active = ($i==$page) ? 'active' : '';
		                echo '<li class="'.$active.'"><a href="?key='.$key.'&status='.$status.'&page='.$i.'">'.$i.'</a></li>';
		                $i++;
		            }
		            $disabled = ($page>=$pages) ? 'disabled' : '';
		            echo '<li class="'.$disabled.'"><a href="?key='.$key.'&status='.$status.'&page='.($page+1).'">&raquo;</a></li>';
		            ?>
		        </ul>
		        
			    
			</div>
			<div class="col-md-3 text-right">
			    <a data-toggle="modal" href="#addprospect"class="btn btn-danger"><i class="fa fa-plus-square" aria-hidden="true"></i> NOUVEAU PROSPECT</a>  
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table class="table table-striped table-condensed">
						<tr>
							<th><?=$lblstatus[_LANG];?></th>
							<th>Date</th>
							<th><?=$lblname[_LANG];?></th>
							<th><?=$lblmail[_LANG];?></th>
							<th><?=$lblphone[_LANG];?></th>
							<th></th>
						</tr>
						
						<?php
						if($xlooklist){
							foreach($xlooklist as $xlook){
								$expired = false;
								
								$iswatching = ($xlook->dtwatch != null && ((strtotime($xlook->dtwatch)+10) >= (time()))) ? '<i class="fa fa-eye" aria-hidden="true"></i>' : '';
								
								$progress = ($xlook->position > 0) ? "(".number_format(($xlook->position / 1200)*100,0)."%)" : "";
								$progress = ($xlook->position >= 1200) ? "(100%)" : $progress;
								if($xlook->status==1) {
									$status = "<span class=\"label label-primary\" style=\"width:100%;display:block;line-height:20px;\">$iswatching ".mb_strtoupper($lblmeeting[_LANG])." $progress</span>";
								}elseif($xlook->status==0) {
									if(strtotime($xlook->expire) < time()) {
										$status 	= "<span class=\"label label-danger\" style=\"width:100%;line-height:20px;display:block;\">".mb_strtoupper($lblexpired[_LANG])."</span>";
										$expired 	= true;
									}else  $status = "<span class=\"label label-default\" style=\"width:100%;display:block;display:block;line-height:20px;\">".mb_strtoupper($lblpending[_LANG])."</span>";
								}elseif($xlook->status==2) {
									$status = "<span class=\"label label-success\" style=\"width:100%;display:block;display:block;line-height:20px;\">".mb_strtoupper($lblinterest[_LANG])." $progress</span>";
								}
								
								echo '<tr>';
								echo '<td class="text-left"><div class=" text-left">'.$status.'</div></td>';
								echo '<td>'.$xlook->dateupdate.'</td>';
								echo '<td><span class="label label-primary">'.mb_strtoupper($xlook->lang).'</span> '.$xlook->name.'</td>';
								echo '<td>'.$xlook->mail.'</td>';
								echo '<td>'.$xlook->phone.'</td>';
								
								$btnInvite  = ($expired) ? '<a data-toggle="modal" href="#inviteprospect'.$xlook->id.'"class="btn btn-sm btn-default"><i class="fa fa-envelope" aria-hidden="true"></i> RÉINVITER</a>' : '';
								$btnConvert = ($xlook->status>=1) ? '<a data-toggle="modal" href="#convertprospect'.$xlook->id.'"class="btn btn-sm btn-danger"><i class="fa fa-check-square" aria-hidden="true"></i> CONVERTIR</a>' : '';
								echo '<td class="text-right">'.$btnInvite.' '.$btnConvert.'
								<a data-toggle="modal" href="#delprospect'.$xlook->id.'" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove text-danger"></span></a></td>';
								echo '</tr>';
								?>
								
								
								<div class="modal fade" id="inviteprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="inviteprospect<?=$xlook->id;?>" aria-hidden="true">
							    	<div class="modal-dialog">
							    	    <form action="" method="post" role="form" enctype="multipart/form-data">
								    	<div class="panel panel-danger panel-xpress-danger">
								    		<div class="panel-heading panel-heading-red"><h4>Réenvoyer un courriel d'invitation à <strong><?=$xlook->name;?></strong></h4></div>
								    		<div class="panel-body">
												<p>
													Votre prospect n'a toujours pas été faire le test et son lien est expiré. Vous pouvez l'inviter de nouveau. ATTENTION ! Il est important de contacter votre prospect avant de lui renvoyer une nouvelle invitation pour l'aviser que le lien n'est actif que pour 24h
												</p>
											</div>
											<div class="panel-footer text-right">
												<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
												<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
												<input type="hidden" name="key" value="<?=$xlook->token;?>"/>
							    			    <input type="hidden" name="validate" value="true"/>
									            <input type="hidden" name="subform" value="inviteprospect"/>
												<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                    <button type="submit" name="subproinvite" class="btn btn-danger">ENVOYER !</button>
											</div>
								    	</div>
								    	</form>
							    	</div>
							    </div>
							    
							    
								
								<div class="modal fade" id="convertprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="convertprospect<?=$xlook->id;?>" aria-hidden="true">
							    	<div class="modal-dialog">
							    	    <form action="" method="post" role="form" enctype="multipart/form-data">
								    	<div class="panel panel-primary">
								    		<div class="panel-heading"><h4>Désirez-vous convertir <strong><?=$xlook->name;?></strong> en client ?</h4></div>
								    		<div class="panel-body">
												<div class="list-group" style="font-size: 0.9em;">
													<p>
														Votre prospect est maintenant client ? Vous devez le convertir en client afin qu'il puisse recevoir un courriel avec le cadeau de bienvenue de votre part (livre de recettes du mieux-être).
													</p>
													
													<?php 
													if($xlook->lang == null){
													?>
													<div class="form-group">
									            		<label for="txtproslang">Language préférée</label>
									            		<select id="txtproslang" name="txtproslang" class="form-control">
									            			<option value="fr">Français</option>
									            			<option value="en">Anglais</option>
									            		</select>
										            </div>
										            <?php
													}else{
													?>
													<input type="hidden" id="txtproslang" name="txtproslang" class="form-control" value="<?=$xlook->lang;?>" placeholder="" required="true">
													<?php
													}
													?>
		            								
		            
													<div class="list-group-item">
														<h4 class="list-group-item-heading">
															<div class="checkbox">
																<label><input type="radio" name="chkinterest" value="1" /> <strong>Client</strong></label>
															</div>
														</h4>
														<p class="list-group-item-text">S'intéresse surtout aux produits et référe un ami de temps en temps</p>
													</div>
												
													<div class="list-group-item">
														<h4 class="list-group-item-heading">
															<div class="checkbox">
																<label><input type="radio" name="chkinterest" value="2" /> <strong>Client bâtisseur (Temps partiel)</strong></label>
															</div>
														</h4>
														<p class="list-group-item-text">Créer un revenu supplémentaire, s'investir à temps partiel.</p>
													</div>
												
													<div class="list-group-item">
														<h4 class="list-group-item-heading">
															<div class="checkbox">
																<label><input type="radio" name="chkinterest" value="3" checked="checked" /> <strong>Client bâtisseur (Temps Plein)</strong></label>
															</div>
														</h4>
														<p class="list-group-item-text">Créer un revenu supplémentaire, s'investir à temps plein. </p>
													</div>
												</div>
											</div>
											<div class="panel-footer text-right">
												<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
												<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
							    			    <input type="hidden" name="validate" value="true"/>
									            <input type="hidden" name="subform" value="convertprospect"/>
												<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			                                    <button type="submit" name="subfuupdate" class="btn btn-danger">Convertir !</button>
											</div>
								    	</div>
								    	</form>
							    	</div>
							    </div>
							    
							    
							    <div class="modal fade" id="delprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="delprospect<?=$xlook->id;?>" aria-hidden="true">
							    	<div class="modal-dialog">
								    	<div class="panel panel-danger">
								    		<div class="panel-heading panel-heading-red text-center"><h3>Supprimer le prospect <i><?=$xlook->name;?></i></h3></div>
								    		<div class="panel-body">
												 <strong><?=$xlook->name;?> - <?=$xlook->mail;?></strong> n'est pas intéressé et je veux supprimer ce prospect définitivement.
											</div>
											<div class="panel-footer text-right">
												<form action="" method="post" enctype="multipart/form-data">
													<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
													<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
								    			    <input type="hidden" name="validate" value="true"/>
										            <input type="hidden" name="subform" value="removeprospect"/>
													
													<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
													<button type="submit" name="subdelprospect" class="btn btn-danger">SUPPRIMER</button>
												</form>
											</div>
								    	</div>
							    	</div>
							    </div>
						<?php
							}
						}else{
						    if(isset($_GET["search"])){
						        echo '<tr>';
			    				echo '<td colspan="6" class="text-center">Aucun prospect n\'a été trouvé, ajustez votre recherche.</td>';
			    				echo '</tr>';
						    }else{
			    				echo '<tr>';
			    				echo '<td colspan="6" class="text-center">Vous n\'avez actuellement aucun prospect.</td>';
			    				echo '</tr>';
						    }
						}
						?>
					</table>
				</div>
			</div>
		</div>



















</div>
</div>
</div>




<div class="modal fade" id="addprospect" tabindex="-1" role="dialog" aria-labelledby="addprospect" aria-hidden="true">
	<div class="modal-dialog">
	    <form action="" method="post" role="form" enctype="multipart/form-data">
    	<div class="panel panel-danger panel-xpress-danger">
    		<div class="panel-heading panel-heading-red"><h4>Ajouter un nouveau prospect</h4></div>
    		<div class="panel-body">
				<p>
					Tous les nouveaux prospects recevront par courriel un lien unique afin de visualiser DIRECTEMENT le vidéo de survol de l'entreprise. Ce lien sera valide pour une durée de 24h.
				</p>
				
				<p>
					Pour de meilleurs résultats, communiquez avec votre prospect AVANT d'envoyer l'invitation. 
				</p>
				
	            	<div class="form-group">
	            		<label for="txtprosname">Nom du prospect</label>
		                <input type="text" id="txtprosname" name="txtprosname" class="form-control" placeholder="" required="true">
		            </div>
		            <div class="form-group">
	            		<label for="txtprosmail">Adresse Courriel</label>
		                <input type="email" id="txtprosmail" name="txtprosmail" class="form-control" placeholder="" required="true">
		            </div>
		            
		            

			</div>
			<div class="panel-footer text-right">
				<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
			    <input type="hidden" name="validate" value="true"/>
	            <input type="hidden" name="subform" value="addprospect"/>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
	            <button class="btn btn-danger" type="submit">Créer mon prospect</button>
			</div>
    	</div>
    	</form>
	</div>
</div>