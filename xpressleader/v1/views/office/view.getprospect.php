<?php 
$lblstatus    		= array("fr"=>"Statut","en"=>"Status");
$lblmeeting    		= array("fr"=>"Survol","en"=>"Meeting");
$lblmeetingdesc    	= array("fr"=>"Le prospect a join un meeting depuis votre calendrier.","en"=>"The prospect has join a meeting from your calendar.");
$lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
$lblpending   		= array("fr"=>"En attente","en"=>"Pending");
$lblpendingdesc    	= array("fr"=>"Une invitation a été envoyé au prospect.","en"=>"An invitation has been sent to the prospect.");
$lblinterestdesc    = array("fr"=>"Le prospect est intéressé à joindre votre entreprise.","en"=>"The prospect is interested to join your company!");
$lblnote    		= array("fr"=>"Notez que les status de vos prospects sont à titre informatifs uniquement. Il n'y a aucune garantie qu'il sont juste.","en"=>"Note that the status of the prospects are for informational purposes only. There is no guarantee that the information entered by them is accurate.");

$curprospect = $prospect->get($cleader->leaderid,_ID);
?>
<div class="container">
	<div class="whiteboard">

<?php
if(!$curprospect){
	if(_LANG=="en"){
?>
		<div class="row">
			<div class="col-lg-12 text-center">
				<br/><br/><br/>
				<h3>We cannot find this prospect, please try again</h3>
				<a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>" class="btn btn-primary">Back to my prospects list</a>
				<br/><br/><br/>
			</div>
		</div>
<?php
	}else{
?>

	<div class="row">
			<div class="col-lg-12 text-center">
				<br/><br/><br/>
				<h3>Aucun prospect n'a été trouvé, s'il vous plaît, réessayez de nouveau.</h3>
				<a href="/<?=_LANG;?>/<?=_CONTROLLER;?>/<?=_VIEW;?>" class="btn btn-primary">Retour à la liste des prospects</a>
				<br/><br/><br/>
			</div>
		</div>

<?php
	}
}else{
		
$input['name'] 			= (isset($curprospect->name)) ? $curprospect->name : '';
$input['associate'] 	= (isset($curprospect->associate)) ? $curprospect->associate : '';
$input['phone'] 		= (isset($curprospect->phone)) ? $curprospect->phone : '';
$input['mail'] 			= (isset($curprospect->mail)) ? $curprospect->mail : '';
$input['birthday'] 		= (isset($curprospect->bday)) ? $curprospect->bday : '';
$input['birthmonth'] 	= (isset($curprospect->bmonth)) ? $curprospect->bmonth : '';
$input['birthyear'] 	= (isset($curprospect->byear)) ? $curprospect->byear : '';
$input['chkinterest'] 	= (isset($curprospect->interest)) ? $curprospect->interest : '';
$input['chkvaluepack'] 	= (isset($curprospect->valuepack)) ? $curprospect->valuepack : '';
$input['chksecurity'] 	= (isset($curprospect->security)) ? $curprospect->security : '';
$input['comment'] 		= (isset($curprospect->message)) ? $curprospect->message : '';
?>


	<div class="page-title">
		<?php 
		if(_LANG=="fr"){
			echo "Prospect <span class=\"text-primary\">{$input['name']}</span>";
		}else{
			echo '<span class="text-primary">Prospect</span> informations';
		}
		?>
	</div>


	<div class="row">
		
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Informations sur le prospect</h3>
                </div>
                <div class="panel-body">
                	<div class="row">
                		<div class="col-md-6">
                			<table class="table table-condensed">
                				<tr>
									<td valign="top">Statut</td>
									<td style="background-color: #eee;">
										<?php
										if($curprospect->status==1) {
											$status = "<span class=\"label label-primary\" style=\"width:100%;\">".mb_strtoupper($lblmeeting[_LANG])."</span>";
											$classlang = 'status-meeting';
										}elseif($curprospect->status==0) {
											$status = "<span class=\"label label-default\" style=\"width:100%;\">".mb_strtoupper($lblpending[_LANG])."</span>";
											$classlang = 'status-pending';
										}elseif($curprospect->status==2) {
											$status = "<span class=\"label label-success\" style=\"width:100%;\">".mb_strtoupper($lblinterest[_LANG])."</span>";
											$classlang = 'status-interest';
										}
										echo $status;
										?>
									</td>
								</tr>
								<tr>
									<td valign="top">Langue préférée</td>
									<td style="background-color: #eee;">
										<?php
										if($curprospect->lang=='fr') echo 'Français'; else echo 'Anglais';
										?>
									</td>
								</tr>
								<tr>
									<td valign="top">Dernière mise à jour</td>
									<td style="background-color: #eee;">
										<?=$curprospect->dateupdate;?>
									</td>
								</tr>
							</table>
                		</div>
                		
                		<div class="col-md-3 text-right">
                			<button data-toggle="modal" href="#convertprospect<?=$curprospect->id;?>" type="button" class="btn btn-success btn-block btn-lg">Adhésion</button>
                		</div>
                		
                		<div class="col-md-3 text-right">
                			<button data-toggle="modal" href="#delprospect<?=$curprospect->id;?>" type="button" class="btn btn-danger btn-block btn-lg">Pas intéressé</button><br/>
                		</div>
                	</div>
					
				</div>
            </div>
		</div>
		
		
            
            <div class="modal fade" id="delprospect<?=$curprospect->id;?>" tabindex="-1" role="dialog" aria-labelledby="delprospect<?=$curprospect->id;?>" aria-hidden="true">
		    	<div class="modal-dialog">
			    	<div class="panel panel-danger">
			    		<div class="panel-heading panel-heading-red text-center"><h3>Remove prospect <i><?=$curprospect->name;?></i></h3></div>
			    		<div class="panel-body">
							 <strong><?=$curprospect->name;?> - <?=$curprospect->mail;?></strong> n'est pas intéressé et je veux supprimer ce prospect définitivement.
						</div>
						<div class="panel-footer text-right">
							<form action="/<?=_LANG;?>/office/prospect" method="post" enctype="multipart/form-data">
								<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
								<input type="hidden" name="prospectid" value="<?=$curprospect->id;?>"/>
			    			    <input type="hidden" name="validate" value="true"/>
					            <input type="hidden" name="subform" value="removeprospect"/>
								
								<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
								<button type="submit" name="subdelprospect" class="btn btn-danger">SUPPRIMER</button>
							</form>
						</div>
			    	</div>
		    	</div>
		    </div>
	</div>
	
	
	

	<div class="row">
	
	    
		
		<div class="col-lg-8">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="InputName">Nom du prospect</label><br>
						<input type="text" class="form-control" name="InputName" id="InputName" value="<?=$input['name'];?>" placeholder="" validate="required">
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label for="InputAssociate">Nom du conjoint(e) (S'il y a lieu)</label>
						<input type="text" class="form-control" name="InputAssociate" id="InputAssociate" value="<?=$input['associate'];?>" placeholder="">
					</div>
				</div>
			</div>
		    
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="inputmail">Courriel</label>
						<input type="text" class="form-control" name="InputMail" id="inputmail" value="<?=$input['mail'];?>" placeholder="" validate="email">
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="InputPhone">Téléphone</label>
						<input type="text" class="form-control" name="InputPhone" maxlength="10" id="InputPhone" value="<?=$input['phone'];?>" placeholder="">
					</div>
				</div>
			</div>
			
			
			<button type="submit" name="subfuupdate" class="btn btn-primary btn-block">Sauvegarder</button>
			
			
		</div>
		
		
		<div class="col-lg-4">
		    <form action="" method="post" role="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="inputfunotes">Notes</label>
                    <textarea name="funotes" id="inputfunotes" class="form-control" rows="9"><?=$curprospect->notes;?></textarea>
                </div>
                
                <input type="hidden" name="fuProspectid" value="<?=$curprospect->id;?>"/>
                
                
            </form>
            
            
    		
		</div>

	</div>
	
	
</form>
<?php
}
?>
</div>
</div>