<?php
$lbltitle		= array("fr"=>"Mettre à jour <span class=\"text-primary\">mon profil</span>","en"=>"Update <span class=\"text-primary\">my profile</span>");
$lblsubmit      = array("fr"=>"SAUVEGARDER MON PROFIL","en"=>"UPDATE MY PROFILE");
$lblgroup       = array("fr"=>"Groupe","en"=>"Group");
$lblfirstname   = array("fr"=>"Prénom","en"=>"Firstname");
$lbllastname    = array("fr"=>"Nom","en"=>"Lastname");
$lblgender      = array("fr"=>"Sexe","en"=>"Gender");
$lblmale        = array("fr"=>"Homme","en"=>"Male");
$lblfemale      = array("fr"=>"Femme","en"=>"Female");
$lbldatebirth   = array("fr"=>"Date de naissance","en"=>"Date of birth");
$lblday         = array("fr"=>"Jour","en"=>"Day");
$lblmonth       = array("fr"=>"Mois","en"=>"Month");
$lblyear        = array("fr"=>"Année","en"=>"Year");
$lblemail       = array("fr"=>"Courriel","en"=>"Email");
$lblconfemail   = array("fr"=>"Confirmez votre courriel","en"=>"Confirm your email");
$lblpwd         = array("fr"=>"Mot de passe","en"=>"Password");
$lblconfpwd     = array("fr"=>"Confirmez votre mot de passe","en"=>"Confirm your password");
$lblphonenumber = array("fr"=>"Téléphone","en"=>"Phone number");
$lbladdress1    = array("fr"=>"Adresse","en"=>"Address");
$lblcountry     = array("fr"=>"Pays","en"=>"Country");
$lblregion      = array("fr"=>"Province / Région","en"=>"Region");
$lblcity        = array("fr"=>"Ville","en"=>"City");
$lblpostal      = array("fr"=>"Code Postal","en"=>"Postal Code");
$lbllanguage    = array("fr"=>"Langue Préférée","en"=>"Preferred Language");

$txtfirstname     = (isset($txtfirstname)) ? $txtfirstname : $cleader->firstname;
$txtlastname      = (isset($txtlastname)) ? $txtlastname : $cleader->lastname;
$txtassociate     = (isset($txtassociate)) ? $txtassociate : $cleader->associate;
$txtmail          = (isset($txtmail)) ? $txtmail : $cleader->mail;
$txtconfmail      = (isset($txtconfmail)) ? $txtconfmail : $cleader->mail;
$txtpwd           = (isset($txtpwd)) ? $txtpwd : $cleader->password;
$txtconfpwd       = (isset($txtconfpwd)) ? $txtconfpwd : $cleader->password;
$txtgender        = (isset($txtgender)) ? $txtgender : $cleader->gender;
$txtbday          = (isset($txtbday)) ? $txtbday : $cleader->bday;
$txtbmonth        = (isset($txtbmonth)) ? $txtbmonth : $cleader->bmonth;
$txtbyear         = (isset($txtbyear)) ? $txtbyear : $cleader->byear;
$txtaddress1      = (isset($txtaddress1)) ? $txtaddress1 : $cleader->address1;
$txtaddress2      = (isset($txtaddress2)) ? $txtaddress2 : $cleader->address2;
$txtcountry       = (isset($txtcountry)) ? $txtcountry : $cleader->country;
$txtregion        = (isset($txtregion)) ? $txtregion : $cleader->region;
$txtcity          = (isset($txtcity)) ? $txtcity : $cleader->city;
$txtpostal        = (isset($txtpostal)) ? $txtpostal : $cleader->postal;
$txtphone         = (isset($txtphone)) ? $txtphone : $cleader->phone;
$txtlanguage      = (isset($txtlanguage)) ? $txtlanguage : $cleader->lang;
?>


	<div class="page-title">
		<?=$lbltitle[_LANG];?>
	</div>
	
	<div id="pagewrap">	
		<form method="post" action="" class="form">
		    <div class="row">
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtfirstname']))?"has-error":"";?>">
		                <label for="txtfirstname"><?=$lblfirstname[_LANG];?>*</label>
		                <input type="text" name="txtfirstname" id="txtfirstname" title="" value="<?=$txtfirstname;?>" maxlength="50" class="form-control" />
		            </div>
		        </div>
		        
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtlastname']))?"has-error":"";?>">
		                <label for="txtlastname"><?=$lbllastname[_LANG];?>*</label>
		                <input type="text" name="txtlastname" id="txtlastname" title="" value="<?=$txtlastname;?>" maxlength="50" class="form-control" />
		            </div>
		        </div>
		    </div>
		    
		    <div class="row">
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtgender']))?"has-error":"";?>">
		                <label for="txtgender"><?=$lblgender[_LANG];?>*</label>
		                <select name="txtgender" id="txtgender" class="form-control">
		                    <option value=""></option>
		                    <option <?=($txtgender=="m")?'selected=selected':'';?> value="m"><?=$lblmale[_LANG];?></option>
		                    <option <?=($txtgender=="f")?'selected=selected':'';?> value="f"><?=$lblfemale[_LANG];?></option>
		                </select>
		            </div>
		        </div>
		        
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtbday'])||isset($haserror['txtbmonth'])||isset($haserror['txtbyear']))?"has-error":"";?>">
		                <label for="selbday"><?=$lbldatebirth[_LANG];?>*</label>
		                <div class="form-inline">
		                    <select class="form-control" id="selbday" name="txtbday" title="" placeholder="Choose a day..." >
		                        <option value=""><?=$lblday[_LANG];?></option>
		                        <?php
		                        for ($i = 1; $i < 32; $i++) {  
		                            $selected = ($txtbday==$i) ? 'selected=selected' : '';
		                            echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
		                        }
		                        ?>
		                    </select>
		                    
		                    <select class="form-control" id="selbmonth" name="txtbmonth" title="" placeholder="Choose a month..." >
		                        <option value=""><?=$lblmonth[_LANG];?></option>
		                        <?php
		                        $months = array("en"=>array('January','February','March','April','May','June','July ','August','September','October','November','December'),
		                        "fr"=>array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet ','Août','Septembre','Octobre','Novembre','Décembre'));
		                        for ($i = 1; $i < 13; $i++) { 
		                            $selected = ($txtbmonth==$i) ? 'selected=selected' : '';
		                            echo '<option '.$selected.' value="'.$i.'">'.$months[_LANG][$i-1].'</option>';
		                        }
		                        ?>
		                    </select>
		                    
		                    <select class="form-control" id="selbyear" name="txtbyear" title="Year of birth is !" placeholder="Choose a year..." >
		                        <option value=""><?=$lblyear[_LANG];?></option>
		                      <?php
		                      for ($i = 1940; $i < date('Y'); $i++) {     
		                          $selected = ($txtbyear==$i) ? 'selected=selected' : '';
		                          echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
		                      }
		                      ?>
		                    </select>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="form-group <?=(isset($haserror['txtaddress1']))?"has-error":"";?>">
		                <label for="txtaddress1"><?=$lbladdress1[_LANG];?>*</label>
		                <input type="text" name="txtaddress1" id="txtaddress1" value="<?=$txtaddress1;?>" maxlength="100" class="form-control" />
		            </div>
		        </div>
		    </div>
		    
		    <div class="row">
		        <div class="col-md-12">
		            <div class="form-group <?=(isset($haserror['txtaddress2']))?"has-error":"";?>">
		                <input type="text" name="txtaddress2" id="txtaddress2" value="<?=$txtaddress2;?>" maxlength="100" class="form-control" />
		            </div>
		        </div>
		    </div>
		    
		    <div class="row">
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtcountry']))?"has-error":"";?>">
		                <label for="txtcountry"><?=$lblcountry[_LANG];?>*</label>
		                <select class="form-control" id="txtcountry" name="txtcountry" title="" data-placeholder="Choose a Country..." >
		                    <option value=""></option>
		                    <?php
		                    foreach($helper->listcountries() as $country){
		                        $selected = ($txtcountry==$country->countryCode) ? 'selected=selected' : '';
		                      echo '<option '.$selected.' value="'.$country->countryCode.'">'.$country->countryName.'</option>';
		                    }
		                    ?>
		                </select>
		            </div>
		        </div>
		        
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtcity']))?"has-error":"";?>">
		                <label for="txtcity"><?=$lblcity[_LANG];?>*</label>
		                <input type="text" name="txtcity" id="txtcity" title="" value="<?=$txtcity;?>" maxlength="50" class="form-control" />
		            </div>
		        </div>
		    </div>
		    
		    <div class="row">
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtregion']))?"has-error":"";?>">
		                <label for="txtregion"><?=$lblregion[_LANG];?>*</label>
		                <input type="text" name="txtregion" id="txtregion" value="<?=$txtregion;?>" title="" maxlength="50" class="form-control" />
		            </div>
		        </div>
		        
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtpostal']))?"has-error":"";?>">
		                <label for="txtpostal"><?=$lblpostal[_LANG];?>*</label>
		                <input type="text" name="txtpostal" id="txtpostal" value="<?=$txtpostal;?>" title="" maxlength="10" class="form-control"  />
		            </div>
		        </div>
		    </div>
		    
		    
		    <div class="row">
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtphone']))?"has-error":"";?>">
		                <label for="txtphone"><?=$lblphonenumber[_LANG];?>*</label>
		                <input type="text" name="txtphone" id="txtphone" value="<?=$txtphone;?>" title="" maxlength="15" class="form-control"  />
		            </div>
		        </div>
		        
		        <div class="col-md-6">
		            <div class="form-group <?=(isset($haserror['txtlanguage']))?"has-error":"";?>">
		                <label for="txtlanguage"><?=$lbllanguage[_LANG];?>*</label>
		                <select class="form-control" id="txtlanguage" name="txtlanguage" title="" data-placeholder="Choose a language..." >
		                    <option value=""></option>
		                    <option <?=($txtlanguage=="fr")?'selected=selected':'';?> value="fr">Français</option>
		                    <option <?=($txtlanguage=="en")?'selected=selected':'';?> value="en">English</option>
		                </select>
		            </div>
		        </div>
		    </div>
			
			<div class="row">
		        <div class="col-md-12">
		        	<input type="hidden" name="txtmail" id="txtmail" value="<?=$txtmail;?>" />
		        	<input type="hidden" name="txtpwd" id="txtpwd" value="<?=$txtpwd;?>" maxlength="12" />
		            <input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
				    <input type="hidden" name="validate" value="true"/>
				    <input type="hidden" name="subform" value="updateprofile"/>
				    <button type="submit" name="updateProfile" class="btn btn-danger btn-lg btn-block"><?=$lblsubmit[_LANG];?></button>
		        </div>
		    </div>
		    
		    
		</form>
	</div>
