<?php 
$lbltitle		= array("fr"=>"Information de <span class=\"text-primary\">paiement</span>","en"=>"Update <span class=\"text-primary\">my billing</span>");

$lblselplan     = array("fr"=>"Choisissez la fréquence de paiement","en"=>"Choose a payment frequency");
$lblmonthly     = array("fr"=>"MENSUEL","en"=>"MONTHLY");
$lblyearly      = array("fr"=>"ANNUEL","en"=>"YEARLY");
$lblccnumber    = array("fr"=>"Numéro de carte de crédit","en"=>"Card number");
$lblccmonth     = array("fr"=>"Mois","en"=>"Month");
$lblccyear      = array("fr"=>"Année","en"=>"Year");
$lblccexp       = array("fr"=>"Date d'expiration","en"=>"Expiration date");
$lblsubmit      = array("fr"=>"METTRE À JOUR MON ABONNEMENT","en"=>"UPDATE MY BILLING");

$lblcancelsub       = array("fr"=>"Annuler mon abonnement","en"=>"Cancel my subscription");
$lblcancelsubdesc       = array("fr"=>"Votre abonnement prendra fin automatiquement le <strong>".$dt->format("d F Y")."</strong>. SVP confirmez votre désabonnement.","en"=>"Your membership will end on <strong>".$dt->format("d F Y")."</strong>. Please confirm to unsubscribe.");

$lblcancel      = array("fr"=>"Annuler","en"=>"Cancel");
$lblconfirm      = array("fr"=>"ME DÉSABONNER","en"=>"UNSUBSCRIBE");

$lblchangesub   = array("fr"=>'Modifier mon <span class="text-primary">plan d\'abonnement</span>',"en"=>'Change my <span class="text-primary">subscribtion plan</span>');
$lblsubdesc     = array("fr"=>"Lorsque vous modifiez votre plan d'abonnement, vous serez facturé à la fin de votre plan actuel.","en"=>"When you change your subscription plan, you will be charged at the end of your current plan. If you are currently on a monthly plan. Your new plan will be effective at the end of the month.");
$lblchpayment   = array("fr"=>'Changer ma <span class="text-primary">méthode de paiement</span>',"en"=>'Update my <span class="text-primary">payment method</span>');
$lblpaydesc     = array("fr"=>"Vous pouvez en tout temps modifier votre méthode de paiement. Cette nouvelle méthode sera effective à la prochaine facturation, soit à votre prochaine mensualité ou la prochaine année.","en"=>"You can always change your payment method. This new method will be effective the next billing or your next monthly payment or the next year.");

$selplan     = (isset($_POST['selplan'])) ? $_POST['selplan'] : $cleader->planid;
?>

    <div class="page-title">
        <?=$lbltitle[_LANG];?>
    </div>
    
    <div id="pagewrap">
    	
        <form action="" method="post" enctype="multipart/form-data" id="form-ccupdate">
            <div class="row">
            	<div class="col-lg-12">
            	    <div class="alert alert-danger payment-errors text-center hide" role="alert"></div>
            		<div class="form-group">
            			<label for="InputName"><?=$lblselplan[_LANG];?></label><br/>
            			<div class="btn-group btn-group-justified" data-toggle="buttons">
            				<label id="plan1" for="selplan1" class="btn <?=($selplan=="1")?'btn-primary':'btn-default';?>">
            					<input type="radio" name="selplan" class="selplan" id="selplan1" value="1" <?=($selplan=="1")?'checked="checked" class="checked"':'';?>> <?=$lblmonthly[_LANG];?> - 20$
            				</label>
            				<label id="plan2" for="selplan2" class="btn <?=($selplan=="2")?'btn-primary':'btn-default';?>">
            					<input type="radio" name="selplan" class="selplan" id="selplan2" value="2" <?=($selplan=="2")?'checked="checked" class="checked"':'';?>> <?=$lblyearly[_LANG];?> - 200$
            				</label>
            			</div>
            		</div>
            		
            		<div class="form-group">
            			<label for=""><?=$lblccnumber[_LANG];?></label>
            			<input type="text" class="form-control" maxlength="16" id="cc_number" placeholder="" validate="required|number|length_min,16">
            		</div>
            		
					<div class="row">
						<div class="col-lg-4">
						    <div class="form-group">
						        <label for=""><?=$lblccexp[_LANG];?></label>
    							<select id="cc_exp_month" class="form-control" validate="required">
    								<option value=""><?=$lblccmonth[_LANG];?></option>
    								<?php
    								$d = 1;
    								while ($d <= 12) {
    								    echo '<option value="'.$d.'">'.str_pad($d,2,"0",STR_PAD_LEFT).'</option>'; 
    								    $d += 1;
    								}
    								?>
    							</select>
							</div>
						</div>
						<div class="col-lg-4">
						    <div class="form-group">
						        <label>&nbsp;</label>
    							<select id="cc_exp_year" class="form-control" validate="required">
    								<option value=""><?=$lblccyear[_LANG];?></option>
    								<?php
    								$today = getdate();
    								$y = $today['year'];
    								$i = 1;
    								while ($i <= 15) {
    								    echo '<option value="'.$y.'">'.$y.'</option>';  
    								    $y += 1;
    								    $i += 1;
    								}
    								?>
    							</select>
							</div>
						</div>
						<div class="col-lg-4">
						    <div class="form-group">
            					<label for="">CVC</label>
            					<input type="text" class="form-control" maxlength="4" id="cc_cvc" placeholder="999" validate="required|number|length_min,3">
            				</div>
						</div>
					</div>
            		
            		<div class="row">
            			<div class="col-lg-12 text-right">
            			    <input type="hidden" name="ctoken" value="<?=$cleader->stripekey;?>"/>
            			    <input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
            			    <input type="hidden" name="validate" value="true"/>
				            <input type="hidden" name="subform" value="updatebilling"/>
            			    <button type="submit" class="btn btn-lg btn-block btn-danger submit"><?=$lblsubmit[_LANG];?></button>
            			</div>
            		</div>
            		
            	</div>
            </div>
        </form>
        <div class="row">
					<div class="col-lg-12 text-right">
							<br><br>
							<strong><a href="#" data-toggle="modal" data-target="#cancelSubModal"><?=$lblcancelsub[_LANG];?></a></strong>
					</div>
				</div>
        
        
        <div class="row">
			<div class="col-lg-12">
				<hr>
			    <h3>Historique de paiements</h3>
			
			    <table class="table table-striped">
					<tr>
						<th>Date</th>
						<th>Invoice</th>
						<th></th>
						<th></th>
					</tr>
					
				<?php
				$xero = new xero();
				$invoices = $xero->lstInvoices($cleader->xeroid);
				
				if($invoices){
				    foreach($invoices as $invoice){
				    	if($invoice->Status == "DELETED" || $invoice->Status == "VOIDED" || $invoice->InvoiceNumber == "") continue;
				        echo '<tr>';
				        echo '<td>'.date("Y-m-d",strtotime($invoice->DateString)).'</td>';
				        echo '<td>'.$invoice->InvoiceNumber.'</td>';
				        echo '<td>'.number_format($invoice->Total,2).'$</td>';
				        echo '<td class="text-right"><a href="/'._LANG.'/invoice/'.$invoice->InvoiceID.'" target="_blank" class="btn btn-default"><i class="fa fa-download" aria-hidden="true"></i></a></td>';
				        echo '</tr>';
				    }
				}
				?>
				</table>
			    
			</div>
		</div>
    </div>

		<div class="modal fade" id="cancelSubModal" tabindex="-1" role="dialog" aria-labelledby="cancelSubModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><?=$lblcancelsub[_LANG];?></h4>
					</div>
					<div class="modal-body">
						<?=$lblcancelsubdesc[_LANG];?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$lblcancel[_LANG];?></button>
						<a href="?unsubscribe=<?=sha1($cleader->leaderid);?>" class="btn btn-primary"><?=$lblconfirm[_LANG];?></a>
					</div>
				</div>
			</div>
		</div>


<?php 
if($cleader->leaderid == '154511') $stripekey = 'pk_test_UY6aDwxaXTOZLaVnZNPJsxso';
else $stripekey = PUBLICSTRIPEKEY;
$jscript .= <<<EOT
$(document).ready(function() { 
	var form = $('#form-ccupdate');
	form.submit(function(event) {
		event.preventDefault();
		Stripe.setPublishableKey('{$stripekey}');
		form.find('.submit').prop('disabled', true);
		Stripe.card.createToken({
			number: $('#cc_number').val(),
			cvc: $('#cc_cvc').val(),
			exp_month: $('#cc_exp_month').val(),
			exp_year: $('#cc_exp_year').val()
		}, stripeResponseHandler);
		return false;
	});
		
	function stripeResponseHandler(status, response) {
		var form = $('#form-ccupdate');
		if (response.error) { 
			$('.payment-errors').removeClass('hide');
			$('.payment-errors').text(response.error.message);
			form.find('.submit').prop('disabled', false);
		} else {
			var token = response.id;
			form.append($('<input type=\"hidden\" name=\"stripeToken\">').val(token));
			form.get(0).submit();
		}
	};
});
EOT;
?>