<?php 
$lbltitle		= array("fr"=>"Créer votre <span class=\"text-primary\">groupe</span>","en"=>"Create a <span class=\"text-primary\">group</span>");
$lblgroupname   = array("fr"=>"Nom de votre groupe", "en"=>"Name of your new group");
$group = $account->getProGroup($cleader->leaderid);
?>

<div class="groupbox">
    
    
    <div class="row">
        <div class="col-md-6">
            <div style="height: 60px;background-color: #337AB7;border-radius:0 50px 50px 0;">
                <a href="#" style="color: #ffffff;">
                <div class="groupimg pull-left" style="margin-right: 20px;">
                    <img src="<?='//media.'.DOMAIN.'/'.$group->corpotoken.'/images/profiles/'.$group->groupleaderid.'.jpg';?>" alt="" class="img" style="max-height: 59px;"/>
                </div>
                <div class="" style="line-height: 35px;font-size: 1.2em;font-weight: 700;text-shadow: 1px 1px 1px #000;text-transform: uppercase;">
                    <i class="fa fa-users" aria-hidden="true"></i> <?=$group->name;?>
                </div>
                Lise Gaudet
                </a>
            </div>
        </div>
        
        
        <div class="col-md-6">

        </div>
    </div>
    
    
    
    
    
    
</div>

    <div class="page-title">
        <?=$lbltitle[_LANG];?> <span class="label label-primary"><i class="fa fa-star" aria-hidden="true"></i> PRO</span>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            Créez votre groupe de leaders et atteignez de nouveaux sommets avec une foules d'outils de communications et d'analyses afin d'aider vos bâtisseurs à mieux performer.
        </div>
    </div>
    

    <div class="row">
        <div class="col-md-12">
            <h3>IMPORTANT</h3>
            <ul>
                <li>Suite à la création de votre groupe, vous quitterez automatiquement le groupe actuel.</li>
                <li>Si vous décidez d'annuler votre abonnement Xpress Leader ou que votre compte est suspendu pour non-paiement, tout vos membres seront transférés dans le groupe principal de votre leader corporatif.</li>
                <li>Les communications avec vos membres par le biais d'Xpress leader seront monitorés par votre leader corporatif.</li>
                <li>En cas de non-respect des politiques et procédures de votre entreprise, votre leader corporatif pourra en tout temps supprimer votre groupe et transférer vos membres.</li>
            </ul>
            
            <form action="" method="post" enctype="multipart/form-data" id="form-addgroup">
            <div class="form-group">
              <label for=""><?=$lblgroupname[_LANG];?></label>
              <input type="text" class="form-control" maxlength="50" id="groupname" placeholder="" required="true">
              <p class="help-block">Maximum 50 caractères, le nom de votre groupe sera vérifié par votre leader corporatif.</p>
            </div>
            </form>
            
            
            
        </div>
    </div>
    