<?php
$lbltitle      = array("fr"=>"Personnaliser ma <span class=\"text-primary\">page public</span>","en"=>"Customize my <span class=\"text-primary\">public page</span>");
$lblprofile    = array("fr"=>"Modifier mon image de profil", "en"=>"Modify my profile image");

$lblfrench      = array("fr"=>"Textes Français","en"=>"French layout");
$lblenglish     = array("fr"=>"textes Anglais","en"=>"English layout");
$lblimgdesc     = array("fr"=>"Seul les JPEG et PNG sont accepté. La taille doit-être moin de 100kb. (400X400)","en"=>"Only JPEG and PNG are accepted. Your file must be less than 100kb. (400x400)");
$lblshowimg     = array("fr"=>"Afficher mon image","en"=>"Show profile image");
$lblshowcity    = array("fr"=>"Afficher ma ville","en"=>"Show my location (city, country)");
$lblshowphone   = array("fr"=>"Afficher mon téléphone","en"=>"Show my phone number");
$lblshowmail    = array("fr"=>"Afficher mon courriel","en"=>"Show my email address");

$lblaboutme         = array("fr"=>"À propos de vous ( Maximum 200 caractères )","en"=>"About you ( Max 200 characters )");
$lblherotext        = array("fr"=>"Titre principal de votre page d'accueil ( Maximum 80 caractères )","en"=>"Main title of the home page ( Max 80 characters )");
$lbloverviewtitle   = array("fr"=>"Titre au dessus du vidéo de survol ( Maximum 80 caractères )","en"=>"Title over the overview video ( Max 80 characters )");

$lblbtnsave     = array("fr"=>"SAUVEGARDER","en"=>"UPDATE MY PAGE");
$layout = $account->getLayout($cleader->leaderid, $cleader->groupleaderid, $cleader->corpoid);

function jsonExtract($string, $lang) {
    $msg = json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE && $string != "") ? $msg->{$lang} : $string;
}
?>

    <div class="page-title">
        <?=$lbltitle[_LANG];?>
    </div>
    
    <div id="pagewrap">
        
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-2">
                            <?php 
                            if(trim($layout->profile)<>"") 
                            echo '<img src="//'.DOMAIN.'/'.$cleader->corpotoken.'/images/profiles/'.$layout->profile.'" class="img-responsive"/>';
                            ?>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <label for="profile"><?=$lblprofile[_LANG];?></label>
                                <input type="file" id="profile" name="profile" placeholder="" accept="image/*">
                                <p class="help-block"><?=$lblimgdesc[_LANG];?></p>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showprofile" value="1" <?=($layout->showprofile==1)?'checked=checked':''?>> <?=$lblshowimg[_LANG];?>
                                    </label>
                                </div>
                                
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showlocation" value="1" <?=($layout->showlocation==1)?'checked=checked':''?>> <?=$lblshowcity[_LANG];?>
                                    </label>
                                </div>
                                
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showphone" value="1" <?=($layout->showphone==1)?'checked=checked':''?>> <?=$lblshowphone[_LANG];?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showemail" value="1" <?=($layout->showemail==1)?'checked=checked':''?>> <?=$lblshowmail[_LANG];?>
                                    </label>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    
        
                    <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active"><a href="#french" aria-controls="french" role="tab" data-toggle="tab"><?=$lblfrench[_LANG];?></a></li>
                        <li role="presentation"><a href="#english" aria-controls="english" role="tab" data-toggle="tab"><?=$lblenglish[_LANG];?></a></li>
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content" style="padding:10px 0;">
                        <div role="tabpanel" class="tab-pane active" id="french">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lblaboutme[_LANG];?></label>
                                    <textarea class="form-control" id="profiledescription_fr" name="profiledescription_fr" rows="4"><?=(isset($layout->profile_description)) ? jsonExtract($layout->profile_description, "fr"):"";?></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lblherotext[_LANG];?></label>
                                    <input type="text" class="form-control" id="herotext_fr" name="herotext_fr" maxlength="80" placeholder="" value="<?=jsonExtract($layout->herotext, "fr");?>">
                                </div>
                            </div>
                            
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lbloverviewtitle[_LANG];?></label>
                                    <input type="text" class="form-control" id="overviewtitle_fr" name="overviewtitle_fr" maxlength="80" placeholder="" value="<?=jsonExtract($layout->overview_title, "fr");?>">
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="english">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lblaboutme[_LANG];?></label>
                                    <textarea class="form-control" id="profiledescription_en" name="profiledescription_en" rows="4"><?=(isset($layout->profile_description)) ? jsonExtract($layout->profile_description, "en"):"";?></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lblherotext[_LANG];?></label>
                                    <input type="text" class="form-control" id="herotext_en" name="herotext_en" maxlength="80" placeholder="" value="<?=jsonExtract($layout->herotext, "en");?>">
                                </div>
                            </div>
                            
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="profiledescription"><?=$lbloverviewtitle[_LANG];?></label>
                                    <input type="text" class="form-control" id="overviewtitle_en" name="overviewtitle_en" maxlength="80" placeholder="" value="<?=jsonExtract($layout->overview_title, "en");?>">
                                </div>
                            </div>
                        </div>
                    </div>                    
                    </div>
                

                    
                    <div class="form-group hide">
                        <label for="overviewdescription">SURVOL - Texte descriptif</label>
                        <textarea class="form-control" id="overviewdescription" name="overviewdescription" rows="3"><?=$layout->overview_description;?></textarea>
                    </div>
                    
                    <input type="hidden" name="corpotoken" value="<?=$cleader->corpotoken;?>"/>
                    <input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
                    <input type="hidden" name="validate" value="true"/>
                    <input type="hidden" name="subform" value="updatelayout"/>
                    <button type="submit" class="btn btn-danger btn-block btn-lg"><?=$lblbtnsave[_LANG];?></button>
                </form>

            </div>
        </div>
    </div>