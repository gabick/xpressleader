<?php 
$lblmyplan      = array("fr"=>"Mon plan","en"=>"My plan");
$lblnextpay     = array("fr"=>"Prochain paiement","en"=>"Next payment");
$lblstatus      = array("fr"=>"Statut","en"=>"Account status");

$lblmnuprofile      = array("fr"=>"Modifier mon profil","en"=>"Update my profile");
$lblmnupayment      = array("fr"=>"Informations de paiements","en"=>"Payments informations");
$lblmnumypage       = array("fr"=>"Personnaliser page public","en"=>"My public page");
$lblmnupresenters   = array("fr"=>"Mes présentateurs","en"=>"Manage presenters");

$lblmnumygroup   = array("fr"=>"Mon groupe","en"=>"My group");
$plan = $billing->getPlan($cleader->planid);
?>
<div class="container">
	<div class="whiteboard">
	    <div class="row">
			<div class="col-lg-4">
                <div class="panel panel-default box-shadow--6dp">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title"><?=$lblmyplan[_LANG];?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <strong><?=(_LANG=="fr") ? $plan->name_fr : $plan->name_en;?></strong>
                    </div>
                </div>
			</div>
			
			<div class="col-lg-4">
                <div class="panel panel-default box-shadow--6dp">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title"><?=$lblnextpay[_LANG];?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <strong>
                            <?php 
                            $dt = new datetimefrench($cleader->nextcharge);
                            echo $dt->format("d F Y")
                            ?>
                        </strong>
                    </div>
                </div>
			</div>
			
			<div class="col-lg-4">
                <div class="panel panel-default box-shadow--6dp">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title"><?=$lblstatus[_LANG];?></h3>
                    </div>
                    <?php 
                    if($cleader->chargetry==2) {
            	    	echo '<div class="panel-body text-danger text-center">
                                    <strong>SUSPENDED</strong>
                              </div>';
            	    }else {
            	        echo '<div class="panel-body text-success text-center">
                                    <strong>ACTIVE</strong>
                              </div>';
            	    }
            	    ?>
                    
                </div>
			</div>
		</div>
	    <div class="row">
	        <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="<?=(_VIEW=="profile"||_VIEW=="")?"active":"";?>"><a href="/<?=_LANG;?>/account/profile"><?=$lblmnuprofile[_LANG];?></a></li>
                    <li role="presentation" class="<?=(_VIEW=="billing")?"active":"";?>"><a href="/<?=_LANG;?>/account/billing"><?=$lblmnupayment[_LANG];?></a></li>
                    <li role="presentation" class="<?=(_VIEW=="customize")?"active":"";?>"><a href="/<?=_LANG;?>/account/customize"><?=$lblmnumypage[_LANG];?></a></li>
                </ul>
	        </div>
	        <div class="col-lg-9">
	            <?php 
	            if(_VIEW=="" || _VIEW=="profile") include_once SITEPATH."/views/account/view.profile.php";
	  			elseif(_VIEW=="billing") include_once SITEPATH."/views/account/view.billing.php";
	  			elseif(_VIEW=="group") include_once SITEPATH."/views/account/view.group.php";
	  			elseif(_VIEW=="customize") include_once SITEPATH."/views/account/view.customize.php";
	  			else include_once SITEPATH."/views/account/view.profile.php";
	            ?>
	        </div>
	    </div>
	</div>
</div>