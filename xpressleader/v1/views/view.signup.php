<?php
$lblvideolink       = array("fr"=>"https://player.vimeo.com/external/175235558.sd.mp4?s=5c3cf85ebe621c5f03dfaf7b633ffc60b2ac95bc&profile_id=165","en"=>"https://player.vimeo.com/external/181858157.sd.mp4?s=ecc4726b089c47ea4ff9a6e7dc712f4bedef8773&profile_id=165");
$lblWelcomeGift		= array("fr"=>"CADEAU DE BIENVENUE","en"=>"YOUR WELCOME GIFT");
$lbldownload		= array("fr"=>"Télécharger maintenant","en"=>"Download now");

$lbltitle           = array("fr"=>"Créez votre compte dès maintenant","en"=>"Create your account now");
$lblfirstname       = array("fr"=>"Nom","en"=>"Firstname");
$lbllastname       	= array("fr"=>"Prénom","en"=>"Lastname");
$lblemail       	= array("fr"=>"Adresse courriel","en"=>"Email address");
$lblconfemail       = array("fr"=>"Confirmation adresse courriel","en"=>"Confirm your email address");
$lblbtn				= array("fr"=>"OUVRIR UN COMPTE XPRESS","en"=>"CREATE YOUR XPRESS ACCOUNT");
$lblbtnconfirm      = array("fr"=>"CONFIRMER MON COURRIEL","en"=>"CONFIRM MY EMAIL");

$lblherebcuz		= array("fr"=>"<h1> Un système clé en main pour moins de 1$ par jour!</h1>", "en"=>"<h1>YOU ARE HERE BECAUSE SOMEONE USES  <img src=\"//media.xpressleader.com/images/logos/xpressleader_black.svg\" style=\"display: inline;height: 80px;margin-bottom:10px;\"/></h1>");
$lblxpintro			= array("fr"=>"Qu'est-ce que Xpressleader.com ?","en"=>"What is Xpressleader.com ?");
$lblxpdesc			= array("fr"=>"Une plateforme web unique de prospections et formations pour les entreprises en marketing relationel. Puissante et simple à utilisée, Xpressleader vous permettra de développer votre entreprise à la vitesse grand V!","en"=>"A unique prospecting and training web platform. Powerful and easy to use, Xpressleader is ideal for relationship marketing businesses.");
$lbllist1			= array("fr"=>"Votre page web personnalisée","en"=>"Your personalized webpage");
$lbllist2			= array("fr"=>"Un système complet de prospection automatisé","en"=>"A complete prospecting system");
$lbllist3			= array("fr"=>"Analyse et suivi de vos prospects","en"=>"Tracking and stats of your prospect");

$lblready           = array("fr"=>"Prêt à devenir un leader ?","en"=>"Ready to become a leader ?");
$lbllegal			= array("fr"=>"Xpress leader LLC est une entreprise indépendante et n'est pas affilié ni associé à aucune autre entreprise. L'ouverture d'un compte, bien que fortement recommandé, n'est pas obligatoire pour le démarrage de votre entreprise.","en"=>"Xpress leader LLC is an independent company and is not affiliated or associated with any other companies. Opening an account, although highly recommended, is not required for starting your business.");


$db->query("SELECT
				invitations.token,
				invitations.leaderid,
				leaders.firstname,
				leaders.lastname,
				leaders.mail AS leadermail,
				invitations.prospectid,
				dbclients.mail AS prospectmail,
				dbclients.`name`,
				dbclients.phone,
				dbclients.`status`,
				dbclients.id
				FROM
				invitations
				INNER JOIN dbclients ON invitations.prospectid = dbclients.id
				INNER JOIN leaders ON invitations.leaderid = leaders.`code` 
			WHERE invitations.token=:token;");
$db->bind(":token", _VIEW);
$prospect = $db->single();
?>

<div style="background-color: rgba(0,0,0,0.6);color:#ffffff;">

		<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<input type="hidden" id="overviewurl" value="<?=$lblvideolink[_LANG];?>" required="true"/>
		            <input type="hidden" id="overviewimg" value="" required="true"/>
		        <div id="jwdynvideo" class="center-block"></div>
			</div>
			<div class="col-lg-4">
				<h3><?=$lblWelcomeGift[_LANG];?></h3>
				<h4>E-book "Trucs et Recettes" <small>French only</small></h4>
				<img src="//media.xpressleader.com/images/trucsetrecettes.png" alt="" class="img img-responsive center-block visible-lg" style="height: 280px;"/>

				<a href="https://media.xpressleader.com/pdf/TrucsRecettes.pdf" target="_blank" class="btn btn-lg btn-block btn-warning"><?=$lbldownload[_LANG];?></a>
			</div>
		</div>
		</div>
</div>

<div class="container">

<div class="whiteboard">
	<div class="row">
		<div class="col-md-12 text-center">
			<?=$lblherebcuz[_LANG];?>
		</div>
        <div class="col-lg-12 text-center">
            <a href="#" class="btn btn-lg btn-danger btn-block" data-toggle="modal" data-target="#signupmodal"><?=$lblbtn[_LANG];?></a>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-6" style="background-color: #f7f7f7;">

			<h2><?=$lblxpintro[_LANG];?></h2>
			<p><?=$lblxpdesc[_LANG];?></p>
			<div style="font-size: 1.3em;line-height: 38px;">
			<ul>
                <li><?=$lbllist1[_LANG];?></li>
                <li><?=$lbllist2[_LANG];?></li>
                <li><?=$lbllist3[_LANG];?></li>
			</ul>
			</div>

		</div>

        <div class="col-lg-6">
            <div  style="background-color: black; margin: 0 auto;">
                <img src="/images/logos/xpressleader_white.svg" class="img img-responsive" style="margin: 0 auto; height: 80px;"/>
                <img src="/mela001/images/only15_<?=_LANG;?>.png" class="img img-responsive" style="margin: 0 auto;">
            </div>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info" style="margin-top: 20px;" role="alert"><strong>IMPORTANT:</strong> <?=$lbllegal[_LANG];?></div>

		</div>
	</div>






	<div id="signupmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="true" style="z-index: 9999;">
        <div class="modal-dialog modal-primary">
            <div class="modal-content">
				<div class="modal-body">
					<div id="signloader" class="text-center"></div>
					<p></p>


					<div id="signupbox">
		                <div id="signmessage"></div>
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group">
		                            <label for="signfirstname"><?=$lblfirstname[_LANG];?></label>
		                            <input type="text" class="form-control" id="signfirstname" placeholder="">
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group">
		                            <label for="signlastname"><?=$lbllastname[_LANG];?></label>
		                            <input type="email" class="form-control" id="signlastname" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="signemail1"><?=$lblemail[_LANG];?></label>
		                            <input type="email" class="form-control" id="signemail1" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="signemail2"><?=$lblconfemail[_LANG];?></label>
		                            <input type="email" class="form-control" id="signemail2" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                    	<input type="hidden" id="lang" value="<?=_LANG;?>"/>
		                        <input type="hidden" id="signleaderid" value="<?=$prospect->leaderid;?>"/>
		                        <input type="hidden" id="signtoken" value="<?=sha1($prospect->leaderid."signupMynewAccount!");?>"/>
		                        <button class="btn btn-block btn-lg btn-success" id="signsubmit"><?=$lblbtnconfirm[_LANG];?> <i class="fa fa-check-square-o" aria-hidden="true"></i></button>
		                    </div>
		                </div>
		            </div>
				</div>
            </div>
        </div>
    </div>


</div>
</div>
