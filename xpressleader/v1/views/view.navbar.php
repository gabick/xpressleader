<?php 
$lblsignin          = (_LANG=="fr") ? "Se connecter" : "Login";
$lblsignout         = (_LANG=="fr") ? "Se déconnecter" : "Log out";
$lblviewpage        = (_LANG=="fr") ? "Ma page publique" : "My public page";
?>
<nav class="navbar navbar-xpress <?=(_CONTROLLER=="webcast") ? "navbar-fixed-top" :"";?>" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="container">
<div class="navbar-header">
	<span class="navbar-brand"><img src="/images/logos/xpressleader_black.svg"/></span>
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-xpress-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-xpress-collapse">
    	
  			
  	<ul class="nav navbar-nav navbar-right">
  	    <?php
    if(!$account->islogin && _CONTROLLER!="webcast"){
    ?>  
  	<ul class="nav navbar-nav">
		<li class="<?=(_CONTROLLER=='home'||_CONTROLLER==''||_CONTROLLER=='login'||_CONTROLLER=='forgotpassword')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>/login"><i class="fa fa-login"></i> <?=(_LANG=='fr')?'Connexion':'Connection';?></a></li>
  	</ul>
  	<?php
    }elseif(_CONTROLLER=="webcast"){
    ?>  
      <ul class="nav navbar-nav">
        <li></li>
      </ul>
    <?php
    }else{
    ?>
  	<ul class="nav navbar-nav ">
  	  <li class="<?=(_CONTROLLER=='home'||_CONTROLLER=='')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>"><i class="fa fa-home"></i> <?=(_LANG=='fr')?'Accueil':'Home';?></a></li>
		  <!--<li class="<?=(_CONTROLLER=='fasttrack')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>/fasttrack"><i class="fa fa-star"></i> <?=(_LANG=='fr')?'FastTrack':'FastTrack';?></a></li>-->
		  <li class="<?=(_CONTROLLER=='office')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>/office"><i class="fa fa-building"></i> <?=(_LANG=='fr')?'Bureau':'Office';?></a></li>
		  <li class="<?=(_CONTROLLER=='meeting')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>/meeting"><i class="fa fa-camera"></i> <?=(_LANG=='fr')?'Meeting':'Meeting';?></a></li>
		  
		  <li><a class="ease" href="/<?=_LANG;?>/<?=$cleader->alias;?>"><i class="fa fa-globe"></i> <?=(_LANG=='fr')?'Ma page publique':'My public page';?></a></li>
  	</ul>
  	<?php
    }
    ?>	
    
		<?php
    			if($account->islogin) {
    ?>
    			  <li class="dropdown ">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$cleader->firstname.' '.$cleader->lastname;?> <span class="caret"></span></a>
              <ul class="dropdown-menu" style="font-size: 0.9em;">
                <li class="<?=(_CONTROLLER=='account')?'active':'';?>"><a class="ease" href="/<?=_LANG;?>/account"><i class="fa fa-user"></i> <?=(_LANG=='fr')?'Mon compte':'My Account';?></a></li>
                <?php 
                if($cleader->ispro){
                ?>
                <li><a class="ease" href="/<?=_LANG;?>/pro"><i class="fa fa-users"></i> <?=(_LANG=='fr')?'Pro':'Pro';?></a></li>
                <?php
                }
                if($iscorpo){
                ?>
                <li><a class="ease" href="/<?=_LANG;?>/corporate"><i class="fa fa-users"></i> <?=(_LANG=='fr')?'Corporatif':'Corporate';?></a></li>
                <?php
                }
                ?>
                <li><a href="/<?=_LANG;?>/logout"><?=$lblsignout;?></a></li>
              </ul>
            </li>

    			  <?php
			    }elseif(isset($pageleader)&&$pageleader){
			    ?>
				    <li><a data-toggle="collapse" href="#collapse-formlogin" aria-expanded="false" aria-controls="collapse-formlogin"><?=$lblsignin;?> <i class="fa fa-sign-in"></i></a></li>
  				<?php
  			  }else{
  			    
  			  }
  			  
	
        if(_LANG=="fr") echo '<ul class="nav navbar-nav pull-right"><li class="text-right"><a href="/en/'._CONTROLLER.'/'._VIEW.'">en</a></li></ul>';
    			  else echo '<ul class="nav navbar-nav pull-right"><li class="text-right"><a href="/fr/'._CONTROLLER.'/'._VIEW.'">fr</a></li></ul>';
    		
    		
    		  
    		
    	?>
	</ul>
  </div>
  
  
    <div id="collapse-formlogin" class="<?=(isset($navbarOpen)&&$navbarOpen)?'':'collapse';?>" role="tabpanel" aria-labelledby="collapse-formlogin">
    		<div class="row">
    		    <div class="col-md-12 text-right">
    		        <?php 
    		        if($vdnerror) echo '<div class="alert navbar-danger pull-left" role="alert">'.$vdnmsg.'</div>';
    		        ?>
                <form class="form-inline" action="" method="post" id="navbar-login">
                    <div class="form-group">
                        <label for="txtusr"></label>
                        <input type="text" class="form-control" id="txtusr" name="txtusr" placeholder="Courriel">
                    </div>
                    <div class="form-group">
                        <label for="txtpwd"></label>
                        <input type="password" class="form-control" id="txtpwd" name="txtpwd" placeholder="Mot de passe">
                        <input type="hidden" name="validate" value="true"/>
                        <input type="hidden" name="navbaropen" value="1"/>
                        <input type="hidden" name="subform" value="signin"/>
                        <button type="submit" class="btn btn-xp-orange">Se connecter</button>
                </form>
    		    </div>
    		</div>
		</div>
		</div>
</nav>