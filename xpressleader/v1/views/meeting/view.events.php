<?php
$lblmnucalendar     = array("fr"=>"MON CALENDRIER","en"=>"MY CALENDAR");
$lblmnuaddmeet      = array("fr"=>"CRÉER UN MEETING","en"=>"CREATE A MEETING");
$lblmnupresentor    = array("fr"=>"PRÉSENTATEURS","en"=>"PRESENTERS");

$lblnomeeting       = array("fr"=>"Il n'y a aucun meeting pour le moment.","en"=>"There is no meeting at the moment.");

$lblcorporate       = array("fr"=>"CORPORATIF","en"=>"CORPORATIVE");
$lblcorporatedesc   = array("fr"=>"Afficher sur tous les calendriers de l'organisation.","en"=>"Show on every calendars of your organization.");


$lblpublic          = array("fr"=>"PUBLIC","en"=>"PUBLIC");
$lblpublicdesc      = array("fr"=>"Afficher sur tous les calendriers du groupe.","en"=>"Show on every calendars of your group.");


$follows = $account->listFollows($cleader->leaderid, $cleader->groupid);

$crmeet_type1       = (isset($crmeet_type1)) ? "checked" : '';
$crmeet_type2       = (isset($crmeet_type2)) ? "checked" : '';
$crmeet_type3       = (isset($crmeet_type3)) ? "checked" : '';
if($crmeet_type1==""&&$crmeet_type2==""&&$crmeet_type3=="") $crmeet_type1 = "checked";

$crmeet_category    = (isset($crmeet_category)) ? $crmeet_category : '1';
$crmeet_pwd         = (isset($crmeet_pwd)) ? $crmeet_pwd : '';
$crmeet_title       = (isset($crmeet_title)) ? $crmeet_title : '';
$crmeet_media       = (isset($crmeet_media)) ? $crmeet_media : '4';
$crmeet_language    = (isset($crmeet_language)) ? $crmeet_language : 'fr';
$crmeet_chat        = (isset($crmeet_chat)) ? "checked" : 'checked';
$crmeet_date        = (isset($crmeet_date)) ? $crmeet_date : '';
$crmeet_time        = (isset($crmeet_time)) ? $crmeet_time : '';

if(isset($_GET['del']) && $_GET['del']<>""){
	$event->delete($_GET['del'], $cleader->leaderid);
}
?>


<div class="container">
    
    
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-tabs nav-justified nav-xpress" role="tablist">
                <li role="presentation" class="<?=(!$vdnerror)?'active':'';?>"><a href="#viewmeet" aria-controls="viewmeet" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i> <?=$lblmnucalendar[_LANG];?></a></li>
                <li role="presentation" class="<?=($vdnerror)?'active':'';?>"><a href="#addmeet" aria-controls="addmeet" role="tab" data-toggle="tab"><i class="fa fa-plus"></i> <?=$lblmnuaddmeet[_LANG];?></a></li>
                <li role="presentation" class=""><a href="#viewpresentor" aria-controls="viewpresentor" role="tab" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i> <?=$lblmnupresentor[_LANG];?></a></li>
            </ul>
        </div>
    </div>
    
    <div class="whiteboard">
	<div class="row">
		<div class="col-lg-12">


            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="addmeet">
                    <br>
                        <form method="post" action="">
                            <div class="row">
                                <?php 
                                $showprivate = "hide";
                                if($account->isCorpoPresenter($cleader->leaderid, $cleader->corpotoken)){
                                    $showprivate = "";
                                ?>
                                <div class="col-md-4">
                                    <div class="well well-sm">
                                    <div class="radio">
                                      <label for="crmeet_type3">
                                        <input id="crmeet_type3" name="crmeet_type" type="radio" <?=$crmeet_type3;?> value="3"> <strong><?=$lblcorporate[_LANG];?></strong><br>
                                        <small><?=$lblcorporatedesc[_LANG];?></small>
                                      </label>
                                    </div>
                                    </div>
                                </div>
                                <?php 
                                }
                                if($account->isGroupPresenter($cleader->leaderid, $cleader->groupid)){
                                    $showprivate = "";
                                ?>
                                <div class="col-md-4">
                                   <div class="well well-sm">
                                    <div class="radio">
                                      <label for="crmeet_type1">
                                        <input id="crmeet_type1" name="crmeet_type" type="radio" <?=$crmeet_type2;?> value="2"> <strong><?=$lblpublic[_LANG];?></strong><br>
                                        <small><?=$lblpublicdesc[_LANG];?></small>
                                      </label>
                                    </div>
                                    </div> 
                                </div>
                                <?php 
                                }
                                ?>
                                <div class="col-md-4">
                                    <div class="well well-sm <?=$showprivate;?>">
                                    <div class="radio">
                                      <label for="crmeet_type2">
                                        <input id="crmeet_type2" name="crmeet_type" type="radio" <?=$crmeet_type1;?> value="1"> <strong>PRIVÉ</strong><br>
                                        <small>Afficher seulement sur votre calendrier.</small>
                                      </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="crmeet_category">Type de meeting</label>
                                    <select id="crmeet_category" name="crmeet_category" class="form-control">
                                        <option <?=($crmeet_category=="1")?"selected=selected":"";?> value="1">Survol de l'entreprise</option>
                                        <option <?=($crmeet_category=="2")?"selected=selected":"";?> value="2">Formation</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="crmeet_pwd">Protéger par mot de passe</label>
                                    <input id="crmeet_pwd" name="crmeet_pwd" value="<?=$crmeet_pwd;?>" type="text" class="form-control">
                                </div>
                            </div>
                            </div>
                            
                            <div class="row">
                            <div class="col-md-12">
                                <div id="event_training" class="<?=($crmeet_category=="1") ? "hide":"";?>">
                                    <div class="well well-sm">Les Formations sont accessibles exclusivement aux membres de votre organisation. Les membres doivent se connecter et cliquer sur le calendrier des évènements situé à gauche sur la page web pour accéder aux formations.</div>
                                    <div class="form-group">
                                        <label for="crmeet_title">Titre de votre formation</label>
                                        <input id="crmeet_title" name="crmeet_title" value="<?=$crmeet_title;?>" type="text" class="form-control">
                                    </div>
                                </div>
                                
                                <div id="event_overview" class="<?=($crmeet_category=="2") ? "hide":"";?>">
                                    <div class="well well-sm">Les Survols de l’Entreprise seront affichés sur votre page personnalisée. Chaque Survol de l’entreprise est accessible 30 minutes avant l'heure prévue et 5 minutes après le début. L’entrée publique correspondant à ce survol sera par la suite inaccessible pour les invités. En se connectant, les membres Xpress Leader peuvent entrer par le calendrier des évènements situé à gauche sur la page web en tout temps. Prenez note que les meetings protégés par mot de passe sont accessibles en tout temps.</div>
                                    <div class="form-group">
                                        <label for="crmeet_media">Document de présentation</label>
                                        <select id="crmeet_media" name="crmeet_media" class="form-control">
                                            <?php 
                                            $medias = $corporate->getEventDoclist($cleader->corpotoken);
                                            foreach($medias as $media){
                                                $selected = ($crmeet_media==$media->id) ? "selected=selected" : "";
                                                echo '<option '.$selected.' value="'.$media->id.'">'.$media->name.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="crmeet_language">Langue</label>
                                        <select id="crmeet_language" name="crmeet_language" class="form-control">
                                            <option <?=($crmeet_language=="fr")?"selected=selected":"";?> value="fr">Français</option>
                                            <option <?=($crmeet_language=="en")?"selected=selected":"";?> value="en">Anglais</option>
                                            <option <?=($crmeet_language=="es")?"selected=selected":"";?> value="es">Espagnol</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="dpmeeting">Date et heure du meeting</label>
                    				<div class="input-group">
                    			        <input id="dpmeeting" name="crmeet_date" type="text" value="<?=$crmeet_date;?>" class="form-control" readonly>
                    			        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    			    </div>
                                    
                                </div>
                            </div>
                    		
                            
                            
                            <div class="row">
                                <div class="col-md-8"></div>
                    			<div class="col-md-4">
                    			    <br>
                    			    <input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
                    			    <input type="hidden" name="validate" value="true"/>
                                    <input type="hidden" name="subform" value="meeting_add"/>
                    				<button type="submit" name="subcrmeet" class="btn btn-block btn-primary">CRÉER MON MEETING</button>
                    			</div>
                    		</div>
                        </form>
                </div>
                <div role="tabpanel" class="tab-pane fade <?=(!$vdnerror)?'in active':'';?>" id="viewmeet">
                    <br><br>
                    <table class="table table-striped">
                        <?php
                        $events = $event->getEvents($cleader->leaderid, $cleader->groupid, "", "", "");
                        if($events){
                            foreach($events as $ev){
                                $openday    = date('d', strtotime($ev->timestart));
                                $openmonth  = (_LANG=="fr") ? new datetimefrench($ev->timestart) : new datetime($ev->timestart);
                                $open       = date('h:ia', strtotime($ev->timestart));
            
                                $timeleft   = $event->getRemaining(time(), strtotime($ev->timestart)-3600);
                                $lifetime   = 180;
                                
                                if($ev->type==1) $lbltype = '';
                                elseif($ev->type>=2) $lbltype = '<span class="label label-primary">Public</span>';
                                elseif($ev->type==3) $lbltype = '<span class="label label-default">Corpo</span>';
                                
                                if($timeleft){
                                    if($timeleft['days']==0 && $timeleft['hours']==0) $joinhtml   = '<strong>'.'<small>Disponible dans:</small><br>'.$timeleft['minutes'].'<small> minutes</small>'.'</strong>';
                                    elseif($timeleft['days']==0) $joinhtml   = '<strong>'.'<small>Disponible dans:</small><br>'.$timeleft['hours'].'<small>h</small> '.$timeleft['minutes'].'<small>m</small>'.'</strong>';
                                    else $joinhtml   = '';
                                }else{
                                    if(!$event->getLifeAccess($ev->timestart,$lifetime)){
                                        $joinhtml   = '<a href="https://'.DOMAIN.'/'._LANG.'/webcast/'.$ev->token.'" target="_blank" class="btn btn-success btn-sm">Joindre le meeting <i class="fa fa-play-circle"></i></a>';
                                    }else $joinhtml = '<span class="text-danger"><strong>Fermé</strong></span>';
                                }
                                
                                $title  = ($ev->category=="1") ? "SURVOL D'ENTREPRISE" : $ev->title;
                                $pwd    = ($ev->leaderid==$cleader->leaderid && $ev->password!="") ? "<br><i class=\"fa fa-key\"></i> <span class=\"text-danger\"><strong>".$ev->password."</strong></span>" : "";
                                
                                if(_LANG=="fr") $eng = ($ev->language=="en") ? '<span class="label label-danger"><i class="fa fa-language"></i>english</span>' : "";
                                if(_LANG=="en") $eng = ($ev->language=="fr") ? '<span class="label label-info"><i class="fa fa-language"></i>français</span>' : $eng;
                                $eng = ($ev->language=="es") ? '<span class="label label-warning"><i class="fa fa-language"></i>español</span>' : $eng;
                                echo '<tr class="">';
                                echo '<td>'.$lbltype.$pwd.'</td>';
                                echo '<td class="calrow"><div class="day">'.$openday.'</div><div class="month">'.$openmonth->format('M').'</div></td>';
                                echo '<td><div class="time">'.$open.' <small>'.$eng.'</small></div>'.$ev->name.'<br></td>';
                                
                                echo '<td>'.$title.'</td>';
                                echo '<td class="text-right">'.$joinhtml.'</td>';
                                if($ev->leaderid==$cleader->leaderid) echo '<td class="text-right"><a href="?del='.$ev->id.'&v='.$ev->timestart.'" type="button" class="btn btn-default"><i class="fa fa-remove"></i></a></td>';
                                else echo '<td></td>';
                                echo '</tr>';
                            }
                        }else{
                            echo '<tr>';
                            echo '<td class="text-center"><strong>'.$lblnomeeting[_LANG].'</strong></td>';
                            echo '</tr>';
                        }
                        ?>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="viewpresentor">
                    <br><br>
                    <div class="row">
                		<div class="col-lg-12">
                
                            <p>
                                Afficher/Masquer les présentateurs de votre choix dans votre calendrier.
                            </p>
                            <table class="table table-striped">
                                <?php 
                                if(count($follows["group"])>0){
                                    foreach($follows["group"] as $leader){
                                        if($leader->leaderid!=$cleader->leaderid){
                                            $switchchecked = ($leader->unfollow=="") ? "checked" : "";
                                            echo '<tr>';
                                                echo '<td>'.$leader->name.'<br><small>'.$leader->city.', '.$leader->region.', '.$leader->country.'</small></td>';
                                                echo '<td class="text-right">';
                                                echo '<input type="checkbox" class="switch-follow-presenter" data-presenterid="'.$leader->leaderid.'"  data-on-text="Afficher" data-on-color="success" data-off-color="default"  data-off-text="Masquer"  data-size="small" '.$switchchecked.'>';
                                                echo '</td>';
                                                echo '</tr>'; 
                                        }
                                    }
                                }else{
                                    echo '<tr>';
                                    echo '<td class="text-center"><strong>Il n\'y a aucun présentateur public pour ce groupe.</strong></td>';
                                    echo '</tr>'; 
                                }
                                
                                ?>
                            </table>
                            <input type="hidden" value="<?=$cleader->leaderid;?>" id="presenter-curleader"/>
                            <input type="hidden" value="<?=sha1($cleader->leaderid.'Pres3nt3r1sN1ce');?>" id="presenter-token"/>
                            
                        </div>
                    </div>
                    
                </div>
                    
                </div>
            </div>
        </div>
    </div>
    </div>
</div>






