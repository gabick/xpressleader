<?php 
$lstmeetings = $bigbluebutton->getMeetingList();
?>

	    <h3><span class="text-primary">Online</span> Conferences</h3>
		<table class="table table-striped">
			<tr>
				<th>MeetingID</th>
				<th>Name</th>
				<th>Moderators</th>
				<th>Participants</th>
				<th>Running</th>
			</tr>
	    <?php 
	    foreach($lstmeetings as $meeting){

			echo '<tr>';
			echo  '<td>'.$meeting->meetingID.'</td>';
		    echo  '<td>'.$meeting->meetingName.'</td>';
		    echo  '<td>'.($meeting->participantCount-$meeting->listenerCount).'</td>';
		    echo  '<td>'.$meeting->listenerCount.'</td>';
		    echo  '<td>'.$meeting->running.'</td>';
		    echo '<tr>';
		    
	    }
	    ?>
	    
	    </table>
