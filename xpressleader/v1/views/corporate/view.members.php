<?php
$lbltitle   = (_LANG=="fr") ? 'Compte corporatif <span class="text-primary">membres</span>' : 'Corporative <span class="text-primary">members</span> list';

$key 		= (isset($_GET["key"]) && $_GET["key"]<>"") ? true : false;
$members 	= $corporate->listLeaders($cleader->corpotoken, $key);
?>


	
		<?
		$stats = $corporate->getSummaries($cleader->corpotoken);
		?>
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-xpress panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"EN ATTENTE":"PENDINGS";?></h3>
					</div>
					<div class="panel-body text-center">
						<h2 style="line-height: 10px;"><?=$stats['pending'];?></h2>
					</div>
				</div>
			</div>
			<div class="col-lg-3 ">
				<div class="panel panel-xpress panel-primary">
					<div class="panel-heading panel-heading-blue">
						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"SURVOLS":"OVERVIEWS";?></h3>
					</div>
					<div class="panel-body text-center">
						<h2 style="line-height: 10px;"><?=$stats['meeting'];?></h2>
					</div>
				</div>
			</div>
			<div class="col-lg-3 ">
				<div class="panel panel-xpress panel-success">
					<div class="panel-heading panel-heading-green">
						<h3 class="panel-title text-center"><?=(_LANG=="fr")?"INTÉRÊTS":"INTERESTS";?></h3>
					</div>
					<div class="panel-body text-center">
						<h2 style="line-height: 10px;"><?=$stats['interest'];?></h2>
					</div>
				</div>
			</div>
			<div class="col-lg-3 ">
				<div class="panel panel-xpress panel-danger">
					<div class="panel-heading panel-heading-red">
						<h3 class="panel-title text-center">CLIENTS</h3>
					</div>
					<div class="panel-body text-center">
						<h2 style="line-height: 10px;"><?=$stats['client'];?></h2>
					</div>
				</div>
			</div>
		</div>
	
	
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="page-title">
							<?=$lbltitle;?>
						</div>
					</div>
					<div class="col-lg-6">
						<form method="get" action="" enctype="multipart/form-data">
						<div class="input-group input-group-md">
							<input type="text" name="key" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">Search!</button>
							</span>
						</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h4>MEMBERS : <?=count($members);?> actives</h4>
						
						<div class="table-responsive">
						<table class="table table-striped table-hover" style="font-size:0.9em;">
				            <tr>
				                <th></th>
				                <th>Id</th>
				                <th>Nom</th>
				                <th>LastLogin</th>
				                <th width="80px">Survols</th>
				                <th width="80px">Intérêts</th>
				                <th width="80px">Adhésions</th>
				                
				                <th></th>
				            </tr>
				            <?php 
				            $stats = $corporate->getCorpoStats($cleader->corpotoken);
				            foreach($members as $leader){
				                if($leader->leaderid == $cleader->leaderid) continue;
				                
				                $ispresenter 	= ($leader->xmeetleader==1) ? '<br><span style="color:#ccc;">Presenter</span>' : '';
				                
				                if($leader->chargetry == 2){
				                	$lblpro = '<span class="label label-warning" style="display:inline-block;width: 100%;">suspended</span>';
				                }elseif($leader->ispro==1) $lblpro = '<span class="label label-primary" style="display:inline-block;width: 100%;">PRO</span>';
				                else $lblpro = '';
				                
				                
				                $stat["overviews"]  = (isset($stats[$leader->leaderid][1])) ? $stats[$leader->leaderid][1] : 0;
				                $stat["interest"]   = (isset($stats[$leader->leaderid][2])) ? $stats[$leader->leaderid][2] : 0;
				                $stat["client"]     = (isset($stats[$leader->leaderid][3])) ? $stats[$leader->leaderid][3] : 0;
				                
				                $profile = ($leader->profileurl != null) ? '<img src="'.$leader->profileurl.'" alt="Profile member" class="img img-responsive pull-left img-rounded" style="max-height: 50px;margin-right:10px;"/>' : '';
				                
				                echo '<tr>';
				                echo '<td><strong>'.$lblpro.'</strong></td>';
				                echo '<td class="hidden-xs">'.$leader->leaderid.'</td>';
				                echo '<td>'.$profile.$leader->name.'<br><small>'.$leader->city.', '.$leader->region.', '.$leader->country.'</small></td>';
				                echo '<td>'.date("Y-m-d",strtotime($leader->datelastlogin)).'</td>';
				                echo '<td><span class="label label-primary" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["overviews"].'</span></td>';
				                echo '<td><span class="label label-success" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["interest"] .'</span></td>';
				                echo '<td><span class="label label-danger" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["client"].'</span></td>';
				                
				                echo '<td class="text-right">
				                			<a href="/'._LANG.'/corporate/members/'.$leader->leaderid.'" type="button" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span> VIEW</a>
				                			<a href="/'.$leader->leaderid.'" target="_BLANK" type="button" class="btn btn-xs btn-default"><i class="fa fa-globe" aria-hidden="true"></i></a>
				                	  </td>';
				                echo '</tr>';
				            }
				            ?>
				        </table>
				        </div>
				        
					</div>
				</div>
			</div>
		</div>
		
