<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-3" style="background-color: rgba(0,0,0,0.5); color:#ffffff;">
        	<div class="">
            <h2>CORPORATIVE</h2>
            <ul class="nav nav-pills nav-stacked transmenu">
                <li role="presentation" class="<?=(_VIEW=="members"||_VIEW=="")?"active":"";?>"><a href="/<?=_LANG;?>/corporate/members"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Liste des membres</a></li>
                <li role="presentation" class="<?=(_VIEW=="livebbb")?"active":"";?>"><a href="/<?=_LANG;?>/corporate/livebbb"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Console des conférences</a></li>
                <li role="presentation" class="<?=(_VIEW=="fasttrack")?"active":"";?>"><a href="/<?=_LANG;?>/corporate/fasttrack"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Gestion FastTrack</a></li>
            </ul>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="whiteboard">
            <?php 
            if(_VIEW=="" || _VIEW=="members") {
                if(_ID != "") include_once SITEPATH."/views/corporate/view.getmember.php";
                else include_once SITEPATH."/views/corporate/view.members.php";
  			}elseif(_VIEW=="livebbb") include_once SITEPATH."/views/corporate/view.livebbb.php";
  			elseif(_VIEW=="fasttrack") include_once SITEPATH."/views/corporate/view.fasttrack.php";
  			else include_once SITEPATH."/views/corporate/view.members.php";
            ?>
            </div>
        </div>
    </div>
        
</div>