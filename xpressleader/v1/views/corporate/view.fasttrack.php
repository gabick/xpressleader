<div class="row">
	<div class="col-lg-12">
		
		<?php
		$teams = $fasttrack->GetTeamsList();
                    if($teams){
                        echo '<table class="table">';
                        echo '<tr>';
                        echo '<th>Nom</th>';
                        echo '<th>Capitaine</th>';
                        echo '<th>Membres</th>';
                        echo '<th></th>';
                        echo '</tr>';
                        
                        foreach($teams as $team){

                            echo '<tr>';
                            echo '<td><strong>'.$team->name.'</strong></td>';
                            echo '<td>'.$team->cpt_name.'</td>';
                            echo '<td>'.$fasttrack->getCountByTeam($team->id).'</td>';
                            echo '<td class="text-right"></td>';
                            echo '</tr>';
                            
                            echo '<tr><td colspan="4">';
                            $players = $fasttrack->getAcceptedPlayers($team->id);
                            if($players){
                                 echo '<table class="table table-bordered" style="font-size:0.8em;">';
                                foreach($players as $player){
                                    echo '<tr>';
                                    echo '<td>'.$player->leadername.'</td>';
                                    echo '</tr>';
                                }
                                  echo '</table>';
                            }
                            echo '</td></tr>';
                        }
                        echo '</table>';
                    }
		?>
	</div>
</div>