<?php
$lbllogin           = (_LANG=="fr") ? "Connectez-vous à votre compte" : "Login to access your account";
$lblusrph           = (_LANG=="fr") ? "Adresse courriel" : "Email address";
$lblpwph            = (_LANG=="fr") ? "Mot de passe" : "Password";
$lblsigninbtn       = (_LANG=="fr") ? "CONNEXION" : "ACCESS MY ACCOUNT";
$lblforgotpass      = (_LANG=="fr") ? "Oublié votre mot de passe?" : "Forgot your password?";

$lblpartnertitle    = array("fr"=>"Déjà partenaire?", "en"=>"Partner already?");
$lblpartnerdesc     = array("fr"=>"Vous êtes déjà partenaire et vous désirez avoir votre page web comme celle-ci ?", "en"=>"You are already a partner and want a website like me ?");
$lblsignup          = array("fr"=>"Joindre mon équipe", "en"=>"Join my team");
?>

<div class="homepage">
<div class="container">
	<div class="row">
		<div class="col-lg-7"></div>
		<div class="col-lg-5">
			<div id="loginform">
			<form method="post" action="/<?=_LANG;?>/login" role="form">
				<h3><?=$lbllogin;?></h3>
			    <div class="form-group">
			        <label for="txtusr" class="control-label"><?=$lblusrph;?>*</label>
			        <input type="text" class="form-control" name="txtusr" id="txtusr" placeholder="">
			    </div>
			    <div class="form-group">
			        <label for="txtpwd" class="control-label"><?=$lblpwph;?>*</label>
			        <input type="password" class="form-control" name="txtpwd" id="txtpwd" placeholder="">
			    </div>
			    <div class="text-right">
					<button type="submit" name="sublogin" class="btn btn-primary btn-block btn-lg"><?=$lblsigninbtn;?></button>
					<br/>
				</div>
				<div class="text-center">
					<a href="/<?=_LANG;?>/forgotpassword"><?=$lblforgotpass;?></a>
				</div>
			    <input type="hidden" name="navbaropen" value=""/>
			    <input type="hidden" name="validate" value="true"/>
			    <input type="hidden" name="subform" value="signin"/>
			</form>
            <div>
                <h3 class="text-left"><?=$lblpartnertitle[_LANG];?></h3>
                <p style="text-align: justify;"><?=$lblpartnerdesc[_LANG];?></p>
                <a style="color: white" href="//xpressleader.com/<?=_LANG;?>/<?=_CONTROLLER;?>signup" class="btn btn-primary btn-block btn-lg btn-xp-black"><?=$lblsignup[_LANG];?></a>
            </div>
			</div>
		</div>
	</div>
</div>
</div>
