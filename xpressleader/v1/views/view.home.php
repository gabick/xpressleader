<?php
$lbltitleaddpro     = array("fr"=>"Invitation pour voir une session d'information (webinaire)", "en"=>"Invitation to see a information session (Webinar)");
$linkvideo     		= array("fr"=>"https://player.vimeo.com/video/174069839", "en"=>"https://player.vimeo.com/video/176696843");
$lbladdprodesc      = array("fr"=>"Inscrivez votre prospect à une session d'information! Vous pourrez choisir une session parmi toutes nos conférences . N'oubliez pas de vous inscrire vous-même aussi pour l'accompagner.", "en"=>"Register your prospect for an information session! You will be able to choose a session among all our conferences. Do not forget to register yourself too to accompany him.");
$lblprosname		= array("fr"=>"Nom du prospect", "en"=>"Prospect name");
$lblprosmail		= array("fr"=>"Courriel du prospect", "en"=>"Email address");
$lbladdprobtn		= array("fr"=>"Inscription", "en"=>"Registration");

$lblfbtitle			= array("fr"=>"ÇA BOUGE SUR XPRESSLEADER CES TEMPS-CI !", "en"=>"IT MOVES ON XPRESSLEADER THESE TIMES !");
//$lblfbdesc			= array("fr"=>"Inscrivez-vous à notre robot <strong style=\"color: #0086fc;\">Facebook Messenger</strong> et recevez par messagerie les événements importants de votre compte xpress leader!", "en"=>"Subscribe to our <strong style=\"color: #0086fc;\">Facebook Messenger</strong> robot and get notified whenever important events occurs from your Xpressleader account.");
//$lblfbbtn			= array("fr"=>"ACTIVER MAINTENANT!", "en"=>"ACTIVATE NOW!");

//$lblfbmbody1		= array("fr"=>"Vous êtes sur le point d'activer notre robot <strong style=\"color: #0086fc;\">Facebook Messenger</strong>. Une fois inscrit, il vous enverra automatiquement par messagerie, des alertes et des infos lors d'événements importants de votre compte Xpress leader.", "en"=>"Your are about to activate our <strong style=\"color: #0086fc;\">Facebook Messenger</strong> robot. Once you subscribe, you will receive by messenger any alerts, informations or notices about important events on your xpressleader account. Note that you will be able to unsubscribe at any moment after this activation.");
//$lblfbmbody2		= array("fr"=>"<strong>Cliquez</strong> sur le bouton au bas pour vous inscrire et activer notre Robot Xpress.", "en"=>"<strong>Click</strong> on the button below to activate the robot.");
//$lblfbmclose		= array("fr"=>"Je ne suis pas intéressé.", "en"=>"I'm not interested");
$lblpublicptitle	= array("fr"=>"Partager le lien de votre page publique", "en"=>"Share your public website link");
$lblpublicpdesc 	= array("fr"=>"Copier le lien de votre page publique afin de le partager dans vos annonces ou sur vos réseaux sociaux. Une vidéo d'introduction invitant les gens à faire le test y sera présentée dès l'ouverture de la page. Les gens intéressés à faire le test pourront entrer leur nom et leur courriel afin de recevoir automatiquement le lien pour faire le test.", "en"=>"Copy the link of your public page to share it in your ads or on your social networks. An introductory video inviting people to take the test will be presented at the opening of the page. People interested in taking the test will be able to enter their name and email to automatically receive the link for the test.");
$lblpublicplink		= array("fr"=>"Votre lien à partager", "en"=>"Your link to share");

$data = array("api_key" => "deb5db6a1819a333c981f7aeaef2b34778dc33385a547fe3eb32d4fce4b6147e", "webinar_id" => "b7b86b05c5", "real_dates"=>1);
$data_string = json_encode($data);

$ch = curl_init('https://webinarjam.genndi.com/api/everwebinar/webinar');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
);
curl_setopt($ch, CURLOPT_TIMEOUT, 5);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);
$result = json_decode($result);
$webinar = $result->webinar;

?>

<div style="background-color: rgba(0,0,0,0.6);width:100%;">
	<div class="container">
	<div class="row" style="color: #eee;">
	    <div class="col-md-7">
	        <div class="embed-responsive embed-responsive-16by9">
	            <iframe class="embed-responsive-item nopadding" src="<?=$linkvideo[_LANG];?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	        </div>
	    </div>

	    <div class="col-md-5">
	        <h2 style="line-height: 40px;margin-top: 10px;"><?=$lbltitleaddpro[_LANG];?></h2>
	        <p><?=$lbladdprodesc[_LANG];?></p>
            <a href="<?=$webinar->registration_url;?>" target="_blank" class="btn btn-danger btn-block btn-lg"><?=$lbladdprobtn[_LANG];?></a>
	    </div>
	</div>
	</div>
</div>

<div class="container">
	<div class="whiteboard">

	<div class="row">
		<div class="col-md-4">
			<div class="fb-follow" data-href="https://www.facebook.com/xpressleader" data-layout="standard" data-size="large" data-show-faces="false"></div>
			<div class="fb-page" data-href="https://www.facebook.com/xpressleader/" data-tabs="timeline,messages" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/xpressleader/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/xpressleader/">Xpressleader.com</a></blockquote></div>
		</div>
	    <div class="col-md-8">

	    	<?php
		if(($cleader->fbuserid == null || $cleader->fbstate != 1)){
			echo '

					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-facebook-square" aria-hidden="true"></i> '.$lblfbtitle[_LANG].'</h3>
						</div>
						<div class="panel-body">
							<p>'.$lblfbdesc[_LANG].'</p>
							
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#messengerbot">'.$lblfbbtn[_LANG].'</button>
						
						</div>
					</div>
				';

			echo '<div id="messengerbot" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" style="background-color: #0084FE;padding:0;">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"><img src="//media.xpressleader.com/images/Facebook-messenger.png" alt="Facebook Messenger" class="img img-responsive center-block" style="max-height: 200px;"/></h4>
					</div>
					<div class="modal-body">
						<div class="modal-desc">
							<p>
								'.$lblfbmbody1[_LANG].'
							</p>
							<p>
								'.$lblfbmbody2[_LANG].'
							</p>

							<div class="fb-send-to-messenger" messenger_app_id="1625156927777036" page_id="962555527186808" data-ref="'.$cleader->leaderid.'" color="blue" size="xlarge"></div>
						</div>	 
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">'.$lblfbmclose[_LANG].'</button>
					</div>
				</div>
			</div>
		</div>';
		}
		?>



	    	<h3><?=$lblpublicptitle[_LANG];?></h3>
	    	<p><?=$lblpublicpdesc[_LANG];?></p>
	    	<div class="form-horizontal">
	    		<div class="form-group">
    			<label class="col-sm-3 control-label"><?=$lblpublicplink[_LANG];?></label>
	    			<div class="col-sm-9">
    					<input type="text" value="https://xpressleader.com/fr/<?=$cleader->alias;?>" class="form-control"/>
    				</div>
	    		</div>
	    	</div>



		    <?php
			$lblname    		= array("fr"=>"Nom","en"=>"Name");
			$lblmail    		= array("fr"=>"Courriel","en"=>"Email");
			$lblphone    		= array("fr"=>"Téléphone","en"=>"Phone");
			$xlooklist 			= $prospect->_list($cleader->leaderid, 0, "", 2);
			if($xlooklist){
			?>
	        <h2 class="text-danger">Prospects à contacter!</h2>
	        <p>Les prospects suivants ont regardés le survol de votre entreprise et ils sont intéressés. Vous devez les contacter dans les plus brefs délais.</p>
	        <div class="table-responsive">
	        	<table class="table table-striped table-condensed">
					<tr>
						<th>Date</th>
						<th><?=$lblname[_LANG];?></th>
						<th><?=$lblmail[_LANG];?></th>
						<th><?=$lblphone[_LANG];?></th>
						<th></th>
					</tr>

					<?php
						foreach($xlooklist as $xlook){

							echo '<tr>';
							echo '<td>'.$xlook->datecreation.'</td>';
							echo '<td>'.$xlook->name.'</td>';
							echo '<td>'.$xlook->mail.'</td>';
							echo '<td>'.$xlook->phone.'</td>';
							echo '<td class="text-right"><a data-toggle="modal" href="#convertprospect'.$xlook->id.'"class="btn btn-sm btn-success">CONVERTIR</span></a>
							<a data-toggle="modal" href="#delprospect'.$xlook->id.'" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>';
							echo '</tr>';
							?>

							<div class="modal fade" id="convertprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="convertprospect<?=$xlook->id;?>" aria-hidden="true">
						    	<div class="modal-dialog">
						    	    <form action="" method="post" role="form" enctype="multipart/form-data">
							    	<div class="panel panel-primary">
							    		<div class="panel-heading"><h4>Désirez-vous convertir <strong><?=$xlook->name;?></strong> en client ?</h4></div>
							    		<div class="panel-body">
											<div class="list-group" style="font-size: 0.9em;">
												<p>
													Votre prospect est maintenant client ? Vous devez le convertir en client afin qu'il puisse recevoir un courriel avec le cadeau de bienvenue de votre part (livre de recettes du mieux-être).
												</p>
												<div class="list-group-item">
													<h4 class="list-group-item-heading">
														<div class="checkbox">
															<label><input type="radio" name="chkinterest" value="1" /> <strong>Client</strong></label>
														</div>
													</h4>
													<p class="list-group-item-text">S'intéresse surtout aux produits et référe un ami de temps en temps</p>
												</div>

												<div class="list-group-item">
													<h4 class="list-group-item-heading">
														<div class="checkbox">
															<label><input type="radio" name="chkinterest" value="2" /> <strong>Client bâtisseur (Temps partiel)</strong></label>
														</div>
													</h4>
													<p class="list-group-item-text">Créer un revenu supplémentaire, s'investir à temps partiel.</p>
												</div>

												<div class="list-group-item">
													<h4 class="list-group-item-heading">
														<div class="checkbox">
															<label><input type="radio" name="chkinterest" value="3" checked="checked" /> <strong>Client bâtisseur (Temps Plein)</strong></label>
														</div>
													</h4>
													<p class="list-group-item-text">Créer un revenu supplémentaire, s'investir à temps plein. </p>
												</div>
											</div>
										</div>
										<div class="panel-footer text-right">
											<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
											<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
						    			    <input type="hidden" name="validate" value="true"/>
								            <input type="hidden" name="subform" value="convertprospect"/>
											<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
		                                    <button type="submit" name="subfuupdate" class="btn btn-danger">Convertir !</button>
										</div>
							    	</div>
							    	</form>
						    	</div>
						    </div>


						    <div class="modal fade" id="delprospect<?=$xlook->id;?>" tabindex="-1" role="dialog" aria-labelledby="delprospect<?=$xlook->id;?>" aria-hidden="true">
						    	<div class="modal-dialog">
							    	<div class="panel panel-danger">
							    		<div class="panel-heading panel-heading-red text-center"><h3>Supprimer le prospect <i><?=$xlook->name;?></i></h3></div>
							    		<div class="panel-body">
											 <strong><?=$xlook->name;?> - <?=$xlook->mail;?></strong> n'est pas intéressé et je veux supprimer ce prospect définitivement.
										</div>
										<div class="panel-footer text-right">
											<form action="" method="post" enctype="multipart/form-data">
												<input type="hidden" name="leaderid" value="<?=$cleader->leaderid;?>"/>
												<input type="hidden" name="prospectid" value="<?=$xlook->id;?>"/>
							    			    <input type="hidden" name="validate" value="true"/>
									            <input type="hidden" name="subform" value="removeprospect"/>

												<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
												<button type="submit" name="subdelprospect" class="btn btn-danger">SUPPRIMER</button>
											</form>
										</div>
							    	</div>
						    	</div>
						    </div>
					<?php
						}
					?>
				</table>

	        </div>
	        <?php
		} else echo '<br><br>';
		?>
	    </div>
	</div>

	</div>
</div>
<?php
$fblang = (_LANG=="fr") ? "fr_CA" : "en_US";
$jscript .= '
    window.fbAsyncInit = function() {
      FB.init({
        appId: "1625156927777036",
        xfbml: true,
        version: "v2.7"
      });
      
      
	FB.Event.subscribe("send_to_messenger", function(e) {
		if(e.event==="clicked") $(\'#messengerbot\').modal(\'hide\');
	});

    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) { return; }
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/'.$fblang.'/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));

';
