<?php 
$lbltitle 	= array("fr"=>"COMPTE PRO", "en"=>"PRO ACCOUNT");
$lblmygroup = array("fr"=>"Modifier mon groupe", "en"=>"Modify my group");
$lblmembers = array("fr"=>"Liste des membres", "en"=>"Members list");

$curgroup = $account->getGroup($cleader->groupleaderid);
?>
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-3" style="background-color: rgba(0,0,0,0.5); color:#ffffff;">
            <h2><?=$lbltitle[_LANG];?></h2>
            <ul class="nav nav-pills nav-stacked transmenu">
                <li role="presentation" class="<?=(_VIEW=="mygroup"||_VIEW=="")?"active":"";?>"><a href="/<?=_LANG;?>/pro/mygroup"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <?=$lblmygroup[_LANG];?></a></li>
                <li role="presentation" class="<?=(_VIEW=="members")?"active":"";?>"><a href="/<?=_LANG;?>/pro/members"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <?=$lblmembers[_LANG];?></a></li>
                <li role="presentation" class="<?=(_VIEW=="livebbb")?"active":"";?>"><a href="/<?=_LANG;?>/pro/livebbb">Console des conférencessss</a></li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-md-12">
                    <div style="min-height: 120px;background-color: #337AB7;padding:10px;">
                        <div class="pull-left">
                            <div style="line-height: 60px;font-size: 2em;color:#ffffff;font-weight: 700;text-shadow: 1px 1px 1px #000;text-transform: uppercase;">
                                <i class="fa fa-users" aria-hidden="true"></i> <?=$curgroup->name;?>
                            </div>
                            <div style="color: #eeeeee;"><?=$curgroup->firstname;?> <?=$curgroup->lastname;?></div>
                        </div>
                        
                        <div class="pull-right text-right" style="color: #eeeeee;">
                            Date de création : <span style="color:#ffffff;"><?=date('Y-m-d',strtotime($curgroup->datecreation));?></span><br>
                            
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            if(_VIEW=="" || _VIEW=="mygroup") {
                include_once SITEPATH."/views/pro/view.mygroup.php";
  			}elseif(_VIEW=="members") { include_once SITEPATH."/views/pro/view.members.php"; 
  			}elseif(_VIEW=="livebbb") { include_once SITEPATH."/views/pro/view.livebbb.php";
  			}else include_once SITEPATH."/views/pro/view.mygroup.php";
            ?>
        </div>
    </div>
        
</div>