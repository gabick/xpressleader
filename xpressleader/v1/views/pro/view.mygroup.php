<?php 
$lbltitle		= array("fr"=>"Mon groupe","en"=>"My group");
$group = $account->getProGroup($cleader->leaderid);
?>

<div class="whiteboard">
    <div class="page-title">
        <h3><?=$lbltitle[_LANG];?>  <span class="text-primary"><?=$group->name;?></span></h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                Hello world
            </p>
        </div>
    </div>
</div>