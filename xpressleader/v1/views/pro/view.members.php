<?php 
$lbltitle		= array("fr"=>"Liste des <span class=\"text-primary\">membres</span>","en"=>"Manage <span class=\"text-primary\">my group</span>");
$lblpresenter   = array("fr"=>"Présentateur", "en"=>"Presenter");
$lblprivate     = array("fr"=>"Privé", "en"=>"Private");
$lblname        = array("fr"=>"Nom", "en"=>"Name");
$lbloverview    = array("fr"=>"Survols", "en"=>"Overviews");
$lblinterest    = array("fr"=>"Intérêts", "en"=>"Interests");
$lblclient      = array("fr"=>"Clients", "en"=>"Clients");


?>

<div class="whiteboard">
    
    <div class="page-title">
        <h3><?=$lbltitle[_LANG];?></h3>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th width="100px"><?=$lblpresenter[_LANG];?></th>
                    <th><?=$lblname[_LANG];?></th>
                    <th width="80px"><?=$lbloverview[_LANG];?></th>
                    <th width="80px"><?=$lblinterest[_LANG];?></th>
                    <th width="80px"><?=$lblclient[_LANG];?></th>
                </tr>
                
                <?php 
                foreach($account->listLeaders($cleader->groupid) as $leader){
                    if($leader->leaderid == $cleader->leaderid) continue;
    								
                    $stat["overviews"]  = 0;
                    $stat["interest"]   = 0;
                    $stat["client"]     = 0;
                    
                    echo '<tr>';
                    echo '<td>';
                        $switchchecked = ($account->isGroupPresenter($leader->leaderid,$leader->groupid)) ? "checked" : "";
                        echo '<input type="checkbox" class="switch-group-presenter" data-presenterid="'.$leader->leaderid.'"  data-on-text="Public" data-on-color="success" data-off-color="default" data-off-text="'.$lblprivate[_LANG].'"  data-size="mini" '.$switchchecked.'>';
                        echo '<input type="hidden" value="'.$cleader->groupid.'" id="group-presenter-groupid"/>';
                        echo '<input type="hidden" value="'.sha1($cleader->groupid.'Pres3nt3r1sN1ce').'" id="group-presenter-token"/>';
                    echo '</td>';
                    echo '<td>'.$leader->name.'<br><small>'.$leader->city.', '.$leader->region.', '.$leader->country.'</small></td>';
                    echo '<td><span class="label label-primary" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["overviews"].' </span></td>';
                    echo '<td><span class="label label-success" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["interest"] .'</span></td>';
                    echo '<td><span class="label label-danger" style="font-size: 1em;display:inline-block;width: 100%;">'.$stat["client"].'</span></td>';
                    echo '</tr>';
                }
                ?>
            </table>
            </div>
        </div>
    </div>
</div>