<?php
$lbllogin           = (_LANG=="fr") ? "Oublié mon mot de passe?" : "Forgot your password ?";
$lblusrph           = (_LANG=="fr") ? "Adresse courriel" : "Email address";
$lblsigninbtn       = (_LANG=="fr") ? "Envoyer votre mot de passe" : "Recover password";
$lblforgotpass      = (_LANG=="fr") ? "Connexion à votre compte" : "Log in your account";
?>
<div class="container">
	<div class="row">
		<div class="col-lg-7"></div>
		<div class="col-lg-5">
			<div id="loginform">
			<form method="post" action="/<?=_LANG;?>/forgotpassword" role="form">
				<h3><?=$lbllogin;?></h3>
			    <div class="form-group">
			        <label for="txtusr" class="control-label"><?=$lblusrph;?>*</label>
			        <input type="text" class="form-control" name="txtusr" id="txtusr" placeholder="">
			    </div>
			    <div class="text-right">
					<button type="submit" name="sublogin" class="btn btn-primary btn-block btn-lg"><?=$lblsigninbtn;?></button>
					<br/>
				</div>
				<div class="text-center">
					<a href="/<?=_LANG;?>/login"><?=$lblforgotpass;?></a>
				</div>
			    <input type="hidden" name="navbaropen" value=""/>
			    <input type="hidden" name="validate" value="true"/>
			    <input type="hidden" name="subform" value="forgotpassword"/>
			</form>
			</div>
		</div>
	</div>
</div>