$(function () {

	if ($("#teaserurl").length) {
		jwplayer.key = "Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";

		var playerInstance = jwplayer('jwdynvideo');
		playerInstance.setup({
			file: $("#teaserurl").val(),
			image: $("#teaserimg").val(),
			width: "100%",
			aspectratio: "16:9",
			controls: tease_controls,
			autostart: tease_autoplay,
			logo: {
				hide: true
			}
		});
		//playerInstance.play();
	}


	if ($("#overviewurl").length) {
		jwplayer.key = "Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";

		var playerInstance = jwplayer('jwdynvideo');
		playerInstance.setup({
			file: $("#overviewurl").val(),
			image: $("#overviewimg").val(),
			width: "100%",
			aspectratio: "16:9",
			controls: true,
			autostart: true,
			logo: {
				hide: true
			}
		});

		//playerInstance.play();
	}


	// if ($('#jwtest').length > 0) {
	// 	jwplayer.key = "Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";
	// 	var jwoverview = jwplayer('jwtest');
	// 	var parentWidth = document.getElementById("player_wrapper") !== null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwtest").parentNode.offsetWidth;
	// 	var controls = false;
	// 	console.log(parentWidth);
	// 	if (parentWidth <= 720) controls = true;
	// 	jwoverview.setup({
	// 		file: $('#jwtest').data("url"),
	// 		width: parentWidth - 33,
	// 		height: parentWidth * 720 / 1280,
	// 		stretching: "exactfit",
	// 		controls: controls,
	// 		autostart: true,
	// 		logo: {
	// 			hide: true
	// 		}
	// 	});
	//
	// 	jwoverview.onReady(function () {
	// 		jwoverview.play();
	// 	});
	//
	// 	// Responsive player resizing after initialization
	// 	jQuery(window).resize(function () {
	// 		var parentWidth = document.getElementById("player_wrapper") != null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
	// 		var calculatedHeight = parentWidth * 720 / 1280;
	// 		if (jwoverview.getWidth() != parentWidth) {
	// 			jwoverview.resize(parentWidth - 33, calculatedHeight);
	// 		}
	// 	});
	//
	// 	$("#playpause").bind("click", function () {
	// 		jwoverview.play();
	// 		$(this).html(function (i, text) {
	// 			return $(this).text() === " Play video" ? "<i class=\"fa fa-pause\" aria-hidden=\"true\"></i> Pause video" : "<i class=\"fa fa-play\" aria-hidden=\"true\"></i> Play video";
	// 		})
	// 	});
	//
	// 	var testRoutes = [{
	// 		'url': 'https://player.vimeo.com/external/260594102.hd.mp4?s=8853bf9c639c6f5b5286640d5b8d2821b09defd5&profile_id=174',
	// 		'answerat': 10
	// 	},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260594095.hd.mp4?s=3efbb87a95380c9f6e2f159ac0f59d7de670e48f&profile_id=174',
	// 			'A': 0,
	// 			'B': 0,
	// 			'C': 0,
	// 			'D': 0,
	// 			'answerat': 7
	// 		},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260594088.hd.mp4?s=0273b26bb80bffcfb962a90169c64a892d4b0217&profile_id=174',
	// 			'A': 0,
	// 			'B': 0,
	// 			'C': 0,
	// 			'answerat': 16
	// 		},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260594075.hd.mp4?s=5ec36c683909712ae3794c0433d31f22bba559f9&profile_id=174',
	// 			'A': 0,
	// 			'B': 1,
	// 			'C': 0,
	// 			'answerat': 19
	// 		},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260594071.hd.mp4?s=cd406edef805af4bf0e74a3a13c00fe3076c43ab&profile_id=174',
	// 			'A': 0,
	// 			'B': 0,
	// 			'C': 0,
	// 			'answerat': 16
	// 		},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260594064.hd.mp4?s=948024d40db81175e6a2581c5fdfa692b0253843&profile_id=174',
	// 			'A': 0,
	// 			'B': 0,
	// 			'C': 0,
	// 			'answerat': 12
	// 		},
	// 		{
	// 			'url': 'https://player.vimeo.com/external/260593850.hd.mp4?s=ef9f3d9f4a7ce3649d73462ace624c1e7bfab9e9&profile_id=174',
	// 			'A': 0,
	// 			'B': 0,
	// 			'C': 0,
	// 			'answerat': 10
	// 		},
	// 	];
	// 	$(".btnanswer").bind("click", function () {
	// 		$('.btncontinue').hide();
	// 		$('.btnanswer').attr("disabled", "disabled");
	// 		$('#panContinue').addClass("hide");
	// 		$('#panAnswers').removeClass("hide");
	// 		var answer = $(this).data("answer");
	// 		var url = testRoutes[parseInt($('#index').val())].url;
	// 		var ansat = testRoutes[parseInt($('#index').val())].answerat;
	// 		$('#points').val(parseInt($('#points').val()) + testRoutes[parseInt($('#index').val())][answer]);
	// 		$('#index').val(parseInt($('#index').val()) + 1);
	// 		console.log($('#points'));
	// 		if ($('#index').val() == 7) {
	// 			$('.btnanswer').each(function () {
	// 				$(this).addClass("hide");
	// 			});
	// 			$('#panInteret').removeClass("hide");
	// 			$.ajax({
	// 				url: "https://app.xpressleader.com/fr/call/testresults?results=" + $('#points').val() + "&token=" + $("#token").val() + "&lang=" + $("#lang").val() + "&leaderid=" + $("#leaderid").val(),
	// 				type: "GET",
	// 				cache: false,
	// 				dataType: 'json',
	// 				timeout: 5000
	// 			});
	// 			console.log("https://app.xpressleader.com/fr/call/testresults?results=" + $('#points').val() + "&token=" + $("#token").val() + "&lang=" + $("#lang").val() + "&leaderid=" + $("#leaderid").val());
	// 		}
	// 		jwoverview.setup({
	// 			file: url,
	// 			width: parentWidth - 33,
	// 			height: parentWidth * 720 / 1280,
	// 			stretching: "exactfit",
	// 			controls: controls,
	// 			autostart: true,
	// 			logo: {
	// 				hide: true
	// 			},
	// 			events: {
	// 				onComplete: function () {
	// 					clearInterval(interval);
	// 					jwoverview.play();
	// 				},
	// 				onTime: function () {
	// 					if (jwoverview.getPosition() >= ansat) {
	// 						$('.btnanswer').each(function () {
	// 							if ($(this).data("answer") in testRoutes[parseInt($('#index').val())]) {
	// 								$(this).removeAttr("disabled");
	// 							}
	// 						});
	// 					}
	// 				}
	// 			}
	// 		});
	// 	});
	// 	jwoverview.play();
	// }

	if ($('.custom-vimeo-test').length > 0) {
		var testRoutes = [
			// {
			// 	'url': 'https://player.vimeo.com/external/260594102.hd.mp4?s=8853bf9c639c6f5b5286640d5b8d2821b09defd5&profile_id=174',
			// 	'answerat': 10,
			// 	'step': 0
			// },
			{
				'fr': 'https://player.vimeo.com/video/260594102?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351673?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'D': 0,
				'answerat': 7,
				'step': 1
			},
			{
				'fr': 'https://player.vimeo.com/video/260594095?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351636?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'answerat': 16,
				'step': 2
			},
			{
				'fr': 'https://player.vimeo.com/video/260594088?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351599?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'answerat': 19,
				'step': 3
			},
			{
				'fr': 'https://player.vimeo.com/video/260594075?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351498?autoplay=1',
				'A': 0,
				'B': 1,
				'C': 0,
				'answerat': 16,
				'step': 4
			},
			{
				'fr': 'https://player.vimeo.com/video/260594071?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351423?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'answerat': 12,
				'step': 5
			},
			{
				'fr': 'https://player.vimeo.com/video/260594064?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351460?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'answerat': 10,
				'step': 6
			},
			{
				'fr': 'https://player.vimeo.com/video/260593850?autoplay=1',
				'en': 'https://player.vimeo.com/video/271351399?autoplay=1',
				'A': 0,
				'B': 0,
				'C': 0,
				'answerat': 10,
				'step': 7
			}
			// Cannot add value to last element @TO CORRECT
		];

		$(".btnanswer").bind("click", function (e) {
			e.preventDefault();
			var elementClicked = $(this);
			var testStepInput   = $('#index');
			var testStepValue   = testStepInput.val();
			ContinueTest(parseInt(testStepValue), elementClicked);
			testStepValue++
			testStepInput.val(testStepValue);
		});
	}

	function CalculatePoints(elementClicked) {
		var testPoints 		= $('#points');
		var testPointsValue = parseInt(testPoints.val());
		var elementPoints = elementClicked.attr('value');

		if (elementPoints > 0) {
			testPointsValue += parseInt(elementPoints);
			testPoints.val(testPointsValue);
		}
	}

	function ContinueTest(testStep, elementClicked) {
		$('.btncontinue').hide();
		$('#panAnswers').removeClass('hide');
		loadVideo(testStep, elementClicked);
	}

	function loadVideo(testStep, elementClicked) {
		var testPoints 		= $('#points');
		var nextStep        = parseInt(testStep) + 1;
		var videoInfo       = testRoutes[testStep];
		var videoIframe     = $('.custom-vimeo-test');
		var lang            =$('#lang').val()
		if (lang == 'en') {
			videoIframe.attr('src', videoInfo.en);
		} else {
			videoIframe.attr('src', videoInfo.fr);
		}
		CalculatePoints(elementClicked);

		if (nextStep ==  testRoutes.length) { // minus one because we get the next step
			endTest();
		}

		timeoutButton(testStep);
	}

	function getRealButtonState(testStep) {
		$('.btnanswer').each(function (index, element) {
			var button = $(this);
			var buttonValue = $(this).attr('data-answer');
			if (buttonValue in testRoutes[testStep]) {
				$(this).removeAttr('disabled', 'disabled');
			} else {
				$(this).attr("disabled", 'disabled');
			}

			$.each(testRoutes[testStep], function( key, value ) {
				if (buttonValue == key) {
					button.attr('value', value)
				}
			});
		});
	}

	function timeoutButton(testStep) {
		$('.btnanswer').attr('disabled', 'disabled');
		setTimeout( function(){
			getRealButtonState(testStep);
		}  , 10000 );
	}

	function endTest(elementClicked) {
		$('.btnanswer').attr("disabled", "disabled");
		$('.btnanswer').hide();
		$('#panInteret').removeClass("hide");


		$.ajax({
			url: "https://app.xpressleader.com/fr/call/testresults?results=" + $('#points').val() + "&token=" + $("#token").val() + "&lang=" + $("#lang").val() + "&leaderid=" + $("#leaderid").val(),
			type: "GET",
			cache: false,
			dataType: 'json',
			timeout: 5000
		});
	}



	if ($('#jwoverview').length > 0) {
		jwplayer.key = "Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";
		var jwoverview = jwplayer('jwoverview');
		var parentWidth = document.getElementById("player_wrapper") !== null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
		var controls = false;
		// console.log(parentWidth);
		if (parentWidth <= 720) controls = true;

		jwoverview.setup({
			file: $('#jwoverview').data("url"),
			image: "https://media.xpressleader.com/images/play.png",
			width: parentWidth - 33,
			height: parentWidth * 720 / 1280,
			stretching: "exactfit",
			controls: controls,
			autostart: true,
			logo: {
				hide: true
			},
			events: {
				onComplete: function () {
					clearInterval(interval);
				},
				onTime: function () {
					$("#overview_position").val(jwoverview.getPosition());
				}
			}
		});

		jwoverview.onReady(function () {
			jwoverview.play();
			overview_checkin(jwoverview);
		});

		// Responsive player resizing after initialization
		jQuery(window).resize(function () {
			var parentWidth = document.getElementById("player_wrapper") != null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
			var calculatedHeight = parentWidth * 720 / 1280;
			if (jwoverview.getWidth() != parentWidth) {
				jwoverview.resize(parentWidth - 33, calculatedHeight);
			}
		});


		$("#playpause").bind("click", function () {
			jwoverview.play();
			$(this).html(function (i, text) {
				return $(this).text() === " Play video" ? "<i class=\"fa fa-pause\" aria-hidden=\"true\"></i> Pause video" : "<i class=\"fa fa-play\" aria-hidden=\"true\"></i> Play video";
			})
		});


		function overview_checkin(jwplayer) {

			window.setInterval(function () {
				$.ajax({
					url: "https://app.xpressleader.com/fr/call/overviewcheckin?key=" + $("#key").val() + "&position=" + $("#overview_position").val(),
					type: "GET",
					cache: false,
					dataType: 'json',
					timeout: 5000
				});
			}, 10000); //10000 = 10 seconds

		}


		$(".addinterest").click(function () {

			var form_data = {
				name: $("#txtinvitename").val(),
				email: $("#txtinvitemail").val(),
				phone: $("#txtinvitephone").val(),
				leaderid: $("#leaderid").val(),
				token: $("#interesttoken").val(),
				key: $("#key").val(),
				lang: $(this).data("lang")
			};

			$.ajax({
				type: "POST",
				url: 'https://app.xpressleader.com/fr/call/addinterest',
				data: form_data,
				cache: false,
				dataType: 'json',
				timeout: 10000,
				success: function (response) {
					if (response.result === false) {
						$('.attmessage').html('<div class="alert alert-danger nopadding text-danger text-center" role="alert">' + response.message[form_data["lang"]] + '</div>');
					} else {
						$(".addprospectbox").fadeOut("slow", function () {
							$('.attmessage').html('<div class="alert alert-success text-danger text-center" role="alert">' + response.message[form_data["lang"]] + '</div>');
						});
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert(textStatus + ': ' + errorThrown);
				}
			});

		});
	}


	$('.cb-slideshow img').hide();

	function bgrotator() {
		$(".cb-slideshow img").first().appendTo('.cb-slideshow').fadeOut(2000);
		$(".cb-slideshow img").first().fadeIn(2000);

		setTimeout(bgrotator, 20000);
	}
	bgrotator();

	$.CBPQTRotator.defaults = {
		// default transition speed (ms)
		speed: 1700,
		// default transition easing
		easing: 'ease',
		// rotator interval (ms)
		interval: 12000
	};

	$('#cbp-qtrotator').cbpQTRotator();

	$('.btnnews').click(function () {
		$('.collapse').collapse('hide');
	});


	$('#videomodal').on('show.bs.modal', function (e) {

		var playerInstance = jwplayer('jwdynvideo');
		playerInstance.setup({
			file: $("#videourl").val(),
			image: "",
			width: 720,
			height: 405,
			controls: false,
			autostart: true,
			logo: {
				hide: true
			},
			events: {
				onComplete: function () {
					$('#videomodal').modal('hide');
				}
			}
		});

		playerInstance.play();

	});

	$('#videomodal').on('hidden.bs.modal', function (e) {
		jwplayer('jwdynvideo').remove();
	});

	$('#signsubmit').click(function () {
		var form_data = {
			firstname: $("#signfirstname").val(),
			lastname: $("#signlastname").val(),
			email1: $("#signemail1").val(),
			email2: $("#signemail2").val(),
			leaderid: $("#signleaderid").val(),
			lang: $("#lang").val(),
			token: $("#signtoken").val()
		};

		$.ajax({
			type: "POST",
			url: 'https://app.xpressleader.com/fr/call/signup',
			data: form_data,
			cache: false,
			dataType: 'json',
			success: function (data) {
				if (data.result === false) {
					$('#signmessage').html('<div class="alert navbar-danger" role="alert">' + data.message[_LANG] + '</div>');
				} else {
					$('#signupbox').fadeOut("normal", function () {
						if (_LANG === "fr") $('#signloader').html("<h3><i class=\"fa fa-spin fa-spinner\"></i> Création de votre compte en cours...</h3>");
						else $('#signloader').html("<h3><i class=\"fa fa-spin fa-spinner\"></i> Account creation in progress...</h3>");
						setTimeout(function () {
							$('#signupbox').html(data.message[_LANG]);
							$('#signupbox').fadeIn();
							$('#signloader').hide();
						}, 5000);
					});
				}

			},
			error: function (xhr, desc, err) {
				console.log(xhr + "\n" + err);
			}
		});
	});


});
