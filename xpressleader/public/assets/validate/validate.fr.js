/**
 * Bootstrap validate
 * English lang module
 */

$.bt_validate.fn = {
  'required' : function(value) { return (value  !== null) && (value !== '')},
  'email' : function(value) { return /^[a-zA-Z0-9-_\.]+@[a-zA-Z0-9-_\.]+\.[a-zA-Z]{2,4}$/.test(value) },
  'www' : function(value) { return /^(http:\/\/)|(https:\/\/)[a-z0-9\/\.-_]+\.[a-z]{2,4}$/.test(value) },
  'date' : function(value) { return /^[\d]{2}\/[\d]{2}\/[\d]{4}$/.test(value) },
  'time' : function(value) { return /^[\d]{2}:[\d]{2}(:{0,1}[\d]{0,2})$/.test(value) },
  'datetime' : function(value) { return /^[\d]{2}\/[\d]{2}\/[\d]{4} [\d]{2}:[\d]{2}:{0,1}[\d]{0,2}$/.test(value) },
  'number' : function(value) { return /^[\d]+$/.test(value) },
  'float' : function(value) { return /^([\d]+)|(\d]+\.[\d]+)$/.test(value) },
  'equal' : function(value, eq_value) { return (value == eq_value); },
  'min' : function(value, min) { return Number(value) >= min },
  'max' : function(value, max) { return Number(value) <= max },
  'between' : function(value, min, max) { return (Number(value) >= min) && (Number(value) <= max)},
  'length_min' : function(value, min) { return value.length >= min },
  'length_max' : function(value, max) { return value.length <= max },
  'length_between' : function(value, min, max) { return (value.length >= min) && (value.length <= max)},
  'custom_mail_eq' : function(value) { return ($('#inputmail').val() == $('#inputmail2').val())},
  'ccexpdate' : function(value) { return /^(0[1-9]|1[0-2])[0-9][0-9]$/.test(value) }
  
}

$.bt_validate.text = {
  'required' : 'Obligatoire',
  'email' : 'Le courriel n\'est pas valide.',
  'www' : 'le lien n\'est pas valide',
  'date' : 'La date n\'est pas valide',
  'time' : 'The value is not valid time',
  'datetime' : 'The value is not valid datetime',
  'number' : 'Ce n\'est pas un numéro valide',
  'float' : 'Ce n\'est pas un numéro valide',
  'equal' : 'La valeur doit être égale à "%1"',
  'min' : 'Doit être égale ou supérieur à %1',
  'max' : 'Doit être égale ou inférieur à %1',
  'between' : 'Doit être entre %1 et %2',
  'length_min' : 'La longueur doit être égale ou supérieur à %1',
  'length_max' : 'TLa longueur doit être égale ou inférieur àn %1',
  'length_between' : 'La longueur doit être entre %1 et %2',
  'custom_mail_eq' : 'Les courriels ne correspondent pas',
  'ccexpdate' : 'Date d\'expiration non valide'
}