$(document).ready(function () {
    
    var interval;
    var loadtext    = ['Soyez toujours respecteux lorsque vous utilisez le clavardage','Assurez-vous que le volume de vos haut-parleurs est bien ajusté','Pour une meilleur expérience, utilisez google Chrome','Si vous éprouvez des problèmes de connexion, essayez de rafraîchir votre page'];
    var count       = loadtext.length-1;
    var current     = 0;
    
    jwplayer.key="Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";
    
    
    
    
    $(".login").click(function() {
        
        var form_data = {
            name: $("#attName").val(),
            email: $("#attEmail").val(),
            password: $("#attPassword").val(),
            eventid: $("#eventid").val(),
            leaderid: $("#leaderid").val(),
            token: $("#token").val(),
            lang: $(this).data("lang")
        };
        
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/fr/call/geteventurl',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                    if(data.result===false){
                        $('#attmessage').html('<div class="alert alert-danger nopadding text-danger text-center" role="alert">'+data.message[_LANG]+'</div>');
                    }else{
                        $( ".loginbox" ).fadeOut( "slow", function() {
                            startLoader(form_data["lang"]);
                        });
                        $( "#castframe" ).removeClass("hide");
                        loadIframe('castframe', data.message[_LANG]);
                        
                        setTimeout(function() {
                            autovideo(jwplayer);
                            stopLoader(form_data["lang"]);
  		                    checkform();
                        }, 6000);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + ': ' + errorThrown);
                }
        });
        
    });
    
    var countdownIntervalId;
    var jwdynmodvideo   = jwplayer('jwdynmodvideo');
    var jwdynvideo      = jwplayer('jwdynvideo');
    var jwoverview      = jwplayer('jwoverview');
		var jwtest      		= jwplayer('jwtest');
    
    if(_iscampaign==="TRUE"){
        console.log("hello");
        var form_data = {
            name: $("#login").find("#attName").val(),
            email: $("#login").find("#attEmail").val(),
            leaderid: $("#login").find("#leaderid").val(),
            token: $("#login").find("#token").val(),
            key: $("#login").find("#key").val(),
            lang: "fr",
            url: "https://player.vimeo.com/external/171692177.sd.mp4?s=3ca94edddde3489f3e01c47ab807e698825615d0&profile_id=165",
            iscampaign: "true"
        };
        
        
        $.ajax({
            type: "POST",
            url: 'https://app.xpressleader.com/fr/call/startwebcast',
            data: form_data,
            cache: false,
            dataType: 'json',
            timeout: 10000,
            success: function(response)
            {
                
                if(response.result===false){
                    $("#login").find(".attmessage").html('<div class="alert alert-danger nopadding text-danger text-center" role="alert">'+response.message[form_data["lang"]]+'</div>');
                }else{
                    $( ".loginbox" ).fadeOut( "slow", function() {
                        //startLoader(form_data["lang"]);
                    });
                    
                    
                    //setTimeout(function() {
                        var parentWidth = document.getElementById("player_wrapper") !== null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
                        
                        var controls = false;
                        if(parentWidth<=720) controls = true;
                    
                        jwoverview.setup({
                            file: form_data["url"],
                            image: "https://xpressleader.com/images/play.png",
                            width: parentWidth,
                            height: parentWidth * 720 / 1280,
                            stretching: "exactfit",
                            controls: controls,
                            autostart: true,
                            logo: {
                                hide: true
                            },
                            events:{
                                onComplete: function() {
                                    clearInterval(interval);
                                },
                                onTime: function() {
                                    $( "#overview_position" ).val(jwoverview.getPosition());
                                }
                            }
                        });
                        
                        jwoverview.onReady(function() { 
                            jwoverview.play();
                            overview_checkin(jwoverview);
                        });
                        
                        
                        
                        if(form_data["lang"]==="fr") {
                            $( "#castfr" ).removeClass("hide");
                        }else{ 
                            $( "#casten" ).removeClass("hide");
                        }
                        
                        
                        //stopLoader(form_data["lang"]);
                    //}, 7000);
                    
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus + ': ' + errorThrown);
            }
        });
    }
    
    
    $("#btnaudio").click(function() {
        jwaudio.pause();
    });
    
    $(".startwebcast").click(function() {
        
        var form_data = {
            name: $("#login").find("#attName").val(),
            email: $("#login").find("#attEmail").val(),
            leaderid: $("#login").find("#leaderid").val(),
            token: $("#login").find("#token").val(),
            key: $("#login").find("#key").val(),
            lang: $(this).data("lang"),
            url: $(this).data("url")
        };
        
        $.ajax({
            type: "POST",
            url: 'https://app.xpressleader.com/fr/call/startwebcast',
            data: form_data,
            cache: false,
            dataType: 'json',
            timeout: 10000,
            success: function(response)
            {
                
                if(response.result===false){
                    $("#login").find(".attmessage").html('<div class="alert alert-danger nopadding text-danger text-center" role="alert">'+response.message[form_data["lang"]]+'</div>');
                }else{
                    $( ".loginbox" ).fadeOut( "slow", function() {
                        //startLoader(form_data["lang"]);
                    });
                    
                    
                    //setTimeout(function() {
                        var parentWidth = document.getElementById("player_wrapper") !== null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
                        
                        var controls = false;
                        if(parentWidth<=720) controls = true;
                    
                        jwoverview.setup({
                            file: form_data["url"],
                            image: "https://xpressleader.com/images/play.png",
                            width: parentWidth,
                            height: parentWidth * 720 / 1280,
                            stretching: "exactfit",
                            controls: controls,
                            autostart: true,
                            logo: {
                                hide: true
                            },
                            events:{
                                onComplete: function() {
                                    clearInterval(interval);
                                },
                                onTime: function() {
                                    $( "#overview_position" ).val(jwoverview.getPosition());
                                }
                            }
                        });
                        
                        jwoverview.onReady(function() { 
                            jwoverview.play();
                            overview_checkin(jwoverview);
                        });
                        
                        
                        
                        if(form_data["lang"]==="fr") {
                            $( "#castfr" ).removeClass("hide");
                        }else{ 
                            $( "#casten" ).removeClass("hide");
                        }
                        
                        
                        //stopLoader(form_data["lang"]);
                    //}, 7000);
                    
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus + ': ' + errorThrown);
            }
        });
        
    });
    
    
    function overview_checkin(jwplayer) {
        
        window.setInterval(function(){
            $.ajax({
                url: "https://app.xpressleader.com/fr/call/overviewcheckin?key="+$("#key").val()+"&position="+$("#overview_position").val(),
                type: "GET",
                cache: false,
                dataType: 'json',
                timeout: 5000
            });
        }, 10000); //10000 = 10 seconds
        
    }
    
    
    
    
    
    
    
    
    
    $(".addinterest").click(function() {
        
        var form_data = {
            name: $("#cast"+$(this).data("lang")).find("#txtinvitename").val(),
            email: $("#cast"+$(this).data("lang")).find("#txtinvitemail").val(),
            phone: $("#cast"+$(this).data("lang")).find("#txtinvitephone").val(),
            leaderid: $("#cast"+$(this).data("lang")).find("#leaderid").val(),
            token: $("#cast"+$(this).data("lang")).find("#interesttoken").val(),
            key: $("#cast"+$(this).data("lang")).find("#key").val(),
            lang: $(this).data("lang")
        };
        
        $.ajax({
            type: "POST",
            url: 'https://app.xpressleader.com/fr/call/addinterest',
            data: form_data,
            cache: false,
            dataType: 'json',
            timeout: 10000,
            success: function(response)
            {
                if(response.result===false){
                    $('.attmessage').html('<div class="alert alert-danger nopadding text-danger text-center" role="alert">'+response.message[form_data["lang"]]+'</div>');
                }else{
                    $( ".addprospectbox" ).fadeOut( "slow", function() {
                        $('.attmessage').html('<div class="alert alert-success text-danger text-center" role="alert">'+response.message[form_data["lang"]]+'</div>');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
            }
        });
        
    });
    
    
    
    
    jQuery(window).resize(function() {
        var parentWidth = document.getElementById("player_wrapper") !== null ? document.getElementById("player_wrapper").parentNode.offsetWidth : document.getElementById("jwoverview").parentNode.offsetWidth;
        var calculatedHeight = parentWidth * 720 / 1280;
        if (jwoverview.getWidth() !== parentWidth) {
            jwoverview.resize(parentWidth, calculatedHeight);
        }
    });
    
    
    
    $("#player_wrapper").bind("click touchstart", function() {
        jwoverview.play(); 
        jwaudio.stop();
    });
    
    
    function loader(){
        $('.hints').fadeOut('fast',function(){
            $('.hints').html(loadtext[current]);
            $('.hints').fadeIn(1000);
        });
        current++;
        if(current > count) { 
            current = 0;
        }
    }
    
    function startLoader(lang){
        $('.load'+lang).fadeIn('fast',function(){
            interval = setInterval(function(){loader();},4000); 
        });
        return false;
    }
    
    function stopLoader(lang){
        $('.load'+lang).fadeOut('fast',function(){
            clearInterval(interval);
        });
        return false;
    }
    
    function loadIframe(iframeName, url) {
        var $iframe = $('#' + iframeName);
        if ( $iframe.length ) {
            $iframe.attr('src',url);   
            return false;
        }
        return true;
    }


    
    
    

	$(".playvideo").click(function() {

        $('.playvideo').html('<span class="glyphicon glyphicon-play"></span> Play');
        if(!$(this).hasClass("btn-danger")){
            $('.playvideo').attr("disabled", "disabled");
            $(this).html('<span class="glyphicon glyphicon-play"></span> Starting...');

            $.ajax({
                url: "/webcast/checkvideo.php?event="+$("#eventkey").val()+"&url=" + encodeURIComponent($(this).attr("data-url")),
                type: "GET",
                cache:false,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                timeout: 10000,
                success: function(response){
                    console.log(response);
                },
                error: function(response){
                    console.log(response);
                }
            });
            
                $('.playvideo').removeClass("btn-danger");
                $('#btnbroadvideo').removeClass("btn-primary");
                $('#btnbroadvideo').addClass("btn-danger");
                $(this).addClass("btn-danger");
                setTimeout(function (ele) {
                    
                    $(ele).html('<span class="glyphicon glyphicon-stop"></span> Stop');
                    $('.playvideo').removeAttr('disabled');
                    
                }, 5000,$(this));
                
        }else{
            $('.playvideo').attr("disabled", "disabled");
            $(this).html('<span class="glyphicon glyphicon-play"></span> Stopping...');
            $.ajax({
                url: "/webcast/checkvideo.php?event="+$("#eventkey").val()+"&url=",
                type: "GET",
                cache: false,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                timeout: 10000
            });
            
                $('.playvideo').removeClass("btn-danger");
                $('#btnbroadvideo').addClass("btn-primary");
                $('#btnbroadvideo').removeClass("btn-danger");
                $(this).addClass("btn-primary");
                
                setTimeout(function (ele) {
                    $(ele).html('<span class="glyphicon glyphicon-play"></span> Play');
                    $('.playvideo').removeAttr('disabled');
                }, 5000,$(this));
                

        }
	});
    
    $('#videomodal').on('hidden.bs.modal', function (e) {
        jwdynvideo.stop();
    });
    
    $('#videomodal').on('show.bs.modal', function (e) {
        if($("#playingvideourl").val().slice(-3)==="mp3") $(this).addClass("hide");
        else $(this).removeClass("hide");
        jwdynvideo.setup({ 
            file: $("#playingvideourl").val(),
            image: "",
            width: 720,
            height: 405,
            controls: false,
            autostart: true,
            logo: {
                hide: true
            },
            events:{
                onComplete: function() {
                    $('#videomodal').modal('hide');
                }
            }
        });
        
        
        
        jwdynvideo.onReady(function() { 
            if($("#playingvideourl").val().slice(-3)==="mp3") jwdynvideo.setVolume(50);
            else jwdynvideo.setVolume(100);
            jwdynvideo.play();
            jwdynvideo.seek($("#offset").val());
        });
        
    });
    
    
    
    
    
    
    $("#playingvideourl").change(function(){
        if($("#playingvideourl").val()==="" && $("#ispresenter").val()==="1") {
            try
            {
                jwdynmodvideo.stop();
                $('#jwdynmodmsg').hide();
            }
            catch(err)
            {
            //Handle errors here
            }
            
        }else{
            duration = 1;
            clearInterval(countdownIntervalId);
            
            var x = 1;
            if($("#ispresenter").val()==="1") {
                $('#duration').html('<img src="//xpressleader.com/image/ajax-loader.gif" style="width: 50px;" alt=""/>');
                
                jwdynmodvideo.setup({ 
                file: $("#playingvideourl").val(),
                image: "",
                width: 300,
                height: 150,
                controls: false,
                autostart: false,
                seek: $("#offset").val(),
                logo: {
                    hide: true
                },
                events:{
                    onComplete: function() {
                        jwdynmodvideo.stop();
                        $('#jwdynmodmsg').hide();
                        $.ajax({
                            url: "/webcast/checkvideo.php?event="+$("#eventkey").val()+"&url=",
                            type: "GET",
                            contentType: 'application/json;charset=utf-8',
                            dataType: 'json',
                            timeout: 10000
                        });
                        
                        $('.playvideo').removeClass("btn-danger");
                        $('#btnbroadvideo').addClass("btn-primary");
                        $('#btnbroadvideo').removeClass("btn-danger");
                        $(this).addClass("btn-primary");
                        
                        setTimeout(function (ele) {
                            $(ele).html('<span class="glyphicon glyphicon-play"></span> Play');
                            $('.playvideo').removeAttr('disabled');
                        }, 5000,$('.playvideo'));
                    },
                    onTime: function() {
                        duration = jwdynmodvideo.getDuration()-$("#offset").val();
                    }
                    
                }
            });
                
                countdownIntervalId = setInterval(function(){
                    x = x + 1;
                    if((duration-x)>0) {
                        $("#duration").html(secondsAsTime(duration-x));
                    }
                }, 1000);
                
                jwdynmodvideo.onReady(function() { 
                    jwdynmodvideo.play(); 
                    jwdynmodvideo.seek($("#offset").val());
                });
    
                $('#jwdynmodmsg').fadeIn();
            }
        }
    });
	
});


function autovideo(jwplayer) {
        
    window.setInterval(function(){ //loop
        
        $.ajax({
            url: "/webcast/checkvideo.php?event="+$("#eventkey").val(),
            type: "GET",
            cache:false,
            contentType: 'application/json;charset=utf-8',
            dataType: 'json',
            timeout: 10000,
            success: function(response){
               $("#videourl").val(response.url);
                    console.log($("#playingvideourl").val());
                    console.log($("#videourl").val());
                   if(response.url!==""){
                        if($("#playingvideourl").val()!==$("#videourl").val()){
                            $("#offset").val(response.offset);
                            $("#playingvideourl").val($("#videourl").val()).change();
                            $('#videomodal').modal('show');
                            if ($('#videomodal').hasClass('in')) console.log($("#videourl").val());
                        }
                   }else{
                       $('#videomodal').modal('hide');
                       $("#playingvideourl").val("").change();
                       $("#videourl").val("");
                       $("#offset").val("");
                   }
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                   $('#videomodal').modal('hide');
                   $("#playingvideourl").val("").change();
                   $("#videourl").val("");
                   $("#offset").val("");
              }
        }); 

    
    }, 5000); //10000 = 10 seconds
}

function secondsAsTime(seconds) {
        // var hours = Math.floor(seconds / 3600);
        // seconds -= hours * 3600;
        var minutes = Math.floor(seconds / 60);
        seconds = Math.floor(seconds % 60);
        function pad(n) {
            return (n < 10) ? '0'+ n : n;
        }
        return pad(minutes) +':'+ pad(seconds);
}


$("#subclosingform").click(function() {
    
    if($("#cfname").val().trim()===""){
        $("#cfname").parent().addClass("has-error");
        $("#help-cfname").html("Votre nom est obligatoire!");
    }else{
        $("#cfname").parent().removeClass("has-error");
        $("#help-cfname").html("");
    }
    
    if($("#cfphone").val().trim()===""){
        $("#cfphone").parent().addClass("has-error");
        $("#help-cfphone").html("Votre téléphone est obligatoire!");
    }else{
        $("#cfphone").parent().removeClass("has-error");
        $("#help-cfphone").html("");
    }
    
    $.ajax({
        url: "/webcast/addlead.php?name="+$("#cfname").val()+"&phone=" + $('#cfphone').val()+"&comment=" + $('#cfcomment').val()+"&eventid=" + $('#cfeventid').val(),
        type: "GET",
        cache: false,
        contentType: 'application/text;charset=utf-8',
        dataType: 'text',
        timeout: 10000,
        success: function(response){
           if(response==="ok"){
                $("#cfcontainer").html("<center><h2>Merci de votre intérêt!<br>Nous communiquerons avec vous dans les plus brefs délais!<h2> <a href=\"https://mieuxetre.info\" class=\"btn btn-success\">Quitter la session</a></center>");
           }
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
    
});

$("#toggleform").click(function() {
    
    if($('#isopen').val()==="1") {
        $('#isopen').val("0");
        $(this).html('<i class="fa fa-user-plus"></i> Closing');
        $(this).removeClass("btn-danger");
        $(this).addClass("btn-xpress-black");
        
    }else{
        $('#isopen').val("1");
        $(this).html('<i class="fa fa-user-times"></i> Closing');
        $(this).removeClass("btn-xpress-black");
        $(this).addClass("btn-danger");
    }
    
    $.ajax({
        url: "/webcast/checkform.php?event="+$("#eventkey").val()+"&isopen=" + $('#isopen').val(),
        type: "GET",
        cache: false,
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        timeout: 10000
    });
        
});


function checkform() {
        
    window.setInterval(function(){ //loop
        
        $.ajax({
            url: "/webcast/checkform.php?event="+$("#eventkey").val(),
            type: "GET",
            cache:false,
            contentType: 'application/json;charset=utf-8',
            dataType: 'json',
            timeout: 10000,
            success: function(response){
                console.log(response.isopen);
                if(response.isopen==="1"){
                    $('#closingform').modal('show');    
                }else $('#closingform').modal('hide');  
            },
            error: function (xhr, ajaxOptions, thrownError) {
                   $('#closingform').modal('hide');
              }
        }); 

        
    
    }, 5000); //10000 = 10 seconds
}

