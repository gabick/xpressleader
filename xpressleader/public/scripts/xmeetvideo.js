// JavaScript Document
$(document).ready(function () {
    jwplayer.key="Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";
    var countdownIntervalId;
    
    var jwdynmodvideo = jwplayer('jwdynmodvideo');
    var jwdynvideo = jwplayer('jwdynvideo');

	$(".playvideo").click(function() {
        
        $('.playvideo').html('<span class="glyphicon glyphicon-play"></span> Play');
        if(!$(this).hasClass("btn-danger")){
            $('.playvideo').attr("disabled", "disabled");
            $(this).html('<span class="glyphicon glyphicon-play"></span> Starting...');
            
            $.ajax({
                url: "/v1/application/calls/call.checkvideo.php?event="+$("#eventkey").val()+"&url=" + encodeURIComponent($(this).attr("data-url")),
                type: "GET",
                cache:false,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                timeout: 10000,
                success: function(response){
                    //console.log('Broadcast : ' + response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        //console.log('Error : ' + xhr.status);
                      }
            });
            
                $('.playvideo').removeClass("btn-danger");
                $('#btnbroadvideo').removeClass("btn-primary");
                $('#btnbroadvideo').addClass("btn-danger");
                $(this).addClass("btn-danger");
                setTimeout(function (ele) {
                    
                    $(ele).html('<span class="glyphicon glyphicon-stop"></span> Stop');
                    $('.playvideo').removeAttr('disabled');
                    
                }, 5000,$(this));
                
        }else{
            $('.playvideo').attr("disabled", "disabled");
            $(this).html('<span class="glyphicon glyphicon-play"></span> Stopping...');
            $.ajax({
                url: "/v1/application/calls/call.checkvideo.php?event="+$("#eventkey").val()+"&url=",
                type: "GET",
                cache:false,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                timeout: 10000,
                success: function(response){
                    //console.log('Broadcast : ' + response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        //console.log('Error : ' + xhr.status);
                      }
            });
            
                $('.playvideo').removeClass("btn-danger");
                $('#btnbroadvideo').addClass("btn-primary");
                $('#btnbroadvideo').removeClass("btn-danger");
                $(this).addClass("btn-primary");
                
                setTimeout(function (ele) {
                    $(ele).html('<span class="glyphicon glyphicon-play"></span> Play');
                    $('.playvideo').removeAttr('disabled');
                }, 5000,$(this));
                

        }
	});
    
    $('#videomodal').on('hidden.bs.modal', function (e) {
        jwdynvideo.stop();
    });
    
    $('#videomodal').on('show.bs.modal', function (e) {
        
        
            jwdynvideo.setup({ 
            file: $("#playingvideourl").val(),
            image: "",
            width: 720,
            height: 405,
            controls: false,
            autostart: true,
            logo: {
                hide: true
            },
            events:{
                onComplete: function() {
                    $('#videomodal').modal('hide');
                }
            }
        });
        
        jwdynvideo.onReady(function() { 
            jwdynvideo.play(); 
            jwdynvideo.seek($("#offset").val());
        });
        
    });
    
    
    
    
    
    
    $("#playingvideourl").change(function(){
        
        if($("#playingvideourl").val()==="" && $("#ispresenter").val()==="1") {
            try
            {
                jwdynmodvideo.stop();
                $('#jwdynmodmsg').hide();
            }
            catch(err)
            {
            //Handle errors here
            }
            
        }else{
            duration = 1;
            clearInterval(countdownIntervalId);
            
            var x = 1;
            if($("#ispresenter").val()==="1") {
                $('#duration').html('<img src="//media.xpressleader.com/image/ajax-loader.gif" style="width: 50px;" alt=""/>');
                
                
                jwdynmodvideo.setup({ 
                file: $("#playingvideourl").val(),
                image: "",
                width: 300,
                height: 150,
                controls: false,
                autostart: false,
                seek: $("#offset").val(),
                logo: {
                    hide: true
                },
                events:{
                    onComplete: function() {
                        jwdynmodvideo.stop();
                        $('#jwdynmodmsg').hide();
                        $.ajax({
                            url: "/v1/application/calls/call.checkvideo.php?event="+$("#eventkey").val()+"&url=",
                            type: "GET",
                            contentType: 'application/json;charset=utf-8',
                            dataType: 'json',
                            timeout: 10000,
                            success: function(response){
                                //console.log('Broadcast : ' + response);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                    //console.log('Error : ' + xhr.status);
                                  }
                        });
                        
                        $('.playvideo').removeClass("btn-danger");
                        $('#btnbroadvideo').addClass("btn-primary");
                        $('#btnbroadvideo').removeClass("btn-danger");
                        $(this).addClass("btn-primary");
                        
                        setTimeout(function (ele) {
                            $(ele).html('<span class="glyphicon glyphicon-play"></span> Play');
                            $('.playvideo').removeAttr('disabled');
                        }, 5000,$('.playvideo'));
                    },
                    onTime: function() {
                        duration = jwdynmodvideo.getDuration()-$("#offset").val();
                    }
                    
                }
            });
                
                countdownIntervalId = setInterval(function(){ //loop
                    x = x + 1;
                    if((duration-x)>0) {
                        $("#duration").html(secondsAsTime(duration-x));
                    }
                }, 1000); //10000 = 10 seconds
                
                //console.log("offset: " + $("#offset").val());
                
                jwdynmodvideo.onReady(function() { 
                    jwdynmodvideo.play(); 
                    jwdynmodvideo.seek($("#offset").val());
                });
    
                $('#jwdynmodmsg').fadeIn();
            }
        }
    });
	
});


function autovideo(jwplayer) {
        
    window.setInterval(function(){ //loop
        
        $.ajax({
            url: "/v1/application/calls/call.checkvideo.php?event="+$("#eventkey").val(),
            type: "GET",
            cache:false,
            contentType: 'application/json;charset=utf-8',
            dataType: 'json',
            timeout: 10000,
            success: function(response){
               $("#videourl").val(response.url);
               if(response.url!==""){
                    if($("#playingvideourl").val()!==$("#videourl").val()){
                        $("#offset").val(response.offset);
                        $("#playingvideourl").val($("#videourl").val()).change();
                        $('#videomodal').modal('show');
                        //console.log("offset: " + $("#offset").val());
                    }
               }else{
                   $('#videomodal').modal('hide');
                   $("#playingvideourl").val("").change();
                   $("#videourl").val("");
                   $("#offset").val("");
               }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                   $('#videomodal').modal('hide');
                   $("#playingvideourl").val("").change();
                   $("#videourl").val("");
                   $("#offset").val("");
              }
        }); 

        
    
    }, 5000); //10000 = 10 seconds
}

function secondsAsTime(seconds) {
        // var hours = Math.floor(seconds / 3600);
        // seconds -= hours * 3600;
        var minutes = Math.floor(seconds / 60);
        seconds = Math.floor(seconds % 60);
        function pad(n) {
            return (n < 10) ? '0'+ n : n;
        }
        return pad(minutes) +':'+ pad(seconds);
}