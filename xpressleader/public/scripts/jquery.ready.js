// JavaScript Document
$(document).ready(function() {
	$('.ttips').tooltip();
	
	$('[data-toggle="tooltip"]').tooltip();
    
    $.fn.bindDelay = function(eventType, eventData, handler, timer ) {
		
		if ( $.isFunction(eventData) ) {
			timer = handler;
			handler = eventData;
		}
		timer = (typeof timer === "number") ? timer : 300;
		var timeouts;
		$(this).bind(eventType, function(event) {
			$('#ResultAjax').empty();
			var spinner = new Spinner(opts).spin(document.getElementById('CustomerAjax'));
			var that = this;
			clearTimeout(timeouts);
			timeouts = setTimeout(function() {
			handler.call(that, event);
		}, timer);
		});
	};
	
	$('.selplan').click(function(){
	    $('.plan').css("border-color","#b5b5b5");
	    $('.plan .title').css("background-color","#333333");
	    $('.plan .description').css("background-color","#333333");
	    $('.tab-pane').removeClass("active");
	    
	    $('#tab'+$(this).attr("data-plan")).addClass("active");
	    $('#'+$(this).attr("data-plan")).css("border-color","#1b87e0");
	    $('#'+$(this).attr("data-plan")+' .title').css("background-color","#1b87e0");
	    $('#'+$(this).attr("data-plan")+' .description').css("background-color","#1467ab");
	    
	    $('html, body').animate({
            scrollTop: $("#planpayment").offset().top
        }, 500);
    });
    
    $('#crmeet_category').on('change', function() {
        if($(this).val()==="2") {
            $('#event_training').removeClass("hide");
            $('#event_overview').addClass("hide");
        }else{
            $('#event_training').addClass("hide");
            $('#event_overview').removeClass("hide");
        } 
    });
    
    
    $('.modal-chgroup').click(function(){
	    $('#chgroup-leaderid').val($(this).attr("data-leaderid"));
    });
    


    
    $('input[class="switch-group-presenter"]').bootstrapSwitch();
    $('input[class="switch-group-presenter"]').on('switchChange.bootstrapSwitch', function(event, state) {
        var groupid       = $('#group-presenter-groupid').val();
        var token           = $('#group-presenter-token').val();
        var presenterid     = $(this).attr("data-presenterid");
        $.post( "//app.xpressleader.com/fr/call/presenter-group", { presenterid: presenterid, groupid: groupid, follow: state, token: token } );
    });
    
    
    $('input[class="switch-follow-presenter"]').bootstrapSwitch();
    $('input[class="switch-follow-presenter"]').on('switchChange.bootstrapSwitch', function(event, state) {
        var curleader       = $('#presenter-curleader').val();
        var presenterid     = $(this).attr("data-presenterid");
        var token           = $('#presenter-token').val();
        $.post( "//app.xpressleader.com/fr/call/presenter-follow", { leaderid: curleader, presenterid: presenterid, follow: state, token: token } );
    });
    
    
    $('#frmvalidate').bt_validate();
	$('.pop').popover();
	
	var inputvalue = $('#InputCode').val();
	$('.previewcode').text(inputvalue);
	
	$('.phone_us').mask('(999) 999-9999');
	$('.postal').mask('S9S 9S9');
	
	$("#plan1").click(function() {
	$(this).addClass("btn-primary");
	$(this).removeClass("btn-default");
	$("#plan2").addClass("btn-default");
	$("#plan2").removeClass("btn-primary");
	});
	
	$("#plan2").click(function() {
	$(this).addClass("btn-primary");
	$(this).removeClass("btn-default");
	$("#plan1").addClass("btn-default");
	$("#plan1").removeClass("btn-primary");
	});
	
	
	$("#type1").click(function(e) {
		$(this).addClass("btn-primary");
		$(this).removeClass("btn-default");
		$("#type2").addClass("btn-default");
		$("#type2").removeClass("btn-primary");
		$("#type3").addClass("btn-default");
		$("#type3").removeClass("btn-primary");
		
		$("#lblkey").addClass("hidden");
		$("#lblcategory").addClass("hidden");
		$("#lbltitle").removeClass("hidden");
		$("#inputkey").val("na");
		$("#inputcategory").val("1");
	});
	
	$("#type2").click(function(e) {
		$(this).addClass("btn-primary");
		$(this).removeClass("btn-default");
		$("#type1").addClass("btn-default");
		$("#type1").removeClass("btn-primary");
		$("#type3").addClass("btn-default");
		$("#type3").removeClass("btn-primary");
		
		
		$("#lblkey").removeClass("hidden");
		$("#lblcategory").addClass("hidden");
		$("#inputcategory").val("1");
		$("#inputtitle").prop('disabled', false);
		$("#inputtitle").val("");
		$("#inputkey").val("");
	});
	
	$("#type3").click(function(e) {
		$(this).addClass("btn-primary");
		$(this).removeClass("btn-default");
		$("#type1").addClass("btn-default");
		$("#type1").removeClass("btn-primary");
		$("#type2").addClass("btn-default");
		$("#type2").removeClass("btn-primary");
		
		$("#lblkey").addClass("hidden");
		$("#lbltitle").removeClass("hidden");
		$("#lblcategory").removeClass("hidden");
		$("#inputcategory").val("1");
		$("#inputtitle").prop('disabled', true);
		$("#inputtitle").val("SURVOL D'ENTREPRISE / BUSINESS OVERVIEW");
		$("#inputkey").val("na");
	});
	
	
	$("#inputcategory").change(function() {
		if($(this).val()==="2"){
			$("#lbltitle").removeClass("hidden");
			$("#inputtitle").prop('disabled', false);
			$("#inputtitle").val("");
			$("#inputkey").val("xpress");
		}else{
			$("#inputtitle").val("SURVOL D'ENTREPRISE / BUSINESS OVERVIEW");
			$("#inputkey").val("na");
			$("#lbltitle").addClass("hidden");
		}
	});

	var opts = {
		lines: 13, // The number of lines to draw
		length: 14, // The length of each line
		width: 7, // The line thickness
		radius: 23, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 60, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#56AD56', // #rgb or #rrggbb
		speed: 1.5, // Rounds per second
		trail: 64, // Afterglow percentage
		shadow: true, // Whether to render a shadow
		hwaccel: true, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: 100, // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};
    
    $.fn.bindDelay = function(eventType, eventData, handler, timer ) {
		
		if ( $.isFunction(eventData) ) {
			timer = handler;
			handler = eventData;
		}
		timer = (typeof timer === "number") ? timer : 300;
		var timeouts;
		$(this).bind(eventType, function(event) {
			$('#ResultAjax').empty();
			var spinner = new Spinner(opts).spin(document.getElementById('CustomerAjax'));
			var that = this;
			clearTimeout(timeouts);
			timeouts = setTimeout(function() {
			handler.call(that, event);
		}, timer);
		});
	};
	
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	
	$('#dpmeeting').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        showMeridian: true,
        minuteStep: 15,
        language: LANG,
        pickerPosition: 'top-right',
        autoclose: true,
        startDate: now
    });
	
	$('#dtnextcharge').datetimepicker({
		language: LANG,
        format: 'yyyy-mm-dd'
	});
	
	
	$('#signsubmit').click(function(){
        var form_data = {
            firstname: $("#signfirstname").val(),
            lastname: $("#signlastname").val(),
            email1: $("#signemail1").val(),
            email2: $("#signemail2").val(),
            leaderid: $("#signleaderid").val(),
            lang: $("#lang").val(),
            token: $("#signtoken").val()
        };
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/fr/call/signup',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                    
                    if(data.result===false){
                        $('#signmessage').html('<div class="alert navbar-danger" role="alert">'+data.message[_LANG]+'</div>');
                    }else{
                        $('#signupbox').fadeOut( "normal", function() {
                            $('#signloader').html("<h3><i class=\"fa fa-spin fa-spinner\"></i> Création de votre compte en cours...</h3>");
                            setTimeout(function(){
                                $('#signupbox').html(data.message[_LANG]);
                                $('#signupbox').fadeIn();
                                $('#signloader').hide();
                            }, 5000);
                        });
                    }
                    
                },
                error: function(xhr, desc, err) {
                	console.log(xhr + "\n" + err);
                }
        });
    });
	
	
	
	
	if ( $( "#prospects-box" ).length ) {
		
		if ( $( "#ctcid" ).val() !== "" && $( "#ctctoken" ).val() !== "" ) getprospect($( "#ctcid" ).val(), $( "#ctctoken" ).val());
		else listContacts($("#statusid").val());
    	setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
    }
    
    $("#srchprospects").click(function() {
    	$('#prosget').hide();
		$('#prosloader').fadeIn();
		listContacts(-1);
		setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
	});
	

	
	$('#key').bind("enterKey",function(e){
	   $('#prosget').fadeOut();	
	   listContacts(-1);
	});
	$('#key').keyup(function(e){
	    if(e.keyCode === 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	
	
	
	
	

    function listContacts(statusid){
    	var form_data = {
    		perpage: $("#perpage").val(),
    		page: $("#page").val(),
    		orderby: $("#orderby").val(),
    		sort: $("#sort").val(),
    		statusid: statusid,
    		key: $("#key").val().trim(),
            leaderid: $("#leaderid").val(),
            lang: $("#lang").val(),
            token: $("#token").val()
        };
        
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/fr/contact/list',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                    if(data.result===false){
                        $('#proslist').html('<div class="alert navbar-danger" role="alert">'+data.html+'</div>');
                    }else{
                        $('#proslist').fadeOut( "normal", function() {
                            $('#proslist').html(data.html);
                            $('#proslist').fadeIn();
                        });
                    }
                },
                error: function(xhr, desc, err) {
                	console.log(xhr + desc + "\n" + err);
                }
        });
    }
    
    
    $('body').on('click', '.loadContact', function (){
    	
		var button 		= $(this);
		var prospectid 	= button.data('prospectid');
		var token 		= button.data('token');

		$('#prosloader').fadeIn();
		getprospect(prospectid, token);
		setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
		
	});
	
	
	$('body').on('click', '#btngetupdate', function (){
		$(this).prop('disabled', true);
		$('#prosloader').fadeIn();
		
		var button 		= $(this);
		var contactid 	= button.data('contactid');
		var leaderid 	= button.data('leaderid');
		var lang 		= button.data('lang');
		var token 		= button.data('token');
		
		var form_data = {
    		ctcname: $("#ctc_name").val(),
    		ctcemail: $("#ctc_email").val(),
    		ctcphone: $("#ctc_phone").val(),
    		ctccity: $("#ctc_city").val(),
    		ctcregion: $("#ctc_region").val(),
    		ctctype: $("#ctc_type").val(),
    		ctclang: $("#ctc_lang").val(),
    		contactid: contactid,
            leaderid: leaderid,
            lang: lang,
            token: token
        };
        console.log($("#ctc_type").val());
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/en/contact/put',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                	if(data.iserror === true){
                		$('.msgbox').html('<div class="row"><div class="col-md-12 text-center"><div class="alert alert-danger" role="alert">'+data.message+'</div></div></div>');
                	}else getprospect(contactid, token, data.message, data.iserror);
                },
                error: function(xhr, desc, err) {
                	getprospect(contactid, token, xhr + desc + "\n" + err, true);
                }
        });
    	
		setTimeout(function(){ $('#prosloader').fadeOut(); $('#btngetupdate').prop('disabled', false); }, 1000);
		
		
	});
	
	$('body').on('click', '#btnSendinvite', function (){
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    	$('#prosloader').fadeIn();
		var button 		= $(this);
		var lang 		= button.data('lang');
		var leaderid 	= button.data('leaderid');
		var contactid 	= button.data('contactid');
		var token 		= button.data('token');
		
		var form_data = {
    		contactid: contactid,
            leaderid: leaderid,
            lang: lang,
            token: token
        };
		
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/en/contact/invite',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                	getprospect(contactid, token, data.message, data.iserror);
                },
                error: function(xhr, desc, err) {
                	console.log(xhr);
                }
        });
		
		setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
		
	});
	
	
	$('body').on('click', '#btnArchiveContact', function (){
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    	$('#prosloader').fadeIn();
		var button 		= $(this);
		var lang 		= button.data('lang');
		var leaderid 	= button.data('leaderid');
		var contactid 	= button.data('contactid');
		var token 		= button.data('token');
		
		var form_data = {
    		contactid: contactid,
            leaderid: leaderid,
            callback: $("#ctc_callback").val(),
            lang: lang,
            token: token
        };
		
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/en/contact/archived',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                	getprospect(contactid, token, data.message, data.iserror);
                },
                error: function(xhr, desc, err) {
                	console.log(xhr);
                }
        });
		
		setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
		
	});
	
	
	$("#addContact").click(function() {
		
		var button 		= $(this);
		var leaderid 	= button.data('leaderid');
		var lang 		= button.data('lang');
		var token 		= button.data('token');
		
		newContact(leaderid, token, lang);
		setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
	});
	
	
	
	
	$('body').on('click', '#btngetadd', function (){
		$(this).prop('disabled', true);
		$('#prosloader').fadeIn();
		
		var button 		= $(this);
		var leaderid 	= button.data('leaderid');
		var lang 		= button.data('lang');
		var token 		= button.data('token');
		
		var form_data = {
    		ctcname: $("#ctc_name").val(),
    		ctcemail: $("#ctc_email").val(),
    		ctcphone: $("#ctc_phone").val(),
    		ctccity: $("#ctc_city").val(),
    		ctcregion: $("#ctc_region").val(),
    		ctclang: $("#ctc_lang").val(),
    		ctctype: $("#ctc_type").val(),
    		contactid: '',
            leaderid: leaderid,
            lang: lang,
            token: token
        };
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/en/contact/put',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                	if(data.iserror === true){
                		$('.msgbox').html('<div class="row"><div class="col-md-12 text-center"><div class="alert alert-danger" role="alert">'+data.message+'</div></div></div>');
                	}else{
                		$('#prosget').fadeOut( "normal", function() {
							listContacts($("#statusid").val());
							setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
						});
                	}
                },
                error: function(xhr, desc, err) {
                	
                }
        });
    	
		setTimeout(function(){ $('#prosloader').fadeOut(); $('#btngetadd').prop('disabled', false); }, 1000);
		
		
	});
	
	
	
	function newContact(leaderid, token, lang){
		
		var form_data = {
            leaderid: leaderid,
            lang: lang,
            token: token
        };
        
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/en/contact/add',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                    if(data.result===false){
                        $('#prosget').html('<div class="alert navbar-danger" role="alert">'+data.html+'</div>');
                    }else{
                        $('#proslist').fadeOut( "normal", function() {
                        	$('#prosloader').fadeIn();
                            $('#prosget').html(data.html);
                            $('#prosget').fadeIn();
                        });
                    }
                },
                error: function(xhr, desc, err) {
                	console.log(xhr + desc + "\n" + err);
                }
        });
        
        
	}
    
    function getprospect(contactid, token, message = "", iserror = false){
    	var form_data = {
    		contactid: contactid,
            leaderid: $("#leaderid").val(),
            lang: $("#lang").val(),
            msg: message,
            iserror: iserror,
            token: token
        };
        
        
        $.ajax({
              type: "POST",
              url: 'https://app.xpressleader.com/fr/contact/get',
              data: form_data,
              cache: false,
              dataType: 'json',
                success: function(data)
                {
                    if(data.result===false){
                        $('#prosget').html('<div class="alert navbar-danger" role="alert">'+data.html+'</div>');
                    }else{
                        $('#proslist').fadeOut( "normal", function() {
                        	$('#prosloader').fadeIn();
                            $('#prosget').html(data.html);
                            $('#prosget').fadeIn();
                        });
                    }
                },
                error: function(xhr, desc, err) {
                	console.log(xhr + desc + "\n" + err);
                }
        });
    }
	
	$('body').on('click', '.perpage', function (){
		$("#perpage").val($(this).data('perpage'));
		listContacts($("#statusid").val());
	});
	
	
	$('body').on('click', '.chpage', function (){
		$("#page").val($(this).data('page'));
		listContacts($("#statusid").val());
	});
	
	
	$('body').on('click', '.backContact', function (){
		$('#prosloader').fadeIn();
		$('#prosget').fadeOut( "normal", function() {
			listContacts($("#statusid").val());
			setTimeout(function(){ $('#prosloader').fadeOut(); }, 1000);
		});
		
	});
	
	
	if ( $( "#overviewurl" ).length ) {
    	jwplayer.key="Tfg2YpaOuhmMdDV2gCe6AiAywZwHR9QFbj+06TMLiIE=";
    
     	var playerInstance = jwplayer('jwdynvideo');
            playerInstance.setup({ 
            file: $("#overviewurl").val(),
            image: $("#overviewimg").val(),
            width: "100%",
            aspectratio: "16:9",
            controls: true,
            autostart: true,
            logo: {
                hide: true
            }
        });
    }
});

