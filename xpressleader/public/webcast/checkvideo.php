<?php
header('Content-type: text/html; charset=UTF-8');
error_reporting(E_ALL);
ini_set('display_errors',0);

if(isset($_GET['event']) && $_GET['event']<>""){
    
    $file = "tmpvideo/".$_GET['event'].".txt";

    if(isset($_GET['url'])){
        $offset = ($_GET['url']<>"") ? time() : "";
		if($_GET['url']==""){
		    unlink($file);
		}else{		   
    		$json = array("url" => $_GET['url'], "offset" => $offset);
            file_put_contents($file, json_encode($json));
		}
    }
    
    if(file_exists($file)){
        $json = json_decode(file_get_contents($file), true);
        $offset = (time() - $json["offset"])-5;
        $offset = ($offset<0) ? 0 : $offset;
        echo json_encode(array("url" => urldecode($json["url"]), "offset" => $offset));
    }else echo "";
}