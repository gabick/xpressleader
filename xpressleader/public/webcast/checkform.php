<?php
header('Content-type: text/html; charset=UTF-8');
error_reporting(E_ALL);
ini_set('display_errors',0);

if(isset($_GET['event']) && $_GET['event']<>""){
    
    $file = "tmpforms/".$_GET['event'].".txt";

    if(isset($_GET['isopen'])){
        $isopen = $_GET['isopen'];
		if($isopen==""){
		    unlink($file);
		}else{		   
    		$json = array("isopen" => $isopen);
            file_put_contents($file, json_encode($json));
		}
    }
    
    if(file_exists($file)){
        $json = json_decode(file_get_contents($file), true);
        echo json_encode(array("isopen" => $json["isopen"]));
    }else echo "";

}
