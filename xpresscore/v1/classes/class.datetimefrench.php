<?php 
class DateTimeFrench extends DateTime {
    
    var $english_days   = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    var $french_days    = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
    var $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    var $french_months  = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
    var $english_mon    = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    var $french_mon     = array('Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc');
    
        
    public function format($format) {
        return str_replace($this->english_mon, $this->french_mon, str_replace($this->english_months, $this->french_months, str_replace($this->english_days, $this->french_days, parent::format($format))));
    }
    
    public function getStringmonth($i,$lang="fr"){
        if($lang=="en"){
            return $this->english_months[$i-1];
        }else return $this->french_months[$i-1];
    }
}