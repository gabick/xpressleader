<?php
//KEY : deb5db6a1819a333c981f7aeaef2b34778dc33385a547fe3eb32d4fce4b6147e
//WEB : a7c8450038
//MEMBER : 169105139238474792

class bigbluebutton {
    
    var $hosts;
    var $error;
    
    function __construct() {
        $this->hosts = unserialize(BBBHOSTS);
    }
    
    /*
    Create a new meeting and return the meeting id
    return : boolean */
    public function create($meetingid, $name, $modpwd, $attpw, $lstupload, $hostindex = 0) {
        
        $bbb        = $this->loadhost($hostindex);
        $checksum   = sha1("createname=".urlencode($name)."&meetingID=$meetingid&attendeePW=$attpw&logoutURL=".urlencode(BBBLOGOUTURL)."&moderatorPW=$modpwd".$bbb->salt);
        $request    = "https://".$bbb->host."/bigbluebutton/api/create?name=".urlencode($name)."&meetingID=$meetingid&attendeePW=$attpw&logoutURL=".urlencode(BBBLOGOUTURL)."&moderatorPW=$modpwd&checksum=".$checksum;
			$xmlupload  = $this->buildpreupload($lstupload);
		$sXML       = $this->apicall($request,$xmlupload);
	
		$result     = new SimpleXMLElement($sXML);

		if($result->returncode=='SUCCESS'){
		    return true;
		    
		}else{
	        $this->error = "Cannot create meeting from the api: [".$result->message."]";
	        return false;
	    }
    }
    
    
    /*
    Join a new meeting and return the meeting id
    return : Url of the meeting */
    public function joinurl($meetingid, $fullname, $pwd, $config, $metadata = "", $hostindex = 0) {
        $bbb        = $this->loadhost($hostindex);
        $token      = $this->setconfig($meetingid, $config, $bbb);
        $paramtoken = (!$token)?"":"&configToken=".$token;
        $checksum   = sha1("joinfullName=".urlencode($fullname)."$paramtoken&meetingID=$meetingid&password=$pwd&userdata-leader=".urlencode($metadata)."".$bbb->salt);

		return "https://".$bbb->host."/bigbluebutton/api/join?"."fullName=".urlencode($fullname)."$paramtoken&meetingID=$meetingid&password=$pwd&userdata-leader=".urlencode($metadata)."&checksum=".$checksum;
    }
    
    /*
    Return list of the current meetings
    return : api object 
        <returncode>SUCCESS</returncode>
        <meetings>
          <meeting>
             <meetingID>test01</meetingID>
             <meetingName>Test</meetingName>
             <createTime>1315254777880</createTime>
             <attendeePW>ap</attendeePW>
             <moderatorPW>mp</moderatorPW>
             <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
             <running>true</running>
          </meeting>
        </meetings>*/
    public function getMeetingList($hostindex = 0){
        
        $bbb        = $this->loadhost($hostindex);
		$checksum   = sha1("getMeetings".$bbb->salt);
		$request    = "https://".$bbb->host."/bigbluebutton/api/getMeetings?checksum=".$checksum;
		$sXML       = $this->apicall($request);
		$result     = new SimpleXMLElement($sXML);
		if($result->returncode=='SUCCESS'){
		    return $result->meetings->meeting;
		}else return false;
		
	}
	
	/*
    Return meeting information
    return : api object 
        <returncode>SUCCESS</returncode>
        <meetingName>Test</meetingName>
        <meetingID>test01</meetingID>
        <createTime>1315254777880</createTime>
        <voiceBridge>70775</voiceBridge>
        <attendeePW>ap</attendeePW>
        <moderatorPW>mp</moderatorPW>
        <running>true</running>
        <recording>false</recording>
        <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
        <startTime>1315254785069</startTime>
        <endTime>0</endTime>
        <participantCount>1</participantCount>
        <maxUsers>20</maxUsers>
        <moderatorCount>1</moderatorCount>
        <attendees>
        <attendee>
            <userID>1</userID>
            <fullName>John Doe</fullName>
            <role>MODERATOR</role>
        </attendee>
        </attendees>
        <metadata/>
        <messageKey/>
        <message/>*/
	public function getmeetingdetail($meetingid, $pwd, $hostindex = 0){
	    
	    $bbb        = $this->loadhost($hostindex);
		$checksum   = sha1("getMeetingInfomeetingID=$meetingid&password=$pwd".$bbb->salt);
		$request    = "https://".$bbb->host."/bigbluebutton/api/getMeetingInfo?meetingID=$meetingid&password=$pwd&checksum=$checksum";
		$sXML       = $this->apicall($request);
		$result     = new SimpleXMLElement($sXML);
		if($result->returncode=='SUCCESS'){
		    return $result;
		}else return false;
		
	}
	
	/*
    Close an active meeting
    return : boolean*/
	public function close($meetingid, $pwd, $hostindex = 0){
	    
	    $bbb        = $this->loadhost($hostindex);
		$checksum   = sha1("endmeetingID=$meetingid&password=$pwd".$bbb->salt);
		$request    = "https://".$bbb->host."/bigbluebutton/api/end?meetingID=$meetingid&password=$pwd&checksum=$checksum";
		$sXML       = $this->apicall($request);
		$result     = new SimpleXMLElement($sXML);
		if($result->returncode=='SUCCESS'){
		    return true;
		}else return false;
		
	}
    
    /*
    Load Configuration file to retreive his token (Use with Join Meeting API)
    return : Token */
    private function setconfig($meetingid,$config,$bbb) {
        
        $configXml  = file_get_contents(APPPATH.APPVERSION."/configs/bigbluebutton/conf.$config.txt");
    
        if(!$configXml){
            $this->error = "Cannot find this layout: [".APPPATH.APPVERSION."/configs/bigbluebutton/config.$config.txt]";
            return false;
        }
		
		$configXml  = str_replace("%BBBHOST%",$bbb->host,$configXml);
		$configXml  = str_replace(array("\n","\r"),'',$configXml);
        $configXml  = str_replace(array("\t"),' ',$configXml);
        $configXml  = preg_replace('/\s+/',' ',$configXml);
        $configXml  = urlencode($configXml);
        
        $checksum   = sha1("setConfigXMLconfigXML=$configXml&meetingID=$meetingid".$bbb->salt);
		$request    = "https://".$bbb->host."/bigbluebutton/api/setConfigXML.xml?"."configXML=$configXml&meetingID=$meetingid&checksum=".$checksum;
	    $sXML       = $this->apicall($request);
	    $result     = new SimpleXMLElement($sXML);

	    if($result->returncode=='SUCCESS'){
            //$token = "&configToken=".$createxml->configToken;
            return $result->configToken;
	    }else{
	        $this->error = "Cannot retreive token from the api: [".$result->message."]";
	        return false;
	    }
    }
    
    /*
    Build the pre-upload xml
    return : XML */
    private function buildpreupload($lstupload) {
	    if($lstupload){
	        $xml = '<?xml version="1.0" encoding="UTF-8"?>
    		<modules>
    		    <module name="presentation">
    		        <document url="'.$lstupload.'"/>
    		    </module>
    		</modules>';
    		return $xml;
    		
	    }else return "";
        
    }
    
    /*
    Call the bigbluebutton api with XML data
    return : XML response */
    private function apicall($url, $data = "") {
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_FAILONERROR,1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, "$data");
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$retValue = curl_exec($ch);
		curl_close($ch);
		return $retValue;
    }
    
    /*
    Load bigbluebutton host informations bye index, or balance de cpu
    return : bbb_server class */
    private function loadhost($index = -1) {
        if($index>=0){
            return new bbb_server($this->hosts[$index][0],$this->hosts[$index][1]);
        }else{
            //#########################
            //TODO - Hosts balancing
            //#########################
            return new bbb_server($this->hosts[0][0],$this->hosts[0][1]);
        }
    }
}

class bbb_server {
    var $host;
    var $salt;
    
    function __construct($host, $salt) {
        $this->host     = $host;
        $this->salt     = $salt;
    }
}