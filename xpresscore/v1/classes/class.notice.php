<?php
class notice{

	var $error = "";

    function __construct() {

    }


    public function build($template, $parameters, $language){
        //Load template
        $tpl = file_get_contents(APPPATH."/".APPVERSION."/emails/$language.$template.html");
        return strtr($tpl, $parameters);
    }

    public function send($from, $to, $subject, $body){

        $recipients     = array($to);
        $messages       = array($body);

        $sendgrid   = new SendGrid(SGKEY);
        $email      = new SendGrid\Email();

        $email  ->setSmtpapiTos($recipients)
                ->setFrom($from)
                ->setFromName('Xpress leader')
                ->setReplyTo($from)
                ->setSubject($subject)
                ->setSubstitutions(array (
                    '%body%' => $messages
                ))
                ->setHtml('%body%');

        $sendgrid->send($email);

        return true;
    }

}
