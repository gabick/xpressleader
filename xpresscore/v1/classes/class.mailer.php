<?php
class mailer{

    private $dbh;
	var $error = "";

    function __construct(database $dbsource) {
        $this->dbh = $dbsource;
    }

    public function send_ses(){
//        require_once("/Users/glecuyer/Documents/Sites/Gabick/xpressleader/xpresscore/v1/assets/SimpleEmailService/SimpleEmailService.php");
//        require_once("/Users/glecuyer/Documents/Sites/Gabick/xpressleader/xpresscore/v1/assets/SimpleEmailService/SimpleEmailServiceMessage.php");
//        require_once("/Users/glecuyer/Documents/Sites/Gabick/xpressleader/xpresscore/v1/assets/SimpleEmailService/SimpleEmailServiceRequest.php");

        require_once("/home/xpressle/public_html/xpresscore/v1/assets/SimpleEmailService/SimpleEmailService.php");
        require_once("/home/xpressle/public_html/xpresscore/v1/assets/SimpleEmailService/SimpleEmailServiceMessage.php");
        require_once("/home/xpressle/public_html/xpresscore/v1/assets/SimpleEmailService/SimpleEmailServiceRequest.php");

        $this->dbh->query('SELECT * FROM mailstacks WHERE sent=0 LIMIT 0,10;');
        foreach($this->dbh->resultset() as $stack){
		    $fromname = ($stack->fromname != "") ? $stack->fromname : 'Xpressleader.com';

            $m = new SimpleEmailServiceMessage();
            $m->addTo($stack->email);
            $m->setFrom("noreply@".SITEDOMAIN);
            $m->setSubject($stack->subject);
            $m->setMessageFromString($stack->altbody,$stack->body);

            $region_endpoint = SimpleEmailService::AWS_US_EAST_1;
            $ses = new SimpleEmailService('AKIAIJRX7AI62ETQX4VQ', 'txNu/NISTCZGEvzRvKkOU0RHhE5vZbsMxzAF5JX/', $region_endpoint);
            $ses->sendEmail($m);

            $this->dbh->query('UPDATE mailstacks SET sent=1, datesend=NOW() WHERE id=:id;');
            $this->dbh->bind(':id', $stack->id);
            $this->dbh->execute();
        }
    }

    /*
    Get all mail from the mailstack and send what need to be send,
    Run every minutes, 10 mails at the time
    return : void */
	public function send(){
	    $mandrill = new mandrill(MDKEY);
	    $this->dbh->query('SELECT * FROM mailstacks WHERE sent=0 LIMIT 0,10;');
		foreach($this->dbh->resultset() as $stack){
		    $fromname = ($stack->fromname != "") ? $stack->fromname : 'Xpressleader.com';
            try {
                $message = array(
                    'html' => $stack->body,
                    'text' => $stack->altbody,
                    'subject' => $stack->subject,
                    'from_email' => "noreply@".SITEDOMAIN,
                    'from_name' => $fromname,
                    'to' => array(
                        array(
                            'email' => $stack->email,
                            'name' => '',
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => "noreply@xpressleader.com"),
                    'important' => true,
                    'track_opens' => true,
                    'track_clicks' => true,
                    "subaccount" => "xpressleader",
                    'inline_css' => true,
                    'url_strip_qs' => null,
                    'preserve_recipients' => true,
                    'tags' => array($stack->mailcode),
                    'metadata' => array('website' => SITEDOMAIN, 'leaderid' => $stack->leaderid, 'email' => $stack->email, 'token' => $stack->token)
                );
                $async = false;
                $ip_pool = 'Main Pool';
                $send_at = '';
                $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);

                $this->dbh->query('UPDATE mailstacks SET sent=1, datesend=NOW() WHERE id=:id;');
                $this->dbh->bind(':id', $stack->id);
                $this->dbh->execute();

                //return $result;

            } catch(Mandrill_Error $e) {
                // Mandrill errors are thrown as exceptions
                echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                throw $e;
            }
		}
	}

	private function getTemplate($name, $lang){
	    return file_get_contents(APPPATH."/".APPVERSION."/emails/$lang.$name.html");
	}

	public function signup($email, $key, $leaderid, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Activation de votre compte":"Account activation";
	    $body       = $this->getTemplate("signup", $lang);
	    $body       = str_replace("%activationlink%","http://".SITEDOMAIN."/".$lang."/activation/$key",$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "signup");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}

    public function sendPasswordEmail($email, $leaderid, $lang, $password){

        $to         = $email;
        $replyto    = "noreply@".SITEDOMAIN;
        $subject    = ($lang=="fr")?"Récupération de mot de passe":"Password recovery";
        $body       = $this->getTemplate("forgotpassword", $lang);
        $body       = str_replace("%password%", $password, $body);
        $altbody    = "";
        $leadername = ' Equipe XpressLeader       XpressLeader Team ';

        $dtNow      = new DateTime();
        $datesend   = $dtNow->format(DateTime::ISO8601);

        $token      = md5($email.time());

        $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto,fromname)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto,:fromname);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "forgotpassword");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
        $this->dbh->bind(':fromname', $leadername);
        $this->dbh->execute();

        return true;
    }




	public function billsucceed($email, $price, $nextcharge, $invoiceurl, $leaderid, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Confirmation de paiement":"Payment confirmation";
	    $body       = $this->getTemplate("billsucceed", $lang);
	    $body       = str_replace("%price%",$price,$body);
	    $body       = str_replace("%nextcharge%",$nextcharge,$body);
	    $body       = str_replace("%invoiceurl%",$invoiceurl,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "billsucceed");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}


	public function billfailed($email, $leaderid, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Échec de paiement":"Payment failure";
	    $body       = $this->getTemplate("billfailed", $lang);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "billfailed");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}



	public function billfailedsuspend($email, $leaderid, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Suspension de votre compte":"Account suspended";
	    $body       = $this->getTemplate("billfailedsuspend", $lang);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "billfailedsuspend");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}


	public function testresults($email, $leaderid, $name, $template, $lang){
	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"test":"Your test results";
	    $body       = $this->getTemplate($template, $lang);
			$body       = str_replace("%name%",$name,$body);
			$body       = str_replace("%leaderid%",$leaderid,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "testresults");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}



	public function activate($email, $password, $leaderid, $lang){
	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Bienvenue chez Xpress Leader!":"Welcome to Xpress Leader!";
	    $body       = $this->getTemplate("activate", $lang);
	    $body       = str_replace("%mail%",$email,$body);
	    $body       = str_replace("%password%",$password,$body);
	    $body       = str_replace("%leaderid%",$leaderid,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "activate");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;

	}


	public function invite($leaderid, $url, $email, $leadername, $prospectname, $subject=null){
	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    if($subject !== null) {

        } else {
            $subject    = (_LANG=="fr")?"Invitation":"Invitation";
        }
	    $body       = $this->getTemplate("invitation", "fr");
	    $body       = str_replace("%invitelink%",$url,$body);
	    $body       = str_replace("%name%",$prospectname,$body);
	    $body       = str_replace("%leaderid%",$leaderid,$body);
	    $body       = str_replace("%leadername%",$leadername,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto,fromname)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto,:fromname);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "invite");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
        $this->dbh->bind(':fromname', $leadername);
	    $this->dbh->execute();

	    return true;

	}



	public function interest($leaderid, $key, $email, $leadername, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Merci de votre intérêt":"Thanks for your interest";
	    $body       = $this->getTemplate("interest", $lang);
	    $body       = str_replace("%token%",$key,$body);
	    $body       = str_replace("%leaderid%",$leaderid,$body);
	    $body       = str_replace("%leadername%",$leadername,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "interest");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;

	}



	public function leaderinterest($leaderid, $email, $prospectname,$prospectemail= null, $phone, $lang){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"$prospectname est intéressé!":$prospectname." is interested!";
	    $body       = $this->getTemplate("leaderinterest", $lang);
        $body       = str_replace("%name%",$prospectname,$body);
        $body       = str_replace("%phone%",$phone,$body);
	    $body       = str_replace("%email%",$prospectemail,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "leaderinterest");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;

	}



	public function convert($leaderid, $key, $email, $leadermail, $leadername, $lang){

        $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $subject    = ($lang=="fr")?"Bienvenue dans l'équipe!":"Welcome to the team!";
	    $body       = $this->getTemplate("convert", $lang);
	    $body       = str_replace("%token%",$key,$body);
	    $body       = str_replace("%leaderid%",$leaderid,$body);
	    $body       = str_replace("%leadername%",$leadername,$body);
	    $body       = str_replace("%leadermail%",$leadermail,$body);
	    $body       = str_replace("%lang%",$lang,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto, fromname)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto, :fromname);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "convert");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
        $this->dbh->bind(':fromname', $leadername);
	    $this->dbh->execute();

	    return true;

	}


	private function addStack($leaderid, $fromname, $to, $replyto,$subject, $body, $altbody, $datesend, $mailcode, $token){

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto, fromname)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto, :fromname);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', $mailcode);
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
        $this->dbh->bind(':fromname', $fromname);
	    if($this->dbh->execute()) return true;
	    else return false;

	}



	public function notice($leaderid, $email, $subject, $title, $message){

	    $to         = $email;
	    $replyto    = "noreply@".SITEDOMAIN;
	    $body       = $this->getTemplate("message", "fr");
	    $body       = str_replace("%title%",$title,$body);
	    $body       = str_replace("%message%",$message,$body);
	    $altbody    = "";

	    $dtNow      = new DateTime();
	    $datesend   = $dtNow->format(DateTime::ISO8601);

	    $token      = md5($email.time());

	    $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "notice");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
	    $this->dbh->execute();

	    return true;
	}



	public function newsletter($leaderid, $email, $name){
	    $to         = $email;
        $replyto    = "noreply@".SITEDOMAIN;
        $subject    = "FORMATION MARIO TURCHETTA!";
        $body       = $this->getTemplate("newsletter", "fr");
        $body       = str_replace("%name%",$name,$body);
        $altbody    = "";

        $dtNow      = new DateTime();
        $datesend   = $dtNow->format(DateTime::ISO8601);

        $token      = md5($email.time());

        $sql = 'INSERT INTO mailstacks(leaderid,email,subject,body,altbody,datesend,mailcode,token,replyto, fromname)
                VALUES(:leaderid,:email,:subject,:body,:altbody,:datesend,:mailcode,:token,:replyto, :fromname);';
        $this->dbh->query($sql);
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':email', $to);
        $this->dbh->bind(':subject', $subject);
        $this->dbh->bind(':body', $body);
        $this->dbh->bind(':altbody', $altbody);
        $this->dbh->bind(':datesend', $datesend);
        $this->dbh->bind(':mailcode', "newsletter");
        $this->dbh->bind(':token', $token);
        $this->dbh->bind(':replyto', $replyto);
        $this->dbh->bind(':fromname', "Xpress leader");
        $this->dbh->execute();
	}
}
