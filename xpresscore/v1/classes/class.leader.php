<?php
class leader {
    
    public $leaderid;
    public $alias;
    public $name;
    public $associate;
    public $title;
    public $mail;
    public $password;
    public $address1;
    public $address2;
    public $city;
    public $region;
    public $country;
    public $postal;
    public $phone;
    public $signature;
    public $bday;
    public $bmonth;
    public $byear;
    public $lang;
    public $stripekey;
    public $dateactivate;
    public $datelastlogin;
    public $xmeetleader;
    public $fbid;
    
    private $corpoid;
    private $datasource;
	public $error = "";
	
	function __construct(database $dbsource) {
		$this->datasource = $dbsource;
    }
    
    public function updateinfo(){
        
        $this->datasource->query('UPDATE leaders SET 
                                    name=:name, 
                                    associate=:associate, 
                                    bday=:bday, 
                                    bmonth=:bmonth, 
                                    byear=:byear, 
                                    address1=:address1, 
                                    address2=:address2, 
                                    city=:city, 
                                    region=:region, 
                                    country=:country, 
                                    phone=:phone,
                                    lang=:lang,
                                    postal=:postal 
                                    WHERE code=:leaderid');
                                    
        $this->datasource->bind(':name', $this->name);
        $this->datasource->bind(':associate', $this->associate);
        $this->datasource->bind(':bday', $this->bday);
        $this->datasource->bind(':bmonth', $this->bmonth);
        $this->datasource->bind(':byear', $this->byear);
        $this->datasource->bind(':address1', $this->address1);
        $this->datasource->bind(':address2', $this->address2);
        $this->datasource->bind(':city', $this->city);
        $this->datasource->bind(':region', $this->region);
        $this->datasource->bind(':country', $this->country);
        $this->datasource->bind(':phone', $this->phone);
        $this->datasource->bind(':lang', $this->lang);
        $this->datasource->bind(':postal', $this->postal);
        $this->datasource->bind(':leaderid', $this->leaderid);

        if($this->datasource->execute()) return true;
        else return false;
        
    }
    
    
    
    
    public function updatemail(){
        
        $this->datasource->query('UPDATE leaders SET 
                                    mail=:mail
                                    WHERE code=:leaderid');
                                    
        $this->datasource->bind(':name', $this->mail);
        $this->datasource->bind(':leaderid', $this->leaderid);

        if($this->datasource->execute()) return true;
        else return false;
        
    }
    
}