<?php
class event {
    var $dbh;
	var $error = "";
	
    function __construct(database $db) {
        $this->dbh = $db;
    }
    
    public function getEventInfo($token){

            $sql = "SELECT 
                        events.leaderid,
                        events.mediaid,
                        events.type,
                        events.category,
                        events.token,
                        events.title,
                        events.password,
                        events.language,
                        events.timestart,
                        events.evmod,
                        events.evusr,
                        events.ischat,
    					leaders.`name`
    					FROM events INNER JOIN leaders ON events.leaderid = leaders.code 
    					WHERE events.token = :token;";
    		$this->dbh->query($sql);
    		$this->dbh->bind(':token', $token);
    		
            $event = $this->dbh->single();
            
            return ($event) ? $event : false;
        
    }
    
    public function createEvent($leaderid, $category, $type, $date, $title, $language, $ischat=0, $password="", $mediaid=""){
        try{
            $opendate = date ("Y-m-d H:i:s",strtotime("$date"));
            $this->error = $opendate;
            
            $token = sha1($opendate.rand(1,100)."createMe3ting!");
            $evmod = sha1($leaderid.$opendate.rand(1,100)."createM3eting!");
            $evusr = sha1(time().rand(1,100)."createMeet1ng!");
            
            $sql = 'INSERT INTO events(leaderid,type,category,password,token,title,ischat,timestart,language,evmod,evusr,mediaid)
				VALUES(
				:leaderid,
				:type,
				:category,
				:password,
				:token,
				:title,
				:ischat,
				:timestart,
				:language,
				:evmod,
				:evusr,
				:mediaid
				);';
				
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid",$leaderid);
			$this->dbh->bind(":type",$type);
			$this->dbh->bind(":category",$category);
			$this->dbh->bind(":password",$password);
			$this->dbh->bind(":token",$token);
			$this->dbh->bind(":title",$title);
			$this->dbh->bind(":ischat",$ischat);
			$this->dbh->bind(":timestart",$opendate);
			$this->dbh->bind(":language",$language);
			$this->dbh->bind(":evmod",$evmod);
			$this->dbh->bind(":evusr",$evusr);
			$this->dbh->bind(":mediaid",$mediaid);
			$this->dbh->execute();
        }catch(exception $e){
            $this->error = $e->getMessage();
            return false;
        }
        
        return true;
    }
    
    public function delete($id, $leaderid){
        
        $this->dbh->query("SELECT id FROM events WHERE id=:id AND leaderid=:leaderid;");
        $this->dbh->bind(":id",$id);
        $this->dbh->bind(":leaderid",$leaderid);
        $this->dbh->execute();
        
        if($this->dbh->rowCount()==1){
            $this->dbh->query("DELETE FROM events WHERE id=:id AND leaderid=:leaderid;");
            $this->dbh->bind(":id",$id);
            $this->dbh->bind(":leaderid",$leaderid);
            $this->dbh->execute();
            return true;
        }else return false;
    }
    
    public function getEvents($leaderid, $groupid, $category, $language, $date, $limit = 10){
        
        $catsql   = ($category =="") ? "" : " AND events.category=$category ";
        $limit = ($limit!="") ? "LIMIT $limit" : "";
        $follows = $this->following($leaderid,$groupid);
        
        if(count($follows)>0){
            $incorpo = join(',', $follows['corpo']);
		    $ingroup = (count($follows['partner'])>0) ? join(',', array_merge($follows['group'],$follows['partner'])) : join(',', $follows['group']);
		    $ingroup = ($ingroup<>"") ? "OR (events.type = 2 AND events.leaderid IN ($leaderid,$ingroup))" : "OR (events.type = 2 AND events.leaderid IN ($leaderid))";
		    
            $sql = "SELECT 
                        events.id,
                        events.leaderid,
                        events.type,
                        events.category,
                        events.token,
                        events.title,
                        events.password,
                        events.language,
                        events.timestart,
    					leaders.`name`
    					FROM events INNER JOIN leaders ON events.leaderid = leaders.code 
    					WHERE DATE_ADD(events.timestart, INTERVAL 3 HOUR)>=NOW() 
    					AND
    					(
    					(events.type = 1 AND events.leaderid=:leaderid)
    					
    					$ingroup
    					
    					OR
    					(events.type = 3 AND events.leaderid IN ($incorpo))
    					)
    					$catsql
    					 ORDER BY timestart ASC $limit;";
    		$this->dbh->query($sql);
    		$this->dbh->bind(':leaderid', $leaderid);
            return $this->dbh->resultSet();
		}else return false;
        
    }
    
    public function getRemaining($now,$future){ 
			
	    if($future < $now){ 
	        // Time has already elapsed 
	        return FALSE; 
	    } 
	    else{ 
	        // Get difference between times 
	        $time = $future - $now; 
	        $minutesFloat = $time/60; 
	        $minutes = floor($minutesFloat); 
	        $hoursFloat = $minutes/60; 
	        $hours = floor($hoursFloat); 
	        $daysFloat = $hours/24; 
	        $days = floor($daysFloat); 
	        return array(  
	            'days' => $days,  
	            'hours' => round(($daysFloat-$days)*24), 
	            'minutes' => round(($hoursFloat-$hours)*60), 
	            'seconds' => round(($minutesFloat-$minutes)*60) 
	            );
	    }
	}
	
	public function getLifeAccess($opendate,$lifetime){
	    $opendate = strtotime($opendate);
        if(time()>=($opendate+($lifetime*60))) return TRUE;
        else return FALSE;
	}

    
    private function following($leaderid, $groupid){
        $result['corpo']    = array();
        $result['group']    = array();
        $result['partner']  = array();
        
        $this->dbh->query('SELECT corpotoken FROM groups WHERE id=:groupid;');
        $this->dbh->bind(':groupid', $groupid);
		$group = $this->dbh->single();
		
		$this->dbh->query('SELECT leaderid FROM follows_groups WHERE groupid=:groupid;');
		$this->dbh->bind(':groupid', $groupid);
		$follows = $this->dbh->resultSet();
		foreach($follows as $follow){
            array_push($result['group'], $follow->leaderid);
		}
		
		$result['group'] = $this->follow_filter($leaderid, $result['group']);
		
		$this->dbh->query('SELECT groupid FROM follows_partners WHERE leaderid=:leaderid AND approval=1;');
        $this->dbh->bind(':leaderid', $leaderid);
		$partners = $this->dbh->resultSet();
		foreach($partners as $partner){
		    $this->dbh->query('SELECT leaderid FROM follows_groups WHERE groupid=:groupid;');
    		$this->dbh->bind(':groupid', $partner->groupid);
    		$follows = $this->dbh->resultSet();
    		foreach($follows as $follow){
    		    array_push($result['partner'], $follow->leaderid);
    		}
		}
		
		
		
		$result['partner'] = $this->follow_filter($leaderid, $result['partner']);
		
        $this->dbh->query('SELECT leaderid FROM follows_corpo WHERE corpotoken=:corpotoken;');
		$this->dbh->bind(':corpotoken', $group->corpotoken);
		$follows = $this->dbh->resultSet();
		foreach($follows as $follow){
		    array_push($result['corpo'], $follow->leaderid);
		}
		
		return $result;
    }
    
    private function follow_filter($leaderid,$follows){
        $lstunfollows = array();
        if(is_array($follows)){
            $this->dbh->query('SELECT unfollow FROM unfollows WHERE leaderid=:leaderid;');
    		$this->dbh->bind(':leaderid', $leaderid);
    		$unfollows = $this->dbh->resultSet();
    		foreach($unfollows as $unfollow){
    		    array_push($lstunfollows, $unfollow->unfollow);
    		}
    		
    		$follows = array_diff($follows, $lstunfollows);
        }
        return $follows;
    }
    
}