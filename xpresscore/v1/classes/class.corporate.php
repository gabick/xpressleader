<?php
class corporate {
    
    private $corpoid;
    private $dbh;
	var $error = "";
	
	function __construct(database $dbsource) {
		$this->dbh = $dbsource;
    }
    
    /*
    Get all leaders from this corporate
    return : leaders objects array */
    public function listLeaders($token, $key = false, $latest = 0){
        
        $limit      = ($latest>0) ? "LIMIT $latest" : "";
        $sqlsearch  = ($key) ? 'AND (leaders.firstname LIKE "%'.$_GET["key"].'%" OR leaders.lastname LIKE "%'.$_GET["key"].'%" OR leaders.code LIKE "%'.$_GET["key"].'%" OR leaders.alias LIKE "%'.$_GET["key"].'%" OR leaders.mail LIKE "%'.$_GET["key"].'%")' : ''; 
        
        $this->dbh->query("SELECT
                            leaders.`code` AS leaderid,
                            leaders.alias,
                            leaders.proid AS proid,
                            corporates.leaderid AS corpoid,
                            billings.planid AS planid,
                            billings.chargetry AS chargetry,
                            plans.state AS plan,
                            plans.name_fr AS planfr,
                            plans.name_en AS planen,
                            corporates.token AS corpotoken,
                            leaders.`name`,
                            leaders.firstname,
                            leaders.lastname,
                            leaders.xmeetleader,
                            leaders.mail,
                            leaders.password,
                            leaders.address1,
                            leaders.address2,
                            leaders.city,
                            leaders.region,
                            leaders.country,
                            leaders.postal,
                            leaders.phone,
                            leaders.lang,
                            leaders.ispro,
                            leaders.datelastlogin,
                            leaders.datecreation,
                            leaders.groupid,
                            groups.`name` AS groupname,
                            groups.leaderid AS proid,
                            fbusers.profile_url AS profileurl,
                            fbusers.state AS fbstate
                            FROM
                            leaders
                            LEFT JOIN billings ON leaders.`code` = billings.leaderid
                            LEFT JOIN corporates ON leaders.guruid = corporates.id
                            LEFT JOIN plans ON plans.id = billings.planid
                            LEFT JOIN groups ON leaders.groupid = groups.id 
                            LEFT JOIN fbusers ON fbusers.leaderid = leaders.`code`
                            WHERE corporates.token=:token AND billings.chargetry < 2 $sqlsearch 
                            ORDER BY billings.chargetry,leaders.`name` ASC 
                            $limit");
        $this->dbh->bind(':token', $token);
		return $this->dbh->resultset();
		
    }
    
    /*
    Get all PRO leaders from this corporate
    return : leaders objects array */
    public function listPros($corpoid, $latest = 0){
        
        $limit = ($latest>0) ? "LIMIT $latest" : "";
        
        $this->dbh->query('SELECT code,alias,name,city,region,country,proteam FROM leaders WHERE guruid=:guruid AND ispro=1 ORDER BY datecreation ASC '.$limit);
        $this->dbh->bind(':guruid', $corpoid);
		return $this->dbh->resultset();
		
    }
    
    
    /*
    Get all Groups from this corporate
    return : leaders objects array */
    public function listGroups($token, $latest = 0){
        
        $limit = ($latest>0) ? "LIMIT $latest" : "";
        
        $this->dbh->query('SELECT
                            groups.id,
                            groups.`name` AS groupname,
                            leaders.`name`,
                            leaders.city,
                            leaders.region,
                            leaders.country,
                            groups.datecreation
                            FROM
                            groups
                            INNER JOIN leaders ON groups.leaderid = leaders.`code` 
                            where groups.corpotoken = :token
                            ORDER BY datecreation ASC '.$limit);
        $this->dbh->bind(':token', $token);
		return $this->dbh->resultset();
		
    }
    
    /*
    Get corporate informations
    return : corporates objects array */
    public function getCorporate($token){
        $this->dbh->query('SELECT * FROM corporates WHERE token=:guruid');
        $this->dbh->bind(':guruid', $token);
		return $this->dbh->single();
    }
    
    /*
    Get a single leader record
    return : leader object */
    public function getleader($corpoid, $leaderid){
        
        $this->dbh->query('SELECT * FROM leaders WHERE guruid=:guruid AND code=:leaderid');
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':guruid', $corpoid);
		return $this->dbh->single();
    }
    
    /*
    Load statistics of this corporate
    return : Stats array */
    public function getstats($corpoid, $leaderid = ""){
        
        $stats = array();
        $stats['interest']  = 0;
        $stats['meet']      = 0;
        $stats['look']      = 0;
        $stats['visit']     = 0;
        
        if($leaderid<>""){
            $this->dbh->query('SELECT * FROM leaders WHERE guruid=:guruid AND id=:leaderid');
            $this->dbh->bind(':guruid', $corpoid);
            $this->dbh->bind(':leaderid', $leaderid);
        }else{
            $this->dbh->query('SELECT * FROM leaders WHERE guruid=:guruid');
            $this->dbh->bind(':guruid', $corpoid);
        }
    		$rows = $this->dbh->resultset();
		
		foreach($rows as $leader){
		    $this->dbh->query('SELECT * FROM xlook_stats WHERE leaderid=:leaderid;');
		    $this->dbh->bind(':leaderid', $leader->code);
		    foreach($this->dbh->resultset() as $look){
		        if($look->status==0) $stats['look'] +=1;
		        elseif($look->status==1) $stats['meet'] +=1;
		        elseif($look->status==2) $stats['interest'] +=1;
		    }
		    
		    $this->dbh->query('SELECT * FROM xlook_visits WHERE leaderid=:leaderid;');
		    $this->dbh->bind(':leaderid', $leader->code);
		    foreach($this->dbh->resultset() as $visit){
		        $stats['visit'] +=1;
		    }
		}
		
		return $stats;
    }
    
    
    public function getCorpoStats($corpoid){
		$sql = 'SELECT leaderid, COUNT(id) AS nbr, `status` AS cat FROM dbclients GROUP BY leaderid,`status` ORDER BY dateupdate DESC;';
		$this->dbh->query($sql);
		
		$stats = array();
		foreach($this->dbh->resultSet() as $res){
			$stats[$res->leaderid][$res->cat] = $res->nbr;
		}
		return $stats;
	}
    
    
    public function getSummaries($corpoid, $leaderid = ""){
		$stats = array();
		$stats["meeting"] 	= 0;
		$stats["interest"] 	= 0;
		$stats["client"] 	= 0;
		$stats["pending"] 	= 0;
		
		$isleader = ($leaderid != "") ? "WHERE leaderid=:leaderid" : "";
		$sql = 'SELECT COUNT(id) AS nbr, `status` AS cat FROM dbclients '.$isleader.' GROUP BY `status` ORDER BY dateupdate DESC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		
		foreach($this->dbh->resultSet() as $res){
		    if($res->cat == 0) $stats["pending"] = $res->nbr;
			if($res->cat == 1) $stats["meeting"] = $res->nbr;
			if($res->cat == 2) $stats["interest"] = $res->nbr;
			if($res->cat == 3) $stats["client"] = $res->nbr;
		}
		
		return $stats;
	}
    
    
    public function updateBilling($leaderid, $planid, $nextcharge){
        
        $this->dbh->query("SELECT leaderid FROM billings WHERE leaderid=:leaderid;");
        $this->dbh->bind(":leaderid", $leaderid);
        $this->dbh->execute();
        
        if($this->dbh->rowCount()==1){
            $this->dbh->query("UPDATE billings SET planid=:planid, nextcharge=:nextcharge  WHERE leaderid=:leaderid");
            $this->dbh->bind(":planid", $planid);
            $this->dbh->bind(":nextcharge", $nextcharge);
            $this->dbh->bind(":leaderid", $leaderid);
            $this->dbh->execute();
        }else{
            $this->dbh->query("INSERT INTO billings(leaderid,planid,nextcharge,chargetry) VALUES(:leaderid,:planid,:nextcharge,0);");
            $this->dbh->bind(":planid", $planid);
            $this->dbh->bind(":nextcharge", $nextcharge);
            $this->dbh->bind(":leaderid", $leaderid);
            $this->dbh->execute();
        }
        return true;
    }
    
    public function listPlans() {
	    $this->dbh->query("SELECT * FROM plans");
		return $this->dbh->resultset();
	}
	
	public function getMedialist($lang = "frca"){
	    $this->dbh->query("SELECT * FROM xmeeting_medias WHERE lang=:lang ORDER BY position ASC");
	    $this->dbh->bind(":lang", $lang);
	    return $this->dbh->resultSet();
	}
	public function getMediaGroup(){
	    $this->dbh->query("SELECT lang FROM xmeeting_medias GROUP BY lang");
	    return $this->dbh->resultSet();
	}
	
	public function getEventDoclist($corpoid){
	    $this->dbh->query("SELECT * FROM event_medias WHERE corpotoken=:corpotoken");
	    $this->dbh->bind(":corpotoken", $corpoid);
	    return $this->dbh->resultSet();
	}

}