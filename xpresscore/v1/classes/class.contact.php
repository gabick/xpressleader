<?php
class contact {
    
    var $dbh;
	var $error = "";
	
	function __construct($dbsource){
		$this->dbh = $dbsource;
	}
	
// 	function contact($dbsource){
// 		$this->dbh = $dbsource;
// 		return true;
// 	}
	
	
	
	/* ADD CONTACT
	/* Add a unique contact.
	###################################################################################*/
    public function add($leaderid, $email, $name, $phone, $city, $region, $lang, $category = 0){
        
        //Check if contact already exist
        $sql = "SELECT id FROM dbclients WHERE mail=:mail AND leaderid=:leaderid;";
    	$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->bind(":mail", $email);
		$this->dbh->execute();
		
		if($this->dbh->rowcount()==0){
		    
		    $sql = 'INSERT INTO dbclients(leaderid, mail, name, phone, city, region, category, lang, datecreation)  VALUES(:leaderid, :mail, :name, :phone, :city, :region, :lang, :category, NOW());'; 
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":mail", $email);
			$this->dbh->bind(":name", $name);
			$this->dbh->bind(":phone", $phone);
			$this->dbh->bind(":city", $city);
			$this->dbh->bind(":region", $region);
			$this->dbh->bind(":lang", $lang);
			$this->dbh->bind(":category", $category);
			if($this->dbh->execute()){
			    return true;
			} else return false;
			
		}else return false;
    }
	
	
	/* UPDATE CONTACT($LEADERID, $EMAIL, $NAME, $PHONE, $INVITE)
	/* Add a unique contact.
	###################################################################################*/
    public function update($contactid, $email, $name, $phone, $city, $region, $lang, $category = 0){
        
        //Check if contact already exist
        $sql = "SELECT id FROM dbclients WHERE id=:contactid;";
    	$this->dbh->query($sql);
		$this->dbh->bind(":contactid", $contactid);
		$this->dbh->execute();
		
		if($this->dbh->rowcount()==0){
		    return false;
		}else{
		    $sql = 'UPDATE dbclients SET name=:name, mail=:mail, phone=:phone, city=:city, region=:region, lang=:lang, category=:category, archived=0 WHERE id=:contactid;'; 
			$this->dbh->query($sql);
			$this->dbh->bind(":contactid", $contactid);
			$this->dbh->bind(":mail", $email);
			$this->dbh->bind(":name", $name);
			$this->dbh->bind(":phone", $phone);
			$this->dbh->bind(":city", $city);
			$this->dbh->bind(":region", $region);
			$this->dbh->bind(":lang", $lang);
			$this->dbh->bind(":category", $category);
			if($this->dbh->execute()){
			    return true;
			} else return false;
		}
		
    }
    
    
    
    
    public function search($leaderid, $status, $key = "", $page = 1, $perpage = 100, $orderby = "name", $sort="ASC"){
		$sqlsearch = "";
		switch(mb_strtoupper($status)){
			case "ALL":
				$sqlsearch .= " AND dbclients.`archived`=0";
				break;
			case "PENDING":
				$sqlsearch = " AND dbclients.`status`=0 AND dbclients.`token` IS NULL AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "INVITED":
				$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`>NOW()) AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "EXPIRED":
				$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`<NOW()) AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "MEETING":
				$sqlsearch = " AND dbclients.`status`=1 AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "INTEREST":
				$sqlsearch = " AND dbclients.`status`=2 AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "BUILDER":
				$sqlsearch = " AND dbclients.`category`=2 AND dbclients.`archived`=0";
				break;
			case "CLIENT":
				$sqlsearch = " AND dbclients.`category`=1 AND dbclients.`archived`=0";
				break;
			case "ARCHIVED":
				$sqlsearch = " AND dbclients.`archived`=1";
				break;
			default:
				$sqlsearch = "";
				break;
		}
		
		if ($key<>"") $sqlsearch =  " AND (dbclients.`name` LIKE '%".$key."%' OR dbclients.`mail` LIKE '%".$key."%')";
		
		$start 		= ($page * $perpage) - $perpage;
		
		$sql 	= 'SELECT * FROM dbclients WHERE dbclients.`leaderid`=:leaderid  '.$sqlsearch.' ORDER BY dbclients.`'.$orderby.'` '.$sort.' LIMIT '.$start.','.$perpage.';';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		return $this->dbh->resultSet();
		
	}
	
	
	
	public function statusCount($leaderid, $status){
		$sqlsearch = "";
		switch(mb_strtoupper($status)){
			case "ALL":
				$sqlsearch .= " AND dbclients.`archived`=0";
				break;
			case "PENDING":
				$sqlsearch = " AND dbclients.`status`=0 AND dbclients.`token` IS NULL AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "INVITED":
				$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`>=NOW()) AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "EXPIRED":
					$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`<NOW()) AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "MEETING":
				$sqlsearch = " AND dbclients.`status`=1 AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "INTEREST":
				$sqlsearch = " AND dbclients.`status`=2 AND dbclients.`category`=0 AND dbclients.`archived`=0";
				break;
			case "BUILDER":
				$sqlsearch = " AND dbclients.`category`=2 AND dbclients.`archived`=0";
				break;
			case "CLIENT":
				$sqlsearch = " AND dbclients.`category`=1 AND dbclients.`archived`=0";
				break;
			case "ARCHIVED":
				$sqlsearch = " AND dbclients.`archived`=1";
				break;
			default:
				$sqlsearch = "";
				break;
		}
		
		$sql 	= 'SELECT COUNT(id) AS nbr FROM dbclients WHERE dbclients.`leaderid`=:leaderid  '.$sqlsearch.';';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$res = $this->dbh->single();
  		if($res){
  			return $res->nbr;
  		}else return 0;
	}
}