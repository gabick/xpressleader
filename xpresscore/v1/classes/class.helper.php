<?php
class helper {
    
    private $datasource;
	var $error = "";
	
    function __construct(database $dbsource) {
        $this->datasource = $dbsource;
    }
    
    public function listcountries(){
        
        $this->datasource->query("SELECT * FROM countries");
        return $this->datasource->resultset();
        
    }
    public function liststates(){
        
        $this->datasource->query("SELECT * FROM states");
        return $this->datasource->resultset();
        
    }
    public function listprovinces(){
        
        $this->datasource->query("SELECT * FROM provinces");
        return $this->datasource->resultset();
        
    }
    
    public function ucfirstHTMLentity($matches){
        return "&".ucfirst(strtolower($matches[1])).";";
}
    
    public function fullUpper($str){
        return  strtoupper(htmlentities($str, null, 'UTF-8'));
        $pattern = '/&([A-Z]+);/';
    }
    
    
}