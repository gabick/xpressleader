<?php
class xero {
    
	var $error = "";
	var $XeroOAuth;
	private $oauthSession;
	
	
	function __construct(){
		require APPPATH.APPVERSION.'/assets/xeroOauth/XeroOAuth.php';
		$useragent = "XPRESSAPP XERO";
        $signatures = array(
            'consumer_key'     => XRO_CONSUMER,
            'shared_secret'    => 's',
            'core_version'=> '2.0',
            'payroll_version'=> '1.0',
            'file_version' => '1.0'
        );
        
        if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
        	$signatures ['rsa_private_key'] = APPPATH.APPVERSION . '/configs/certs/privatekey.pem';
        	$signatures ['rsa_public_key'] = APPPATH.APPVERSION . '/configs/certs/publickey.cer';
        }
		
		$this->XeroOAuth = new XeroOAuth ( array_merge ( array (
        		'application_type' => XRO_APP_TYPE,
        		'oauth_callback' => OAUTH_CALLBACK,
        		'user_agent' => $useragent 
        ), $signatures ) );
        
    	
    	$this->XeroOAuth->config['access_token']          = $this->XeroOAuth->config ['consumer_key'];
        $this->XeroOAuth->config['access_token_secret']   = $this->XeroOAuth->config ['shared_secret'];
        $this->XeroOAuth->config['session_handle']        = '';

		
		return true;
	}
	
	
	
	public function lstInvoices($contactid){
	    
	    $response = $this->XeroOAuth->request('GET', $this->XeroOAuth->url('Invoices', 'core'), array('Where' => 'Contact.ContactID==Guid("'.$contactid.'")'), "","json");
	    if ($this->XeroOAuth->response['code'] == 200) {
	        $response   = json_decode($response['response']);
	        return $response->Invoices;
	    }
    
	    return false;
	    
	}
	
	public function getInvoice($invoiceid){
	    
	    $response = $this->XeroOAuth->request('GET', $this->XeroOAuth->url('Invoices', 'core'), array('Where' => 'InvoiceID==Guid("'.$invoiceid.'")'), "","json");
	    if ($this->XeroOAuth->response['code'] == 200) {
	        $response   = json_decode($response['response']);
	        return $response->Invoices;
	    }
    
	    return false;
	    
	}
	
	public function addContact($leaderid, $name, $email, $address1, $address2, $city, $region, $postalcode, $country, $xeroid = ""){
	    $xmlcontactid = ($xeroid != "") ? "<ContactID>$xeroid</ContactID>" : "";
	    
	    $name = htmlspecialchars($name);
	    $address1 = htmlspecialchars($address1);
	    $address2 = htmlspecialchars($address2);
	    $region = htmlspecialchars($region);
	    $xml = "<Contacts><Contact>
	              $xmlcontactid
                  <AccountNumber>$leaderid</AccountNumber>
                  <Name>$name</Name>
                  <EmailAddress>$email</EmailAddress>
                  <Addresses>  
                    <Address>  
                      <AddressType>POBOX</AddressType>  
                        <AddressLine1>$address1</AddressLine1>  
                        <AddressLine2>$address2</AddressLine2> 
                        <City>$city</City>  
                        <Region>$region</Region>
                        <PostalCode>$postalcode</PostalCode> 
                        <Country>$country</Country>
                    </Address>
                  </Addresses> 
                  <DefaultCurrency>USD</DefaultCurrency>
                </Contact></Contacts>";
        
        $response = $this->XeroOAuth->request('POST', $this->XeroOAuth->url('Contacts', 'core'), array(), $xml,"json");
	    if ($this->XeroOAuth->response['code'] == 200) {
	        $contacts =  json_decode($response['response']);
	        return $contacts->Contacts[0]->ContactID;
	    }else return false;
	}
	
	
	public function addInvoice($contactid, $lines, $AccountCode = 200, $payment = true){
	    $curdate = date("Y-m-d");
	    
	    $xmllines = "";
	    foreach($lines as $line){
	        $xmllines .= "<LineItem>
	                        <ItemCode>{$line->item}</ItemCode>
                            <Description>{$line->description}</Description>
                            <Quantity>{$line->quantity}</Quantity>
                            <UnitAmount>{$line->price}</UnitAmount>
                            <AccountCode>$AccountCode</AccountCode>
                          </LineItem>";
                        
	    }
	    
	    $xml = "<Invoices>
                      <Invoice>
                        <Type>ACCREC</Type>
                        <Contact>
                          <ContactID>$contactid</ContactID>
                        </Contact>
                        <LineAmountTypes>NoTax</LineAmountTypes>
                        <DueDate>$curdate</DueDate>
                        <LineItems>
                          $xmllines
                       </LineItems>
                       <Status>AUTHORISED</Status>
                       <BrandingThemeID>fa4b8877-5ed2-45fa-8841-dc1ca606a0b2</BrandingThemeID>
                       <SentToContact>true</SentToContact>
                     </Invoice>
                   </Invoices>";
        $response = $this->XeroOAuth->request('POST', $this->XeroOAuth->url('Invoices', 'core'), array(), $xml,"json");
	    if ($this->XeroOAuth->response['code'] == 200) {
	        $invoices   =  json_decode($response['response']);
	        $InvoiceID  =  $invoices->Invoices[0]->InvoiceID;
	        $AmountDue  = $invoices->Invoices[0]->AmountDue;
	        
	        $xml = "
	        <Payments>
              <Payment>
                <Invoice>
                  <InvoiceID>$InvoiceID</InvoiceID>
                </Invoice>
                <Account>
                  <Code>100</Code>
                </Account>
                <Date>$curdate</Date>
                <Amount>$AmountDue</Amount>
              </Payment>
            </Payments>";
            
            $response = $this->XeroOAuth->request('POST', $this->XeroOAuth->url('Payments', 'core'), array(), $xml,"json");
	        if ($this->XeroOAuth->response['code'] == 200) {
	            return $InvoiceID;
	        }else return false;
	    }else return false;
	}
	
	
	public function getOnlineInvoice($invoiceid){
	    
	    $response = $this->XeroOAuth->request('GET', $this->XeroOAuth->url('Invoices/'.$invoiceid.'/OnlineInvoice', 'core'), array(), "","json");
	    if ($this->XeroOAuth->response['code'] == 200) {
	        $OnlineInvoices   =  json_decode($response['response']);
	        return $OnlineInvoices->OnlineInvoices[0]->OnlineInvoiceUrl;
	    }else return false;
	   
	}
}