<?php
class account {

    var $dbh;
	var $error = "";

	var $leader;
	var $islogin = false;

	function __construct(database $db) {
	    $this->dbh = $db;
    }

    public function signin($username, $password) {
		$this->dbh->query("SELECT code,alias,name,mail,city,region,country,lang FROM leaders WHERE mail=:username AND password=:password");
		$this->dbh->bind(':username', $username);
        $this->dbh->bind(':password', $password);
		$leader = $this->dbh->single();

		if($leader){

		    $this->dbh->query("UPDATE leaders SET datelastlogin=NOW() WHERE code=:leaderid");
            $this->dbh->bind(':leaderid', $leader->code);
            $this->dbh->execute();

            $this->leader = $leader;
            return true;

		}else return false;

	}


	public function signup($firstname, $lastname, $email, $leaderid, $lang) {

		$this->dbh->query("SELECT code FROM leaders WHERE mail=:email");
		$this->dbh->bind(':email', $email);
		$leader = $this->dbh->single();

		if($leader) return false;
		else{

		    //Generate an activation key
		    $activekey = md5($email.$lastname.time());

		    $this->dbh->query("INSERT INTO activations(`key`,mail, password, referal,lang,firstname,lastname) VALUES(:key,:email,:password,:referal,:lang,:firstname,:lastname);");
            $this->dbh->bind(':key', $activekey);
            $this->dbh->bind(':email', $email);
            $this->dbh->bind(':password', "");
            $this->dbh->bind(':referal', $leaderid);
            $this->dbh->bind(':lang', $lang);
            $this->dbh->bind(':firstname', $firstname);
            $this->dbh->bind(':lastname', $lastname);
            $this->dbh->execute();

            $mailer = new mailer($this->dbh);
			$mailer->signup($email, $activekey, $leaderid, $lang);
		    return true;
		}

	}

	public function unsubscribe($leaderid){
		$this->dbh->query("UPDATE billings SET chargetry=2 WHERE leaderid=:leaderid");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->execute();
		return true;
	}


	public function activate($sessactivate) {

		if(!isset($sessactivate)) return false;
		if($this->existEmail($sessactivate["mail"])) return false;

		//Verify validation Key and get email
		$activationinfo = $this->validateKey($sessactivate["key"]);

		//Get referal information
		$referalinfo = $this->getLeader($sessactivate["referal"]);

		if($activationinfo){

			//Generate leader account number
			$leaderid = $this->CreateLeaderId();

			try {
				//Create Stripe Customer and assign subscribtion
				\Stripe\Stripe::setApiKey(STRIPEKEY);
				$stripecustomer 	= \Stripe\Customer::create(array(
										"description" => $leaderid,
										"email" => $sessactivate["mail"],
										"source"  => $sessactivate["cctoken"] //Obtained with Stripe.js
									));


			} catch (Stripe_Error $e) {
				$body = $e->getJsonBody();
				$err  = $body['error'];
			  	$this->error = $err['message'];
			  	return false;
			} catch (Exception $e) {
				$this->error  = $e->getMessage();
				return false;
			}

			$sql = "INSERT INTO leaders(
					leaders.`code`,
					leaders.alias,
					leaders.`name`,
					leaders.firstname,
					leaders.lastname,
					leaders.associate,
					leaders.mail,
					leaders.`password`,
					leaders.address1,
					leaders.address2,
					leaders.city,
					leaders.region,
					leaders.country,
					leaders.postal,
					leaders.phone,
					leaders.bday,
					leaders.bmonth,
					leaders.byear,
					leaders.lang,
					leaders.plan,
					leaders.referal,
					leaders.guruid,
					leaders.groupid,
					leaders.stripekey,
					leaders.`status`,
					leaders.dateactivate,
					leaders.datecreation
					) 
					VALUES (:code, :alias, :name, :firstname, :lastname, :associate, :mail, :password, :address1, :address2, :city, :region, :country, :postal, :phone, :bday, :bmonth, :byear, :lang, :plan, :referal, :guruid, 1, :stripekey, 1, NOW(), NOW())";
			$this->dbh->query($sql);
			$this->dbh->bind(':code',$leaderid);
			$this->dbh->bind(':alias',$leaderid);
			$this->dbh->bind(':name',$sessactivate["firstname"].' '.$sessactivate["lastname"]);
			$this->dbh->bind(':firstname',$sessactivate['firstname']);
			$this->dbh->bind(':lastname',$sessactivate['lastname']);
			$this->dbh->bind(':associate',"");
			$this->dbh->bind(':mail',$activationinfo->mail);
			$this->dbh->bind(':password',$sessactivate['password']);
			$this->dbh->bind(':address1',$sessactivate['address1']);
			$this->dbh->bind(':address2',$sessactivate['address2']);
			$this->dbh->bind(':city',$sessactivate['city']);
			$this->dbh->bind(':region',$sessactivate['region']);
			$this->dbh->bind(':country',$sessactivate['country']);
			$this->dbh->bind(':postal',$sessactivate['postal']);
			$this->dbh->bind(':phone',$sessactivate['phone']);
			$this->dbh->bind(':bday',$sessactivate['bday']);
			$this->dbh->bind(':bmonth',$sessactivate['bmonth']);
			$this->dbh->bind(':byear',$sessactivate['byear']);
			$this->dbh->bind(':lang',$sessactivate['lang']);
			$this->dbh->bind(':plan',$sessactivate['plan']);
			$this->dbh->bind(':referal',$sessactivate['referal']);
			$this->dbh->bind(':guruid',"1");
			$this->dbh->bind(':stripekey',$stripecustomer["id"]);
			if($this->dbh->execute()){


				$billing = new billing($this->dbh);
				if($billing->subscribe($leaderid, $sessactivate['plan'])){

					$mailer = new mailer($this->dbh);
					$mailer->activate($activationinfo->mail, $sessactivate['password'], $leaderid, $sessactivate['lang']);

					//Delete activation key
					$this->dbh->query("DELETE FROM activations WHERE mail=:email;");
					$this->dbh->bind(':email',$activationinfo->mail);
					$this->dbh->execute();

					return true;

				}else{
					$this->dbh->query("DELETE FROM leaders WHERE `code`=:leaderid;");
					$this->dbh->bind(":leaderid", $leaderid);
					$this->dbh->execute();
					return false;
				}




			}else return false;

		}

	}

	public function forgotpassword($email) {
        $sql = "SELECT code,password,lang FROM leaders WHERE mail = '$email'";
		$this->dbh->query($sql);
		$this->dbh->bind(':email', $email);
        $forgottenLeader = $this->dbh->single();

		if($forgottenLeader){
            $mailer = new mailer($this->dbh);
            $mailer->sendPasswordEmail($email, $forgottenLeader->code, $forgottenLeader->lang, $forgottenLeader->password);
		    return true;
		}
		else return false;

	}

	public function validateKey($key){
	    $this->dbh->query("SELECT * FROM activations WHERE `key`=:key;");
		$this->dbh->bind(':key', $key);
		return $this->dbh->single();
	}

	public function existLeaderid($leaderid){
	    $this->dbh->query("SELECT * FROM leaders WHERE `code`=:leaderid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->execute();

		if($this->dbh->rowCount()>=1) return true;
		else return false;
	}

	public function existEmail($email){
	    $this->dbh->query("SELECT id FROM leaders WHERE mail=:email;");
		$this->dbh->bind(':email', $email);
		$this->dbh->execute();

		if($this->dbh->rowCount()>=1) return true;
		else return false;
	}

	public function clearActivations(){
	    $this->dbh->query("DELETE FROM activations WHERE DATE_ADD(datecreation, INTERVAL 1 DAY)<=NOW();");
	    $this->dbh->execute();
	    return true;
	}

	/*
    Update profile from a leaderid
    return : leader object */
	public function updateProfile($leaderid, $firstname, $lastname, $gender, $bday, $bmonth, $byear, $mail, $pwd, $address1, $address2, $country, $city, $region, $postal, $phone, $language) {
	    $this->dbh->query("UPDATE leaders 
                    	    SET 
                    	    name=:name, 
                    	    firstname=:firstname, 
                    	    lastname=:lastname,
                    	    gender=:gender, 
                    	    bday=:bday, 
                    	    bmonth=:bmonth, 
                    	    byear=:byear, 
                    	    mail=:mail, 
                    	    password=:password, 
                    	    address1=:address1, 
                    	    address2=:address2, 
                    	    country=:country, 
                    	    city=:city, 
                    	    region=:region, 
                    	    postal=:postal, 
                    	    phone=:phone, 
                    	    lang=:language,
                    	    dateupdate=NOW()
                    	    WHERE code=:leaderid;");

	    $this->dbh->bind(':name', $firstname. ' ' .$lastname);
	    $this->dbh->bind(':firstname', $firstname);
	    $this->dbh->bind(':lastname', $lastname);
	    $this->dbh->bind(':gender', $gender);
	    $this->dbh->bind(':bday', $bday);
	    $this->dbh->bind(':bmonth', $bmonth);
	    $this->dbh->bind(':byear', $byear);
	    $this->dbh->bind(':mail', $mail);
	    $this->dbh->bind(':password', $pwd);
	    $this->dbh->bind(':address1', $address1);
	    $this->dbh->bind(':address2', $address2);
	    $this->dbh->bind(':country', $country);
	    $this->dbh->bind(':city', $city);
	    $this->dbh->bind(':region', $region);
	    $this->dbh->bind(':postal', $postal);
	    $this->dbh->bind(':phone', $phone);
	    $this->dbh->bind(':language', $language);
	    $this->dbh->bind(':leaderid', $leaderid);

	    if($this->dbh->execute()) return true;
	    else return false;
	}

	public function updateStripe($leaderid, $token){
		$this->dbh->query("UPDATE leaders SET stripekey=:token, dateupdate=NOW() WHERE code=:leaderid;");
	    $this->dbh->bind(':token', $token);
	    $this->dbh->bind(':leaderid', $leaderid);
	    if($this->dbh->execute()) return true;
	    else return false;
	}

	/*
    Get a leader by his leader id,
    Checkalias define if we're searching the alias too
    return : leader object */
	public function getLeader($leaderid, $checkalias = false) {

	    $this->dbh->query("SELECT
                                leaders.`code` AS leaderid,
                                leaders.alias,
                                corporates.leaderid AS corpoid,
                                billings.planid AS planid,
                                billings.nextcharge,
                                billings.chargetry,
                                plans.name_fr AS planfr,
                                plans.name_en AS planen,
                                plans.state AS plan,
                                corporates.token AS corpotoken,
                                leaders.`name`,
                                leaders.firstname,
                                leaders.lastname,
                                leaders.xmeetleader,
                                leaders.gender,
                                leaders.mail,
                                leaders.password,
                                leaders.address1,
                                leaders.address2,
                                leaders.city,
                                leaders.region,
                                leaders.country,
                                leaders.associate,
                                leaders.postal,
                                leaders.phone,
                                leaders.bmonth,
                                leaders.bday,
                                leaders.byear,
                                leaders.lang,
                                leaders.ispro,
                                leaders.datelastlogin,
                                leaders.datecreation,
                                leaders.stripekey,
                                leaders.groupid,
                                leaders.fbid,
                                leaders.xeroid,
                                leaders.ctcperpage,
                                fbusers.fbid AS fbuserid,
                                fbusers.state AS fbstate,
                                fbusers.profile_url AS fbprofile,
                                groups.`name` AS groupname,
                                groups.leaderid AS groupleaderid
                                FROM
                                leaders
                                LEFT JOIN billings ON leaders.`code` = billings.leaderid
                                LEFT JOIN corporates ON leaders.guruid = corporates.id
                                LEFT JOIN plans ON plans.id = billings.planid
                                LEFT JOIN fbusers ON leaders.`code` = fbusers.leaderid
                                LEFT JOIN groups ON leaders.groupid = groups.id WHERE code=:leaderid OR alias=:leaderid");

	    $this->dbh->bind(':leaderid', $leaderid);
		return $this->dbh->single();
	}

	/*
    Get a Group by a leader id,
    Checkalias define if we're searching the alias too
    return : leader object */
	public function getGroup($leaderid, $checkalias = false) {
	    $this->dbh->query("SELECT
							leaders.`code` AS leaderid,
							leaders.firstname,
							leaders.lastname,
							groups.leaderid AS groupleaderid,
							groups.`name`,
							groups.corpotoken,
							groups.datecreation,
							corporates.ciename,
							corporates.`name` AS corponame
							FROM
							leaders
							INNER JOIN groups ON groups.id = leaders.groupid
							INNER JOIN corporates ON leaders.guruid = corporates.id
                            WHERE leaders.`code`=:leaderid;");
	    $this->dbh->bind(':leaderid', $leaderid);
	    return $this->dbh->single();
	}


	/*
    Get a Group by a PRO leader id,
    Checkalias define if we're searching the alias too
    return : leader object */
	public function getProGroup($leaderid, $checkalias = false) {
	    $this->dbh->query("SELECT
                            leaders.`code` AS leaderid,
                            leaders.firstname,
                            leaders.lastname,
                            groups.leaderid AS groupleaderid,
                            groups.`name`,
                            groups.corpotoken
                            FROM
                            leaders
                            INNER JOIN groups ON groups.leaderid = leaders.`code`
                            WHERE leaders.`code`=:leaderid;");
	    $this->dbh->bind(':leaderid', $leaderid);
	    return $this->dbh->single();
	}

	/*
    Create a Group for a leader,
    name is max 40 chars
    return : boolean */
	public function addGroup($leaderid, $name) {

	    $leader = $this->getLeader($leaderid);

	    //Return false if the leader have a group already
	    if($leader->groupleaderid==$leaderid) {
	        return false;
	    }

	    $this->dbh->query("INSERT INTO groups(leaderid,name,corpotoken) VALUES(:leaderid,:name,:corpotoken);");
	    $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':name', $name);
        $this->dbh->bind(':corpotoken', $leader->corpotoken);

        if($this->dbh->execute()){
            $groupid = $this->dbh->lastInsertId();

            $this->dbh->query("UPDATE leaders SET groupid=:groupid WHERE code=:leaderid");
    	    $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->bind(':groupid', $groupid);
            $this->dbh->execute();

            $this->dbh->query("DELETE FROM follows_groups WHERE leaderid=:leaderid");
    	    $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->execute();

            $this->dbh->query("DELETE FROM follows_partners WHERE leaderid=:leaderid");
    	    $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->execute();

            $this->dbh->query("INSERT INTO follows_groups(leaderid,groupid) VALUES(:leaderid,:groupid);");
            $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->bind(':groupid', $groupid);
            $this->dbh->execute();

            return true;
        }else return false;
	}

	/*
    Delete a group and everything related to it
    return : boolean */
	public function removeGroup($groupid){

	    $this->dbh->query("DELETE FROM groups WHERE id=:groupid");
	    $this->dbh->bind(':groupid', $groupid);
        $this->dbh->execute();
	}

	/*
    Assign a leader to a PRO account(Group)
    return : boolean */
	public function assignGroup($leaderid, $groupid) {

	    $this->dbh->query("UPDATE leaders SET groupid=:groupid WHERE code=:leaderid;");
	    $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':groupid', $groupid);
        if($this->dbh->execute()){

            //Check if this leader own a group
            $this->dbh->query("SELECT id FROM groups WHERE leaderid=:leaderid;");
            $this->dbh->bind(':leaderid', $leaderid);
            $group = $this->dbh->single();

            if($group){
                //Re-assign all leaders from the group to the new group
                $this->dbh->query("UPDATE leaders SET groupid=:groupid WHERE groupid=:owngroup;");
                $this->dbh->bind(':groupid', $groupid);
                $this->dbh->bind(':owngroup', $group->id);

                //Delete the owned group
                $this->dbh->query("DELETE FROM groups WHERE id=:groupid");
        	    $this->dbh->bind(':groupid', $group->id);
                $this->dbh->execute();
            }
            //Delete all public presenters of this leader
            $this->dbh->query("DELETE FROM follows_groups WHERE leaderid=:leaderid;");
    	    $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->execute();

            //Delete all partners of this leader
            $this->dbh->query("DELETE FROM follows_partners WHERE leaderid=:leaderid;");
    	    $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->execute();

            return true;

        }else return false;
	}

	/*
    Send a request to join a group
    return : boolean */
	public function requestGroup($leaderid, $groupid, $message="") {

	    $this->dbh->query("DELETE FROM group_requests WHERE leaderid=:leaderid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->execute();

		$date = new DateTime('NOW');
        $date->add(new DateInterval('P3D'));

	    $this->dbh->query("INSERT INTO group_requests(leaderid,groupid,message,status,expiration) VALUES(:leaderid,:groupid,:message,0,:expiration);");
	    $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':groupid', $groupid);
        $this->dbh->bind(':message', $message);
        $this->dbh->bind(':expiration', $date->format('Y-m-d H:i:s'));
        if($this->dbh->execute()){
            return true;
        }else return false;
	}

	/*
    Get my sent request to join a group
    return : boolean */
	public function getGroupRequest($leaderid) {

	    $this->dbh->query("SELECT
                            group_requests.id,
                            group_requests.leaderid,
                            group_requests.groupid,
                            group_requests.message,
                            group_requests.`status`,
                            group_requests.expiration,
                            groups.`name` AS groupname,
                            leaders.`name`
                            FROM
                            group_requests
                            INNER JOIN groups ON group_requests.groupid = groups.id
                            INNER JOIN leaders ON groups.leaderid = leaders.`code` WHERE group_requests.leaderid=:leaderid;");
		$this->dbh->bind(':leaderid', $leaderid);
        return $this->dbh->resultset();

	}

	/*
    Get my sent request to join a group
    return : boolean */
	public function getGroupProRequest($groupid) {

	    $this->dbh->query("SELECT
                            group_requests.id,
                            group_requests.leaderid,
                            group_requests.groupid,
                            group_requests.message,
                            group_requests.`status`,
                            group_requests.expiration,
                            groups.`name` AS groupname,
                            leaders.`name`
                            FROM
                            group_requests
                            INNER JOIN groups ON group_requests.groupid = groups.id
                            INNER JOIN leaders ON group_requests.leaderid = leaders.`code` WHERE group_requests.groupid=:groupid;");
		$this->dbh->bind(':groupid', $groupid);
        return $this->dbh->resultSet();

	}

	public function deleteGroupRequest($leaderid) {

	    $this->dbh->query("DELETE FROM group_requests WHERE leaderid=:leaderid;");
		$this->dbh->bind(':leaderid', $leaderid);
		if($this->dbh->execute()){
		    return true;
		}else return false;

	}

	/*
    Get all billing informations from a leader
    return : billing object */
	public function getBilling($leaderid) {

	    $this->dbh->query("SELECT * FROM billings WHERE leaderid=:leaderid");
		$this->dbh->bind(':leaderid', $leaderid);
		return $this->dbh->single();
	}

    /*
    Get plan from the planid
    return : boolean */
	public function getPlan($planid) {

	    $this->dbh->query("SELECT * FROM plans WHERE id=:planid");
		$this->dbh->bind(':planid', $planid);
		return $this->dbh->single();

	}


	public function isGroupPresenter($leaderid,$groupid) {

	    $this->dbh->query("SELECT * FROM follows_groups WHERE leaderid=:leaderid AND groupid=:groupid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()==1) return true;
		else return false;
	}

	public function isCorpoPresenter($leaderid,$corpotoken) {

	    $this->dbh->query("SELECT * FROM follows_corpo WHERE leaderid=:leaderid AND corpotoken=:corpotoken;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':corpotoken', $corpotoken);
		$this->dbh->execute();

		if($this->dbh->rowCount()==1) return true;
		else return false;
	}

	public function addGroupPresenter($leaderid,$groupid) {

	    $this->dbh->query("SELECT * FROM follows_groups WHERE leaderid=:leaderid AND groupid=:groupid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()==1){
		    return false;
		}else{
		    $this->dbh->query("INSERT INTO follows_groups(leaderid,groupid) VALUES(:leaderid,:groupid);");
    		$this->dbh->bind(':leaderid', $leaderid);
    		$this->dbh->bind(':groupid', $groupid);
    		$this->dbh->execute();
    		return true;
		}

	}


	public function removeGroupPresenter($leaderid,$groupid) {

	    $this->dbh->query("SELECT * FROM follows_groups WHERE leaderid=:leaderid AND groupid=:groupid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()==1){
		    $this->dbh->query("DELETE FROM follows_groups WHERE leaderid=:leaderid AND groupid=:groupid;");
    		$this->dbh->bind(':leaderid', $leaderid);
    		$this->dbh->bind(':groupid', $groupid);
    		$this->dbh->execute();
    		return true;
		}else{
    		return false;
		}

	}


	public function getGroupStats($groupid){

		$sql = 'SELECT leaderid, COUNT(id) AS nbr, `status` AS cat FROM dbclients GROUP BY leaderid,`status` ORDER BY dateupdate DESC;';
		$this->dbh->query($sql);

		$stats = array();
		foreach($this->dbh->resultSet() as $res){
			$stats[$res->leaderid][$res->cat] = $res->nbr;
		}
		return $stats;

	}


	/*
    Get all leaders from this corporate
    return : leaders objects array */
    public function listLeaders($groupid, $latest = 0){

        $limit = ($latest>0) ? "LIMIT $latest" : "";

        $this->dbh->query('SELECT
                            leaders.`code` AS leaderid,
                            leaders.alias,
                            leaders.proid AS proid,
                            corporates.leaderid AS corpoid,
                            billings.planid AS planid,
                            plans.name_fr AS planfr,
                            plans.name_en AS planen,
                            corporates.token AS corpotoken,
                            leaders.`name`,
                            leaders.mail,
                            leaders.address1,
                            leaders.address2,
                            leaders.city,
                            leaders.region,
                            leaders.country,
                            leaders.postal,
                            leaders.phone,
                            leaders.lang,
                            leaders.ispro,
                            leaders.datelastlogin,
                            leaders.datecreation,
                            leaders.groupid,
                            groups.`name` AS groupname,
                            groups.leaderid AS proid
                            FROM
                            leaders
                            LEFT JOIN billings ON leaders.`code` = billings.leaderid
                            LEFT JOIN corporates ON leaders.guruid = corporates.id
                            LEFT JOIN plans ON plans.id = billings.planid
                            LEFT JOIN groups ON leaders.groupid = groups.id 
                            WHERE leaders.groupid = :groupid
                            ORDER BY name ASC '.$limit);
        $this->dbh->bind(':groupid', $groupid);


		return $this->dbh->resultset();

    }

	public function follow($leaderid, $presenterid, $state){
	    if($state=="true"){
	        $sql = "DELETE FROM unfollows WHERE leaderid=:leaderid AND unfollow=:presenterid;";
	        $this->dbh->query($sql);
            $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->bind(':presenterid', $presenterid);
            $this->dbh->execute();
            return true;
	    }else{
	        $this->dbh->query("SELECT leaderid FROM unfollows WHERE leaderid=:leaderid AND unfollow=:presenterid;");
            $this->dbh->bind(':leaderid', $leaderid);
            $this->dbh->bind(':presenterid', $presenterid);
            $this->dbh->execute();
            if($this->dbh->rowCount()==0){
    	        $this->dbh->query("INSERT INTO unfollows(leaderid,unfollow) VALUES(:leaderid,:presenterid);");
                $this->dbh->bind(':leaderid', $leaderid);
                $this->dbh->bind(':presenterid', $presenterid);
                $this->dbh->execute();
                return true;
            }else return false;
	    }

	}

	public function listFollows($leaderid,$groupid, $groupleaderid = ""){

	    $result["corpo"] = array();
        $result["group"] = array();

        $this->dbh->query('SELECT corpotoken FROM groups WHERE id=:groupid;');
        $this->dbh->bind(':groupid', $groupid);
		$group = $this->dbh->single();

		$lstfollows = array();
		$this->dbh->query('SELECT leaderid FROM follows_groups WHERE groupid=:groupid;');
		$this->dbh->bind(':groupid', $groupid);
		$follows = $this->dbh->resultSet();
		foreach($follows as $follow){
		    array_push($lstfollows, $follow->leaderid);
		}

		$this->dbh->query('SELECT groupid FROM follows_partners WHERE leaderid=:leaderid OR leaderid=:groupleader AND approval=1;');
        $this->dbh->bind(':leaderid', $leaderid);
        $this->dbh->bind(':groupleader', $groupleaderid);
		$partners = $this->dbh->resultSet();
		foreach($partners as $partner){
		    $this->dbh->query('SELECT leaderid FROM follows_groups WHERE groupid=:groupid;');
    		$this->dbh->bind(':groupid', $partner->groupid);
    		$follows = $this->dbh->resultSet();
    		foreach($follows as $follow){
    		    array_push($lstfollows, $follow->leaderid);
    		}
		}

		if(count($lstfollows)>0){
    		$in = join(',', $lstfollows);
    		$this->dbh->query("SELECT
                                leaders.`code` AS leaderid,
                                leaders.alias,
                                leaders.`name`,
                                leaders.mail,
                                leaders.address1,
                                leaders.address2,
                                leaders.city,
                                leaders.region,
                                leaders.country,
                                leaders.lang,
                                leaders.groupid,
                                groups.leaderid AS groupleader,
                                (SELECT unfollow FROM unfollows WHERE leaderid='$leaderid' AND unfollow=leaders.`code`) AS unfollow
                                FROM
                                leaders
                                LEFT JOIN groups ON leaders.groupid = groups.id 
                                WHERE leaders.`code` IN ($in)");
            $result['group'] = $this->dbh->resultSet();
		}

        $lstfollows = array();
        $this->dbh->query('SELECT leaderid FROM follows_corpo WHERE corpotoken=:corpotoken;');
		$this->dbh->bind(':corpotoken', $group->corpotoken);
		$follows = $this->dbh->resultSet();
		foreach($follows as $follow){
		    array_push($lstfollows, $follow->leaderid);
		}

		if(count($lstfollows)>0){
    		$in = join(',', $lstfollows);
    		$this->dbh->query("SELECT
                                leaders.`code` AS leaderid,
                                leaders.alias,
                                leaders.`name`,
                                leaders.mail,
                                leaders.address1,
                                leaders.address2,
                                leaders.city,
                                leaders.region,
                                leaders.country,
                                leaders.lang,
                                leaders.groupid,
                                groups.leaderid AS groupleader
                                FROM
                                leaders
                                LEFT JOIN groups ON leaders.groupid = groups.id 
                                WHERE leaders.`code` IN ($in)");
            $result['corpo'] = $this->dbh->resultSet();
		}
		return $result;
	}

	public function listGroups($corpotoken){
	    $this->dbh->query('SELECT * FROM groups WHERE corpotoken=:corpotoken;');
	    $this->dbh->bind(':corpotoken', $corpotoken);
	    return $this->dbh->resultSet();
	}

	public function listPartners($leaderid){

	    $lstgroups = array();
		$this->dbh->query('SELECT
                            follows_partners.groupid,
                            follows_partners.approval,
                            groups.`name` AS groupname,
                            groups.leaderid AS groupleaderid,
                            leaders.`name`,
                            leaders.city,
                            leaders.region,
                            leaders.country
                            FROM
                            follows_partners
                            INNER JOIN groups ON follows_partners.groupid = groups.id
                            INNER JOIN leaders ON groups.leaderid = leaders.`code`
                            WHERE follows_partners.leaderid=:leaderid;');
        $this->dbh->bind(':leaderid', $leaderid);
		return $this->dbh->resultSet();

	}


	public function listAvailablePartners($leaderid){
		$this->dbh->query('SELECT
                            groups.id AS groupid,
                            UPPER(groups.`name`) AS groupname,
                            groups.leaderid AS groupleaderid,
                            leaders.`name`,
                            leaders.city,
                            leaders.region,
                            leaders.country
                            FROM
                            groups
                            INNER JOIN leaders ON groups.leaderid = leaders.`code`;');
        $this->dbh->bind(':leaderid', $leaderid);
		return $this->dbh->resultSet();
	}

	public function addPartner($leaderid,$groupid,$message=""){
	    $this->dbh->query("SELECT leaderid FROM follows_partners WHERE leaderid=:leaderid AND groupid=:groupid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()==0){
		    $this->dbh->query("INSERT INTO follows_partners(token,leaderid,groupid,approval,message) VALUES(:token,:leaderid,:groupid,1,:message);");
		    $this->dbh->bind(':token', sha1($leaderid.time().'addP4rt3r4l1fe'));
    	    $this->dbh->bind(':leaderid', $leaderid);
    	    $this->dbh->bind(':groupid', $groupid);
    	    $this->dbh->bind(':message', nl2br($message));
    	    $this->dbh->execute();
    	    return true;
		}else return false;
	}

	public function removePartner($leaderid,$groupid){
	    $this->dbh->query("SELECT leaderid FROM follows_partners WHERE leaderid=:leaderid AND groupid=:groupid;");
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()>0){
		    $this->dbh->query("DELETE FROM follows_partners WHERE leaderid=:leaderid AND groupid=:groupid;");
    	    $this->dbh->bind(':leaderid', $leaderid);
    	    $this->dbh->bind(':groupid', $groupid);
    	    $this->dbh->execute();
    	    return true;
		}else return false;
	}


	public function getLayout($leaderid,$proid,$corpoid){
	    $this->dbh->query('SELECT profile,profile_description,showprofile,showlocation,showphone,showemail,herotext,overview_title,overview_description FROM layouts WHERE leaderid=:leaderid;');
		$this->dbh->bind(':leaderid', $leaderid);
		$layout = $this->dbh->single();

		if($layout){
		    return $layout;
		}else{
    		$this->dbh->query('SELECT "" AS profile,"" AS profile_description,0 AS showlocation,0 AS showphone,0 AS showemail,0 AS showprofile, herotext,overview_title,overview_description FROM layouts WHERE leaderid=:leaderid;');
    		$this->dbh->bind(':leaderid', $proid);
    		$layout = $this->dbh->single();
    		if($layout) return $layout;
    		else{
    		    $this->dbh->query('SELECT "" AS profile,"" AS profile_description,0 AS showlocation,0 AS showphone,0 AS showemail,0 AS showprofile, herotext,overview_title,overview_description FROM layouts WHERE leaderid=:leaderid;');
        		$this->dbh->bind(':leaderid', $corpoid);
        		return $this->dbh->single();
    		}
		}
	}

	public function saveLayout($leaderid,$corpotoken,$profile,$profiledesc,$showprofile,$showlocation,$showphone,$showemail,$herotext,$overviewtitle,$overviewdescription){

	    $this->dbh->query('SELECT profile,profile_description,showprofile,showlocation,showphone,showemail,herotext,overview_title,overview_description FROM layouts WHERE leaderid=:leaderid;');
		$this->dbh->bind(':leaderid', $leaderid);
		$layout = $this->dbh->single();

		if($layout){
		    $this->dbh->query('UPDATE layouts SET 
		    profile=:profile,
		    profile_description=:profile_description,
		    showprofile=:showprofile,
		    showlocation=:showlocation,
		    showphone=:showphone,
		    showemail=:showemail,
		    herotext=:herotext,
		    overview_title=:overview_title,
		    overview_description=:overview_description
		     WHERE leaderid=:leaderid;');

		    $this->dbh->bind(':profile', $profile);
		    $this->dbh->bind(':profile_description', $profiledesc);
		    $this->dbh->bind(':showprofile', $showprofile);
		    $this->dbh->bind(':showlocation', $showlocation);
		    $this->dbh->bind(':showphone', $showphone);
		    $this->dbh->bind(':showemail', $showemail);
		    $this->dbh->bind(':herotext', $herotext);
		    $this->dbh->bind(':overview_title', $overviewtitle);
		    $this->dbh->bind(':overview_description', $overviewdescription);
		    $this->dbh->bind(':leaderid', $leaderid);

		    if($this->dbh->execute()) return true;

		}else{

		    $this->dbh->query('INSERT INTO layouts(leaderid,corpotoken,profile,profile_description,showprofile,showlocation,showphone,showemail,herotext,overview_title,overview_description) 
			    VALUES(
				    :leaderid,
				    :corpotoken,
				    :profile,
				    :profile_description,
				    :showprofile,
				    :showlocation,
				    :showphone,
				    :showemail,
				    :herotext,
				    :overview_title,
				    :overview_description
			    );');

		    $this->dbh->bind(':leaderid', $leaderid);
		    $this->dbh->bind(':corpotoken', $corpotoken);
		    $this->dbh->bind(':profile', $profile);
		    $this->dbh->bind(':profile_description', $profiledesc);
		    $this->dbh->bind(':showprofile', $showprofile);
		    $this->dbh->bind(':showlocation', $showlocation);
		    $this->dbh->bind(':showphone', $showphone);
		    $this->dbh->bind(':showemail', $showemail);
		    $this->dbh->bind(':herotext', $herotext);
		    $this->dbh->bind(':overview_title', $overviewtitle);
		    $this->dbh->bind(':overview_description', $overviewdescription);

		    if($this->dbh->execute()) return true;

		}
		return false;
	}

	public function isFromGroup($leaderid, $groupid){
	    $this->dbh->query('SELECT id FROM leaders WHERE `code`=:leaderid AND groupid=:groupid;');
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->bind(':groupid', $groupid);
		$this->dbh->execute();

		if($this->dbh->rowCount()==1) return true;
		else return false;
	}


	//PRIVATE FUNCTIONS
	//###########################################################################################

	private function CreateValidationKey(){
		return md5(md5(time().'xPr3ssLe4d3R'.rand(0,9999)));
	}

	private function CreateRandomPassword(){
		return substr(hash('sha512',rand().'xPr3ssLe4d3R'),0,12);
	}

	private function CreateLeaderId(){
		for (;;) {
	        $leaderid = substr(number_format(time() * rand(),0,'',''),0,6);
	        if(!$this->IsLeaderIdExist($leaderid)) return $leaderid;
	    }
	}

	private function IsMailExist($mail){
		$sql = 'SELECT mail FROM leaders WHERE mail=:email';
		$this->dbh->bind(':email', $mail);
		$this->dbh->execute();

		if($this->dbh->rowCount()>0) return true;
		else return false;
	}

	private function IsLeaderIdExist($leaderid){
		$sql = 'SELECT code FROM leaders WHERE code=:leaderid';
		$this->dbh->bind(':leaderid', $leaderid);
		$this->dbh->execute();

		if($this->dbh->rowCount()>0) return true;
		else return false;
	}

}
