<?php
class freshbooks {
    
    private $fbhost;
    private $fbapi;
    
    function __construct($fbhost, $fbapi) {
		$this->fbhost = $fbhost;
		$this->fbapi  = $fbapi;
    }
    
    
    public function build($apicall){
        
        $xml  = file_get_contents(APPPATH."/".APPVERSION."/freshbooks/$apicall.xml");
        return new SimpleXMLElement($xml);
        
    }
    
    
    public function request($fbrequest)
    {

        $ch = curl_init();    // initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $this->fbhost); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 40s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fbrequest->asXML()); // add POST fields
        curl_setopt($ch, CURLOPT_USERPWD, $this->fbapi . ':X');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        return new SimpleXMLElement(curl_exec($ch));

    }
    
    public function getPDF($invoiceid){
        
        $xml    = file_get_contents(APPPATH."/".APPVERSION."/freshbooks/invoice.getPDF.xml");
        $obj    = new SimpleXMLElement($xml);
        
        $obj->invoice_id = $invoiceid;
        
        $ch = curl_init();    // initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $this->fbhost); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 40s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $obj->asXML()); // add POST fields
        curl_setopt($ch, CURLOPT_USERPWD, $this->fbapi . ':X');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        return curl_exec($ch);
    }

}
