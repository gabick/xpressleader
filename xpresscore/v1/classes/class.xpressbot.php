<?php
class xpressbot {
    
    var $dbh;
	var $error = "";
	
	private $api_url = 'https://graph.facebook.com/v2.7';
	public $botdata;
	
	var $MESSAGE_URL = "https://graph.facebook.com/v2.7/me/messages?access_token=";
	
	var $userObj      = null;
	var $response       = "";
	
	private $ISOUT          = array("fr"=>"Oups! Vous êtes déjà désabonné :)", "en"=>"Oops! Your already unsubscribed from me =)");
	
	private $START_OPTIN    = array("fr"=>"Bonjour %firstname%, je suis le robot Xpressleader. Désirez-vous recevoir les alertes et informations importantes concernant votre compte ?", "en"=>"Hello %firstname%, i am the Xpress Robot. Would you like to receive alerts and useful informations about your account from me ?");
	private $START_ROPTIN    = array("fr"=>"Bonjour %firstname%, désirez-vous recevoir à nouveau les alertes et informations importantes concernant votre compte ?", "en"=>"Hello %firstname%, would you like to receive again alerts and useful informations about your account from me ?");
	
	private $START_OPTOUT   = array("fr"=>"Désirez-vous vous désabonner de notre système d'alerte automatisé ?", "en"=>"Would you like to unsubscribe from me ?");
	
	private $OPTIN_YES      = array("fr"=>"BRAVO! Vous êtes maintenant abonné à notre système d'alertes facebook. Je vais vous envoyer des notices par messagerie lorsque des événements importants surviendront dans votre compte.", "en"=>"BRAVO! Your are now subscribe and i'm ready to communicate with you any useful informations that i find in your Xpressleader.com account. Let's get started!");
    private $OPTIN_YES2     = array("fr"=>"En tout temps vous pouvez vous désabonner afin de ne plus recevoir nos alertes.", "en"=>"Don't forget, at any moment, you can unsubscribe from me and i'll stop sending you message promptly!");
    private $btnunsub       = array("fr"=>"ME DÉSABONNER", "en"=>"UNSUBSCRIBE ME");
    
    private $OPTIN_NO       = array("fr"=>"Aucun problème %firstname%, connectez-vous à votre compte Xpressleader afin d'activer vos alertes facebook si vous changez d'avis.", "en"=>"No problem %firstname%! Login to your Xpress leader account or type SUBSCRIBE if you change your mind.");
    private $btnconnect     = array("fr"=>"Se connecter", "en"=>"Log in");
    
    private $OPTOUT_YES     = array("fr"=>"Vous êtes maintenant désabonné de notre système d'alerte facebook. Vous ne recevrez plus aucun message automatisé de ma part.", "en"=>"You are now unsubscribe, you will no longuer get any message from me. Farewell leader :(");
    private $OPTOUT_NO      = array("fr"=>"OK! Je vous tiens informé des prochains événements! :)", "en"=>"GOT IT! You are still a subscriber.I keep you in touch leader =)");
    
    private $btnno          = array("fr"=>"NON!", "en"=>"NO!");
    private $btnyes         = array("fr"=>"OUI ABSOLUMENT!", "en"=>"SURE OF COURSE!");
    private $btnyesplease   = array("fr"=>"OUI, S'IL VOUS PLAÎT", "en"=>"YES, PLEASE");
    
    
    private $lblwatching   	= array("fr"=>"regarde présentement votre survol", "en"=>"is currently watching the overview");
    private $lblcompleted   = array("fr"=>"complété", "en"=>"completed");
    
    function __construct(database $db) {
	    $this->dbh = $db;
    }
	
	public function verify_payload($signature,$payload) {
		$signature = explode('=',$signature)[1];
		$verify = hash_hmac('sha1',$payload,FBAPPSECRET);
		return ($verify == $signature);
	}

	public function batch_singlepost($recipients,$image,$title,$subtitle,$url) {
		$batch = array();
		foreach($recipients as $recipient) {
			unset($bodyparts);

			$message = array(
				'attachment' => array(
					'type' => 'template',
					'payload' => array(
						'template_type' => 'generic',
						'elements' => array(array(
							'title' => $title,
							'image_url' => $image,
							'subtitle' => $subtitle,
							'buttons' => array(array(
								'type' => 'web_url',
								'url' => $url,
								'title' => $this->bot->configuration->view_article
							))
						))
					)
				)
			);
			
			$bodyparts[] = 'recipient='.json_encode(array('id'=>$recipient));
			$bodyparts[] = 'message='.json_encode($message);
			$body = implode('&',$bodyparts);
			$batch[] = array('method'=>'POST','relative_url'=>'me/messages','body'=>$body);
		}
		$output = 'batch='.urlencode(json_encode($batch));
		$this->batch_post($output);
	}

	private function batch_post($data,$parameters=array()) {
		$defaultParameters = array('access_token='.FBPAGETOKEN,'include_headers=false');
		$url = $this->api_url.'?'.implode('&',array_merge($parameters,$defaultParameters));
		$options = array(
			'http' => array(
				'method'  => 'POST',
				'content' => $data
			)
		);
		$context = stream_context_create($options);
		return json_decode(file_get_contents($url, false, $context));
	}
	
	private function send($url,$method,$data){
		$options = array(
			'http' => array(
				'method'  => $method,
				'header'  => 'Content-type: application/json',
				'content' => ($data!=null?json_encode($data):null)
			)
		);
		
		$context = stream_context_create($options);
		$results = file_get_contents($url, false, $context);
		return json_decode($results);
	}
	
	
	public function execute($request){
		$obj = json_decode($request);
		
		
		foreach($obj->entry as $entry){
			
			foreach($entry->messaging as $messaging){
				$userid     = $messaging->sender->id;
				
				//GET EVENT ID
				$eventid    = $this->getEvent($messaging);
				if(isset($messaging->optin->ref)) {
					$eventid = 0;
					$messaging->postback = new stdClass();
					$messaging->postback->payload = 'GETSTARTED';
					
				}
				
				
				switch($eventid){
					
					case 0 : //POSTBACK
						$payload = $messaging->postback->payload;
						
						if($payload=="OPTIN_YES"){
							$fbuser     = $this->getUser($userid);
							$this->sendMessage($fbuser, $this->OPTIN_YES[$fbuser->lang]);
							
							$buttons = array(
												array(
												"type"=>"postback",
												"title"=>$this->btnunsub[$fbuser->lang],
												"payload"=>"OPTOUT_YES")
											);
							$this->sendTemplate($fbuser, $this->OPTIN_YES2[$fbuser->lang], $buttons);
							$this->updateState($fbuser->fbid, 1);
							
						}elseif($payload=="OPTIN_NO"){
							$fbuser     = $this->getUser($userid);
							$buttons = array(
												array(
												"type"=>"web_url",
												"url"=>"https://xpressleader.com/".$fbuser->lang,
												"title"=>$this->btnconnect[$fbuser->lang])
											);
							$this->sendTemplate($fbuser, $this->OPTIN_NO[$fbuser->lang], $buttons, array("%firstname%"=>$fbuser->firstname));
							$this->updateState($fbuser->fbid, 0);
							
						}elseif($payload=="OPTOUT_YES"){
							$fbuser     = $this->getUser($userid);
							$this->sendMessage($fbuser, $this->OPTOUT_YES[$fbuser->lang]);
							$this->updateState($fbuser->fbid, 2);
							
						}elseif($payload=="OPTOUT_NO"){
							$fbuser     = $this->getUser($userid);
							$this->sendMessage($fbuser, $this->OPTOUT_NO[$fbuser->lang]);
							$this->updateState($fbuser->fbid, 1);
						
						}elseif($payload=="STATS"){
							$fbuser     = $this->getUser($userid);
							mail("info@sotnox.net", "Bot response", print_r($messaging,true));
							
						}elseif($payload=="GETSTARTED"){
						    
						    $referral 	= (isset($messaging->optin->ref)) ? $messaging->optin->ref : '' ;
							$fbuser     = $this->getUser($userid, $referral);
							
							
							
							
							
							if($fbuser->state == 0 || $fbuser->state == 2) {
								$buttons = array(
												array(
												"type"=>"postback",
												"title"=>$this->btnno[$fbuser->lang],
												"payload"=>"OPTIN_NO"),
												
												array(
												"type"=>"postback",
												"title"=>$this->btnyes[$fbuser->lang],
												"payload"=>"OPTIN_YES")
											);
								$this->sendTemplate($fbuser, $this->START_OPTIN[$fbuser->lang], $buttons, array("%firstname%"=>$fbuser->firstname));
								$this->updateState($fbuser->fbid, 0);
											
							}else{
								$buttons = array(
												array(
													"type"=>"postback",
													"title"=>$this->btnno[$fbuser->lang],
													"payload"=>"OPTOUT_NO"),
												array(
													"type"=>"postback",
													"title"=>$this->btnyesplease[$fbuser->lang],
													"payload"=>"OPTOUT_YES")
											);
								$this->sendTemplate($fbuser, $this->START_OPTOUT[$fbuser->lang], $buttons); //$this->bot->configuration->optout_question
							}
							
							
							
						}else{
							$fbuser     = $this->getUser($userid);
							
							$this->dbh->query("SELECT id,name,mail,phone,`position`,`status`,dtwatch FROM dbclients WHERE id=:prospectid;");
							$this->dbh->bind(":prospectid", $payload);
							$prospect = $this->dbh->single();
							
							$progress = ($prospect->position > 0) ? number_format(($prospect->position / 1200)*100,0) : 0;
							$progress = ($prospect->position >= 1200) ? 100 : $progress;
							$progress = "- $progress% ".$this->lblcompleted[$fbuser->lang];
							
							$iswatching = ($prospect->dtwatch != null && ((strtotime($prospect->dtwatch)+10) >= (time()))) ? $this->lblwatching[$fbuser->lang] : "";
							$message 	= $prospect->name. " " . $iswatching . " " . $progress;
							
							$this->sendMessage($fbuser, $message);
						}
						
					break;
					
					case 4 : // KEYWORDS FILTERING
				        
						if($userid != "962555527186808"){
							$fbuser  = $this->getUser($userid);
							
							if(!$fbuser) return "200";
							
							switch(true){ //You can put thos keywords in a sentence.
								case stripos($this->normalize($messaging->message->text),'DESINSCRIRE') !== false:
								case stripos($this->normalize($messaging->message->text),'DESINSCRIPTION') !== false:
								case stripos($this->normalize($messaging->message->text),'DESABONNEMENT') !== false:
								case stripos($this->normalize($messaging->message->text),'DESABONNER') !== false:
								case stripos($this->normalize($messaging->message->text),'UNSUBSCRIBE') !== false:
									if($fbuser->state == 1) {
										$buttons = array(
												array(
													"type"=>"postback",
													"title"=>$this->btnno[$fbuser->lang],
													"payload"=>"OPTOUT_NO"),
												array(
													"type"=>"postback",
													"title"=>$this->btnyesplease[$fbuser->lang],
													"payload"=>"OPTOUT_YES")
											);
        								$this->sendTemplate($fbuser, $this->START_OPTOUT[$fbuser->lang], $buttons);
        								$this->updateState($fbuser->fbid, 3);
									}else{
										$this->sendMessage($fbuser, $this->ISOUT[$fbuser->lang]);
										
										$buttons = array(
												array(
												"type"=>"postback",
												"title"=>$this->btnno[$fbuser->lang],
												"payload"=>"OPTIN_NO"),
												
												array(
												"type"=>"postback",
												"title"=>$this->btnyes[$fbuser->lang],
												"payload"=>"OPTIN_YES")
											);
        								
        								$this->sendTemplate($fbuser, $this->START_ROPTIN[$fbuser->lang], $buttons, array("%firstname%"=>$fbuser->firstname));
        								$this->updateState($fbuser->fbid, 0);
									}
								break;
								
								
								case stripos($this->normalize($messaging->message->text),'INSCRIPTION') !== false:
								case stripos($this->normalize($messaging->message->text),'ABONNEMENT') !== false:
								case stripos($this->normalize($messaging->message->text),'ABONNER') !== false:
								case stripos($this->normalize($messaging->message->text),'INSCRIRE') !== false:
								case stripos($this->normalize($messaging->message->text),'SUBSCRIBE') !== false:
									if($fbuser->state == 0 || $fbuser->state == 2) {
										$buttons = array(
												array(
												"type"=>"postback",
												"title"=>$this->btnno[$fbuser->lang],
												"payload"=>"OPTIN_NO"),
												
												array(
												"type"=>"postback",
												"title"=>$this->btnyes[$fbuser->lang],
												"payload"=>"OPTIN_YES")
											);
        								
        								$this->sendTemplate($fbuser,$this->START_ROPTIN[$fbuser->lang], $buttons, array("%firstname%"=>$fbuser->firstname));
        								$this->updateState($fbuser->fbid, 0);
									}else{
										$this->sendMessage($fbuser, "");
									} 
								
								
								break;
								
								
								
								case stripos($this->normalize($messaging->message->text),'AIDE') !== false:
								case stripos($this->normalize($messaging->message->text),'HELP') !== false:
									
									$replies = array(
										
										array(
											"content_type"=>"text",
											"title"=>"UNSUBSCRIBE",
											"payload"=>"OPTOUT_YES"),
											
										array(
											"content_type"=>"text",
											"title"=>"SUBSCRIBE",
											"payload"=>"MENU"),
										array(
											"content_type"=>"text",
											"title"=>"STATS",
											"payload"=>"MENU")
									);
									
									$this->sendQR($fbuser, "Hello %firstname%, here's some keywords that i understand well:",$replies, array("%firstname%"=>$fbuser->firstname)); //$this->bot->configuration->qr_help
								break;
							}
							
	
						}
					break;
					
				}
			}
		}
		
		return "200";
	}
	

	private function getUser($userid, $ref = ''){
		
		// GET USERID ON FACEBOOK
		$url = "https://graph.facebook.com/v2.7/$userid?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token=";
		$fbuserObj = $this->send($url.FBPAGETOKEN, "GET", null);
		
		// CHECK IF USERID EXIST IN DB
		$this->dbh->query("SELECT
                            fbusers.id,
                            fbusers.fbid,
                            fbusers.leaderid,
                            fbusers.profile_url,
                            fbusers.state,
                            fbusers.dateupdate,
                            leaders.lang,
                            leaders.firstname,
                            leaders.lastname
                            FROM
                            fbusers
                            INNER JOIN leaders ON fbusers.leaderid = leaders.`code` WHERE fbusers.fbid=:fbid;");
		$this->dbh->bind(":fbid", $userid);
		$xpuser = $this->dbh->single();
		if($xpuser){
			
			$this->dbh->query("UPDATE fbusers SET `profile_url`=:profileurl WHERE `fbid`=:fbid;");
			$this->dbh->bind(":profileurl", $fbuserObj->profile_pic);
			$this->dbh->bind(":fbid", $userid);
			$this->dbh->execute();

		    $fbuser = new stdClass();
       		$fbuser->fbid 		= $userid;
       		$fbuser->leaderid 	= $xpuser->leaderid;
       		$fbuser->firstname 	= $fbuserObj->first_name;
       		$fbuser->lastname 	= $fbuserObj->last_name;
       		$fbuser->gender 	= $fbuserObj->gender;
       		$fbuser->locale 	= $fbuserObj->locale;
       		$fbuser->profilepic = $fbuserObj->profile_pic;
       		$fbuser->timezone 	= $fbuserObj->timezone;
       		$fbuser->lang       = $xpuser->lang; 
       		$fbuser->name       = $xpuser->firstname.' '.$xpuser->lastname;
       		$fbuser->firstname  = $xpuser->firstname;
       		$fbuser->lastname   = $xpuser->lastname; 
       		$fbuser->state      = $xpuser->state; 

    		return $fbuser;
		}else{
		    if($ref != ''){
		        $this->dbh->query("INSERT INTO fbusers(`fbid`,`leaderid`,`profile_url`) VALUES(:fbid,:leaderid,:profileurl);");
		        $this->dbh->bind(":fbid", $userid);
		        $this->dbh->bind(":leaderid", $ref);
    			$this->dbh->bind(":profileurl", $fbuserObj->profile_pic);
    			if($this->dbh->execute()){
    			    
    			    $this->dbh->query("SELECT
                                fbusers.id,
                                fbusers.fbid,
                                fbusers.leaderid,
                                fbusers.profile_url,
                                fbusers.state,
                                fbusers.dateupdate,
                                leaders.lang,
                                leaders.firstname,
                                leaders.lastname
                            FROM
                                fbusers
                            INNER JOIN leaders ON fbusers.leaderid = leaders.`code` WHERE fbusers.fbid=:fbid;");
            		$this->dbh->bind(":fbid", $userid);
            		$xpuser = $this->dbh->single();
            		
    			    $fbuser = new stdClass();
               		$fbuser->fbid 		= $userid;
               		$fbuser->leaderid 	= $xpuser->leaderid;
               		$fbuser->firstname 	= $fbuserObj->first_name;
               		$fbuser->lastname 	= $fbuserObj->last_name;
               		$fbuser->gender 	= $fbuserObj->gender;
               		$fbuser->locale 	= $fbuserObj->locale;
               		$fbuser->profilepic = $fbuserObj->profile_pic;
               		$fbuser->timezone 	= $fbuserObj->timezone;
               		$fbuser->lang       = $xpuser->lang; 
               		$fbuser->name       = $xpuser->firstname.' '.$xpuser->lastname;
               		$fbuser->firstname  = $xpuser->firstname;
               		$fbuser->lastname   = $xpuser->lastname; 
               		$fbuser->state      = 0;
               		return $fbuser;
    			}else return false;
    
    		    
		    }else return false;
		} return false;
		
		
		
	}
	
	
	private function updateState($userid, $state){
		$this->dbh->query("UPDATE fbusers SET `state`=:state WHERE `fbid`=:fbid;");
		$this->dbh->bind(":state", $state);
		$this->dbh->bind(":fbid", $userid);
		$this->dbh->execute();
	}
	
	private function getEvent($messagingObj){
		if(isset($messagingObj->postback)){
			return 0;
		}elseif(isset($messagingObj->delivery)){
			return 1;
		}elseif(isset($messagingObj->read)){
			return 2;
		}elseif(isset($messagingObj->is_echo)){
			return 3;
		}else return 4;
	}
	
	
	
	private function sendMessage($userObj, $answer, $params = array()){
		$query = array(
				"recipient"=>array("id"=>$userObj->fbid), 
				"message"=>array("text"=>strtr($answer,$params))
			);
		return $this->send($this->MESSAGE_URL.FBPAGETOKEN, "POST", $query);
	}
	
	
	
	private function sendTemplate($userObj, $answer, $buttons,  $params = array()){
		$query = array(
				"recipient"=>array("id"=>$userObj->fbid), 
				
				"message"=>array(
					"attachment"=>array(
						"type"=>"template",
						"payload"=>array(
							"template_type"=>"button",
							"text"=>strtr($answer,$params),
							"buttons"=>$buttons
							
						)
					)
				));
		return $this->send($this->MESSAGE_URL.FBPAGETOKEN, "POST", $query);
	}
	
	
	public function Notifytemplate($leaderid, $message, $buttons){
	    $this->dbh->query("SELECT fbid FROM fbusers WHERE leaderid=:leaderid AND state=1;");
	    $this->dbh->bind(":leaderid", $leaderid);
	    $fbuser = $this->dbh->single();
	    
	    if($fbuser){
	        $userObj = $this->getUser($fbuser->fbid);
	        
	        $this->sendTemplate($userObj, $message[$userObj->lang], $buttons[$userObj->lang]);
	        return true;
	    }
	    
	    return false;
	}
	
	public function Notify($leaderid, $message){
	    $this->dbh->query("SELECT fbid FROM fbusers WHERE leaderid=:leaderid AND state=1;");
	    $this->dbh->bind(":leaderid", $leaderid);
	    $fbuser = $this->dbh->single();
	    
	    if($fbuser){
	        $userObj = $this->getUser($fbuser->fbid);
	        $this->sendMessage($userObj, $message[$userObj->lang]);
	        return true;
	    }
	    
	    return false;
	}
	
	
	
	private function sendGeneric($userObj, $elements,  $params = array()){
		$query = array(
				"recipient"=>array("id"=>$userObj->fbid), 
				"message"=>array(
					"attachment"=>array(
						"type"=>"template",
						"payload"=>array(
							"template_type"=>"generic",
							"elements"=>$elements
						)
					)
				));
		return $this->send($this->MESSAGE_URL.FBPAGETOKEN, "POST", $query);
	}
	
	
	
	private function sendQR($userObj, $answer, $replies,  $params = array()){
		$query = array(
				"recipient"=>array("id"=>$userObj->fbid), 
				"message"=>array(
						"text"=>strtr($answer,$params),
						"quick_replies"=>$replies
				));
		return $this->send($this->MESSAGE_URL.FBPAGETOKEN, "POST", $query);
	}


	
	
	
	
	//UTILITIES
	private function normalize($s) {
	    $replace = array(
			        'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
			        'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
			        'Þ'=>'B',
			        'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
			        'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
			        'Ğ'=>'G',
			        'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
			        'Ł'=>'L',
			        'Ñ'=>'N', 'Ń'=>'N',
			        'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
			        'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
			        'Ț'=>'T',
			        'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
			        'Ý'=>'Y',
			        'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
			        'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
			        'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
			        'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
			        'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
			        'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
			        'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
			        'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
			        'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
			        'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
			        'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
			        'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
			        'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
			        'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
			        'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
			        'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
			        'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
			        'ק'=>'q',
			        'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
			        'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
			        'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
			        'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
			        'в'=>'v', 'ו'=>'v', 'В'=>'v',
			        'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
			        'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
			        'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
			    );
		return strtoupper(strtr($s, $replace));
	}
	
}