<?php
class fasttrack {
    
    var $dbh;
	var $error = "";
	
	var $leader;
	var $islogin = false;
	
	function __construct(database $db) {
	    $this->dbh = $db;
    }
    
    
    function getMyTeam($leaderid) {
		$sql = 'SELECT
								leaders.`code` as leaderid,
								CONCAT(leaders.firstname,SPACE(1), leaders.lastname) as leadername,
								ft_teams_pending.name,
								ft_teams_pending.cptleaderid as cptid,
								ft_offers.id,
								ft_offers.teamid,
								ft_offers.datecreation,
								ft_offers.`status`,
								ft_offers.request_offer
								FROM
								leaders
								Inner Join ft_offers ON leaders.`code` = ft_offers.leaderid
								Inner Join ft_teams_pending ON ft_offers.teamid = ft_teams_pending.id
							WHERE ft_offers.leaderid=:leaderid AND ft_offers.status=1 ORDER BY id ASC;';
		$this->dbh->query($sql);
        $this->dbh->bind(":leaderid", $leaderid);
        $this->dbh->execute();
        
        if($this->dbh->rowCount()>=1){
			return $this->dbh->single();
		}else{
			return false;
		}
	}
    
    
    ###########################
	#Retourne si c'est un cpt #
	#qui consulte la fiche    #
	###########################
	public function isCpt($teamid, $leaderid) {
		$sql = 'SELECT id,cptleaderid FROM ft_teams_pending WHERE id=:teamid AND cptleaderid=:leaderid;';
		
		$this->dbh->query($sql);
        $this->dbh->bind(":teamid", $teamid);
        $this->dbh->bind(":leaderid", $leaderid);
        $this->dbh->execute();
        
        if($this->dbh->rowCount()>=1){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	public function getTeamByCpt($leaderid) {
		$sql = 'SELECT * FROM ft_teams_pending WHERE cptleaderid=:leaderid;';
		
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			return $this->dbh->single();
		}else{
			return false;
		}	
	}
	
	public function getTeamById($teamid) {
		$sql = 'SELECT * FROM ft_teams_pending WHERE id=:teamid;';
		
		$this->dbh->query($sql);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			return $this->dbh->single();
		}else{
			return false;
		}	
	}
	
	
	####################################
	#Retourne la liste des équipe      #
	#en attente de joueurs 						 #
	####################################
	function GetTeamsList() {
		$sql = 'SELECT
								leaders.`code` as leaderid,
								leaders.firstname,
								leaders.lastname,
								ft_teams_pending.id,
								ft_teams_pending.datecreation,
								ft_teams_pending.name,
								ft_teams_pending.cptleaderid,
								ft_teams_pending.maxusers,
								(SELECT CONCAT(leaders.firstname,SPACE(1), leaders.lastname) FROM leaders WHERE leaders.`code`=ft_teams_pending.cptleaderid) as cpt_name
								FROM
								leaders
								inner Join ft_teams_pending ON leaders.`code` = ft_teams_pending.cptleaderid
								GROUP BY ft_teams_pending.id
							;';
		$this->dbh->query($sql);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>0){
			return $this->dbh->resultset();
		}else{
			return false;
		}
	}
	
	
	###########################
	#Retourne la liste des    		 #
	#offres d'une équipe                 #
	###########################
	function GetTeamOffers($teamid) {

		$sql = 'SELECT 
							leaders.firstname,
							leaders.lastname,
							ft_offers.id,
							ft_offers.datecreation,
							ft_offers.status
							FROM ft_offers
							left join leaders ON ft_offers.leaderid = leaders.`code` 
							WHERE ft_offers.teamid=:teamid AND ft_offers.status=1;';
		$this->dbh->query($sql);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			return $this->dbh->resultSet();
		}else{
			return false;
		}
	}
	
	
	
	###########################
	#Retourne la liste des    		 #
	#offres d'une équipe                 #
	###########################
	function addTeam($leaderid, $name) {
		
		if($name==""){
			return false;
		}
		
		$sql = 'SELECT name FROM ft_teams_pending WHERE name=:name;';
		$this->dbh->query($sql);
		$this->dbh->bind(":name", $name);
		$this->dbh->execute();
		if($this->dbh->rowCount()>=1){
			return false;
		}else{
			
			$sql = 'INSERT INTO ft_teams_pending(name, cptleaderid) VALUES(:name, :cptleaderid);';
			$this->dbh->query($sql);
			$this->dbh->bind(":cptleaderid", $leaderid);
			$this->dbh->bind(":name", $name);
			if($this->dbh->execute()){
				$myteam = $this->getTeamByCpt($leaderid);
				
				$sql = 'INSERT INTO ft_offers(leaderid,teamid,status,request_offer) VALUES(:leaderid,:teamid,1,1);';
				$this->dbh->query($sql);
				$this->dbh->bind(":leaderid", $leaderid);
				$this->dbh->bind(":teamid", $myteam->id);
				$this->dbh->execute();
				
				$sql = 'UPDATE ft_offers SET status=2 WHERE leaderid=:leaderid  AND request_offer=2 AND teamid<>:teamid;';
				$this->dbh->query($sql);
				$this->dbh->bind(":leaderid", $leaderid);
				$this->dbh->bind(":teamid", $myteam->id);
				$this->dbh->execute();
				
				$sql = 'DELETE FROM ft_offers WHERE leaderid=:leaderid  AND request_offer=1 AND teamid<>:teamid;';
				$this->dbh->query($sql);
				$this->dbh->bind(":leaderid", $leaderid);
				$this->dbh->bind(":teamid", $myteam->id);
				$this->dbh->execute();
				
				return true;
			}else return false;
			
			return false;
		}
	}
	
	
	#############################
	#Retourne la liste des  		#
	#joueurs acceptés						#
	#############################
	function getAcceptedPlayers($teamid) {
		$sql = 'SELECT
								leaders.`code` as leaderid,
								CONCAT(leaders.firstname,SPACE(1), leaders.lastname) as leadername,
								ft_teams_pending.name,
								ft_teams_pending.cptleaderid as cptid,
								ft_offers.id,
								ft_offers.teamid,
								ft_offers.datecreation,
								ft_offers.`status`,
								ft_offers.request_offer
								FROM
								leaders
								Inner Join ft_offers ON leaders.`code` = ft_offers.leaderid
								Inner Join ft_teams_pending ON ft_offers.teamid = ft_teams_pending.id
							WHERE ft_offers.teamid=:teamid AND ft_offers.status=1 ORDER BY id ASC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			return $this->dbh->resultSet();
		}else{
			return false;
		}
	}
	
	
	##########################
	#Retourne la liste des   #
	#demandes pour un joueur #
	##########################
	function GetPlayerRequests($leaderid) {
		$sql = 'SELECT
								leaders.`code` as leaderid,
								CONCAT(leaders.firstname,SPACE(1), leaders.lastname) as leadername,
								ft_teams_pending.name,
								ft_teams_pending.cptleaderid as cptid,
								ft_offers.id,
								ft_offers.teamid,
								ft_offers.datecreation,
								ft_offers.`status`,
								ft_offers.request_offer
								FROM
								leaders
								Inner Join ft_offers ON leaders.`code` = ft_offers.leaderid
								Inner Join ft_teams_pending ON ft_offers.teamid = ft_teams_pending.id
							WHERE leaders.`code`=:leaderid AND ft_offers.request_offer=2 AND ft_offers.`status`!=1 ORDER BY id ASC;';
							
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			return $this->dbh->resultSet();
		}else{
			return false;
		}
	}
	
	
	
	#############################
	#CPT Retourne la liste des  #
	#demandes pour un joueur    #
	#############################
	function getCptPlayerRequests($teamid) {
		$sql = 'SELECT
								leaders.`code` as leaderid,
								leaders.firstname,
								leaders.lastname,
								ft_teams_pending.name,
								ft_teams_pending.cptleaderid,
								CONCAT(leaders.firstname,SPACE(1), leaders.lastname) as leadername,
								ft_offers.id,
								ft_offers.teamid,
								ft_offers.datecreation,
								ft_offers.`status`,
								ft_offers.request_offer
								FROM
								leaders
								Inner Join ft_offers ON leaders.`code` = ft_offers.leaderid
								Inner Join ft_teams_pending ON ft_offers.teamid = ft_teams_pending.id
							WHERE ft_offers.request_offer=2 AND ft_teams_pending.id=:teamid AND ft_offers.status=0;';
		$this->dbh->query($sql);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		if($this->dbh->rowCount()>=1){
			return $this->dbh->resultSet();
		}else{
			return false;
		}
	}
	
	
	
	
	function setRequest($leaderid, $teamid) {

		$sql = 'SELECT id FROM ft_offers WHERE leaderid=:leaderid AND teamid=:teamid;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		
		if($this->dbh->rowCount()>=1){
			$sql = 'UPDATE ft_offers SET status=0 WHERE leaderid=:leaderid AND teamid=:teamid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
		}else{
			$sql = 'INSERT INTO ft_offers(leaderid,teamid,`status`,request_offer) VALUES(:leaderid,:teamid,0,2);';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
		}
		return true;
	}
	
	
	function resRequest($leaderid, $teamid, $reponse) {

		if($reponse){
			$sql = 'UPDATE ft_offers SET status=1 WHERE leaderid=:leaderid AND teamid=:teamid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
			$sql = 'UPDATE ft_offers SET status=3 WHERE leaderid=:leaderid AND request_offer=2 AND teamid!=:teamid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
			$sql = 'DELETE FROM ft_offers WHERE leaderid=:leaderid AND request_offer=1 AND teamid!=:teamid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
			return true;
		}else{
			$sql = 'UPDATE ft_offers SET status=2 WHERE leaderid=:leaderid AND teamid=:teamid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":teamid", $teamid);
			$this->dbh->execute();
			return true;
		}

	}
	
	
	########################################
	#Retourne le nombres de poste à combler#
	#######################################
	function getCountByTeam($teamid) {

		$sql = 'SELECT id FROM ft_offers WHERE teamid=:teamid AND status=1;';
		$this->dbh->query($sql);
		$this->dbh->bind(":teamid", $teamid);
		$this->dbh->execute();
		
		return $this->dbh->rowCount();
	}
	
	
}