<?php
class prospect {

    var $dbh;
	var $error = "";
	var $xmail;

	function __construct($dbsource){
		$this->dbh = $dbsource;
	}

// 	function prospect($dbsource){
// 		$this->dbh = $dbsource;
// 		return true;
// 	}

	/* ADDPROSPECT($LEADERID, $EMAIL, $NAME, $PHONE, $INVITE)
	/* Add a unique prospect by specifying his record id
	###################################################################################*/
    public function add($leaderid, $email, $name, $invite = 1){

    	$sql = "SELECT id FROM dbclients WHERE mail=:mail AND leaderid=:leaderid;";
    	$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":mail", $email);
			$this->dbh->execute();
		if($this->dbh->rowcount()==0){
			$sql = 'INSERT INTO dbclients(leaderid, mail, name, datecreation)  VALUES(:leaderid, :mail, :name, NOW());';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":mail", $email);
			$this->dbh->bind(":name", $name);
			if($this->dbh->execute()){

				$key = sha1($leaderid.$email."S3ndM3S0me!nv1t4T1on");

				$sql = 'INSERT INTO invitations(prospectid, leaderid, token, expire, istest)  VALUES(:prospectid, :leaderid, :token, DATE_ADD(NOW(), INTERVAL 1 DAY), 1);';
				$this->dbh->query($sql);
				$this->dbh->bind(":prospectid", $this->dbh->lastInsertId());
				$this->dbh->bind(":leaderid", $leaderid);
				$this->dbh->bind(":token", $key);
				$this->dbh->execute();

			}else $this->error = 2;

		}else{


			$sql = 'UPDATE dbclients SET name=:name WHERE mail=:mail AND leaderid=:leaderid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":mail", $email);
			$this->dbh->bind(":name", $name);
			if($this->dbh->execute()){

				$key = sha1($leaderid.$email."S3ndM3S0me!nv1t4T1on");

				$sql = 'UPDATE invitations SET expire=DATE_ADD(NOW(), INTERVAL 1 DAY), istest=1 WHERE token=:token;';
				$this->dbh->query($sql);
				$this->dbh->bind(":token", $key);
				$this->dbh->execute();

			}else $this->error = 2;

			//return true;
		}
		if($invite==1){
			$sql = "SELECT code,alias,firstname,lastname FROM leaders WHERE code=:leaderid;";
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$leader = $this->dbh->single();
			$url 	= $leader->alias."/test/".$key;
			$mailer = new mailer($this->dbh);
			$mailer->invite($leaderid, $url, $email, $leader->firstname.' '.$leader->lastname,$name);
		}

		return true;
	}

	public function invite($leaderid, $prospectid, $key){
		$sql = "SELECT
					leaders.`code` AS leaderid,
					leaders.alias,
					leaders.firstname,
					leaders.lastname,
					dbclients.id AS prospectid,
					dbclients.mail AS prospectmail,
					dbclients.`name` AS prospectname
					FROM
					leaders
					INNER JOIN dbclients ON dbclients.leaderid = leaders.`code` 
					WHERE dbclients.id=:prospectid AND leaders.`code`=:leaderid;";
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->bind(":prospectid", $prospectid);
		$prospect = $this->dbh->single();

		if($prospect){
			$sql = 'UPDATE invitations SET expire=DATE_ADD(NOW(), INTERVAL 1 DAY) WHERE leaderid=:leaderid AND prospectid=:prospectid;';
			$this->dbh->query($sql);
			$this->dbh->bind(":leaderid", $leaderid);
			$this->dbh->bind(":prospectid", $prospectid);
			if($this->dbh->execute()){
				$url = $prospect->alias."/test/".$key;
				$mailer = new mailer($this->dbh);
				$mailer->invite($leaderid, $url, $prospect->prospectmail, $prospect->firstname.' '.$prospect->lastname ,$prospect->prospectname);
				return true;
			}else return false;
		}else return false;
	}

	public function leaderInviteManually($leaderid, $leaderEmail, $prospectInfo) {
        $mailer = new mailer($this->dbh);
        $mailer->leaderinterest($leaderid, $leaderEmail, $prospectInfo[0]['name'],$prospectInfo[0]['email'], $prospectInfo[0]['phone'], $prospectInfo[0]['lang']);
    }

	/* GETPROSPECT($LEADERID, $ID)
	/* Get a unique prospect by specifying his record id
	###################################################################################*/
    public function get($leaderid, $id){
		$sql = 'SELECT *
				FROM dbclients
				WHERE dbclients.leaderid=:leaderid AND dbclients.id=:id ORDER BY DateUpdate DESC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->bind(":id", $id);
  		$prospect = $this->dbh->single();

  		if($prospect) return $prospect;
  		else return false;
	}

	/* REMOVE($LEADERID, $ID)
	/* Delete a prospect and return boolean
	###################################################################################*/
	public function remove($leaderid, $id){
		if($leaderid<>"" && $id<>""){
			$this->dbh->query("DELETE FROM dbclients WHERE leaderid=:leaderid AND id=:id;");
    		$this->dbh->bind(":leaderid", $leaderid);
    		$this->dbh->bind(":id", $id);
    		if($this->dbh->execute()) return true;
    		else return false;
		}else return false;
	}


	public function interest($leaderid, $key, $phone, $lang, $name = null){
		$this->dbh->query("SELECT
								invitations.token,
								invitations.leaderid,
								leaders.firstname,
								leaders.lastname,
								leaders.mail AS leadermail,
								invitations.prospectid,
								dbclients.mail AS prospectmail,
								dbclients.`name`,
								dbclients.phone,
								dbclients.`status`,
								dbclients.id
								FROM
								invitations
								INNER JOIN dbclients ON invitations.prospectid = dbclients.id
								INNER JOIN leaders ON invitations.leaderid = leaders.`code` 
							WHERE invitations.leaderid=:leaderid AND invitations.token=:token;");
		$this->dbh->bind(":leaderid", $leaderid);
    	$this->dbh->bind(":token", $key);
		$prospect = $this->dbh->single();

  		if($this->dbh->rowCount()==1){

  			$status = ($prospect->status < 2) ? 2 : $prospect->status;
  			if($name == null){
  				$this->dbh->query("UPDATE dbclients SET `status`=:status, phone=:phone, lang=:lang WHERE id=:prospectid;");
	            $this->dbh->bind(":prospectid", $prospect->id);
	            $this->dbh->bind(":status", $status);
	            $this->dbh->bind(":phone", $phone);
	            $this->dbh->bind(":lang", $lang);
  			}else{
  				$this->dbh->query("UPDATE dbclients SET `status`=:status, name=:name, phone=:phone, lang=:lang WHERE id=:prospectid;");
	            $this->dbh->bind(":prospectid", $prospect->id);
	            $this->dbh->bind(":name", $name);
	            $this->dbh->bind(":status", $status);
	            $this->dbh->bind(":phone", $phone);
	            $this->dbh->bind(":lang", $lang);
  			}


            if($this->dbh->execute()){


            	$this->dbh->query("SELECT
								invitations.token,
								invitations.leaderid,
								leaders.firstname,
								leaders.lastname,
								leaders.mail AS leadermail,
								invitations.prospectid,
								dbclients.mail AS prospectmail,
								dbclients.`name`,
								dbclients.phone,
								dbclients.`status`,
								dbclients.id
								FROM
								invitations
								INNER JOIN dbclients ON invitations.prospectid = dbclients.id
								INNER JOIN leaders ON invitations.leaderid = leaders.`code` 
							WHERE invitations.leaderid=:leaderid AND invitations.token=:token;");
				$this->dbh->bind(":leaderid", $leaderid);
		    	$this->dbh->bind(":token", $key);
				$prospect = $this->dbh->single();

				$bot = new xpressbot($this->dbh);
				$message = array(
	                    "fr"=>$prospect->name." (".$prospect->prospectmail.") est intéressé à votre entreprise! Connectez-vous à votre compte Xpress pour avoir tous les détails.",
	                    "en"=>$prospect->name." (".$prospect->prospectmail.") is interested to your business! Login to your xpress account to view all the details."
	                );
	            $bot->Notify($leaderid, $message);

            	$leadername = $prospect->firstname. ' ' .$prospect->lastname;
            	$mailer = new mailer($this->dbh);
				$mailer->interest($leaderid, $key, $prospect->prospectmail, $leadername, $lang);
				$mailer->leaderinterest($leaderid, $prospect->leadermail, $prospect->name,null, $prospect->phone, $lang);
				return true;
            }
		}
		return false;
	}



	public function convert($leaderid, $prospectid, $interest, $lang="fr"){
		$this->dbh->query("SELECT
								invitations.token,
								invitations.leaderid,
								leaders.firstname,
								leaders.lastname,
								leaders.mail AS leadermail,
								invitations.prospectid,
								dbclients.mail AS prospectmail,
								dbclients.`name`,
								dbclients.phone,
								dbclients.`status`,
								dbclients.lang,
								dbclients.id
								FROM
								invitations
								INNER JOIN dbclients ON invitations.prospectid = dbclients.id
								INNER JOIN leaders ON invitations.leaderid = leaders.`code` 
							WHERE invitations.leaderid=:leaderid AND invitations.prospectid=:prospectid;");
		$this->dbh->bind(":leaderid", $leaderid);
    	$this->dbh->bind(":prospectid", $prospectid);
		$prospect = $this->dbh->single();

  		if($this->dbh->rowCount()==1){

  			$this->dbh->query("UPDATE dbclients SET `status`=3, category=:cat, lang=:lang WHERE id=:prospectid;");
            $this->dbh->bind(":prospectid", $prospect->id);
            $this->dbh->bind(":lang", $lang);
            $this->dbh->bind(":cat", $interest);

            if($this->dbh->execute()){

            	$leadername = $prospect->firstname. ' ' .$prospect->lastname;
            	$mailer = new mailer($this->dbh);
				$mailer->convert($leaderid, $prospect->token, $prospect->prospectmail, $prospect->leadermail, $leadername, $lang);

				return true;
            }
		}
		return false;
	}

	public function getSummaries($leaderid){
		$stats = array();
		$stats["meeting"] 	= 0;
		$stats["interest"] 	= 0;
		$stats["client"] 	= 0;

		$sql = 'SELECT COUNT(id) AS nbr, `status` AS cat FROM dbclients WHERE leaderid=:leaderid GROUP BY `status` ORDER BY dateupdate DESC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);

		foreach($this->dbh->resultSet() as $res){
			if($res->cat == 1) $stats["meeting"] = $res->nbr;
			if($res->cat == 2) $stats["interest"] = $res->nbr;
			if($res->cat == 3) $stats["client"] = $res->nbr;
		}
		return $stats;
	}

	public function getStats($leaderid, $lang='fr'){

		$stats = array();

		$stats["interest"] 	= 0;
		$stats["meeting"] 	= 0;
		$stats["order"] 	= 0;
		$stats["client"] 	= 0;
		$stats["visit"] 	= $this->getVisits($leaderid);
		$stats["events"] 	= "";
		$stats["evinterests"] 	= "";
		$stats["evmeetings"] 	= "";
		$stats["evlooks"] 	    = "";

		$sql = 'SELECT * FROM prospects WHERE leaderid=:leaderid AND unlook=0 ORDER BY dateupdate DESC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);

		$eventcount = 0;
  		foreach($this->dbh->resultSet() as $xlook){
  		    if($xlook->status == 0) {
  		        $stats["interest"] 	+= 1;
  		        if($eventcount<=50) {
  		            $stats["events"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> veut en savoir plus.</td></tr>': '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> wants to know more.</td></tr>';
  		            $stats["evlooks"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> veut en savoir plus.</td></tr>': '<tr><td class="text-center"><div class="status-look">LOOK</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> wants to know more.</td></tr>';
  		        }
  		    }elseif($xlook->status == 1) {
  				$stats["meeting"] 	+= 1;
  				$presname = '';
  				$presname = ($xlook->presname<>'' && $lang=='fr') ? 'par <strong>'.$xlook->presname.'</strong>' : $presname;
  				$presname = ($xlook->presname<>'' && $lang<>'fr') ? 'by <strong>'.$xlook->presname.'</strong>' : $presname;
  				if($eventcount<=50)  {
  				    $stats["events"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> a vu une présentation '.$presname.'</td></tr>': '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> viewed a business overview '.$presname.'</td></tr>';
  			        $stats["evmeetings"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> a vu une présentation '.$presname.'</td></tr>': '<tr><td class="text-center"><div class="status-meeting">MEETING</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> viewed a business overview '.$presname.'</td></tr>';
  				}
  			}elseif($xlook->status == 2) {
  				$stats["order"] 	+= 1;
  				if($eventcount<=50)  {
  				    $stats["events"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-order">INTÉRÊT</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> est intéressé par l\'opportunité!</td></tr>': '<tr><td class="text-center"><div class="status-order">INTERESTED</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> is interested by your opportunity!</td></tr></li>';
  			        $stats["evinterests"] .= ($lang=='fr') ? '<tr><td class="text-center"><div class="status-order">INTÉRÊT</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> est intéressé par l\'opportunité!</td></tr>': '<tr><td class="text-center"><div class="status-order">INTERESTED</div></td><td>'.$xlook->dateupdate.'</td><td><strong><a href="/'._LANG.'/office/prospect/'.$xlook->id.'">'.$xlook->name.'</a></strong> is interested by your opportunity!</td></tr></li>';
  				}
  			}

  		    $eventcount++;
  		}

  		return $stats;
	}

	public function getVisits($accnumber){

		$sql = 'SELECT * FROM prospects WHERE leaderid="'.$accnumber.'";';
		$this->dbh->query($sql);
  		$this->dbh->execute();
  		return $this->dbh->rowCount();
	}


	public function _list($leaderid, $type, $key="", $status=-1, $page=1){

	    $sqlsearch = "";

	    if($key<>"") $sqlsearch .= " AND (name LIKE '%".$key."%' OR mail LIKE '%".$key."%')";
		else $sqlsearch .= "";

		if($status<>-1) $sqlsearch .= " AND status=".$status;
		else $sqlsearch .= "";

		$perpage = 50;
		$limit = ($page * $perpage) - $perpage;

		$sql = 'SELECT
					dbclients.id,
					dbclients.position,
					dbclients.leaderid,
					dbclients.mail,
					dbclients.`name`,
					dbclients.associate,
					dbclients.phone,
					dbclients.category,
					dbclients.`status`,
					dbclients.dateupdate,
					dbclients.datecreation,
					dbclients.lang,
					dbclients.dtwatch,
					dbclients.city,
					dbclients.region,
					dbclients.country,
					invitations.token,
					invitations.expire
					FROM
					dbclients
					INNER JOIN invitations ON invitations.prospectid = dbclients.`id`
 					WHERE dbclients.leaderid=:leaderid AND dbclients.category=0 '.$sqlsearch.' ORDER BY dbclients.dateupdate DESC;';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$stats = $this->dbh->resultSet();
  		if($stats) return $stats;
		else return false;
	}


	public function search($leaderid, $status, $key = "", $page = 1, $perpage = 100, $orderby = "name", $sort="ASC"){
		$sqlsearch = "";
		switch(mb_strtoupper($status)){
			case "ALL":
				$sqlsearch .= "";
				break;
			case "PENDING":
				$sqlsearch = " AND dbclients.`status`=0 AND dbclients.`token` IS NULL AND dbclients.`category`=0";
				break;
			case "INVITED":
				$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`>=NOW()) AND dbclients.`category`=0";
				break;
			case "EXPIRED":
					$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`<NOW()) AND dbclients.`category`=0";
				break;
			case "MEETING":
				$sqlsearch = " AND dbclients.`status`=1 AND dbclients.`category`=0";
				break;
			case "INTEREST":
				$sqlsearch = " AND dbclients.`status`=2 AND dbclients.`category`=0";
				break;
			case "BUILDER":
				$sqlsearch = " AND dbclients.`category`=2";
				break;
			case "CLIENT":
				$sqlsearch = " AND dbclients.`category`=1";
				break;
			default:
				$sqlsearch = "";
				break;
		}

		if ($key<>"") $sqlsearch =  " AND (dbclients.`name` LIKE '%".$key."%' OR dbclients.`mail` LIKE '%".$key."%')";


		$start 		= ($page * $perpage) - $perpage;

		$sql 	= 'SELECT * FROM dbclients WHERE dbclients.`leaderid`=:leaderid  '.$sqlsearch.' ORDER BY dbclients.`'.$orderby.'` '.$sort.' LIMIT '.$start.','.$perpage.';';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		return $this->dbh->resultSet();

	}


	public function statusCount($leaderid, $status){
		$sqlsearch = "";
		switch(mb_strtoupper($status)){
			case "ALL":
				$sqlsearch .= "";
				break;
			case "PENDING":
				$sqlsearch = " AND dbclients.`status`=0 AND dbclients.`token` IS NULL AND dbclients.`category`=0";
				break;
			case "INVITED":
				$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`>=NOW()) AND dbclients.`category`=0";
				break;
			case "EXPIRED":
					$sqlsearch = " AND dbclients.`status`=0 AND (dbclients.`tokexpire` IS NOT NULL AND dbclients.`token` IS NOT NULL AND dbclients.`tokexpire`<NOW()) AND dbclients.`category`=0";
				break;
			case "MEETING":
				$sqlsearch = " AND dbclients.`status`=1 AND dbclients.`category`=0";
				break;
			case "INTEREST":
				$sqlsearch = " AND dbclients.`status`=2 AND dbclients.`category`=0";
				break;
			case "BUILDER":
				$sqlsearch = " AND dbclients.`category`=2";
				break;
			case "CLIENT":
				$sqlsearch = " AND dbclients.`category`=1";
				break;
			default:
				$sqlsearch = "";
				break;
		}

		$sql 	= 'SELECT COUNT(id) AS nbr FROM dbclients WHERE dbclients.`leaderid`=:leaderid  '.$sqlsearch.';';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$res = $this->dbh->single();
  		if($res){
  			return $res->nbr;
  		}else return 0;
	}

	public function getStatus($prospectId) {
        $sql = 'SELECT Status FROM dbclients WHERE Id ="'.$prospectId.'";';
        $this->dbh->query($sql);
        $this->dbh->execute();
        return $this->dbh->resultSet()[0]->Status;
    }

	public function changeStatus($prospectId, $statusCode) {
    $sql = 'UPDATE dbclients SET Status = "'.$statusCode.'" WHERE Id ="'.$prospectId.'";';
    $this->dbh->query($sql);
    $this->dbh->execute();
    return $sql;
}
}
