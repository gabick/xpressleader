<?php
class billing {
    
    private $dbh;
	var $error = "";
	
    function __construct(database $dbsource) {
        $this->dbh = $dbsource;
    }
    
    /*
    Add/Modify a subscribtion to a leader
    return : Boolean */
    public function subscribe($leaderid, $planid, $trial = 0){
        
        $plan = $this->getplan($planid);
        
        $this->dbh->query("SELECT * FROM leaders WHERE code=:leaderid");
        $this->dbh->bind(":leaderid", $leaderid);
        $leader = $this->dbh->single();
        
        $this->dbh->query("SELECT * FROM billings WHERE leaderid=:leaderid");
        $this->dbh->bind(":leaderid", $leaderid);
        $bill = $this->dbh->single();
        
        if($this->dbh->rowCount()==1){
            
            $sqlchargenow = ($bill->chargetry>0) ? ", nextcharge=initialcharge" : "";
            
            $this->dbh->query("UPDATE billings SET planid=:planid $sqlchargenow WHERE leaderid=:leaderid");
            $this->dbh->bind(":planid", $planid);
            $this->dbh->bind(":leaderid", $leaderid);
            $this->dbh->execute();
            
            if($bill->chargetry>0) $this->dobills($leaderid);
            
            return true;

        }else{
            
            $sql = ($trial>0) ? "INSERT INTO billings(leaderid, initialcharge, nextcharge, planid) VALUES(:leaderid, DATE_ADD(CURDATE(), INTERVAL $trial DAY), DATE_ADD(CURDATE(), INTERVAL $trial DAY), :planid)":
                                "INSERT INTO billings(leaderid, initialcharge, nextcharge, planid) VALUES(:leaderid, CURDATE(), CURDATE(), :planid)";
            
            $this->dbh->query($sql);
            $this->dbh->bind(":leaderid", $leaderid);
            $this->dbh->bind(":planid", $planid);
            if($this->dbh->execute()){
                if($this->dobills($leaderid, true)) return true;
            }
        }
        
        return false;
    }
    
    /*
    Add a subscribtion to a leader
    return : Boolean */
    public function suspend($leaderid){
        
        $this->dbh->query("UPDATE billings SET status=2 WHERE leaderid=:leaderid");
        $this->dbh->bind(":leaderid", $leaderid);
        $this->dbh->execute();
        
    }
    
    /*
    Return a plan
    return : Plan object */
    public function getplan($planid){
        
        $this->dbh->query("SELECT * FROM plans WHERE id=:planid");
        $this->dbh->bind(":planid", $planid);
        return $this->dbh->single();
        
    }
    
    /*
    Create fbid
    return : fbid */
    private function createFbid($leader){
        
        $fb  = new freshbooks(FBHOST, FBAPI);
        $obj = $fb->build("client.create");
        $obj->client->first_name    = $leader->firstname;
        $obj->client->last_name     = $leader->lastname;
        $obj->client->organization  = $leader->code;
        $obj->client->email         = $leader->mail;
        $obj->client->home_phone    = $leader->phone;
        $obj->client->language      = $leader->lang;
        $obj->client->currency_code = "USD";
        $obj->client->p_street1     = $leader->address1;
        $obj->client->p_street2     = $leader->address2;
        $obj->client->p_city        = $leader->city;
        $obj->client->p_state       = $leader->region;
        $obj->client->p_country     = $leader->country;
        $obj->client->p_code        = $leader->postal;
        
        $response = $fb->request($obj);
        if($response['status']=="ok"){
            return $response->client_id;
        }else return false;
        
    }
    
    /*
    Daily process for payments
    return : Boolean */
    public function dobills($leaderid = "", $signup = false){
        $sqleader = ($leaderid!="") ? "AND leaders.`code`='$leaderid'" : "";
        $sql = "SELECT
                billings.leaderid,billings.planid,billings.nextcharge,billings.chargetry,
                leaders.`name`,leaders.id,leaders.`code`,leaders.alias,leaders.gender,
                leaders.associate,leaders.title,leaders.mail,leaders.`password`,leaders.address1,leaders.address2,
                leaders.city,leaders.region,leaders.country,leaders.postal,
                leaders.phone,leaders.signature,leaders.bday,leaders.bmonth,leaders.byear,leaders.lang,
                leaders.plan,leaders.referal,leaders.guruid,leaders.stripekey,leaders.dateactivate,
                leaders.dateupdate,leaders.datecreation,leaders.datelastlogin,leaders.xmeetleader,
                leaders.fbid,leaders.ispro,leaders.proid,leaders.`status`,
                leaders.proteam,leaders.groupid,leaders.firstname,leaders.lastname,leaders.xeroid
                FROM
                billings
                INNER JOIN leaders ON billings.leaderid = leaders.`code`
                WHERE DATE(billings.nextcharge)<=NOW() $sqleader ;";

        $this->dbh->query($sql);
        $rows = $this->dbh->resultset();
        $xero = new xero();
        foreach($rows as $leader){
            /* 
            Get the current plan of the leader */
            $plan       = $this->getplan($leader->planid);
            $plandesc   = ($leader->lang=="fr") ? $plan->name_fr : $plan->name_en;
            
            /*
            Make the payment */
            $stripeobj = ($leader->chargetry<2 || $leaderid!="") ? $this->pay($leader->stripekey, $plan->price, $plandesc) : null;
            if(($stripeobj && $stripeobj->paid) || ($leader->planid>12)){
                
                $stripeid = ($stripeobj) ? $stripeobj->id : "No_charge";
                
                if($plan->isannual==1) {
                    if($leader->chargetry==1) $sqlnextcharge = "DATE_ADD(DATE_SUB(nextcharge, INTERVAL 3 DAY), INTERVAL 1 YEAR)";
                    elseif($leader->chargetry>1) $sqlnextcharge = "DATE_ADD(DATE(NOW()), INTERVAL 1 YEAR)";
                    else $sqlnextcharge = "DATE_ADD(nextcharge, INTERVAL 1 YEAR)";
                }else{
                    if($leader->chargetry==1) $sqlnextcharge = "DATE_ADD(DATE_SUB(nextcharge, INTERVAL 3 DAY), INTERVAL 1 MONTH)";
                    elseif($leader->chargetry>1) $sqlnextcharge = "DATE_ADD(DATE(NOW()), INTERVAL 1 MONTH)";
                    else $sqlnextcharge = "DATE_ADD(nextcharge, INTERVAL 1 MONTH)";
                }
                $this->dbh->query("UPDATE billings SET initialcharge=$sqlnextcharge, nextcharge=$sqlnextcharge, chargetry=0 WHERE leaderid=:leaderid");
                $this->dbh->bind(":leaderid", $leader->leaderid);
                $this->dbh->execute();
                
                $this->dbh->query("SELECT nextcharge FROM billings WHERE leaderid=:leaderid");
                $this->dbh->bind(":leaderid", $leader->leaderid);
                $newbill    = $this->dbh->single();
                $nextcharge = $newbill->nextcharge;
                
                
                /*
                Check if client exist in XERO
                if not, create it */
                
                if($leader->xeroid=="") {
                    $xeroid = $xero->addContact($leader->code, $leader->firstname.' '.$leader->lastname, $leader->mail, $leader->address1, $leader->address2, $leader->city, $leader->region, $leader->postal, $leader->country, $leader->xeroid);
                    if($xeroid){
                        $this->dbh->query("UPDATE `leaders` SET `xeroid`=:xeroid WHERE `code`=:leaderid;");
                        $this->dbh->bind(":xeroid", $xeroid);
                        $this->dbh->bind(":leaderid", $leader->code);
                        $this->dbh->execute();
                    }
                }else $xeroid = $leader->xeroid;
                
                
                /*
                Create xero Invoice */
                $lines = array();
                $lines[0] = new stdClass();
                $lines[0]->item = $plan->state;
                $lines[0]->description = ($leader->lang=="fr") ? $plan->name_fr : $plan->name_en;
                $lines[0]->quantity = 1;
                $lines[0]->price = $plan->price;
                
                $invoiceid  = $xero->addInvoice($xeroid, $lines, $AccountCode = 200);
                $invoiceurl = $xero->getOnlineInvoice($invoiceid);
                
                /* 
                generate a unique token */
                $token = urlencode(uniqid(md5(rand()), true));
                
                /* 
                Save the transaction */
                $this->dbh->query("INSERT INTO transactions(token,fbid,plan,price,stripeid,cname,caddress1,caddress2,cemail,cphone,cregion,ccountry,cpostal,datepayment) 
                                            VALUES(:token,:fbid,:plan,:price,:stripeid,:cname,:caddress1,:caddress2,:cemail,:cphone,:cregion,:ccountry,:cpostal,NOW())");
                $this->dbh->bind(":token", $token);
                $this->dbh->bind(":fbid", $invoiceid);
                $this->dbh->bind(":plan", $plandesc);
                $this->dbh->bind(":price", $plan->price);
                $this->dbh->bind(":stripeid", $stripeid);
                $this->dbh->bind(":cname", $leader->name);
                $this->dbh->bind(":caddress1", $leader->address1);
                $this->dbh->bind(":caddress2", $leader->address2);
                $this->dbh->bind(":cemail", $leader->mail);
                $this->dbh->bind(":cphone", $leader->phone);
                $this->dbh->bind(":cregion", $leader->region);
                $this->dbh->bind(":ccountry", $leader->country);
                $this->dbh->bind(":cpostal", $leader->postal);
                $this->dbh->execute();
                
                /* 
                Send notice by email */
                if($leader->lang=="fr") setlocale(LC_TIME, "fr_FR");
                $oDate      = new datetimefrench($nextcharge);
                $dtnextcharge = $oDate->format("d F Y");
                
                
                $mailer = new mailer($this->dbh);
			    $mailer->billsucceed($leader->mail, $plan->price, $dtnextcharge, $invoiceurl, $leader->leaderid, $leader->lang);

            }else{
                
                if($signup) return false;
                
                $chargetry  = $leader->chargetry;
                $dateadd    = "";
                
                if($leader->chargetry==0) { //First payment failure, re-try in 3 days
                    
                    $chargetry  = 1; //First try
                    $status     = 1; //Still active
                    $dateadd    = ",nextcharge=DATE_ADD(nextcharge, INTERVAL 3 DAY)"; //Add 3 days till the next try
                    
                    $mailer = new mailer($this->dbh);
			        $mailer->billfailed($leader->mail, $leader->leaderid, $leader->lang);

                }elseif($leader->chargetry==1) { //Second failure, suspend account
                    
                    $chargetry  = 2;  //Reset chargetry
                    $dateadd    = ""; //Keep date
                    
                    $mailer = new mailer($this->dbh);
			        $mailer->billfailedsuspend($leader->mail, $leader->leaderid, $leader->lang);
                }
                
                //Payment failed, update billing table
                $this->dbh->query("UPDATE billings SET chargetry=:chargetry $dateadd WHERE leaderid=:leaderid");
                $this->dbh->bind(":chargetry", $chargetry);
                $this->dbh->bind(":leaderid", $leader->leaderid);
                $this->dbh->execute();
                
            }
        }
        return true;
        
    }
    

    public function fbid($token){
        
        $this->dbh->query("SELECT fbid FROM transactions WHERE token=:token");
        $this->dbh->bind(":token", $token);
        $row = $this->dbh->single();
        return $row->fbid;
        
    }
    
    public function listTransactions($fbid){
        $this->dbh->query("SELECT * FROM transactions WHERE fbid=:fbid");
        $this->dbh->bind(":fbid", $fbid);
        return $this->dbh->resultSet();
    }
    
    
    private function pay($token, $amount, $description){
        try {
            if($token == "cus_8KfkUdRvdgWmsK"){
                $stripekey = "sk_test_V4oSzsHvrXAoXuu3f7JSZr31";
            }else $stripekey = STRIPEKEY;
            
            
            if($amount==0) return false;
            
    		\Stripe\Stripe::setApiKey($stripekey);
    		return \Stripe\Charge::create(array(
    							  "amount" => $amount*100,
    							  "currency" => "usd",
    							  "customer" => $token,
    							  "description" => $description)
    							);
			
	  	} catch (Stripe_Error $e) {
            
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $this->error = $err['message'];
            
			return false;
			
		}catch (exception $e) {

			return false;
			
		}
    }
    
    
    
    
}