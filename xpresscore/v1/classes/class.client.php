<?php
class client {

    var $dbh;
	var $error = "";
	var $xmail;

	function __construct($dbsource){
		//get dbh
		$this->dbh = $dbsource;
	}


//	function client($dbsource){
//		//get dbh
//		$this->dbh = $dbsource;
//		return true;
//	}

	/* GETCLIENT($LEADERID, $ID)
	/* Get a unique client by specifying his record id
	###################################################################################*/
    public function get($leaderid, $id){
		$sql = 'SELECT * FROM dbclients WHERE leaderid=:leaderid AND id=:id ORDER BY DateUpdate DESC;';
		$this->dbh->query($sql);
		$this->dbh->bind(":leaderid", $leaderid);
		$this->dbh->bind(":id", $id);
  		$prospect = $this->dbh->single();

  		if($prospect) return $prospect;
  		else return false;
	}

	/* REMOVE($LEADERID, $ID)
	/* Delete a client and return boolean
	###################################################################################*/
	public function remove($accnumber, $id){
		if($accnumber<>"" && $id<>""){
			$this->dbh->query("DELETE FROM dbclients WHERE leaderid=:leaderid AND id=:id;");
    		$this->dbh->bind(":leaderid", $leaderid);
    		$this->dbh->bind(":id", $id);
    		if($this->dbh->execute()) return true;
    		else return false;
		}else return false;
	}


	public function getList($leaderid, $key="", $page=1){

	    $sqlsearch = "";

	    if($key<>"") $sqlsearch .= " AND (name LIKE '%".$key."%' OR mail LIKE '%".$key."%')";
		else $sqlsearch .= "";

		$perpage = 50;
		$limit = ($page * $perpage) - $perpage;

		$sql = 'SELECT * FROM dbclients WHERE leaderid=:leaderid '.$sqlsearch.' ORDER BY DateUpdate DESC;';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$stats = $this->dbh->resultSet();
  		if($stats) return $stats;
		else return false;
	}


	public function _list($leaderid, $key="", $page=1){

	    $sqlsearch = "";

	    if($key<>"") $sqlsearch .= " AND (name LIKE '%".$key."%' OR mail LIKE '%".$key."%')";
		else $sqlsearch .= "";

		$perpage = 50;
		$limit = ($page * $perpage) - $perpage;

		$sql = 'SELECT * FROM dbclients WHERE leaderid=:leaderid AND `status`>=3 '.$sqlsearch.' ORDER BY dateupdate DESC;';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$stats = $this->dbh->resultSet();
  		if($stats) return $stats;
		else return false;
	}


	public function search($leaderid, $key, $status, $page = 1){

		$sqlsearch = "";
		if($key<>"") $sqlsearch .= " AND (name LIKE '%".$key."%' OR mail LIKE '%".$key."%')";
		else $sqlsearch .= "";


		$perpage 	= 50;
		$limit 		= ($page * $perpage) - $perpage;


		$sql = 'SELECT
					dbclients.id,
					dbclients.position,
					dbclients.leaderid,
					dbclients.mail,
					dbclients.`name`,
					dbclients.associate,
					dbclients.phone,
					dbclients.category,
					dbclients.`status`,
					dbclients.dateupdate,
					dbclients.datecreation,
					dbclients.lang,
					invitations.token,
					invitations.expire
					FROM
					dbclients
					INNER JOIN invitations ON invitations.prospectid = dbclients.id 
					WHERE dbclients.leaderid=:leaderid '.$sqlsearch.' ORDER BY dbclients.name ASC;';
		$res	= $this->dbh->query($sql);
  		$this->dbh->bind(":leaderid", $leaderid);
  		$stats = $this->dbh->resultSet();
  		if($stats) return $stats;
		else return false;
	}
}
