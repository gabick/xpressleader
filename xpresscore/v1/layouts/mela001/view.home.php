<?php
$lblfamily      = array("fr"=>"Famille", "en"=>"Family");
$lblmom         = array("fr"=>"Maman à la maison?", "en"=>"Mom at home?");
$lblmomdesc     = array("fr"=>"
                        <p>
                            <strong>La famille et les enfants occupent une grande partie de notre vie. 
                            Nous voulons les voir grandir et s’épanouir, mais rester à la maison 
                            n'est pas une option que tous peuvent se permettre financièrement.</strong>
                        </p>
                        
                        <p>
                            Quelles sont vos raisons et avantages de travailler à partir de la maison?
                        </p>
                        
                        <ul>
                            <li>Avoir un horaire flexible pour être disponible pour les enfants et présente lors des évènements importants de la vie;</li>
                            <li>Choisir de travailler à temps plein ou partiel pour vous assurer un revenu permanent selon vos besoins;</li>
                            <li>Avoir une carrière enrichissante avec la reconnaissance de vos pairs;</li>
                            <li>Gagnez des revenus substantiels sans sacrifier votre santé, et votre qualité de vie.</li>
                            <li>Contribuer à assurer le bien-être financier et au mieux-être de votre famille.</li>
                        </ul>
                        
                        <p>
                            Travaillez directement de la maison!
                        </p>",
                 "en"=>"<p>
                            <strong>Family and children occupy a large part of our lives. We want to see them grow and flourish, but staying home is not an option that everyone can afford financially.</strong>
                        </p>
                        
                        <p>What are your reasons and benefits of working from home?</p>
                        
                        <ul>
                            <li>Have a flexible schedule to be available for children and present during important life events;</li>
                            <li>Choose to work full or part-time to ensure a permanent income as required;</li>
                            <li>Have a rewarding career with the recognition of your peers;</li>
                            <li>Earn a substantial income without sacrificing your health and your quality of life.</li>
                            <li>To contribute to the financial well-being and the well-being of your family.</li>
                        </ul>
                        
                        <p>Work directly from the house!</p>");

$lblliberty     = array("fr"=>"liberté financière", "en"=>"Financial Freedom");
$lblrevenu      = array("fr"=>"Revenus supplémentaires?", "en"=>"Extra income?");
$lblrevenudesc  = array("fr"=>"<p>
                            <strong>Faites comme nous et...</strong>
                        </p>
                        
                        <ul>
                            <li>Augmentez vos revenus</li>
                            <li>Sans investissement</li>
                            <li>Sans inventaire</li>
                            <li>Sans risque</li>
                        </ul>
                        
                        <p>
                            Ce concept d’affaire vous permet d’être rémunéré indéfiniment sur les efforts investis dans le passé. C'est donc dire que les revenus 
                            peuvent être illimités. Notre équipe de professionnels travaillera avec vous en utilisant vos compétences pour atteindre vos objectifs de revenu permanent.
                        </p>
                        
                        <p>
                            Une solution simple et efficace d'augmenter son revenu. Travailler selon votre horaire, à temps plein ou partiel afin de multiplier votre 
                            revenu. Des milliers de gens ont déjà choisi d'améliorer leur qualité de vie de cette façon!
                        </p>",
                "en"=>"<p>
                            <strong>Do like us and ...</strong>
                        </p>
                        
                        <ul>
                            <li>Increase your revenues</li>
                            <li>No Investment</li>
                            <li>No inventory</li>
                            <li>Without risk</li>
                        </ul>
                        
                        <p>This business concept allows you to be paid indefinitely on the efforts invested in the past. This means that the income can be unlimited. Our team of professionals will work with you using your skills to achieve your permanent income goals.</p>
                        
                        <p>A simple and effective solution to increase his income. Work according to your schedule, full or part time in order to multiply your income. Thousands of people have chosen to improve their quality of life in this way!</p>");



$lblquality     = array("fr"=>"Qualité de vie", "en"=>"Wellness");
$lblretire      = array("fr"=>"Prêt pour la retraite?", "en"=>"Ready for retirement?");
$lblretiredesc  = array("fr"=>"<p>
                            <strong>C’est difficile d’imaginer prendre la retraite quand on croule sous les
                            dettes, ou quand on voit le coût de la vie augmenter et que les fonds 
                            de retraite baissent.</strong>
                        </p>
                        
                        <p>
                            Qu’est-ce qui est le plus important pour vous?
                        </p>
                        
                        <ul>
                            <li>Avoir une meilleure qualité de vie ?</li>
                            <li>Avoir une paix d’esprit et une bonne santé ?</li>
                            <li>Pouvoir voyager plusieurs semaines ou même plusieurs mois par année ?</li>
                            <li>Avoir les moyens d’aider financièrement vos enfants et gâter vos petits-enfants ?</li>
                            <li>Arrêter de travailler bien avant 67 ans, et en ayant un revenu très confortable et permanent, transférable a vos héritiers ?</li>
                            <li>Éliminer toutes dettes et vivre librement ?</li>
                            <li>Avoir la liberté de temps et la liberté financière ?</li>
                            <li>Nous vous aidons à préparer votre retraite en commençant dès maintenant votre plan « B » à temps partiel en étant votre propre patron.</li>
                        </ul>
                        
                        <p>
                            Travaillez à temps partiel et vivez à temps plein!
                        </p>",
                "en"=>"<p>
                            <strong>It's hard to imagine retire when drowning in debt, or when you see the cost of living increase and pension funds decline.</strong>
                        </p>
                        
                        <p>What is most important to you?</p>
                        
                        <ul>
                            <li>Have a better quality of life?</li>
                            <li>Have peace of mind and good health?</li>
                            <li>Being able to travel several weeks or even months per year?</li>
                            <li>Having the means to financially help your children and spoil your grandchildren?</li>
                            <li>Stop working before age 67, and having a very comfortable and permanent income transferable to your heirs?</li>
                            <li>Eliminate all debts and live freely?</li>
                            <li>Have time freedom and financial freedom?</li>
                            <li>We help you prepare for your retirement now starting your plan \"B\" part-time by being your own boss.</li>
                        </ul>
                        
                        <p>Work part time and live full time!</p>");

$lblvideourl            = array("fr"=>"https://player.vimeo.com/external/260594105.hd.mp4?s=59af78915e9fc2c361c9c292aa469adcdc6403b0&profile_id=174", "en"=>"https://player.vimeo.com/external/271351696.hd.mp4?s=0d7c605390c241d61fadd1d351f9b4ed491e841d&profile_id=174");
$lblinterested          = array("fr"=>"", "en"=>"Are you interested?");
$lblinteresteddesc      = array("fr"=>"Je veux faire le test maintenant!", "en"=>"I want to do the test now!");
$lblinterestedText       = array("fr"=>"Inscrivez vos coordonnées afin de recevoir automatiquement le lien par courriel pour faire le test.", "en"=>"Enter your contact information in order to automatically receive the email link for the test.");
$lblschedule            = array("fr"=>"Sessions", "en"=>"Sessions");
$lblname                = array("fr"=>"Votre nom", "en"=>"Your name");
$lblmail                = array("fr"=>"Adresse courriel", "en"=>"Email address");
$lblbtnwatch            = array("fr"=>"Continuer", "en"=>"Continue");
$lbljoinlive            = array("fr"=>"JOINDRE UNE SESSION D'INFORMATIONS LIVE", "en"=>"JOIN A LIVE BUSINESS OVERVIEW");
$lblhost                = array("fr"=>"HÔTE", "en"=>"HOST");

$lblquote1              = array("fr"=>"Le talent permet de gagner des matchs, mais le travail d’équipe et l’intelligence permettent de gagner les championnats.", "en"=>"Talent wins games, but teamwork and intelligence wins championships.");
$lblquote2              = array("fr"=>"Ne laissez pas les personnes négatives et toxiques louer de l'espace dans votre tête! Augmentez le loyer et foutez-les à la porte!", "en"=>"Don't let negative and toxic people rent space in your head. raise the rent and kick them out!");
$lblquote3              = array("fr"=>"Le succès est une décision. Décidez de ce que vous ferez de votre vie , sinon quelqu'un d'autre le fera pour vous.", "en"=>"If you don't run your own life, somebody else will.");
$lblquote4              = array("fr"=>"Je suis convaincu qu’au moins la moitié de ce qui sépare les entrepreneurs qui réussissent de ceux qui ne réussissent pas est de la pure persévérance", "en"=>"I'm convinced that about half of what separates the successful entrepreneurs from the non-successful ones is pure perseverance");
$lblquote5              = array("fr"=>"Beaucoup trop de gens cherchent la bonne personne au lieu d'essayer d'être la bonne personne.", "en"=>"Far too many people are looking for the right person, instead of trying to be the right person.");


$data = array("api_key" => "deb5db6a1819a333c981f7aeaef2b34778dc33385a547fe3eb32d4fce4b6147e", "webinar_id" => "a1025a173c", "real_dates"=>1);
$data_string = json_encode($data);

$ch = curl_init('https://webinarjam.genndi.com/api/everwebinar/webinar');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);
curl_setopt($ch, CURLOPT_TIMEOUT, 5);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);
$result = json_decode($result);
$webinar = $result->webinar;
?>
<style>
    .jw-preview {
        background:transparent !important;
    }
</style>
<div id="herotext">
    <?=jsonExtract($layout->herotext);?>
</div>

<div class="whitebox ">
    <div class="row">
        <div class="col-md-4">
            <div class="ic_container">
                <a role="button" class="btnnews" data-toggle="collapse" data-parent="#accordion" href="#news1" aria-expanded="false" aria-controls="news1">
                    <img src="/<?=PAGECORPOKEY;?>/images/news/news1.jpg" alt="" class="img-responsive img-rounded"/>
                    <div class="overlay" style="display:none;"></div>
                    <div class="ic_caption">
                        <p class="ic_category"><?=$lblfamily[_LANG];?> <i class="fa fa-caret-down" aria-hidden="true"></i></p>
                        <h3><?=$lblmom[_LANG];?></h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ic_container">
                <a role="button" class="btnnews" data-toggle="collapse" data-parent="#accordion" href="#news2" aria-expanded="false" aria-controls="news2">
                    <img src="/<?=PAGECORPOKEY;?>/images/news/news2.jpg" alt="" class="img-responsive img-rounded"/>
                    <div class="overlay" style="display:none;"></div>
                    <div class="ic_caption">
                        <p class="ic_category"><?=$lblliberty[_LANG];?> <i class="fa fa-caret-down" aria-hidden="true"></i></p>
                        <h3><?=$lblrevenu[_LANG];?></h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ic_container">
                <a role="button" class="btnnews" data-toggle="collapse" data-parent="#accordion" href="#news3" aria-expanded="false" aria-controls="news3">
                    <img src="/<?=PAGECORPOKEY;?>/images/news/news3.jpg" alt="" class="img-responsive img-rounded"/>
                    <div class="overlay" style="display:none;"></div>
                    <div class="ic_caption">
                        <p class="ic_category"><?=$lblquality[_LANG];?> <i class="fa fa-caret-down" aria-hidden="true"></i></p>
                        <h3><?=$lblretire[_LANG];?></h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="col-md-12">
                <div id="news1" class="collapse" role="tabpanel" aria-labelledby="news1">
                    <div class="well">
                        <h4><?=$lblmom[_LANG];?></h4>
                        <?=$lblmomdesc[_LANG];?>
                    </div>
                </div>
                <div id="news2" class="collapse" role="tabpanel" aria-labelledby="news2">
                    <div class="well">
                        <h4><?=$lblrevenu[_LANG];?></h4>
                        <?=$lblrevenudesc[_LANG];?>
                    </div>
                </div>
                <div id="news3" class="collapse" role="tabpanel" aria-labelledby="news3">
                    <div class="well">
                        <h4><?=$lblretire[_LANG];?></h4>
                        <?=$lblretiredesc[_LANG];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
			<input type="hidden" id="teaserurl" value="<?=$lblvideourl[_LANG];?>" required="true"/>
			<input type="hidden" id="teaserimg" value="/mela001/images/teaserimgv2<?=_LANG;?>.jpg" required="true"/>
			<div id="jwdynvideo"></div>
    </div>

    <div class="col-md-4" style="padding: 10px 20px 0 0;">
        <p><strong><?=$lblinteresteddesc[_LANG];?></strong></p>
        <p><?=$lblinterestedText[_LANG];?></p>
        <form action="" method="post">
            <div class="form-group">
                <label for="txtprosname"><?=$lblfirstname[_LANG];?></label>
                <input type="text" id="txtprosname" name="txtprosname" class="form-control" placeholder="" required="true">
            </div>
            <div class="form-group">
                <label for="txtprosmail"><?=$lblmail[_LANG];?></label>
                <input type="email" id="txtprosmail" name="txtprosmail" class="form-control" placeholder="" required="true">
            </div>

            <input type="hidden" name="leaderid" value="<?=$pageleader->leaderid;?>"/>
            <input type="hidden" name="validate" value="true"/>
            <input type="hidden" name="subform" value="addprospect"/>
            <button class="btn btn-danger btn-block btn-lg" type="submit"><?=$lblbtnwatch[_LANG];?></button>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-xpress">
            <div class="panel-body">
<?php
$events = $event->getEvents($pageleader->leaderid, $pageleader->groupid, "1", "", "");
if($events){
?>

                <h4><?=$lbljoinlive[_LANG];?></h4>

                    <?php

                        $i = 1;
                        foreach($events as $ev){
                            if($ev->category=="2") continue;

                            $openday    = date('d', strtotime($ev->timestart));
                            $openmonth  = (_LANG=="fr") ? new datetimefrench($ev->timestart) : new datetime($ev->timestart);
                            $open       = date('h:ia', strtotime($ev->timestart));

                            $timeleft   = $event->getRemaining(time(), strtotime($ev->timestart)-1800);
                            $lifetime   = 5;

                            if($ev->type==1) $lbltype = '';
                            elseif($ev->type>=2) $lbltype = '<span class="label label-primary">Public</span>';
                            elseif($ev->type==3) $lbltype = '<span class="label label-default">Corpo</span>';

                            if($timeleft){
                                if($timeleft['days']==0 && $timeleft['hours']==0) $joinhtml   = '<div class="text-center" style="line-height: 30px;"><strong>'.'<small>Disponible dans:</small><br>'.$timeleft['minutes'].'<small> minutes</small>'.'</strong></div>';
                                elseif($timeleft['days']==0) $joinhtml   = '<div class="text-center" style="line-height: 30px;"><strong>'.'<small>Disponible dans:</small> '.$timeleft['hours'].'<small>h</small> '.$timeleft['minutes'].'<small>m</small>'.'</strong></div>';
                                else $joinhtml   = '';
                            }else{
                                if(!$event->getLifeAccess($ev->timestart,$lifetime)){
                                    $joinhtml   = '<a href="https://'.DOMAIN.'/'._LANG.'/webcast/'.$ev->token.'/'.$pageleader->leaderid.'" class="btn btn-success btn-md btn-block">Joindre le meeting <i class="fa fa-play-circle"></i></a>';
                                }else $joinhtml = '<div class="text-center" style="line-height: 30px;"><span class="text-danger"><strong>FERMÉ</strong></span></div>';
                            }

                            $title  = ($ev->category=="1") ? "" : $ev->title;

                            if(_LANG=="fr") $eng = ($ev->language=="en") ? '<span class="label label-danger"><i class="fa fa-language"></i>english</span>' : "";
                            if(_LANG=="en") $eng = ($ev->language=="fr") ? '<span class="label label-info"><i class="fa fa-language"></i>français</span>' : $eng;
                            $eng = ($ev->language=="es") ? '<span class="label label-warning"><i class="fa fa-language"></i>español</span>' : $eng;

                            if($i==1) echo '<div class="row" style="margin-top:20px;">';

                            echo '<div class="col-md-4" style="min-height: 90px;border-left: solid 3px #4f4f4f;border-bottom: solid 1px #4f4f4f;border-radius: 5px;">';
                            echo '<div class="row">';
                                echo '<div class="col-md-3">';
                                    echo '<div class="calrow"><div class="day">'.$openday.'</div><div class="month">'.$openmonth->format('M').'</div></div>';
                                echo '</div>';
                                echo '<div class="col-md-9">';
                                    echo '<div class="time">'.$open.' <small>'.$eng.'</small></div> <span style="color:#aeaeae;font-size:0.8em;">HÔTE:</span> '.$ev->name;
                                echo '</div>';
                            echo '</div>';
                            echo '<div class="row">';
                                echo '<div class="col-md-12">';
                                    echo $title;
                                echo '</div>';
                            echo '</div>';
                            echo '<div class="row">';
                                echo '<div class="col-md-12">';
                                    echo $joinhtml;
                                echo '</div>';
                            echo '</div>';
                            echo '</div>';

                            if($i==3) echo '</div>';

                            $i = ($i==3) ? 1 : $i+1;
                        }
                        if($i<=3) echo '</div>';
                    ?>

<?php
}

$events = $event->getEvents($pageleader->leaderid, $pageleader->groupid, "2", "", "");
if($events){
?>

                <h4>PROCHAINES FORMATIONS LIVE!</h4>
                    <?php
                        foreach($events as $ev){
                            if($ev->category=="1") continue;

                            $openday    = date('d', strtotime($ev->timestart));
                            $openmonth  = (_LANG=="fr") ? new datetimefrench($ev->timestart) : new datetime($ev->timestart);
                            $open       = date('h:ia', strtotime($ev->timestart));

                            $timeleft   = $event->getRemaining(time(), strtotime($ev->timestart)-1800);
                            $lifetime   = 30;

                            if($ev->type==1) $lbltype = '';
                            elseif($ev->type>=2) $lbltype = '<span class="label label-primary">Public</span>';
                            elseif($ev->type==3) $lbltype = '<span class="label label-default">Corpo</span>';

                            if($timeleft){
                                if($timeleft['days']==0 && $timeleft['hours']==0) $joinhtml   = '<div style="line-height: 20px;"><strong>'.'<small>Disponible dans:</small><br>'.$timeleft['minutes'].'<small> minutes</small>'.'</strong></div>';
                                elseif($timeleft['days']==0) $joinhtml   = '<div style="line-height: 20px;"><strong>'.'<small>Disponible dans:</small><br> '.$timeleft['hours'].'<small>h</small> '.$timeleft['minutes'].'<small>m</small>'.'</strong></div>';
                                else $joinhtml   = '';
                            }else{
                                if(!$event->getLifeAccess($ev->timestart,$lifetime)){
                                    $joinhtml   = '<a href="https://'.DOMAIN.'/'._LANG.'/webcast/'.$ev->token.'/'.$pageleader->leaderid.'" class="btn btn-xp-green btn-block">Joindre le meeting <i class="fa fa-play-circle"></i></a>';
                                }else $joinhtml = '<div class="text-center" style="line-height: 30px;"><span class="text-danger"><strong>FERMÉ</strong></span></div>';
                            }

                            $title  = ($ev->category=="1") ? "" : $ev->title;

                            if(_LANG=="fr") $eng = ($ev->language=="en") ? '<span class="label label-danger"><i class="fa fa-language"></i>english</span>' : "";
                            if(_LANG=="en") $eng = ($ev->language=="fr") ? '<span class="label label-info"><i class="fa fa-language"></i>français</span>' : $eng;
                            $eng = ($ev->language=="es") ? '<span class="label label-warning"><i class="fa fa-language"></i>español</span>' : $eng;

                            echo '<div class="row" style="margin-top:15px;">';

                            echo '<div class="col-md-12" style="min-height: 50px;border-left: solid 3px #4f4f4f;border-bottom: solid 1px #4f4f4f;border-radius: 5px;">';
                            echo '<div class="row">';
                                echo '<div class="col-md-1">';
                                    echo '<div class="calrow"><div class="day">'.$openday.'</div><div class="month">'.$openmonth->format('M').'</div></div>';
                                echo '</div>';
                                echo '<div class="col-md-4">';
                                    echo '<div class="time">'.$open.' <small>'.$eng.'</small></div> <span style="color:#aeaeae;font-size:0.8em;">HÔTE:</span> '.$ev->name.'';
                                echo '</div>';

                                echo '<div class="col-md-4">';
                                    echo $title;
                                echo '</div>';

                                echo '<div class="col-md-3">';
                                    echo $joinhtml;
                                echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        }
                    ?>


<?php
}
?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="cbp-qtrotator" class="cbp-qtrotator">
			<div class="cbp-qtcontent">
				<blockquote>
				  <p><?=$lblquote1[_LANG];?></p>
				  <footer>Michael Jordan</footer>
				</blockquote>
			</div>
			<div class="cbp-qtcontent">
				<blockquote>
				  <p><?=$lblquote5[_LANG];?></p>
				  <footer>Gloria Steinem</footer>
				</blockquote>
			</div>
			<div class="cbp-qtcontent">
				<blockquote>
				  <p><?=$lblquote2[_LANG];?></p>
				  <footer>Robert Tew</footer>
				</blockquote>
			</div>
			<div class="cbp-qtcontent">
				<blockquote>
				  <p><?=$lblquote3[_LANG];?></p>
				  <footer>John Atkinston</footer>
				</blockquote>
			</div>
			<div class="cbp-qtcontent">
				<blockquote>
				  <p><?=$lblquote4[_LANG];?></p>
				  <footer>Steve Job</footer>
				</blockquote>
			</div>
		</div>
    </div>
</div>
