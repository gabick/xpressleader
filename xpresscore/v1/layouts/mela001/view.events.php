<h3><i class="fa fa-calendar"></i> Calendrier des meetings</h3>
<div class="whitebox">
<table class="table" border="0">
    <?php
    $events = $event->getEvents($pageleader->leaderid, $pageleader->groupid, "", "", "");
    if($events){
        foreach($events as $ev){
            $openday    = date('d', strtotime($ev->timestart));
            $openmonth  = date('M', strtotime($ev->timestart));
            $open       = date('h:ia', strtotime($ev->timestart));

            $timeleft   = $event->getRemaining(time(), strtotime($ev->timestart)-3600);
            $lifetime   = 180;
            
            $title  = ($ev->category=="1") ? "SURVOL D'ENTREPRISE" : $ev->title;
            $pwd    = ($ev->leaderid==$curleader->leaderid && $ev->password!="") ? "<span class=\"text-danger\"><strong><i class=\"fa fa-key\"></i> </strong></span>" : "";
            
            if($ev->type==1) $lbltype = '<span class="label label-danger">Privé</span>';
            else $lbltype = '';
            
            if($timeleft){
                if($timeleft['days']==0 && $timeleft['hours']==0) $joinhtml   = '<strong>'.'<small>Disponible dans:</small><br>'.$timeleft['minutes'].'<small> minutes</small>'.'</strong>';
                elseif($timeleft['days']==0) $joinhtml   = '<strong>'.'<small>Disponible dans:</small><br>'.$timeleft['hours'].'<small>h</small> '.$timeleft['minutes'].'<small>m</small>'.'</strong>';
                else $joinhtml   = '<button type="button" class="btn btn-default"><i class="fa fa-bell-o"></i></button>';
            }else{
                if(!$event->getLifeAccess($ev->timestart,$lifetime)){
                    $joinhtml   = '<button type="button" class="btn btn-success btn-sm">Joindre le metting <i class="fa fa-play-circle"></i></button>';
                }else $joinhtml = '<span class="text-danger"><strong>Fermé</strong></span>';
            }
            
            
            
            if(_LANG=="fr") $eng = ($ev->language=="en") ? '<span class="label label-danger"><i class="fa fa-language"></i>english</span>' : "";
            if(_LANG=="en") $eng = ($ev->language=="fr") ? '<span class="label label-info"><i class="fa fa-language"></i>français</span>' : $eng;
            $eng = ($ev->language=="es") ? '<span class="label label-warning"><i class="fa fa-language"></i>español</span>' : $eng;
            echo '<tr class="">';
            echo '<td class="calrow"><div class="day">'.$openday.'</div><div class="month">'.$openmonth.'</div></td>';
            echo '<td><div class="time">'.$open.' <small>'.$eng.'</small></div>'.$ev->name.'<br></td>';
            echo '<td>'.$pwd.$title.'</td>';
            
            echo '<td class="text-right">'.$joinhtml.'</td>';
            echo '</tr>';
        }
    }else{
        echo '<tr>';
        echo '<td><strong>Il n\'y a aucun survol de disponible pour le moment</strong></td>';
        echo '</tr>';
    }
    ?>
</table>
</div>