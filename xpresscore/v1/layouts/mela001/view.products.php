<?php
$db = new database();

$db->query("SELECT * FROM products WHERE corpoid=1 ORDER BY type ASC");
$result = $db->resultset();


foreach($result as $product){
    switch($product->type){
        case 1:
            $cat = '<span class="label label-success">Santé</span>';
            break;
        case 2:
            $cat = '<span class="label label-info">Soins et beauté</span>';
            break;
        case 3:
            $cat = '<span class="label label-default">Personnel</span>';
            break;
        case 4:
            $cat = '<span class="label label-danger">Entretien ménager</span>';
            break;
        case 6:
            $cat = '<span class="label label-warning">Pharmacie</span>';
            break;
        default:
            $cat = "";
            break;
    }
    echo '<div class="col-md-4">';
    echo '<div class="ic_container">';
        echo '<a role="button" class="btnnews" data-toggle="collapse" data-parent="#accordion" href="#news1" aria-expanded="false" aria-controls="news1">';
            echo '<img src="//media.xpressleader.com/products/'.$product->image.'" alt="" class="img-responsive img-rounded" style="height: 200px;"/>';
            echo '<div class="overlay" style="display:none;"></div>';
            echo '<div class="ic_caption">';
                echo '<p class="ic_category">'.$cat.'</p>';
                echo '<h3>'.$product->name.'</h3>';
            echo '</div>';
        echo '</a>';
    echo '</div>';
    echo '</div>';
}
/*
foreach($result as $product){
    switch($product->type){
        case 1:
            $cat = '<span class="label label-success">Santé</span>';
            break;
        case 2:
            $cat = '<span class="label label-info">Soins et beauté</span>';
            break;
        case 3:
            $cat = '<span class="label label-default">Personnel</span>';
            break;
        case 4:
            $cat = '<span class="label label-danger">Entretien ménager</span>';
            break;
        case 6:
            $cat = '<span class="label label-warning">Pharmacie</span>';
            break;
        default:
            $cat = "";
            break;
    }
    echo '<div class="row">';
    echo '<div class="col-md-12">';
    echo '<h1>'.$cat.' '.$product->name.'</h1>';
    echo '</div>';
    echo '</div>';
    
    
    echo '<div class="row">';
    echo '<div class="col-md-3 text-center">';
    echo '<img src="//media.xpressleader.com/products/'.$product->image.'" class="img-responsive img-rounded"/>';
    echo '</div>';
    
    echo '<div class="col-md-9">';
    echo '<div class="whitebox">';
    echo '<p><strong>'.$product->title.'</strong></p>';
    echo '<p>'.$product->description.'</p>';
    
    echo '<div class="row">';
    if($product->ingredient<>""){
        echo '<div class="col-md-12">';
        echo '<h3>Ingrédients</h3>';
        echo '<p>'.$product->ingredient.'</p>';
        echo '</div>';
    }
    if($product->usage<>""){
        echo '<div class="col-md-12">';
        echo '<h3>Utilisation</h3>';
         echo '<p>'.$product->usage.'</p>';
        echo '</div>';
    }
    echo '</div>';
    
    echo '<div class="row">';
    if($product->usermanual<>""){
        echo '<div class="col-md-6">';
        echo '<h3>Mode d\'emploi</h3>';
        echo '<p>'.$product->usermanual.'</p>';
        echo '</div>';
    }
    if($product->advantage<>""){
        echo '<div class="col-md-6">';
        echo '<h3>Avantages</h3>';
        echo '<p>'.$product->advantage.'</p>';
        echo '</div>';
    }
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}
*/
?>