<?php 
$lbltitle   = array("fr"=>"Ce vidéo de 23 minutes peut changer votre vie !","en"=>"This 23 minutes video can change your life !");
$lblurl     = array("fr"=>"https://player.vimeo.com/external/171692177.sd.mp4?s=3ca94edddde3489f3e01c47ab807e698825615d0&profile_id=165", "en"=>"https://player.vimeo.com/external/173271444.sd.mp4?s=5f8ea0512d131ee263de55733dfe4ab753e8114d&profile_id=165");

$lblinterest        = array("fr"=>"Ça vous intéresse? Vous avez des questions?","en"=>"Interested ? Questions ?");
$lblinterestdesc    = array("fr"=>"Je suis la pour y répondre ! Remplissez le formulaire ci-dessous et recevez par courriel un document explicatif qui résume ce vidéo.","en"=>"Your are interested but still have questions ? I am here to answer them. Complete the form below and receive a summarized document that explain everything you just saw.");
$lblname            = array("fr"=>"Votre nom","en"=>"Your name");
$lblemail           = array("fr"=>"Adresse courriel","en"=>"Email address");
$lblphone           = array("fr"=>"Téléphone","en"=>"Phone number");
$lblbtninfo         = array("fr"=>"Soumettre","en"=>"Submit");

?>
<div class="row">
    <div class="col-md-12" id="player_wrapper">
        <div>
            <div class="pull-left">
                <h3 style="line-height: 16px;"><?=jsonExtract($layout->overview_title);?></h3>
            </div>
            <div class="pull-right">
                <button id="playpause" type="button" class="btn btn-xp-black" style="font-size: 1.1em;"><i class="fa fa-pause" aria-hidden="true"></i> Pause video</button>
                <a href="//xpressleader.com/<?=(_LANG=="fr")?'en':'fr';?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/<?=_ID;?>" class="btn btn-xp-black" style="font-size: 1.1em;"><?=$lbllanguage[_LANG];?></a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center" id="player_wrapper">
        <div id="jwoverview" data-lang="<?=_LANG;?>" data-url="<?=$lblurl[_LANG];?>" class="center-block"></div>
        <input type="hidden" id="overview_position" value=""/>
    </div>
</div>


<div class="row">
    <div class="col-md-12">

        <div class="attmessage text-center"></div>
        <div class="addprospectbox">
            <div class="row">
                <div class="col-lg-12">
                    <h3 style="line-height: 20px;"><?=$lblinterest[_LANG];?></h3>
                    <p><?=$lblinterestdesc[_LANG];?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 text-left">
                    <div class="form-group">
                        <label><?=$lblname[_LANG];?></label>
                        <input type="text" name="txtinvitename" id="txtinvitename" value="<?=(!isset($iscampaign))?$cast->name:'';?>" class="form-control" required="true" <?=(!isset($iscampaign))?'readonly="true"':'';?>/> 
                    </div>
                </div>
                
                <div class="col-lg-3 text-left">
                    <div class="form-group">
                        <label><?=$lblemail[_LANG];?></label>
                        <input type="email" name="txtinvitemail" id="txtinvitemail" value="<?=$cast->mail;?>" class="form-control" required="true" readonly="true" />
                    </div>
                </div>
                
                <div class="col-lg-3 text-left">
                    <div class="form-group">
                        <label><?=$lblphone[_LANG];?></label>
                        <input type="text" name="txtinvitephone" id="txtinvitephone" value="" class="form-control" required="true"/>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <label>&nbsp;</label>
                    <input type="hidden" id="interesttoken" value="<?=sha1(_ID."AddT3HSuperPr0sp3ct!");?>"/>
                    <input type="hidden" id="key" value="<?=_ID;?>" />
                    <input type="hidden" id="leaderid" value="<?=$cast->leaderid;?>"/>
                    <button type="button" data-lang="<?=_LANG;?>" class="btn btn-danger btn-block addinterest"><?=$lblbtninfo[_LANG];?></button>
                </div>
            </div>
        </div>

    </div>
</div>