<?php 
$lbltitle = array("fr"=>"DÉSOLÉ!", "en"=>"SORRY!");
$lblmsg = array("fr"=>"Cette page n'existe plus ou est expirée", "en"=>"That page does not exist or is expired");
$lblbtn = array("fr"=>"RETOUR PAGE D'ACCUEIL", "en"=>"BACK TO HOMEPAGE");
?>
<div class="row">
    <div class="col-md-12 text-center" style="color: #ffffff;">
        <h1><?=$lbltitle[_LANG];?></h1>
        <h4><?=$lblmsg[_LANG];?></h4>
        <p>
            <a href="/<?=_LANG;?>/<?=_CONTROLLER;?>" class="btn btn-success btn-lg"><?=$lblbtn[_LANG];?></a>
        </p>
    </div>
</div>
