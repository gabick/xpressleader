<?php
$lblvideolink       = array("fr"=>"https://player.vimeo.com/external/175235558.sd.mp4?s=5c3cf85ebe621c5f03dfaf7b633ffc60b2ac95bc&profile_id=165","en"=>"https://player.vimeo.com/external/181858157.sd.mp4?s=ecc4726b089c47ea4ff9a6e7dc712f4bedef8773&profile_id=165");
$lblWelcomeGift		= array("fr"=>"CADEAU DE BIENVENUE","en"=>"YOUR WELCOME GIFT");
$lbldownload		= array("fr"=>"Télécharger maintenant","en"=>"Download now");

$lbltitle           = array("fr"=>"Créez votre compte dès maintenant","en"=>"Create your account now");
$lblfirstname       = array("fr"=>"Nom","en"=>"Firstname");
$lbllastname       	= array("fr"=>"Prénom","en"=>"Lastname");
$lblemail       	= array("fr"=>"Adresse courriel","en"=>"Email address");
$lblconfemail       = array("fr"=>"Confirmation adresse courriel","en"=>"Confirm your email address");
$lblbtn				= array("fr"=>"OUVRIR MON COMPTE DÈS MAINTENANT!","en"=>"CREATE YOUR ACCOUNT NOW!");
$lblbtnconfirm      = array("fr"=>"CONFIRMER MON COURRIEL","en"=>"CONFIRM MY EMAIL");

$lblherebcuz		= array("fr"=>"<h2 style=\"color:#ffffff;line-height:30px;margin:0;\"><img src=\"//media.xpressleader.com/images/logos/xpressleader_white.svg\" style=\"display: inline;height: 70px;margin-bottom:10px;\"/> &nbsp; LA VOIE RAPIDE VERS LE SUCCÈS</h2>", "en"=>"<h3>YOU ARE HERE BECAUSE SOMEONE USES  <img src=\"//media.xpressleader.com/images/logos/xpressleader_white.svg\" style=\"display: inline;height: 60px;margin-bottom:10px;\"/></h3>");
$lblxpintro			= array("fr"=>"Qu'est-ce que Xpressleader.com ?","en"=>"What is Xpressleader.com ?");
$lblxpdesc			= array("fr"=>"Une plateforme web unique de prospections pour les entreprises en marketing relationnel. Puissante et simple à utilisée, la plate-forme Xpressleader vous permettra de développer votre entreprise à la vitesse grand V!","en"=>"A unique web-based prospecting platform for businesses in relationship marketing. Powerful and simple to use, the Xpressleader platform will allow you to grow your business at great V speed!");
$lbldollar			= array("fr"=>"Un système clé en main pour moins de 1$ par jour!","en"=>"Start using Xpress for less than a dollar per day!");
$lbllist1			= array("fr"=>"Votre page web personnalisée","en"=>"Your personalized webpage");
$lbllist2			= array("fr"=>"Un système complet de prospection automatisé","en"=>"A complete prospecting system");
$lbllist3			= array("fr"=>"Analyse et suivi de vos prospects","en"=>"Tracking and stats of your prospect");

$lblready           = array("fr"=>"Prêt à devenir un leader ?","en"=>"Ready to become a leader ?");
$lbllegal			= array("fr"=>"Xpress leader LLC est une entreprise indépendante et n'est pas affilié ni associé à aucune autre entreprise. L'ouverture d'un compte, bien que fortement recommandé, n'est pas obligatoire pour le démarrage de votre entreprise.","en"=>"Xpress leader LLC is an independent company and is not affiliated or associated with any other companies. Opening an account, although highly recommended, is not required for starting your business.");
?>

<div class="row">
	<div class="col-lg-12">
		<input type="hidden" id="overviewurl" value="<?=$lblvideolink[_LANG];?>" required="true"/>
            <input type="hidden" id="overviewimg" value="" required="true"/>
        <div id="jwdynvideo" class="center-block"></div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-center">
	    <div  style="background-color: rgba(0,0,0,0.8); padding: 10px;">
		<h1><?=$lbldollar[_LANG];?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<a href="#" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#signupmodal" style="border-radius: 0;"><?=$lblbtn[_LANG];?></a>

	</div>
</div>


<div class="row">
	<div class="col-lg-8">

		<h1><?=$lblxpintro[_LANG];?></h1>
		<p><?=$lblxpdesc[_LANG];?></p>
		<div style="font-size: 1.3em;line-height: 38px;">
		<ul>
			<li><?=$lbllist1[_LANG];?></li>
			<li><?=$lbllist2[_LANG];?></li>
			<li><?=$lbllist3[_LANG];?></li>
		</ul>
		<a href="#" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#signupmodal" style="border-radius: 0;"><?=$lblbtn[_LANG];?></a>
		</div>

	</div>
	<div class="col-lg-4">
		<div  style="background-color: rgba(0,0,0,0.5);">
			<img src="/images/logos/xpressleader_white.svg" class="img img-responsive" style="height: 80px;"/>
        	<img src="/<?=PAGECORPOKEY;?>/images/only15_<?=_LANG;?>.png" class="img img-responsive">
        </div>
	</div>
</div>




<div class="row">
	<div class="col-md-12">
		<div class="alert whitebox" style="margin-top: 20px;" role="alert"><strong>IMPORTANT:</strong> <?=$lbllegal[_LANG];?></div>
	</div>
</div>
