<?php
$hiding = false;

if (isset($_POST['test-final-step-button'])) {
    $leaderid = $_POST['leaderid'];
    $leaderEmail = $pageleader->mail;
    $prospectInfo = [
        [
            'name' => $_POST['txtprosname'],
            'phone' => $_POST['txtprosphone'],
            'email' => $_POST['txtprosmail'],
            'lang' => $_POST['lang'],

        ]
    ];
    $hiding = true;
    $prospect->leaderInviteManually($leaderid,$leaderEmail, $prospectInfo);
    $prospectId = isset($_GET['prospectid']) ? $_GET['prospectid'] : null;
    $prospect->changeStatus($prospectId, 2);
}

$lbltitle         = array("fr"=>"Information supplémentaire","en"=>"Additional information");
$lblurl           = array("fr"=>"https://player.vimeo.com/video/359615311", "en"=>"https://player.vimeo.com/video/272157854");
$lblbtninfo       = array("fr"=>"CONTINUER","en"=>"CONTINUE");
$lblbtninterest   = array("fr"=>"Ça m'intéresse","en"=>"I'm interested!");

$lblname                = array("fr"=>"Votre nom", "en"=>"Your name");
$lblmail                = array("fr"=>"Adresse courriel", "en"=>"Email address");
$lblphone               = array("fr"=>"Votre Numero de téléphone", "en"=>"Your phone number");
$lblbtnwatch            = array("fr"=>"Je veux plus d’infos", "en"=>"I want more information");
$linkvideo     		= array("fr"=>"https://player.vimeo.com/video/359615311", "en"=>"https://player.vimeo.com/video/272157854");

$thanksHeader = array("fr"=>"Merci de votre intérêt!","en"=>"Thank bodyyou for your interest!");
$thanksMessage1 = array("fr"=>"Nous avons reçu votre demande, nous vous contacterons sous peu","en"=>"We have received your inquiry, we will contact you shortly");
?>
<style>
    .jw-preview {
        background:transparent !important;
    }
</style>
<?php if ($hiding): ?>
<div class="row">
    <div class="col-md-12 text-center" id="thanks">
        <h1 style="margin-bottom:1em"><?= $thanksHeader[_LANG]; ?></h1>
        <span><img src="/<?=PAGECORPOKEY;?>/images/checkmark-256x256.png" alt="check-mark" width="64px"></span>
        <p style="margin-top:1em; margin-bottom:1em;"><strong><?= $thanksMessage1[_LANG]; ?></strong></p>
    </div>
</div>
<?php else: ?>
<div class="row">
        <div class="col-md-12" id="player_wrapper">
            <div>
                <div class="pull-left">
                    <h3 style="line-height: 16px;"><?=$lbltitle[_LANG];?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center" id="player_wrapper">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item nopadding" src="<?=$lblurl[_LANG];?>?autoplay=1" allow="autoplay ; fullscreen" frameborder="1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="attmessage text-center"></div>
            <div class="addprospectbox" style="padding: 40px;">
                <form action="#" method="post">
                    <div class="form-group">
                        <label for="txtprosname"><?=$lblname[_LANG];?></label>
                        <input type="text" id="txtprosname" name="txtprosname" class="form-control" placeholder="" required="true">
                    </div>
                    <div class="form-group">
                        <label for="txtprosmail"><?=$lblphone[_LANG];?></label>
                        <input type="tel" id="txtprosphone" name="txtprosphone" class="form-control" placeholder="" required="true">
                    </div>
                    <div class="form-group">
                        <label for="txtprosmail"><?=$lblmail[_LANG];?></label>
                        <input type="email" id="txtprosmail" name="txtprosmail" class="form-control" placeholder="" required="true">
                    </div>

                    <input type="hidden" name="leaderid" value="<?=$pageleader->leaderid;?>"/>
                    <input type="hidden" name="validate" value="true"/>
                    <input type="hidden" name="subform" value="add"/>
                    <input type="hidden" name="lang" value="<?=_LANG;?>"/>
                    <input type="hidden" name="test-final-step-button" value="true">
                    <button id="test-final-step-button" class="btn btn-danger btn-block btn-lg" type="submit"><?=$lblbtnwatch[_LANG];?></button>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<input type="hidden" id="points" name="txtpts" value="0"/>
<input type="hidden" id="index" name="txtindex" value="0"/>
<input type="hidden" id="token" value="<?=_ID;?>"/>
<input type="hidden" id="leaderid" value="<?=_CONTROLLER;?>"/>
<input type="hidden" id="lang" value="<?=_LANG;?>"/>
