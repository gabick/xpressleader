<?php
$lblmnuhome         = (_LANG=="fr") ? "Accueil" : "Home";
$lblmnuevent        = (_LANG=="fr") ? "Formations" : "Trainings";
$lblmnuproduct      = (_LANG=="fr") ? "Produits & Services" : "Products & Services";
$lblmnubusiness     = (_LANG=="fr") ? "Zone des bâtiseurs" : "Business center";

$lbllanguage        = array("fr"=>"Switch to <span style=\"color:#05aafa;\">English</span>", "en"=>"Voir <span style=\"color:#05aafa;\">en Français</span>");

$lbltitle           = array("fr"=>"Créez votre compte dès maintenant","en"=>"Create your account now");
$lblfirstname       = array("fr"=>"Nom","en"=>"Firstname");
$lbllastname       	= array("fr"=>"Prénom","en"=>"Lastname");
$lblemail       	= array("fr"=>"Adresse courriel","en"=>"Email address");
$lblconfemail       = array("fr"=>"Confirmation adresse courriel","en"=>"Confirm your email address");
$lblbtn				= array("fr"=>"OUVRIR UN COMPTE XPRESS","en"=>"CREATE YOUR XPRESS ACCOUNT");
$lblbtnconfirm      = array("fr"=>"CONFIRMER MON COURRIEL","en"=>"CONFIRM MY EMAIL");

$lblsignin          = (_LANG=="fr") ? "Se connecter" : "Login";
$lblfindph          = (_LANG=="fr") ? "Trouver un leader" : "Find a leader";
$lbllogin           = (_LANG=="fr") ? "Connectez-vous à votre compte" : "Login to access your account";
$lblusrph           = (_LANG=="fr") ? "Adresse courriel" : "Email address";
$lblpwph            = (_LANG=="fr") ? "Mot de passe" : "Password";
$lblsigninbtn       = (_LANG=="fr") ? "Connexion" : "Log in";
$lblcancelbtn       = (_LANG=="fr") ? "Annuler" : "Cancel";
$lblforgotpass      = (_LANG=="fr") ? "Mot de passe oublié?" : "Forgot your password?";

$lblogin          	= array("fr"=>"Connexion", "en"=>"Log In");
$lblsignup          = array("fr"=>"Joindre mon équipe", "en"=>"Join my team");
$layout             = $account->getLayout($pageleader->leaderid, $pageleader->groupleaderid,$pageleader->corpoid);

$testFinalStep = null;
if(isset($_GET["testFinalStep"])) {
    $testFinalStep = true;
}

$testFinalStepSubmit = null;
if(isset($_GET["testFinalStepSubmit"])) {
    $testFinalStepSubmit = true;
}

if(_ID != ""){
	$db->query("SELECT
					invitations.token,
					invitations.leaderid,
					leaders.firstname,
					leaders.lastname,
					leaders.mail AS leadermail,
					invitations.prospectid,
					dbclients.mail AS prospectmail,
					dbclients.`name`,
					dbclients.phone,
					dbclients.`status`,
					dbclients.id
					FROM
					invitations
					INNER JOIN dbclients ON invitations.prospectid = dbclients.id
					INNER JOIN leaders ON invitations.leaderid = leaders.`code` 
				WHERE invitations.token=:token;");
	$db->bind(":token", _VIEW);
	$prospect = $db->single();
}

function jsonExtract($string) {
    $msg = json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE && $string != "") ? $msg->{_LANG} : $string;
}
?>
<!DOCTYPE html>
<html>
    <head>
    	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"/>
        <title>Xpress Leader</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="apple-touch-icon" sizes="57x57" href="/images/logos/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/images/logos/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/logos/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/images/logos/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/logos/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/images/logos/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/logos/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/images/logos/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/images/logos/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/images/logos/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/logos/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/images/logos/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/logos/favicon/favicon-16x16.png">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/images/logos/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Raleway:300,600' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="/<?=PAGECORPOKEY;?>/styles/default.css?v2">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="signupmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="true" style="z-index: 9999;">
        <div class="modal-dialog modal-primary">
            <div class="">
				<div class="modal-body">
					<div id="signloader" class="text-center"></div>
					<div id="signupbox">
						<h3><?=$lbltitle[_LANG];?></h3>
		                <div id="signmessage"></div>
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group">
		                            <label for="signfirstname"><?=$lblfirstname[_LANG];?></label>
		                            <input type="text" class="form-control" id="signfirstname" placeholder="">
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group">
		                            <label for="signlastname"><?=$lbllastname[_LANG];?></label>
		                            <input type="email" class="form-control" id="signlastname" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="signemail1"><?=$lblemail[_LANG];?></label>
		                            <input type="email" class="form-control" id="signemail1" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="signemail2"><?=$lblconfemail[_LANG];?></label>
		                            <input type="email" class="form-control" id="signemail2" placeholder="">
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-md-12">
		                    	<input type="hidden" id="lang" value="<?=_LANG;?>"/>
		                        <input type="hidden" id="signleaderid" value="<?=$pageleader->leaderid;?>"/>
		                        <input type="hidden" id="signtoken" value="<?=sha1($pageleader->leaderid."signupMynewAccount!");?>"/>
		                        <button class="btn btn-block btn-lg btn-success" id="signsubmit"><?=$lblbtnconfirm[_LANG];?> <i class="fa fa-check-square-o" aria-hidden="true"></i></button>
		                    </div>
		                </div>
		            </div>
				</div>
            </div>
        </div>
    </div>


		<div class="cb-slideshow">
		    <img src="/<?=PAGECORPOKEY;?>/images/bg1.jpg">
			<img src="/<?=PAGECORPOKEY;?>/images/bg2.jpg">
			<img src="/<?=PAGECORPOKEY;?>/images/bg3.jpg">
		</div>

        <?php
        $navbarContainer = "container";
        if(isset($cleader)) include_once(SITEPATH."/views/view.navbar.php");
        if($vdnerror)   echo '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-danger" role="alert">Impossible d\'envoyer une invitation à cette adresse.</div></div></div>';
        if($vdnsuccess) echo '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-success" role="alert">Une invitation à été envoyé à votre adresse courriel.</div></div></div>';
        ?>


		<div class="container">
		    <div class="row">
		        <div class="col-md-3" style="">
		            <div class="blackbox">

        		        <div id="profile" class="whitebox">
                            <ul id="profile-menu">
        		                <?php
        		                if(trim($layout->profile)<>"") echo '<li><img src="/'.PAGECORPOKEY.'/images/profiles/'.$layout->profile.'" class="img-responsive img-rounded"/></li>';
        		                echo '<li class="text-center"><strong>'.mb_strtoupper($pageleader->name).'</strong></li>';
            		            if($layout->showlocation==1) echo '<li><i class="fa fa-globe"></i> <small>'.$pageleader->city.', '.$pageleader->region.', '.$pageleader->country.'</small></li>';
        		                if($layout->showphone==1) echo '<li><i class="fa fa-phone"></i> <small>'.$pageleader->phone.'</small></li>';
        		                if($layout->showemail==1) echo '<li><i class="fa fa-envelope"></i> <small>'.$pageleader->mail.'</small></li>';
        		                ?>
        		            </ul>
        		        </div>



        		        <div style="padding:10px;text-align: justify;">
        		            <?php if(trim(jsonExtract($layout->profile_description))<>"") echo '<p>'.jsonExtract($layout->profile_description).'</p>'; ?>
                        </div>
    		        </div>
    		    </div>

    		    <div class="col-md-9">

    		        <div class="blackbox">
    		        	<?php
    		        	if(_VIEW!="overview" && _VIEW != "test"){
    		        	?>

    		        	<div class="row">
						    <div class="col-md-12">
					            <div class="pull-right">
<!--					            	<a href="#" class="btn btn-xp-black" data-toggle="modal" data-target="#signupmodal" style="font-size: 1.1em;line-height: 30px;">--><?//=$lblsignup[_LANG];?><!--</a>-->
					                <a href="//xpressleader.com/<?=(_LANG=="fr")?'en':'fr';?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/<?=_ID;?>" class="btn btn-xp-black" style="font-size: 1.1em;line-height: 30px;"><?=$lbllanguage[_LANG];?></a>
					            </div>
						    </div>
						</div>

    		            <?php
    		        	}
    		        	if($testFinalStep !== null) {
                            include "view.test-finalstep.php";
                        } elseif($testFinalStepSubmit != null) {
                             include "view.home.php";
                        } elseif($isoverview) {
    		            	if($cast) include "view.overview.php";
    		            	else include "notfound.php";

										}elseif($istest){
											if($cast) include "view.test.php";
    		            	else include "notfound.php";
    		            }elseif($islogin&&($pageleader->groupleaderid==$curleader->groupleaderid)){

        		            switch(_VIEW){
        		                case "home":
        		                    include "view.home.php";
        		                    break;
        		                case "signup":
        		                    include "view.signup.php";
        		                    break;
        		                case "products":
        		                    include "view.products.php";
        		                    break;
        		                case "business":
        		                    include "view.business.php";
        		                    break;
        		                default:
        		                    include "view.home.php";
        		                    break;
        		            }

    		            }else{
    		            	switch(_VIEW){
        		                case "home":
        		                    include "view.home.php";
        		                    break;
        		                case "signup":
        		                    include "view.signup.php";
        		                    break;
        		                default:
        		                    include "view.home.php";
        		                    break;
        		            }
    		            }
    		            ?>

    		        </div>
    		    </div>
		    </div>

		</div>
		<script>
		    var _LANG = "<?=_LANG;?>";
		    var tease_autoplay = <?=(_VIEW=="show")?'true':'false';?>;
		    var tease_controls = <?=(_VIEW=="show")?'false':'true';?>;

		</script>
        <script src="/<?=PAGECORPOKEY;?>/scripts/modernizr.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="/assets/jwplayer-7.4.4/jwplayer.js"></script>
        <script src="/<?=PAGECORPOKEY;?>/scripts/qtrotator.js"></script>
        <script src="/<?=PAGECORPOKEY;?>/scripts/v1.global.js?v6"></script>
    </body>
</html>
