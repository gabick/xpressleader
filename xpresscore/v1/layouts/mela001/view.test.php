<?php
$lbltitle         = array("fr"=>"Passer votre test","en"=>"This is your test");
$linkvideo        = array("fr"=>"https://player.vimeo.com/video/260594105", "en"=>"https://player.vimeo.com/video/271351696");
$lblbtninfo       = array("fr"=>"CONTINUER","en"=>"CONTINUE");
$lblbtninterest   = array("fr"=>"Ça m'intéresse","en"=>"I'm interested!");
$db               = new database();
$prospect         = new prospect($db);
$prospectId       = $cast->prospectid;
$status = $prospect->getStatus($prospectId);
if ($status !== '2') {
    $prospect->changeStatus($prospectId, 1);
}
?>
<div class="row">
    <div class="col-md-12" id="player_wrapper">
        <div>
            <div class="pull-left">
                <h3 style="line-height: 16px;"><?=$lbltitle[_LANG];?></h3>
            </div>
            <div class="pull-right">
<!--                <button id="playpause" type="button" class="btn btn-xp-black" style="font-size: 1.1em;"><i class="fa fa-pause" aria-hidden="true"></i> Pause video</button>-->
                <a href="//xpressleader.com/<?=(_LANG=="fr")?'en':'fr';?>/<?=_CONTROLLER;?>/<?=_VIEW;?>/<?=_ID;?>" class="btn btn-xp-black" style="font-size: 1.1em;"><?=$lbllanguage[_LANG];?></a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item nopadding custom-vimeo-test" src="<?=$linkvideo[_LANG];?>?autoplay=1" allow="autoplay ; fullscreen" frameborder="1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="attmessage text-center"></div>
        <div class="addprospectbox" style="padding: 40px;">
            <div class="row hide" id="panAnswers">
              <div class="col-lg-3 text-center">
                  <button type="button" data-lang="<?=_LANG;?>" disabled="disabled" data-url="" data-answer="A" class="btn btn-block btn-primary btn-lg btnanswer">A</button>
              </div>
              <div class="col-lg-3 text-center">
                  <button type="button" data-lang="<?=_LANG;?>" disabled="disabled" data-url="" data-answer="B" class="btn btn-block btn-primary btn-lg btnanswer">B</button>
              </div>
              <div class="col-lg-3 text-center">
                  <button type="button" data-lang="<?=_LANG;?>" disabled="disabled" data-url="" data-answer="C" class="btn btn-block btn-primary btn-lg btnanswer">C</button>
              </div>
              <div class="col-lg-3 text-center">
                  <button type="button" data-lang="<?=_LANG;?>" disabled="disabled" data-url="" data-answer="D" class="btn btn-block btn-primary btn-lg btnanswer">D</button>
              </div>
            </div>
            <div class="row" id="panContinue">
              <div class="col-lg-12 text-right">
                <button type="button" data-lang="<?=_LANG;?>" data-url="" class="btn btn-danger btn-lg btnanswer btncontinue"><?=$lblbtninfo[_LANG];?></button>
              </div>
            </div>
            <div class="row hide" id="panInteret">
              <div class="col-lg-12 text-right">
                <a href="https://xpressleader.com/fr/<?=_CONTROLLER;?>?testFinalStep=true&prospectid=<?= $prospectId; ?>" class="btn btn-danger btn-lg"><?=$lblbtninterest[_LANG];?></a>
              </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="points" name="txtpts" value="0"/>
<input type="hidden" id="index" name="txtindex" value="0"/>
<input type="hidden" id="token" value="<?=_ID;?>"/>
<input type="hidden" id="leaderid" value="<?=_CONTROLLER;?>"/>
<input type="hidden" id="lang" value="<?=_LANG;?>"/>
