<?php
header('Content-type: text/html; charset=UTF-8');
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);

ini_set('session.cookie_domain', '.xpressleader.com' );
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);

/* Set Default Timezone */
date_default_timezone_set('America/Montreal');

/* Analyzing input */
$dispatch   = (isset($_GET['dispatch'])) ? $_GET["dispatch"] : "";
$dispatch   = ltrim ($dispatch,'/');
$input      = explode("/",$dispatch);
$count      = 0;
foreach($input as $i) { $_D[$count] = $i; $count++; }
unset($input,$count,$i);

/* Load variables */
require_once APPPATH.APPVERSION.'/configs/set.vars.php';

if(_LANG<>"fr" && _LANG<>"en") {
	if(_LANG=="") header('location: /fr');
	else header('location: /fr/'._LANG);
}else{
    if(_LANG=="fr") setlocale(LC_TIME, "fr_CA");
}

/* Load assets classes */
require_once APPPATH.APPVERSION.'/assets/stripe-3.12.1/init.php';

/* Classes Autoloader */
spl_autoload_register(function ($class) {
    if(substr($class,0,11) != 'OAuthSimple') {  // ADDED BY FH - 2014-07-22
        include APPPATH.APPVERSION.'/classes/class.' . $class . '.php';
    }                                                           // ADDED BY FH - 2014-07-22
});
session_name("xpress");
session_start();

$db             = new database();
$mailer         = new mailer($db);
$account        = new account($db);
$helper         = new helper($db);
$corporate      = new corporate($db);
$leader         = new leader($db);
$billing        = new billing($db);
$bigbluebutton  = new bigbluebutton();
$event          = new event($db);
$prospect       = new prospect($db);
$client         = new client($db);
$contact        = new contact($db);
$fasttrack      = new fasttrack($db);

if(isset($_POST['validate'])&&$_POST['validate']){
    if(isset($_POST['subform'])&&$_POST['subform']) include_once(APPPATH.APPVERSION.'/validations/validate.'.$_POST['subform'].'.php');
}

if(defined('ISAPP')){ 
    
    /* Start the application process */
    if(_CONTROLLER=="call"){
        if(_VIEW=="signup") include(APPPATH.APPVERSION.'/calls/job.signup.php');
        elseif(_VIEW=="geteventurl") include(APPPATH.APPVERSION.'/calls/job.geteventurl.php');
        elseif(_VIEW=="startwebcast") include(APPPATH.APPVERSION.'/calls/job.startwebcast.php');
        elseif(_VIEW=="overviewcheckin") include(APPPATH.APPVERSION.'/calls/job.overviewcheckin.php');
				elseif(_VIEW=="testcheckin") include(APPPATH.APPVERSION.'/calls/job.testcheckin.php');
        elseif(_VIEW=="addinterest") include(APPPATH.APPVERSION.'/calls/job.addinterest.php');
        elseif(_VIEW=="presenter-follow") include(APPPATH.APPVERSION.'/calls/job.presenter-follow.php');
        elseif(_VIEW=="presenter-group") include(APPPATH.APPVERSION.'/calls/job.presenter-group.php');
        elseif(_VIEW=="xpressbot") include(APPPATH.APPVERSION.'/calls/job.xpressbot.php');
        elseif(_VIEW=="newsletter") include(APPPATH.APPVERSION.'/calls/newsletter.php');
        elseif(_VIEW=="mailhook") include(APPPATH.APPVERSION.'/calls/mailhook.php');
				elseif(_VIEW=="testresults") include(APPPATH.APPVERSION.'/calls/job.testresults.php');
        die();
    }elseif(_CONTROLLER=="contact"){
        if(_VIEW=="add") include(APPPATH.APPVERSION.'/calls/contact/add.php');
        elseif(_VIEW=="delete") include(APPPATH.APPVERSION.'/calls/contact/delete.php');
        elseif(_VIEW=="list") include(APPPATH.APPVERSION.'/calls/contact/list.php');
        elseif(_VIEW=="get") include(APPPATH.APPVERSION.'/calls/contact/get.php');
        elseif(_VIEW=="put") include(APPPATH.APPVERSION.'/calls/contact/put.php');
        elseif(_VIEW=="invite") include(APPPATH.APPVERSION.'/calls/contact/invite.php');
        elseif(_VIEW=="archived") include(APPPATH.APPVERSION.'/calls/contact/archived.php');
        die();
        
    }elseif(_CONTROLLER=="job"){
        if(_VIEW=="dailybill") include(APPPATH.APPVERSION.'/calls/job.dailybill.php');
        if(_VIEW=="clearactivations") include(APPPATH.APPVERSION.'/calls/job.clearactivations.php');
        if(_VIEW=="sendmail") include(APPPATH.APPVERSION.'/calls/job.sendmail.php');
        die();
    }elseif(_CONTROLLER=="email"){
        
        if(_VIEW<>"") echo file_get_contents(APPPATH.APPVERSION.'/emails/'._LANG.'.'._VIEW.'.html');
        die();
    }
}elseif(defined('ISSITE')){
    
    $account->islogin = (isset($_SESSION['islogin'])&&$_SESSION['leader']) ? true : false;
    if($account->islogin){
        $cleader = $account->getLeader($_SESSION['leader']->code, false);
        $iscorpo = ($cleader->corpoid==$cleader->leaderid) ? true : false;
        
        $dtncharge  = new DateTime($cleader->nextcharge);
	    $dtime  	= new DateTime('NOW');
	    
        if($cleader->chargetry==2 && $dtncharge->format("Y-m-d") < $dtime->format("Y-m-d")) {
            if(_VIEW!="billing") header("location: //".DOMAIN."/"._LANG."/account/billing");
        }
    }
	
	if(isset($_GET['unsubscribe']) && $_GET['unsubscribe'] == sha1($cleader->leaderid)){
		$account->unsubscribe($cleader->leaderid);
		$vdnsuccess = true;
		$vdnerror   = false;
		$vdnmsg = array("fr"=>"Votre abonnement a été mise à jour avec succès! Votre compte sera automatiquement suspendu a votre prochain renouvellement.","en"=>"Your billing informations have been updated successfully! Your Xpress Account will be suspended on the next renewal date.");
	}
    
    if(_CONTROLLER=="logout") {
        if(isset($_SESSION['leader'])) {
            $redirect = "//".DOMAIN."/"._LANG;
            session_destroy();
            $_SESSION["islogin"] = false;
            header("location: $redirect");
        }else header("location: //".DOMAIN."/"._LANG);
        die();
    }
    if(_CONTROLLER == 'activation'){
	    
    	//Get the activation key
    	$key = _VIEW;
    	$validkey = false;
    	if($key<>''){
    		
    		$activation = $account->validateKey($key);
    		if($activation){
    		    
    		  $_SESSION["activation"]["mail"] 	    = $activation->mail;
    			$_SESSION["activation"]["key"] 		    = $activation->key;
    			$_SESSION["activation"]["referal"] 	    = $activation->referal;
    			$_SESSION["activation"]["lang"] 	    = $activation->lang;
    			$_SESSION["activation"]["firstname"] 	= $activation->firstname;
    			$_SESSION["activation"]["lastname"] 	= $activation->lastname;
    		    
    		    include_once(APPPATH.APPVERSION.'/validations/legacy/validate.activation.php');
    		    if($activationexpire) header('location: //xpressleader.com/'._LANG.'/'.$key);
    			
    			$validkey = true;
    			
    		}else $validkey = false;
    		
    		if(_ID=='5') $validkey=true;
    		
    	}else $validkey = false;

    	include SITEPATH.'/layout.activation.php';
    	die();
    	
    }elseif(_CONTROLLER=="webcast"){
        
        if(_VIEW ==""){
            header('location: //xpressleader.com/'._LANG);
        }
        
        $curevent   = $event->getEventInfo(_VIEW);

        if(!$curevent){
            $db->query("SELECT
                            invitations.prospectid,
                            invitations.leaderid,
                            invitations.token,
                            invitations.expire,
                            dbclients.mail,
                            dbclients.`name`
                            FROM
                            invitations
                            INNER JOIN dbclients ON invitations.prospectid = dbclients.id WHERE invitations.token=:token AND invitations.expire>NOW();");
            $db->bind(":token", _VIEW);
            $cast = $db->single();

            if($cast){
                $ismod = false;
                $validEvent = false;
                
                $db->query("SELECT * FROM leaders WHERE code=:leaderid");
                $db->bind(":leaderid", $cast->leaderid);
                $pres = $db->single();
            }else{
                $ismod = false;
                $validEvent = false;
                $validcast = false;
            }
        }else{
						if($account->islogin){
								$validEvent = true;
								$ismod      = ($curevent->leaderid==$cleader->leaderid) ? true : false;
						}else{
								
								$ismod      = false;
								$leaderid   = (_ID=="") ? "" : _ID;

								if($leaderid != "" && $account->existLeaderid($leaderid)) {
										if(!$event->getLifeAccess($curevent->timestart,180)){
												$validEvent = true;
										} else $validEvent = false;
								}else $validEvent = false;
						}
						
						if($ismod&&$validEvent){
							$db->query("SELECT url FROM event_medias WHERE id=:lstupload");
							$db->bind(":lstupload", $curevent->mediaid);
							$evmedia = $db->single();

								if($bigbluebutton->create($curevent->token, $curevent->name, $curevent->evmod, $curevent->evusr, $evmedia->url)){
										$joinUrl =  $bigbluebutton->joinUrl($curevent->token, $cleader->firstname.' '.$cleader->lastname, $curevent->evmod,  "default");
								}else $joinUrl = '';

						}elseif($account->islogin&&$validEvent){
								$db->query("SELECT url FROM event_medias WHERE id=:lstupload");
							$db->bind(":lstupload", $curevent->mediaid);
							$evmedia = $db->single();
								if($bigbluebutton->create($curevent->token, $curevent->name, $curevent->evmod, $curevent->evusr, $evmedia->url)){
										$config = ($curevent->ischat==1) ? "default" : "nochat";
										$joinUrl =  $bigbluebutton->joinUrl($curevent->token, $cleader->firstname. ' '.$cleader->lastname, $curevent->evusr, "default");
								}else $joinUrl = '';
						}else $joinUrl = '';
        }
        include_once SITEPATH."/layout.webcast.php";
        
        die();
    	
    }elseif(_CONTROLLER=="invoice"){
        
        if(_VIEW != ""){
            $xero = new xero();
            $redirect = $xero->getOnlineInvoice(_VIEW);
            if($redirect){
                header('location:'.$redirect.'/Invoice/DownloadPdf');
            }
            die();
        }
        
    }elseif(_CONTROLLER<>""){
        
        //Get leader object of the page
        $pageleader = $account->getLeader(_CONTROLLER, true);
        $isoverview = false;
				$istest = false;
        
        if($pageleader){
            $islogin    = (isset($_SESSION['islogin'])&&$_SESSION['leader']) ? true : false;
            if($islogin) $curleader = $account->getLeader($_SESSION['leader']->code, false);

            if($pageleader) define("PAGECORPOKEY", $pageleader->corpotoken);
            else  define("PAGECORPOKEY", "");
            
						if(_VIEW == "test"){
							if(_ID!=""){
								$istest = true;
								$db->query("SELECT
																invitations.prospectid,
																invitations.leaderid,
																invitations.token,
																invitations.expire,
																dbclients.mail,
																dbclients.`name`
																FROM
																invitations
																INNER JOIN dbclients ON invitations.prospectid = dbclients.id WHERE invitations.token=:token AND invitations.expire>NOW() AND invitations.istest=1;");
								$db->bind(":token", _ID);
								$cast = $db->single();
							}
						}elseif(_VIEW == "overview"){
                
                if(_ID!=""){
                    $isoverview = true;
                    $db->query("SELECT
                                    invitations.prospectid,
                                    invitations.leaderid,
                                    invitations.token,
                                    invitations.expire,
                                    dbclients.mail,
                                    dbclients.`name`
                                    FROM
                                    invitations
                                    INNER JOIN dbclients ON invitations.prospectid = dbclients.id WHERE invitations.token=:token AND invitations.expire>NOW();");
                    $db->bind(":token", _ID);
                    $cast = $db->single();
                    
                    if($cast){
                        $db->query("SELECT id,name,mail,phone,`status` FROM dbclients WHERE leaderid=:leaderid AND mail=:email;");
                        $db->bind(":leaderid", $cast->leaderid);
                        $db->bind(":email", $cast->mail);
                        $prospect = $db->single();
                        
                        if($prospect){
                            
                            $db->query("UPDATE dbclients SET `status`=1, lang=:lang, dtwatch=NOW() WHERE leaderid=:leaderid AND mail=:email AND `status`<=1;");
                            $db->bind(":lang", _LANG);
                            $db->bind(":leaderid", $cast->leaderid);
                            $db->bind(":email", $cast->mail);
                            $db->execute();
                            if($prospect->status<1){
                                $bot = new xpressbot($db);
                                $buttons = array("fr"=> array(array(
                								"type"=>"postback",
                								"title"=>"VOIR STATUS",
                								"payload"=>$prospect->id)),
                								"en"=> array(array(
                								"type"=>"postback",
                								"title"=>"GET STATUS",
                								"payload"=>$prospect->id))
                							);
                							
                							
            					$message = array(
                                        "fr"=>$prospect->name." (".$prospect->mail.") regarde présentement un vidéo de survol.", 
                                        "en"=>$prospect->name." (".$prospect->mail.") is currently watching a business overview."
                                    );
                                    
                                try {
                                    $bot->Notifytemplate($cast->leaderid,$message, $buttons);
                                }catch(Exception $e){
                                    
                                }
                            }
                        }
                    }
                }
                    
            }
            
            if(PAGECORPOKEY<>"") include_once APPPATH.APPVERSION.'/layouts/'.PAGECORPOKEY."/layout.php";
            else include_once SITEPATH."/pageNotFound.php";
            die();
        }else{
            include_once SITEPATH."/layout.site.php";
            die();
        }
    }
    
    include_once SITEPATH."/layout.site.php";
    die();
    
}else{
    
    /* Start the website process */
    if(_CONTROLLER=="logout") {
        if(isset($_SESSION['leader'])) {
            $redirect = "//".DOMAIN."/".$_SESSION['leader']->code;
            session_destroy();
            $_SESSION["islogin"] = false;
            header("location: $redirect");
        }else header("location: //".DOMAIN);
        die();
        
    }elseif(_CONTROLLER=="invoice"){
        
        header('Content-type: application/pdf');
        $fb     = new freshbooks(FBHOST, FBAPI);
        $bill   = new billing($db);
        echo $fb->getPDF($bill->fbid(_VIEW));
        die();
    
    }elseif(_CONTROLLER=="activation"){
        
        $activation = $account->validateKey(_VIEW);
        
        if($activation){
            include_once(APPPATH.APPVERSION.'/validations/legacy/validate.activation.php');
            
			$_SESSION["activation"]["mail"] 	= $activation->mail;
			$_SESSION["activation"]["password"] = $activation->password;
			$_SESSION["activation"]["key"] 		= $activation->key;
			$_SESSION["activation"]["referal"] 	= $activation->referal;
			$_SESSION["activation"]["lang"] 	= $activation->lang;
			$validkey = true;
		}else $validkey = false;
		
		if(_ID=='5') $validkey=true;
    
    }elseif(_CONTROLLER=="account"){
        
        $islogin    = (isset($_SESSION['islogin'])&&$_SESSION['leader']) ? true : false;
        
        if($islogin){
            $curleader  = $account->getLeader($_SESSION['leader']->code, false);
            if($curleader){
                $leaderplan = $curleader->plan;
            }else die();
        }
    }elseif(_CONTROLLER<>""){
        //Get leader object of the page
        $pageleader = $account->getLeader(_CONTROLLER, true);
        
        if($pageleader){
            $islogin    = (isset($_SESSION['islogin'])&&$_SESSION['leader']) ? true : false;
            if($islogin) $curleader = $account->getLeader($_SESSION['leader']->code, false);

            if($pageleader) define("PAGECORPOKEY", $pageleader->corpotoken);
            else  define("PAGECORPOKEY", "");
            
        }else define("PAGECORPOKEY", "");
    }
}
