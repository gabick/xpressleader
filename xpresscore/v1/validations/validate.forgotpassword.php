<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
$error  = array();
if($_POST){
    if($_POST["txtusr"]==""){
        $error['txtusr']        = ($_POST["txtusr"]=="") ? "has-error" : "";
        $vdnmsg     = array("fr"=>"<strong>Attention</strong> Informations manquantes.","en"=>"<strong>Warning</strong> Missing informations.");
    	$vdnerror = true;
    	$navbarOpen = $_POST["navbaropen"];
    }elseif (!filter_var($_POST['txtusr'], FILTER_VALIDATE_EMAIL)) {
        $error['txtusr'] = "has-error";
        $vdnmsg     = array("fr"=>"<strong>Attention</strong> L'adresse courriel est invalide.","en"=>"<strong>Warning</strong> Email address is invalid.");
    	$vdnerror   = true;
    	$navbarOpen = $_POST["navbaropen"];
    }elseif(!$account->forgotpassword($_POST["txtusr"], _LANG)){
        $vdnmsg     = array("fr"=>"<strong>Échec!</strong> Votre courriel n'existe pas dans notre système, vérifiez vos informations et essayez de nouveau.","en"=>"<strong>Oops!</strong> your email does not exist in our system, check your information and try again.");
    	$vdnerror = true;
    	$navbarOpen = $_POST["navbaropen"];
    }else{
        $vdnmsg     = array("fr"=>"<strong>Bravo!</strong> Vous recevrez votre mot de passe par courriel dans les prochaines minutes.","en"=>"<strong>Bravo!</strong> Your password has been sent to your mailbox.");
	    $vdnsuccess = true;
        $account->forgotpassword($_POST["txtusr"], _LANG);

    }
}
?>
