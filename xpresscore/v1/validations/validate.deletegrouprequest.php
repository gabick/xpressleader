<?php
if(!$account->deleteGroupRequest($_POST["leaderid"])){
    
    $vdnmsg = array("fr"=>"<strong>Oops!</strong> Il est impossible de supprimer votre demande en ce moment, réessayez plus tard.","en"=>"<strong>Oops!</strong> We cannot remove your request at the moment, please try again later.");
	$vdnerror = true;
	
}else{
    $vdnmsg = array("fr"=>"Votre demande a été supprimée avec succès!","en"=>"Request have been removed successfully!");
	$vdnsuccess = true;
	
}
?>