<?php
$error          = array();

if($_POST){
    
    if($_POST["txtcie"]=="" || $_POST["txtactivity"]=="" || $_POST["txtemployee"]=="" || $_POST["txtcontact"]=="" || $_POST["txtphone"]=="" || $_POST["txtemail"]=="" || $_POST["txtcountry"]=="" || $_POST["txtlang"]==""){
        
        $error['txtcie']         = ($_POST["txtcie"]=="") ? "has-error" : "";
        $error['txtactivity']    = ($_POST["txtactivity"]=="") ? "has-error" : "";
        $error['txtemployee']    = ($_POST["txtemployee"]=="") ? "has-error" : "";
        $error['txtcontact']     = ($_POST["txtcontact"]=="") ? "has-error" : "";
        $error['txtphone']       = ($_POST["txtphone"]=="") ? "has-error" : "";
        $error['txtemail']       = ($_POST["txtemail"]=="") ? "has-error" : "";
        $error['txtlang']        = ($_POST["txtlang"]=="") ? "has-error" : "";
        
    	if(_LANG=='en') $vdnmsg = "<strong>Warning</strong> Missing informations.";
    	else $vdnmsg = "<strong>Attention</strong> Informations manquantes.";
    	$vdnerror = true;
    	
    }elseif (!filter_var($_POST['txtemail'], FILTER_VALIDATE_EMAIL)) {
        
        $error['txtemail'] = "has-error";
        
        if(_LANG=='en') $vdnmsg = "<strong>Warning</strong> Email address is invalid.";
    	else $vdnmsg = "<strong>Attention</strong> L'adresse courriel est invalide.";
    	$vdnerror = true;
    	
    }else{
        
        $body = '<strong>Entreprise</strong><br>';
        $body .= $_POST['txtcie']  .'<br><br>';
        
        $body .= '<strong>Domaine d\'activité</strong><br>';
        $body .= $_POST['txtactivity']  .'<br><br>';
        
        $body .= '<strong>Nombre d\'employés</strong><br>';
        $body .= $_POST['txtemployee']  .'<br><br>';
        
        $body .= '<strong>Nom du contact</strong><br>';
        $body .= $_POST['txtcontact']  .'<br><br>';
        
        $body .= '<strong>Téléphone</strong><br>';
        $body .= $_POST['txtphone']  .' '.$_POST['txtext'] .'<br><br>';
        
        $body .= '<strong>Courriel</strong><br>';
        $body .= $_POST['txtemail']  .'<br><br>';
        
        if($_POST['txtcountry']<>""){
            $body .= '<strong>Pays</strong><br>';
            $body .= $_POST['txtcountry']  .'<br><br>';
        }
        
        $body .= '<strong>Language préféré</strong><br>';
        $body .= $_POST['txtlang']  .'<br><br>';
        
        try {
            $html = file_get_contents(ROOT_APP."/application/emails/corpoform.html");
            $html = str_replace("%message%", $body, $html);
            $message = array(
                'html' => $html,
                'text' => '',
                'subject' => 'Corpo request',
                'from_email' => 'noreply@xpressleader.com',
                'from_name' => 'Xpress leader',
                'to' => array(
                    array(
                        'email' => 'support@xpressleader.com',
                        'name' => '',
                        'type' => 'to'
                    )
                ),
                'headers' => array('Reply-To' => $_POST["txtemail"]),
                'important' => true,
                'track_opens' => false,
                'track_clicks' => false,
                'inline_css' => true,
                'url_strip_qs' => null,
                'preserve_recipients' => true,
                'tags' => array('corpoform'),
                'metadata' => array('website' => DOMAIN)
            );
            $async = false;
            $ip_pool = 'Main Pool';
            $send_at = '';
            $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);

        
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }

    	$vdnmsg = "<strong>Bravo!</strong> The Leader have been assigned to a new group!";
    	$vdnsuccess = true;
    	
    }

}




