<?php
$error  = array();
if($_POST){
    if($_POST["txtusr"]=="" || $_POST["txtpwd"]==""){
        
        $error['txtusr']        = ($_POST["txtusr"]=="") ? "has-error" : "";
        $error['txtpwd']        = ($_POST["txtpwd"]=="") ? "has-error" : "";
        
        $vdnmsg = array("fr"=>"<strong>Attention</strong> Informations manquantes.","en"=>"<strong>Warning</strong> Missing informations.");
    	$vdnerror = true;
    	$navbarOpen = $_POST["navbaropen"];
    }elseif (!filter_var($_POST['txtusr'], FILTER_VALIDATE_EMAIL)) {
        
        $error['txtusr'] = "has-error";
        $vdnmsg = array("fr"=>"<strong>Attention</strong> L'adresse courriel est invalide.","en"=>"<strong>Warning</strong> Email address is invalid.");
    	$vdnerror = true;
    	$navbarOpen = $_POST["navbaropen"];
    }elseif(!$account->signin($_POST["txtusr"],$_POST["txtpwd"])){
        $vdnmsg = array("fr"=>"<strong>Échec!</strong> L'authentification a échoué, vérifiez vos informations et essayez de nouveau.","en"=>"<strong>Login Failed!</strong> Authentication failed, check your information and try again.");
    	$vdnerror = true;
    	$navbarOpen = $_POST["navbaropen"];
    }else{
    	$_SESSION["leader"]   = $account->leader;
    	$_SESSION["islogin"] 	= true;
    	$navbarOpen = $_POST["navbaropen"];
    	if($navbarOpen) header('location: //'.DOMAIN.'/'._CONTROLLER);
    	else  header('location: //'.DOMAIN.'/'.$account->leader->lang);
    }
}
?>