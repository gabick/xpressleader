<?php
if($_POST){
    if(isset($_POST['subsignup'])){
        $vdnmsg = "";
        $_SESSION['signup'] = array();
        extract($_POST);
        
        if($sign_firstname==""){
            $haserror['firstname'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_lastname==""){
            $haserror['lastname'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_gender==""){
            $haserror['gender'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_bday==""||$sign_bmonth==""||$sign_byear==""){
            $haserror['bday'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_pwd==""){
            $haserror['pwd'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_confpwd==""){
            $haserror['confpwd'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_address1==""){
            $haserror['address1'] = true;
            $haserror['address2'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_country==""){
            $haserror['country'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_city==""){
            $haserror['city'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_region==""){
            $haserror['region'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_postal==""){
            $haserror['postal'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_phone==""){
            $haserror['phone'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if($sign_language==""){
            $haserror['language'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Certaines informations sont manquantes.", "en"=>"<strong>Activation failed</strong> Missing information(s).");
            $vdnerror   = true;
        }
        
        if(!$vdnerror && (strlen($sign_pwd)>=6 && strlen($sign_pwd)<=12)){
            $haserror['pwd'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Le mot de passe doit contenir entre 6 et 12 caractères", "en"=>"<strong>Activation failed</strong> Password must contains between 6 ans 12 characters.");
            $vdnerror   = true;
        }elseif(!$vdnerror && ($sign_pwd!=$sign_confpwd)){
            $haserror['pwd'] = true;
            $vdnmsg     = array("fr"=>"<strong>Échec d'activation!</strong> Les mots de passe doivent-être identique.", "en"=>"<strong>Activation failed</strong> Passwords don't matches.");
            $vdnerror   = true;
        }else{
            
            //SUCCESS!!!
            $_SESSION['signup'] = $_POST;
            $step = 3;
        }
        
    }
    
}