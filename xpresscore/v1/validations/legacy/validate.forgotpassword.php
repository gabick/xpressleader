<?php
$forgoterror = false;
$forgotmsg = "";
$forgotsuccess = false;

if(isset($_POST['subforgotpassword'])){

	if($_POST["inputmail"]==""){
		if(_LANG=='en') $forgotmsg = "<strong>Warning</strong> Missing informations.";
		else $forgotmsg = "<strong>Attention</strong> Informations manquantes.";
		$forgoterror = true;
		
	}elseif(!$account->ForgotPassword($_POST["inputmail"])){
		if(_LANG=='en') $forgotmsg = "<strong>Error!</strong> This email doesnt belong to any account.";
		else $forgotmsg = "<strong>Échec!</strong> Ce courriel n'existe pas..";
		$forgoterror = true;
	}else{
		if(_LANG=='en') $forgotmsg = "<strong>Bravo!</strong> your password has been sent to you.";
		else $forgotmsg = "<strong>Yay!</strong> Votre mot de passe a été envoyé!";
		$forgotsuccess = true;
		
	}
	
}
?>