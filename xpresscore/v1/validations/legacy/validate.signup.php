<?php
$signuperror = false;
$signupmsg = "";
$signupsuccess = false;

if(isset($_POST['subsignup'])){

	if($_POST["inputmail"]=="" || $_POST["inputmail2"]=="" || $_POST["inputcode"]==""){
		$signupmsg = "<strong>Attention</strong> Informations manquantes.";
		$signuperror = true;
	}elseif($_POST["inputmail"]<>$_POST["inputmail2"]){
		$signupmsg = "<strong>Attention</strong> Les courriels ne correspondent pas.";
		$signuperror = true;
	}elseif(!$xaccount->GetReferal($_POST["inputcode"])){
		$signupmsg = "<strong>Attention</strong> Le numéro de référence n'est pas valide.";
		$signuperror = true;
	}else{
		if(!$signuperror){
			if($xaccount->Open($_POST["inputmail"], $_POST["inputcode"], _LANG)){
				$signupsuccess = true;
			}else{
				$signupmsg = "<strong>Attention</strong> L'adresse courriel est déjà présent dans notre système, peut-être avez-vous simplement <a href='http://xpressleader.com/"._LANG."/forgotpassword'>oublié votre mot de passe ?</a>";
				$signuperror = true;
			}
		}
	}
}
?>