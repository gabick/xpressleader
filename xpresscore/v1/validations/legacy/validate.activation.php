<?php
$activationerror 	= false;
$activationexpire 	= false;

$_SESSION["activation"]["lang"] = _LANG;

if(isset($_POST['activationstep1'])){
	
	$termsaccept = (isset($_POST["termsaccept"])) ? $_POST["termsaccept"] : '';
	
	if($termsaccept){
		
		$_SESSION["activation"]["termsaccept"] = $termsaccept;
		$_SESSION["activation"]["step2"] = false;
		$_SESSION["activation"]["step3"] = false;
		$_SESSION["activation"]["step4"] = false;
		
		header('location: /'._LANG.'/'._CONTROLLER.'/'._VIEW.'/2');
	}else{
		$vdnerror = true;
		if(_LANG=="fr") $vdnmsg = "<strong>Erreur</strong> Vous devez accepter les termes et conditions avant de poursuivre l'activation de votre compte.";
		else $vdnmsg = "<strong>Error</strong> Please accept our terms and conditions before continuing.";
	}
}


if(isset($_POST['activationstep2'])){
	
	if(isset($_SESSION["activation"]["termsaccept"]) && $_SESSION["activation"]["termsaccept"]=="true"){
		
		$_SESSION["activation"]["firstname"] 	= (isset($_POST["InputFirstname"])) ? $_POST["InputFirstname"] : '';
		$_SESSION["activation"]["lastname"] 	= (isset($_POST["InputLastname"])) ? $_POST["InputLastname"] : '';
		$_SESSION["activation"]["password"] 	= (isset($_POST["InputPassword"])) ? $_POST["InputPassword"] : '';
		$_SESSION["activation"]["phone"] 		= (isset($_POST["InputPhone"])) ? $_POST["InputPhone"] : '';
		$_SESSION["activation"]["address1"] 	= (isset($_POST["InputAddress1"])) ? $_POST["InputAddress1"] : '';
		$_SESSION["activation"]["address2"] 	= (isset($_POST["InputAddress2"])) ? $_POST["InputAddress2"] : '';
		$_SESSION["activation"]["city"] 		= (isset($_POST["InputCity"])) ? $_POST["InputCity"] : '';
		$_SESSION["activation"]["postal"] 		= (isset($_POST["InputPostal"])) ? $_POST["InputPostal"] : '';
		$_SESSION["activation"]["region"] 		= (isset($_POST["InputRegion"])) ? $_POST["InputRegion"] : '';
		$_SESSION["activation"]["country"] 		= (isset($_POST["InputCountry"])) ? $_POST["InputCountry"] : 'CA';
		$_SESSION["activation"]["bday"] 		= (isset($_POST["InputBday"])) ? $_POST["InputBday"] : '';
		$_SESSION["activation"]["bmonth"] 		= (isset($_POST["InputBmonth"])) ? $_POST["InputBmonth"] : '';
		$_SESSION["activation"]["byear"] 		= (isset($_POST["InputByear"])) ? $_POST["InputByear"] : '';

		if($_SESSION["activation"]["firstname"]<>"" && $_SESSION["activation"]["lastname"]<>"" && $_SESSION["activation"]["password"]<>"" &&$_SESSION["activation"]["phone"]<>"" && $_SESSION["activation"]["address1"]<>"" && $_SESSION["activation"]["city"]<>"" && $_SESSION["activation"]["postal"]<>"" && $_SESSION["activation"]["country"]<>"" && $_SESSION["activation"]["region"]<>""){
			if(strlen($_SESSION["activation"]["password"])<6){
				$vdnerror = true;
				if(_LANG=="fr") $vdnmsg = "<strong>Oops!</strong> Votre mot de passe doit être en 6 et 12 caractères.";
				else $vdnmsg = "<strong>Oops</strong> The password must contain between 6 and 12 chars";
			}else{
				$_SESSION["activation"]["step2"] = true;
				header('location: /'._LANG.'/'._CONTROLLER.'/'._VIEW.'/3');
			}
			
		}else{
			$vdnerror = true;
			if(_LANG=="fr") $vdnmsg = "<strong>Erreur</strong> Des informations sont manquants, s'il vous plaît, vérifier que tous les champs obligatoires sont bien remplis.";
			else $vdnmsg = "<strong>Oops</strong> Some informations are missing, please fill all mandatory fields before continuing.";
		}
		
	}else{
		$activationexpire = true;
	}
}

if(isset($_POST['activationstep3'])){
	
	if($_SESSION["activation"]["step2"] && $_SESSION["activation"]["termsaccept"]=="true"){
		
		$plan 			= (isset($_POST["selplan"])) ? $_POST["selplan"] : '';
		$token 			= (isset($_POST["stripeToken"])) ? $_POST["stripeToken"] : '';
		
		if($plan<>'' || $token<>''){
			
			if($plan==''){
				$vdnerror = true;
			  	if(_LANG=="fr"){
					$vdnmsg = "<strong>Oops</strong> Vous devez choisir un forfait!";
			  	}else $vdnmsg = "<strong>Something went wrong</strong> Have you forgot to choose a plan ? Please try again";
			}else{
				$_SESSION["activation"]["plan"] = $plan;
				$_SESSION["activation"]["cctoken"] = $token;
	
				if($account->activate($_SESSION["activation"])){
					session_destroy();
					header('location: /'._LANG.'/'._CONTROLLER.'/'._VIEW.'/5');
				}else{
					$vdnerror 	= true;
					$vdnmsg 	= $account->error;
				}
			}

		}else{
			$vdnerror = true;
			if(_LANG=="fr"){
				$vdnmsg = "<strong>Erreur</strong> Vérifier vos informations et essayez de nouveau. ".$account->error;
		  	}else $vdnmsg = "<strong>Something went wrong</strong> Your credit card is not valid, please try again. ".$account->error;
		}
		
	}else{
		$activationexpire = true;
	}
	
}

?>