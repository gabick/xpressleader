<?php
$loginerror = false;
$loginmsg = "";
$loginsuccess = false;

if(isset($_POST['sublogin'])){
	
	$ismanager = (isset($_POST["manager"])) ? true : false;

	if($_POST["inputmail"]=="" || $_POST["inputpassword"]==""){
		if(_LANG=='en') $loginmsg = "<strong>Warning</strong> Missing informations.";
		else $loginmsg = "<strong>Attention</strong> Informations manquantes.";
		$loginerror = true;
		
	}elseif(!$xaccount->Login($_POST["inputmail"],$_POST["inputpassword"], $ismanager)){
		if(_LANG=='en') $loginmsg = "<strong>Login Failed!</strong> Authentication failed, check your information and try again.";
		else $loginmsg = "<strong>Échec!</strong> L'authentification a échoué, vérifiez vos informations et essayez de nouveau.";
		$loginerror = true;
		
	}else{
		
		//Login Success!
		$_SESSION["islogin"] 	= true;
		$_SESSION["leaderid"] 	= $xaccount->leaderid;
		$_SESSION["isactive"] 	= $xaccount->isactive;
		$cleader = $xleader->GetLeader($xaccount->leaderid);
		$xaccount->islogin = true;
		
	}
	
}
?>