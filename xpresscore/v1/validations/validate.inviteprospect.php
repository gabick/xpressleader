<?php
$error  = array();
if($_POST){
    if(isset($_POST["leaderid"])){
        if($prospect->invite($_POST["leaderid"], $_POST["prospectid"], $_POST["key"])){
            $vdnmsg = array("fr"=>"Une invitation a été envoyé à votre prospect!","en"=>"A new invite has been sent to your prospect.");
	        $vdnsuccess = true;
        }else{
            $vdnerror = true;
            $vdnmsg = array("fr"=>"Impossible de renvoyer une invitation à votre prospect. Supprimez-le et essayez de le recréer.","en"=>"We cannot send a new invitation to this prospect, please try delete it and re-create it.");
        }
    }
}