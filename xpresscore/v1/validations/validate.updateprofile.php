<?php
if($_POST){
    
    extract($_POST);
    
    $haserror['txtfirstname']   = ($_POST["txtfirstname"]=="") ? "has-error" : null;
    $haserror['txtlastname']    = ($_POST["txtlastname"]=="") ? "has-error" : null;
    $haserror['txtmail']        = ($_POST["txtmail"]=="") ? "has-error" : null;
    //$haserror['txtconfmail']    = ($_POST["txtconfmail"]=="") ? "has-error" : null;
    $haserror['txtgender']      = ($_POST["txtgender"]=="") ? "has-error" : null;
    $haserror['txtbday']        = ($_POST["txtbday"]=="") ? "has-error" : null;
    $haserror['txtbmonth']      = ($_POST["txtbmonth"]=="") ? "has-error" : null;
    $haserror['txtbyear']       = ($_POST["txtbyear"]=="") ? "has-error" : null;
    
    $haserror['txtlanguage']    = ($_POST["txtlanguage"]=="") ? "has-error" : null;
    $haserror['txtcity']        = ($_POST["txtcity"]=="") ? "has-error" : null;
    $haserror['txtregion']      = ($_POST["txtregion"]=="") ? "has-error" : null;
    $haserror['txtpostal']      = ($_POST["txtpostal"]=="") ? "has-error" : null;
    $haserror['txtgender']      = ($_POST["txtgender"]=="") ? "has-error" : null;
    $haserror['txtcity']        = ($_POST["txtcity"]=="") ? "has-error" : null;
    
    $cleader = $account->getLeader($leaderid);
    
    if($_POST["txtfirstname"]=="" || $_POST["txtlastname"]=="" || $_POST["txtgender"]=="" || $_POST["txtbday"]=="" || $_POST["txtbmonth"]=="" || $_POST["txtbyear"]=="" || $_POST["txtcity"]=="" || $_POST["txtcountry"]=="" || $_POST["txtregion"]=="" || $_POST["txtpostal"]=="" || $_POST["txtphone"]=="" || $_POST["txtpwd"]=="" || $_POST["txtlanguage"]=="" ){

    	$vdnmsg = array("fr"=>"<strong>Attention</strong> Informations manquantes. Vérifier que le formulaire est dûment rempli et essayez de nouveau. (* Obligatoire)","en"=>"<strong>Oops!</strong> Missing information(s), please try again.");
    	$vdnerror = true;
    /*
    }elseif($_POST["txtmail"]<>$_POST["txtconfmail"]){
        
        $haserror['txtmail']       = "has-error";
        $haserror['txtconfmail']   = "has-error";
        
        $vdnmsg = array("fr"=>"<strong>Oops</strong> Les courriels ne sont pas identiques, réessayez de nouveau.","en"=>"<strong>Oops!</strong> Emails address does'nt matches, please try again.");
    	$vdnerror = true;
    
    }elseif(!filter_var($_POST["txtmail"], FILTER_VALIDATE_EMAIL)) {
        
        $haserror['txtmail']       = "has-error";
        $haserror['txtconfmail']   = "has-error";
        
        $vdnmsg = array("fr"=>"<strong>Oops</strong> Votre courriel n'est pas valide, réessayez de nouveau.","en"=>"<strong>Oops!</strong> Your email address is not valid, please try again.");
    	$vdnerror = true;
    
    }elseif(!$account->existEmail($leaderid, $_POST["txtmail"])&&($cleader->mail!=$_POST["txtmail"])){
        
        $haserror['txtmail']       = "has-error";
        $haserror['txtconfmail']   = "has-error";
        
        $vdnmsg = array("fr"=>"<strong>Oops!</strong> Cette adresse courriel existe déjà dans notre système.","en"=>"<strong>Oops!</strong> Email address already exist in our database.");
    	$vdnerror = true;
    */	
    }elseif(!$account->updateProfile($leaderid, $txtfirstname, $txtlastname, $txtgender, $txtbday, $txtbmonth, $txtbyear, $txtmail, $txtpwd, $txtaddress1, $txtaddress2, $txtcountry, $txtcity, $txtregion, $txtpostal, $txtphone, $txtlanguage)){
        
        $vdnmsg = array("fr"=>"<strong>Oops!</strong> Impossible de mettre à jour votre profil, réessayez plus tard.","en"=>"<strong>Oops!</strong> We cannot update your profile at the moment, please try again later.");
    	$vdnerror = true;
    	
    }else{
        
        $vdnsuccess = true;
        $vdnerror   = false;
        $vdnmsg = array("fr"=>"Votre profil a été mise à jour avec succès!","en"=>"Your profile have been updated successfully!");
    }
}