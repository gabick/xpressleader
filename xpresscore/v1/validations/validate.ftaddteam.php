<?
$error  = array();
if($_POST){
    if($_POST["txtteamname"]==""){
        
        $vdnerror = true;
        $vdnmsg = array("fr"=>"Vous devez fournir un nom d'équipe valide.","en"=>"Please, enter a team name.");
            
    }else{
        if(trim($_POST["txtteamname"]) != "" && strlen($_POST["txtteamname"])>=6){
            if($fasttrack->addTeam($_POST["leaderid"], $_POST["txtteamname"])){
                $vdnmsg = array("fr"=>"Votre équipe a été crée avec succès!","en"=>"Your team have been created successfully.");
    	        $vdnsuccess = true;
            }else{
                $vdnerror = true;
                $vdnmsg = array("fr"=>"Impossible de créer votre équipe.","en"=>"This team name already exist!");
            }
        }else{
            $vdnerror = true;
            $vdnmsg = array("fr"=>"Le nom de votre équipe n'est pas valide. Un nom d'équipe doit-être unique et d'un minnimum de 5 caractères","en"=>"Team name is not valid.");
        }
    }
}