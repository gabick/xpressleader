<?php
$error  = array();
if($_POST){
    if(isset($_POST["partner"])&&$_POST["partner"]=="add"){
        if($_POST["groupid"]==""){
            $vdnerror = true;
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez spécifier un groupe pour envoyer votre demande.","en"=>"<strong>Oops!</strong> Please choose a group to send your request.");
        }else{
            if($account->addPartner($_POST["leaderid"],$_POST["groupid"],$_POST["message"])){
                $vdnpartner = true;
            }
        }
    }elseif(isset($_POST["partner"])&&$_POST["partner"]=="remove"){
        if($_POST["groupid"]==""){
            $vdnerror = true;
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez spécifier un groupe pour envoyer votre demande.","en"=>"<strong>Oops!</strong> Please choose a group to send your request.");
        }else{
            if($account->removePartner($_POST["leaderid"],$_POST["groupid"])){
                $vdnpartner = true;
            }
        }
    }
}