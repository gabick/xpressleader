<?php
if($_POST){
    
    $signup['txtfirstname']  = (isset($_POST["txtfirstname"])) ? $_POST["txtfirstname"] : "";
    $signup['txtlastname']   = (isset($_POST["txtlastname"])) ? $_POST["txtlastname"]: "";
    $signup['txtmail']       = (isset($_POST["txtmail"])) ? $_POST["txtmail"] : "";
    $signup['txtconfmail']   = (isset($_POST["txtconfmail"])) ? $_POST["txtconfmail"] : "";
    $signup['txtrefcode']   = (isset($_POST["txtrefcode"])) ? $_POST["txtrefcode"] : "";
        
    if($_POST["txtfirstname"]=="" || $_POST["txtlastname"]=="" || $_POST["txtmail"]=="" || $_POST["txtconfmail"]=="" || $_POST["txtrefcode"]==""){
        
        $error['txtfirstname']  = ($_POST["txtfirstname"]=="") ? "has-error" : "";
        $error['txtlastname']   = ($_POST["txtlastname"]=="") ? "has-error" : "";
        $error['txtmail']       = ($_POST["txtmail"]=="") ? "has-error" : "";
        $error['txtconfmail']   = ($_POST["txtconfmail"]=="") ? "has-error" : "";
        $error['txtrefcode']   = ($_POST["txtrefcode"]=="") ? "has-error" : "";
        
        $vdnmsg = array("fr"=>"<strong>Attention</strong> Informations manquantes.","en"=>"<strong>Warning</strong> Missing informations.");
    	$vdnerror = true;
    
    }elseif($_POST["txtmail"]<>$_POST["txtconfmail"]){
        
        $error['txtmail']       = "has-error";
        $error['txtconfmail']   = "has-error";
        
        $vdnmsg = array("fr"=>"<strong>Oops</strong> Les courriels ne sont pas identiques, réessayez de nouveau.","en"=>"<strong>Oops!</strong> Emails address does'nt matches, please try again.");
    	$vdnerror = true;
    
    }elseif(!filter_var($_POST["txtmail"], FILTER_VALIDATE_EMAIL)) {
        
        $error['txtmail']       = "has-error";
        $error['txtconfmail']   = "has-error";
        
        $vdnmsg = array("fr"=>"<strong>Oops</strong> Votre courriel n'est pas valide, réessayez de nouveau.","en"=>"<strong>Oops!</strong> Your email address is not valid, please try again.");
    	$vdnerror = true;
    
    }elseif(!$account->existLeaderid($_POST["txtrefcode"])) {
        $vdnmsg = array("fr"=>"<strong>Oops</strong> Votre code de référence n'est pas valide, réessayez de nouveau.","en"=>"<strong>Oops!</strong> The reference code is not valid, please try again.");
    	$vdnerror = true;
    	
    }elseif($account->existEmail($_POST["txtmail"])) {
        $vdnmsg = array("fr"=>"<strong>Échec d'inscription!</strong> Cette adresse courriel existe déjà dans notre système.  Avez-vous <a href=\"/"._LANG."/forgotpassword\" style=\"color:#d5e7f9;\">oubliez votre mot de passe</a> ?","en"=>"<strong>Signup Failed!</strong> Email address already exist in our database. Have you <a href=\"/"._LANG."/forgotpassword\" style=\"color:#d5e7f9;\">forgot your password</a> ?");
    	$vdnerror = true;
        
    }elseif(!$account->signup($_POST["txtfirstname"], $_POST["txtlastname"], $_POST["txtmail"], $_POST["txtrefcode"])){
        $vdnmsg = array("fr"=>"<strong>Désolé!</strong> impossible de procéder à votre abonnement, svp contactez-nous support@xpressleader.com.","en"=>"<strong>Sorry</strong> We cannot proceed to your subscribtion right now, try later or contact us at support@xpressleader.com.");
    	$vdnerror = true;
    	
    }else{
        
        unset($signup);
        $vdnmsg = array("fr"=>"<strong>Bravo!</strong> Dans quelques minutes, vous recevrez un courriel contenant un lien unique afin d'activer votre compte.","en"=>"<strong>Good job!</strong> In a few minutes, you should receive an email from us with a unique link to activate your account.");
        $vdnsuccess = true;
        $vdnerror   = false;
    }
}