<?php
if($_POST){
    
    if(!isset($_POST['selplan']) || $_POST['selplan'] == "") {
        $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez choisir une fréquence de paiement (Mensuel ou Annuel)","en"=>"<strong>Oops!</strong> you need to select a payment frequency. (Monthly or Yearly)");
        $vdnerror = true;
    }elseif(!isset($_POST['selplan']) || $_POST['leaderid'] == "" || !isset($_POST['stripeToken']) || $_POST['stripeToken'] == ""){
        $vdnmsg = array("fr"=>"<strong>ATTENTION!</strong> Une erreur système nous empêche de mettre à jour votre abonnement, s'il vous plaît, contacter nous à support@xpessleader.com.","en"=>"<strong>ERROR!</strong> We cannot update your billing informations at the moment. This is caused by a system error. Please contact us by mail at support@xpressleader.com");
        $vdnerror = true;
    }else{
        
        
        if(isset($_POST['ctoken'])&&$_POST['ctoken']!=""){
            if($_POST['leaderid'] == '154511') $stripekey = "sk_test_V4oSzsHvrXAoXuu3f7JSZr31";
            else $stripekey = STRIPEKEY;
            \Stripe\Stripe::setApiKey($stripekey);
            $cu = \Stripe\Customer::retrieve($_POST['ctoken']);
            $cu->source = $_POST['stripeToken']; // obtained with Stripe.js
            $cu->save();
        
            if($billing->subscribe($_POST['leaderid'], $_POST['selplan'])){
                $vdnsuccess = true;
                $vdnerror   = false;
                $vdnmsg = array("fr"=>"Votre abonnement a été mise à jour avec succès!","en"=>"Your billing informations have been updated successfully!");
            }
        }
    }
    
}