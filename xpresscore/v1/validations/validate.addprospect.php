<?
$error  = array();
if($_POST){
    if($_POST["txtprosmail"]=="" || $_POST["txtprosname"]==""){
        
        $vdnerror = true;
        $vdnmsg = array("fr"=>"Vous devez fournir un nom et un courriel valide.","en"=>"Please, enter a name and a valid mail.");
            
    }elseif (!filter_var($_POST['txtprosmail'], FILTER_VALIDATE_EMAIL)) {
        
        $vdnerror = true;
        $vdnmsg = array("fr"=>"Votre courriel n'est pas valide","en"=>"The email address is not valid.");
        
    }else{
        if(isset($_POST["leaderid"])){
            if($prospect->add($_POST["leaderid"], $_POST["txtprosmail"], $_POST["txtprosname"],1)){
                $vdnmsg = array("fr"=>"Votre prospect a été crée et une invitation lui a été envoyé!","en"=>"The prospect have been created successfully and an invite been sent.");
    	        $vdnsuccess = true;
            }else{
                $vdnerror = true;
                $vdnmsg = array("fr"=>"Ce prospect existe déjà!","en"=>"The prospect already exist!");
            }
        }
    }
}