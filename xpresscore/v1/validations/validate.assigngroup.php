<?php
if($_POST["chgroupid"]==""){
    
    $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez sélectionner un nouveau groupe.","en"=>"<strong>Oops!</strong> You must choose a new group.");
	$vdnerror = true;
	
}elseif(!$account->requestGroup($_POST["leaderid"],$_POST["chgroupid"], $_POST["chgroupmsg"])){
    
    $vdnmsg = array("fr"=>"<strong>Oops!</strong> Il est impossible d'envoyer votre demande en ce moment, réessayez plus tard.","en"=>"<strong>Oops!</strong> We cannot change your group at the moment, please try again later.");
	$vdnerror = true;
	
}else{
    $vdnmsg = array("fr"=>"Votre demande changement de groupe a été envoyé avec succès!","en"=>"Your request have been assigned to a new group!");
	$vdnsuccess = true;
	
}
?>