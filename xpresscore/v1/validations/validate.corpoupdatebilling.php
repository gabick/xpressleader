<?php
if($_POST){
    
    if(!isset($_POST['nextCharge']) || $_POST['nextCharge'] == "") {
        $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez choisir une date de paiement","en"=>"<strong>Oops!</strong> you need to select the date of the next charge");
        $vdnerror = true;
        
    }else{
        
        if($corporate->updateBilling($_POST['leaderid'], $_POST['selPlan'],$_POST['nextCharge'])){
            $vdnsuccess = true;
            $vdnerror   = false;
            $vdnmsg = array("fr"=>"Votre abonnement a été mise à jour avec succès!","en"=>"Your billing informations have been updated successfully!");
        }
    }
}