<?php
if(isset($_POST["leaderid"])&&isset($_POST["corpotoken"])){
    $corpotoken = $_POST["corpotoken"];
    $target_dir = "/Users/glecuyer/Documents/Sites/Gabick/xpressleader/xpressleader/public/$corpotoken/images/profiles/";

    $profile = (file_exists($target_dir.$_POST["leaderid"].".jpg")) ? $_POST["leaderid"].".jpg" : "";
    $showprofile = (isset($_POST["showprofile"])) ? "1" : "";
    $showlocation = (isset($_POST["showlocation"])) ? "1" : "";
    $showphone = (isset($_POST["showphone"])) ? "1" : "";
    $showemail = (isset($_POST["showemail"])) ? "1" : "";

    $uploaded = true;

    if(isset($_FILES["profile"])&&$_FILES["profile"]['size']>0){

        $target_file = $target_dir . $_POST["leaderid"].".jpg";

        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["profile"]["tmp_name"]);
        if(!$check) {
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Votre image n'est pas valide, seulement les format JPG, JPEG, PNG & GIF sont autorisé.","en"=>"<strong>Oops!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
    	    $vdnerror = true;
            $uploaded = false;
        }

        // Check file size
        if ($_FILES["profile"]["size"] > 400000) {
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Votre image est trop volumineuse, maximum 400kb.","en"=>"<strong>Oops!</strong> Your image is too large, must be less than 400kb.");
    	    $vdnerror = true;
            $uploaded = false;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Votre image n'est pas valide, seulement les format JPG, JPEG, PNG & GIF sont autorisé.","en"=>"<strong>Oops!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
    	    $vdnerror = true;
            $uploaded = false;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploaded) {
            if (!move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file)) {
                $vdnmsg = array("fr"=>"<strong>Oops!</strong> La mise à jour de votre personnalisation a échoué. Réessayez plus tard.","en"=>"<strong>Oops!</strong> Your customization failed, please try again later.");
        	    $vdnerror = true;
                $uploaded = false;
            }
        }

        $profile = $_POST["leaderid"].".jpg";
    }

    if($uploaded){
        $profiledesc    = json_encode(array("fr"=>$_POST["profiledescription_fr"],"en"=>$_POST["profiledescription_en"]));
        $herotext       = json_encode(array("fr"=>$_POST["herotext_fr"],"en"=>$_POST["herotext_en"]));
        $overview       = json_encode(array("fr"=>$_POST["overviewtitle_fr"],"en"=>$_POST["overviewtitle_en"]));

       if($account->saveLayout($_POST["leaderid"],$_POST["corpotoken"],$profile,$profiledesc,$showprofile, $showlocation,$showphone,$showemail,$herotext,$overview,$_POST["overviewdescription"])){
            $vdnmsg = array("fr"=>"Votre page web a été mise à jour avec succès!","en"=>"Your customization has been updated successfully!");
    	    $vdnsuccess = true;
        }else{
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> La mise à jour de votre personnalisation a échoué. Réessayez plus tard.","en"=>"<strong>Oops!</strong> Your customization failed, please try again later.");
    	    $vdnerror = true;
        }
    }


}
