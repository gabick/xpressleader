<?
$error  = array();
if($_POST){
    if($_POST["teamid"]=="" || $_POST["leaderid"]==""){
        
        $vdnerror = true;
        $vdnmsg = array("fr"=>"Impossible d'envoyer une demande à cette équipe","en"=>"Cannot send a request to this team..");
            
    }else{
        if($fasttrack->setRequest($_POST["leaderid"], $_POST["teamid"])){
            $vdnmsg = array("fr"=>"Votre demande a été envoyé.","en"=>"Your request have been sent successfully.");
	        $vdnsuccess = true;
        }else{
            $vdnerror = true;
            $vdnmsg = array("fr"=>"Impossible d'envoyer une demande à cette équipe.","en"=>"Cannot send a request to this team.");
        }
    }
}