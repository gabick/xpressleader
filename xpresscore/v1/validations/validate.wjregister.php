<?php
if($_POST){
  $data = array("api_key" => "deb5db6a1819a333c981f7aeaef2b34778dc33385a547fe3eb32d4fce4b6147e", "webinar_id" => "804851398b", "real_dates"=>1);
  $data_string = json_encode($data);

  $ch = curl_init('https://webinarjam.genndi.com/api/everwebinar/register');
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
  );
  curl_setopt($ch, CURLOPT_TIMEOUT, 5);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

  //execute post
  $result = curl_exec($ch);

  //close connection
  curl_close($ch);
  $result = json_decode($result);
}