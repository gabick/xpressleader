<?
$error  = array();
if($_POST){
    if($_POST["teamid"]=="" || $_POST["playerid"]==""){
        
        $vdnerror = true;
        $vdnmsg = array("fr"=>"Impossible d'accepter la demande pour cette équipe.","en"=>"Cannot accept a request for this team..");
            
    }else{
        if($fasttrack->resRequest($_POST["playerid"], $_POST["teamid"], true)){
            $vdnmsg = array("fr"=>"La demande a été accepté avec succès.","en"=>"The request have been accepted successfully.");
	        $vdnsuccess = true;
        }else{
            $vdnerror = true;
            $vdnmsg = array("fr"=>"Impossible d'accepter la demande pour cette équipe.","en"=>"Cannot accept a request for this team..");
        }
    }
}