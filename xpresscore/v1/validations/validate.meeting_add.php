<?php
if($_POST){
    if(isset($_POST['subcrmeet'])){
        $vdnmsg = "";
        extract($_POST);
        $crmeet_chat = (isset($crmeet_chat)) ? "1" : "";
        if($crmeet_date==""){
            $vdnerror = true;
            $vdnmsg = array("fr"=>"<strong>Oops!</strong> Vous devez spécifier la date et l'heure de votre meeting.","en"=>"<strong>Oops!</strong> Please enter the date and time of your meeting.");
        }else{
            if($crmeet_category=="2"&&$crmeet_title==""){
                $vdnerror = true;
                $vdnmsg = array("fr"=>"<strong>Oops!</strong> Pour les formations, vous devez spécifier un titre.","en"=>"<strong>Oops!</strong> For training, please enter the subject of your meeting."); 
            }else{
                if($event->createEvent($leaderid, $crmeet_category, $crmeet_type, $crmeet_date, $crmeet_title, $crmeet_language, $crmeet_chat, $crmeet_pwd, $crmeet_media)){
                    $vdnsuccess = true;
                    $vdnmsg = array("fr"=>"Votre meeting a été ajouté à votre calendrier.","en"=>"Your meeting has been added to your calendar.");
                    
                    $crmeet_language = "fr";
                    $crmeet_media = "";
                    $crmeet_type1 = ""; $crmeet_type2 = ""; $crmeet_type3 = "";
                    $crmeet_type = "";
                    $crmeet_date = "";
                    $crmeet_time = "";
                    $crmeet_chat = "1";
                    $crmeet_pwd = "";
                    
                }else{
                    $vdnerror = true;
                    $vdnmsg = array("fr"=>$event->error,"en"=>$event->error);
                }
            }
            
        }
        
        
    }
}
?>