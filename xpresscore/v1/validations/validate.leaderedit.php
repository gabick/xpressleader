<?php
if($_POST){
    
    $errorclass = array();
    $errormsg   = array();
    
    $leader = new leader($db);
    $leader->leaderid   = $_POST['txtcode'];
    $leader->name       = $_POST['txtname'];
    $leader->associate  = $_POST['txtassociate'];
    $leader->bday       = $_POST['selbday'];
    $leader->bmonth     = $_POST['selbmonth'];
    $leader->byear      = $_POST['selbyear'];
    $leader->phone      = $_POST['txtphone'];
    $leader->address1   = $_POST['txtaddress1'];
    $leader->address2   = $_POST['txtaddress2'];
    $leader->city       = $_POST['txtcity'];
    $leader->country    = $_POST['selcountry'];
    $leader->lang       = $_POST['sellanguage'];
    $leader->postal     = $_POST['txtpostal'];
    
    
    if($leader->country=="US") $leader->region = $_POST['selstates'];
    elseif($leader->country=="CA") $leader->region = $_POST['selprovince'];
    else $leader->region = $_POST['selregion'];
    
    
    if($leader->updateinfo()){
        $vdnsuccess = true;
        $vdnmsg     = "Leader profil has been updated successfully.";
    }else{
        $vdnerror   = true;
        $vdnmsg     = "Missing information(s)";
    }
}
?>