<?php 
if(isset($_REQUEST["mandrill_events"])){
    
    $events = json_decode($_REQUEST["mandrill_events"]);
    
    foreach($events as $event){
        if($event->msg->subaccount=="xpressleader" && isset($event->msg->metadata->token)) {
            //mail("info@softnox.net", "test hook", print_r($event->msg, true));

            $db->query("SELECT id FROM mailstacks WHERE token=:token AND email=:email;");
            $db->bind(":token", $event->msg->metadata->token);
            $db->bind(":email", $event->msg->metadata->email);
            $stack = $db->single();
            
            $state = (isset($event->msg->opens)&&count($event->msg->opens)>0) ? "open" : $event->msg->state;
            $state = (isset($event->msg->clicks)&&count($event->msg->clicks)>0) ? "click" : $state;
            
            if($db->rowCount() == 1){
                $db->query("UPDATE mailstacks SET sgevent=:state WHERE id=:stackid;");
                $db->bind(":state", $state);
                $db->bind(":stackid", $stack->id);
                $db->execute();
            }
            
        }
    }
    
}
die();