<?php 

/**
 * Persist the OAuth access token and session handle somewhere
 * In my example I am just using the session, but in real world, this is should be a storage engine
 *
 * @param array $params the response parameters as an array of key=value pairs
 */
function persistSession($response)
{
    if (isset($response)) {
        $_SESSION['access_token']       = $response['oauth_token'];
        $_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];
      	if(isset($response['oauth_session_handle']))  $_SESSION['session_handle']     = $response['oauth_session_handle'];
    } else {
        return false;
    }
}
/**
 * Retrieve the OAuth access token and session handle
 * In my example I am just using the session, but in real world, this is should be a storage engine
 *
 */
function retrieveSession()
{
    if (isset($_SESSION['access_token'])) {
        $response['oauth_token']            =    $_SESSION['access_token'];
        $response['oauth_token_secret']     =    $_SESSION['oauth_token_secret'];
        $response['oauth_session_handle']   =    $_SESSION['session_handle'];
        return $response;
    } else {
        return false;
    }
}


function outputError($XeroOAuth)
{
    echo 'Error: ' . $XeroOAuth->response['response'] . PHP_EOL;
    pr($XeroOAuth);
}
/**
 * Debug function for printing the content of an object
 *
 * @param mixes $obj
 */
function pr($obj)
{
    if (!is_cli())
        echo '<pre style="word-wrap: break-word">';
    if (is_object($obj))
        print_r($obj);
    elseif (is_array($obj))
        print_r($obj);
    else
        echo $obj;
    if (!is_cli())
        echo '</pre>';
}

function is_cli()
{
    return (PHP_SAPI == 'cli' && empty($_SERVER['REMOTE_ADDR']));
}

die();
$sql = "SELECT
                billings.leaderid,billings.planid,billings.nextcharge,billings.chargetry,
                leaders.`name`,leaders.id,leaders.`code`,leaders.alias,leaders.gender,
                leaders.associate,leaders.title,leaders.mail,leaders.`password`,leaders.address1,leaders.address2,
                leaders.city,leaders.region,leaders.country,leaders.postal,
                leaders.phone,leaders.signature,leaders.bday,leaders.bmonth,leaders.byear,leaders.lang,
                leaders.plan,leaders.referal,leaders.guruid,leaders.stripekey,leaders.dateactivate,
                leaders.dateupdate,leaders.datecreation,leaders.datelastlogin,leaders.xmeetleader,
                leaders.fbid,leaders.ispro,leaders.proid,leaders.`status`,
                leaders.proteam,leaders.groupid,leaders.firstname,leaders.lastname,leaders.xeroid
                FROM
                billings
                INNER JOIN leaders ON billings.leaderid = leaders.`code`
                WHERE leaders.`code`= '449685';";

        $db->query($sql);
        $rows = $db->resultset();

        foreach($rows as $leader){

            $db->query("SELECT stripeid FROM transactions WHERE stripeid='ch_9J26QjwJL9J6k9';");
            $db->execute();
            if($db->rowCount() > 0) die("Transaction Already exist!");
            /* 
            Get the current plan of the leader */
            $plan       = $billing->getplan($leader->planid);
            $plandesc   = ($leader->lang=="fr") ? $plan->name_fr : $plan->name_en;
            
            
            /*
                Check if client exist in XERO
                if not, create it */
                $xero = new xero();
                if($leader->xeroid=="") {
                    $xeroid = $xero->addContact($leader->code, $leader->firstname.' '.$leader->lastname, $leader->mail, $leader->address1, $leader->address2, $leader->city, $leader->region, $leader->postal, $leader->country, $leader->xeroid);
                    if($xeroid){
                        $db->query("UPDATE `leaders` SET `xeroid`=:xeroid WHERE `code`=:leaderid;");
                        $db->bind(":xeroid", $xeroid);
                        $db->bind(":leaderid", $leader->code);
                        $db->execute();
                    }
                }else $xeroid = $leader->xeroid;
                
                
                
                 /*
                Create xero Invoice */
                $lines = array();
                $lines[0] = new stdClass();
                $lines[0]->item = $plan->state;
                $lines[0]->description = ($leader->lang=="fr") ? $plan->name_fr : $plan->name_en;
                $lines[0]->quantity = 1;
                $lines[0]->price = $plan->price;
                
                $invoiceid  = $xero->addInvoice($xeroid, $lines, $AccountCode = 200);
                $invoiceurl = $xero->getOnlineInvoice($invoiceid);
                
                /* 
                generate a unique token */
                $token = urlencode(uniqid(md5(rand()), true));
                
                /* 
                Save the transaction */
                $db->query("INSERT INTO transactions(token,fbid,plan,price,stripeid,cname,caddress1,caddress2,cemail,cphone,cregion,ccountry,cpostal,datepayment) 
                                            VALUES(:token,:fbid,:plan,:price,:stripeid,:cname,:caddress1,:caddress2,:cemail,:cphone,:cregion,:ccountry,:cpostal,NOW())");
                $db->bind(":token", $token);
                $db->bind(":fbid", $invoiceid);
                $db->bind(":plan", $plandesc);
                $db->bind(":price", $plan->price);
                $db->bind(":stripeid", "ch_9IesdJ0coJyohG");
                $db->bind(":cname", $leader->name);
                $db->bind(":caddress1", $leader->address1);
                $db->bind(":caddress2", $leader->address2);
                $db->bind(":cemail", $leader->mail);
                $db->bind(":cphone", $leader->phone);
                $db->bind(":cregion", $leader->region);
                $db->bind(":ccountry", $leader->country);
                $db->bind(":cpostal", $leader->postal);
                $db->execute();
                
                
                $db->query("SELECT nextcharge FROM billings WHERE leaderid=:leaderid");
                $db->bind(":leaderid", $leader->leaderid);
                $newbill    = $db->single();
                $nextcharge = $newbill->nextcharge;
                
                /* 
                Send notice by email */
                if($leader->lang=="fr") setlocale(LC_TIME, "fr_FR");
                $oDate      = new datetimefrench($nextcharge);
                $dtnextcharge = $oDate->format("d F Y");
                
                
                $mailer = new mailer($db);
			    $mailer->billsucceed($leader->mail, $plan->price, $dtnextcharge, $invoiceurl, $leader->leaderid, $leader->lang);
        }
die();



$initialCheck = $XeroOAuth->diagnostics ();
$checkErrors = count ( $initialCheck );
if ($checkErrors > 0) {
	// you could handle any config errors here, or keep on truckin if you like to live dangerously
	foreach ( $initialCheck as $check ) {
		echo 'Error: ' . $check . PHP_EOL;
	}
}  else {

        $session = persistSession ( array (
    			'oauth_token' => $XeroOAuth->config ['consumer_key'],
    			'oauth_token_secret' => $XeroOAuth->config ['shared_secret'],
    			'oauth_session_handle' => '' 
    	) );
    	$oauthSession = retrieveSession ();
    	
    	if (isset ( $oauthSession ['oauth_token'] )) {
    		$XeroOAuth->config ['access_token'] = $oauthSession ['oauth_token'];
    		$XeroOAuth->config ['access_token_secret'] = $oauthSession ['oauth_token_secret'];
    		
    		$response = $XeroOAuth->request('GET', $XeroOAuth->url('Accounts', 'core'), array('Where' => 'Type=="BANK"'));
            if ($XeroOAuth->response['code'] == 200) {
                $accounts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
                echo "There are " . count($accounts->Accounts[0]). " accounts in this Xero organisation, the first one is: </br>";
                pr($accounts->Accounts[0]->Account);
            } else {
                outputError($XeroOAuth);
            }
    	}
    	
    	
    	
		
		
		
	
	
}




















die();
$db->query("SELECT `code`,mail,firstname FROM leaders");
foreach($db->resultSet() as $leader){
    $mailer->newsletter($leader->code,$leader->mail, $leader->firstname);
}