<?php
if(isset($_GET['results'])){
  $result   = $_GET['results'];
  $leaderid = $_GET['leaderid'];
  $token    = $_GET['token'];
  $lang     = $_GET['lang'];
  
  $db->query("SELECT
                  invitations.prospectid,
                  invitations.leaderid,
                  invitations.token,
                  invitations.expire,
                  dbclients.mail,
                  dbclients.`name`
                  FROM
                  invitations
                  INNER JOIN dbclients ON invitations.prospectid = dbclients.id WHERE invitations.token=:token AND invitations.expire>NOW() AND invitations.istest=1;");
  $db->bind(":token", $token);
  $cast = $db->single();
  
  $tmp = ($result > 0) ? "testres_builder" : "testres_client";
  $mailer->testresults($cast->mail, $leaderid, $cast->name, $tmp, $lang);
}