<?php
if(isset($_GET['hub_mode']) && $_GET['hub_mode'] == 'subscribe') {
	$verify = (isset($_GET['hub_verify_token']) && $_GET['hub_verify_token']===FBPAGETOKEN);
	exit($verify?$_GET['hub_challenge']:'');
} else {
	
	$bot = new xpressbot($db);
	$request = file_get_contents('php://input');
	$signature = isset($_SERVER['HTTP_X_HUB_SIGNATURE']) ? $_SERVER['HTTP_X_HUB_SIGNATURE'] : null;
	if($signature != null && $bot->verify_payload($signature,$request)) {
		$bot->execute($request);
	} exit('200');
}
die();