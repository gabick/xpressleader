<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid       = $_POST['leaderid'];
    $salt           = "g3Tl1stPr0sp3cts";
    
    if($_POST['token'] == sha1($leaderid.$salt)){
        
        $result         = "";
        $lang           = (isset($_POST['lang'])) ? $_POST['lang'] : "en";

        $lbltitle    		= array("fr"=>"NOUVEAU CONTACT","en"=>"NEW CONTACT");
        $lblname            = array("fr"=>"Nom du contact","en"=>"Contact name");
        $lbltype            = array("fr"=>"Catégorie","en"=>"Type of contact");
        $lblbuilder         = array("fr"=>"Bâtisseur","en"=>"Builder");
        $lblemail           = array("fr"=>"Adresse courriel","en"=>"Email address");
        $lbllang            = array("fr"=>"Langue préférée","en"=>"Prefered language");
        $lblenglish         = array("fr"=>"Anglais","en"=>"English");
        $lblfrench          = array("fr"=>"Français","en"=>"French");

        $lblphone           = array("fr"=>"Téléphone","en"=>"Phone number");
        $lblcity            = array("fr"=>"Ville","en"=>"City");
        $lblregion          = array("fr"=>"Région/Province/État","en"=>"Province/State");
        $lblregionCA        = array("fr"=>"PROVINCES DU CANADA","en"=>"CANADA PROVINCES");
        $lblregionUS        = array("fr"=>"ÉTATS-UNIS","en"=>"UNITED STATES");
        $lblback   		    = array("fr"=>"Retour","en"=>"Back to list");
        $lblbtncancel       = array("fr"=>"Annuler","en"=>"Cancel");
        $lblbtnupdate       = array("fr"=>"Sauvegarder","en"=>"Create contact");
		
		$profile = '<div class="row">';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_name">'.$lblname[$lang].'*</label>
		                <input type="text" name="ctc_name" id="ctc_name" title="" value="" maxlength="50" class="form-control" />
		            </div>';
		$profile .= '</div>';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_type">'.$lbltype[$lang].'</label>
		                <select class="form-control" name="ctc_type" id="ctc_type">
        					<option value="0">Prospect</option>
        					<option value="1">Client</option>
        					<option value="2">'.$lblbuilder[$lang].'</option>
        				</select>
		            </div>';
		$profile .= '</div>';
		$profile .= '</div>';
		
		$profile .= '<div class="row">';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_email">'.$lblemail[$lang].'*</label>
		                <input type="text" name="ctc_email" id="ctc_email" title="" value="" maxlength="50" class="form-control" />
		            </div>';
		$profile .= '</div>';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_lang">'.$lbllang[$lang].'</label>
		                <select class="form-control" name="ctc_lang" id="ctc_lang">
        					<option value=""></option>
        					<option value="en">'.$lblenglish[$lang].'</option>
        					<option value="fr">'.$lblfrench[$lang].'</option>
        				</select>
		            </div>';
		$profile .= '</div>';
		$profile .= '</div>';
		
		$profile .= '<div class="row">';
		$profile .= '<div class="col-md-12">';
		$profile .= '<div class="form-group">
		                <label for="ctc_phone">'.$lblphone[$lang].'</label>
		                <input type="text" name="ctc_phone" id="ctc_phone" title="" value="" maxlength="50" class="form-control" />
		            </div>';
		$profile .= '</div>';
		$profile .= '</div>';
		
		
		$profile .= '<div class="row">';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_city">'.$lblcity[$lang].'</label>
		                <input type="text" name="ctc_city" id="ctc_city" title="" value="" maxlength="50" class="form-control" />
		            </div>';
		$profile .= '</div>';
		$profile .= '<div class="col-md-6">';
		$profile .= '<div class="form-group">
		                <label for="ctc_region">'.$lblregion[$lang].'</label>
		                <select class="form-control" name="ctc_region" id="ctc_region">
        					<option value=""></option>
        					<option value="">'.$lblregionCA[$lang].'</option>
        					<option value="AB"> &nbsp; Alberta</option>
        					<option value="BC"> &nbsp; British Columbia</option>
        					<option value="MB"> &nbsp; Manitoba</option>
        					<option value="NB"> &nbsp; New Brunswick</option>
        					<option value="NL"> &nbsp; Newfoundland and Labrador</option>
        					<option value="NS"> &nbsp; Nova Scotia</option>
        					<option value="ON"> &nbsp; Ontario</option>
        					<option value="PE"> &nbsp; Prince Edward Island</option>
        					<option value="QC"> &nbsp; Quebec</option>
        					<option value="SK"> &nbsp; Saskatchewan</option>
        					<option value="NT"> &nbsp; Northwest Territories</option>
        					<option value="NU"> &nbsp; Nunavut</option>
        					<option value="YT"> &nbsp; Yukon</option>
        					<option value="">'.$lblregionUS[$lang].'</option>
        					<option value="AL"> &nbsp; Alabama</option> 
        					<option value="AK"> &nbsp; Alaska</option> 
        					<option value="AZ"> &nbsp; Arizona</option> 
        					<option value="AR"> &nbsp; Arkansas</option> 
        					<option value="CA"> &nbsp; California</option> 
        					<option value="CO"> &nbsp; Colorado</option> 
        					<option value="CT"> &nbsp; Connecticut</option> 
        					<option value="DE"> &nbsp; Delaware</option> 
        					<option value="DC"> &nbsp; District Of Columbia</option> 
        					<option value="FL"> &nbsp; Florida</option> 
        					<option value="GA"> &nbsp; Georgia</option> 
        					<option value="HI"> &nbsp; Hawaii</option> 
        					<option value="ID"> &nbsp; Idaho</option> 
        					<option value="IL"> &nbsp; Illinois</option> 
        					<option value="IN"> &nbsp; Indiana</option> 
        					<option value="IA"> &nbsp; Iowa</option> 
        					<option value="KS"> &nbsp; Kansas</option> 
        					<option value="KY"> &nbsp; Kentucky</option> 
        					<option value="LA"> &nbsp; Louisiana</option> 
        					<option value="ME"> &nbsp; Maine</option> 
        					<option value="MD"> &nbsp; Maryland</option> 
        					<option value="MA"> &nbsp; Massachusetts</option> 
        					<option value="MI"> &nbsp; Michigan</option> 
        					<option value="MN"> &nbsp; Minnesota</option> 
        					<option value="MS"> &nbsp; Mississippi</option> 
        					<option value="MO"> &nbsp; Missouri</option> 
        					<option value="MT"> &nbsp; Montana</option> 
        					<option value="NE"> &nbsp; Nebraska</option> 
        					<option value="NV"> &nbsp; Nevada</option> 
        					<option value="NH"> &nbsp; New Hampshire</option> 
        					<option value="NJ"> &nbsp; New Jersey</option> 
        					<option value="NM"> &nbsp; New Mexico</option> 
        					<option value="NY"> &nbsp; New York</option> 
        					<option value="NC"> &nbsp; North Carolina</option> 
        					<option value="ND"> &nbsp; North Dakota</option> 
        					<option value="OH"> &nbsp; Ohio</option> 
        					<option value="OK"> &nbsp; Oklahoma</option> 
        					<option value="OR"> &nbsp; Oregon</option> 
        					<option value="PA"> &nbsp; Pennsylvania</option> 
        					<option value="RI"> &nbsp; Rhode Island</option> 
        					<option value="SC"> &nbsp; South Carolina</option> 
        					<option value="SD"> &nbsp; South Dakota</option> 
        					<option value="TN"> &nbsp; Tennessee</option> 
        					<option value="TX"> &nbsp; Texas</option> 
        					<option value="UT"> &nbsp; Utah</option> 
        					<option value="VT"> &nbsp; Vermont</option> 
        					<option value="VA"> &nbsp; Virginia</option> 
        					<option value="WA"> &nbsp; Washington</option> 
        					<option value="WV"> &nbsp; West Virginia</option> 
        					<option value="WI"> &nbsp; Wisconsin</option> 
        					<option value="WY"> &nbsp; Wyoming</option>
        				</select>
		            </div>';
		$profile .= '</div>';
		$profile .= '</div>';

		
		
        $result = '<div class="msgbox"></div>';
        $result .= '<div class="panel-heading panel-heading-black text-left" style="line-height: 80px;"><h3 class="modal-title" style="line-height: 70px;"><button type="button" class="btn btn-danger backContact" aria-label="Close"><i class="fa fa-chevron-left" aria-hidden="true"></i> '.$lblback[$lang].'</button> '.$lbltitle[$lang].'</h3></div>';
        $result .= '<div class="panel-body text-left">';
        $result .= $profile;
        $result .= '<div class="row">';
		$result .= '<div class="col-md-6"></div>';
		$result .= '<div class="col-md-3"><button type="button" id="btngetcancel" class="btn btn-block btn-default backContact">'.$lblbtncancel[$lang].'</button></div>';
		$result .= '<div class="col-md-3"><button type="button" id="btngetadd" class="btn btn-block btn-danger" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-token="'.sha1($leaderid.$salt).'">'.$lblbtnupdate[$lang].'</button></div>';
		$result .= '</div>';            
        $result .= '</div>';
        
        echo json_encode(array("result"=>true,"html"=>$result));
        die();
    }
}


