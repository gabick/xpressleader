<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid       = $_POST['leaderid'];
    $contactid      = $_POST['contactid'];
    $salt           = "g3Tl1stPr0sp3cts";
    
    if($_POST['token'] == sha1($leaderid.$contactid.$salt)){
        
        $lang           = (isset($_POST['lang'])) ? $_POST['lang'] : "en";
        $sqlcomeback    = (isset($_POST['callback'])) ? ",datecomeback=DATE_ADD(NOW(), INTERVAL ".$_POST['callback']." MONTH)" : "";
        
        $db->query("UPDATE dbclients SET archived=1, category=0, `status`=0 $sqlcomeback WHERE id=:contactid;");
        $db->bind(":contactid",$contactid);
        if($db->execute()){
            $lblmsg    		= array("fr"=>"Votre contact est maintenant archivé.","en"=>"The contact has been moved to the archive.");
            echo json_encode(array("message"=>$lblmsg[$lang],"iserror"=>"false"));
        }else{
            $lblmsg    		= array("fr"=>"Impossible d'archiver ce contact.","en"=>"We cannot move this contact to the archive.");
            echo json_encode(array("message"=>$lblmsg[$lang],"iserror"=>"true"));
        }
        
    }
}
die();