<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid       = $_POST['leaderid'];
    $contactid      = $_POST['contactid'];
    $lang           = $_POST['lang'];
    $salt           = "g3Tl1stPr0sp3cts";
    
    
    if($_POST['token'] == sha1($leaderid.$salt)){
        
        $name       = trim($_POST['ctcname']);
        $email      = trim($_POST['ctcemail']);
        $phone      = trim($_POST['ctcphone']);
        $city       = trim($_POST['ctccity']);
        $region     = trim($_POST['ctcregion']);
        $category   = trim($_POST['ctctype']);
        $ctclang   = trim($_POST['ctclang']);
        
        if($name == "" || $email == ""){
            $iserror    = true;
            $msg        = array("fr"=>"Le nom du contact et le courriel sont obligatoires.", "en"=>"The name and email address of your contact are mandatory.");
            echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
            die();
            
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $iserror    = true;
            $msg        = array("fr"=>"L'adresse courriel n'est pas valide.", "en"=>"Email address is not valid, please try again.");
            echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
            die();
 
        }else{
            if(!$contact->add($leaderid, $email, $name, $phone, $city, $region, $ctclang, $category)){
                $iserror    = true;
                $msg        = array("fr"=>"Impossible de créer le contact car il existe déjà.", "en"=>"Cannot create your contact, it's already exist.");
                echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
                die();
            }
        }
        
        
        $iserror    = false;
        $msg        = array("fr"=>"Votre contact a été crée avec succès.", "en"=>"Your contact has been created successfully.");
        echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
        die();
    
    }elseif($_POST['token'] == sha1($leaderid.$contactid.$salt)){
        
        $name       = trim($_POST['ctcname']);
        $email      = trim($_POST['ctcemail']);
        $phone      = trim($_POST['ctcphone']);
        $city       = trim($_POST['ctccity']);
        $region     = trim($_POST['ctcregion']);
        $category   = trim($_POST['ctctype']);
        $ctclang   = trim($_POST['ctclang']);
        
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $iserror    = true;
            $msg        = array("fr"=>"L'adresse courriel n'est pas valide.", "en"=>"Email address is not valid, please try again.");
            echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
            die();
            
        }elseif($name == ""){
            $iserror    = true;
            $msg        = array("fr"=>"Le nom du contact est obligatoire.", "en"=>"The name of your contact is mandatory.");
            echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
            die();
            
        }else{
            if(!$contact->update($contactid, $email, $name, $phone, $city, $region, $ctclang, $category)){
                $iserror    = true;
                $msg        = array("fr"=>"Impossible de mettre à jour le contact.", "en"=>"Cannot update this contact, refresh your page and try again.");
                echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
                die();
            }
        }
        
        
        
        $iserror    = false;
        $msg        = array("fr"=>"Votre contact a été mise à jour.", "en"=>"Your contact has been updated.");
        echo json_encode(array("message"=>$msg[$lang],"iserror"=>$iserror));
        die();
    }

}