<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid       = $_POST['leaderid'];
    $contactid      = $_POST['contactid'];
    $salt           = "g3Tl1stPr0sp3cts";
    
    $iserror        = ($_POST['iserror']=="true") ? true : false;
    $msg            = $_POST['msg'];
    
    if($_POST['token'] == sha1($leaderid.$contactid.$salt)){
        
        $result = "";
        $lang           = (isset($_POST['lang'])) ? $_POST['lang'] : "en";
        
        $prospect 	= $prospect->get($leaderid, $contactid);
        if($prospect){
            $lblstatus    		= array("fr"=>"Statut","en"=>"Status");
            $lblmeeting    		= array("fr"=>"Survol","en"=>"Meeting");
            $lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
            $lblpending   		= array("fr"=>"À contacter","en"=>"To contact");
            $lblinvited    	    = array("fr"=>"Invité(e)","en"=>"Invited");
            $lblexpired   		= array("fr"=>"Invitation expirée","en"=>"Expired invite");
            $lblarchived    	= array("fr"=>"Archive","en"=>"Archived");
            $lblname            = array("fr"=>"Nom du contact","en"=>"Contact name");
            $lbltype            = array("fr"=>"Catégorie","en"=>"Type of contact");
            $lblbuilder         = array("fr"=>"Bâtisseur","en"=>"Builder");
            $lblemail           = array("fr"=>"Adresse courriel","en"=>"Email address");
            $lbllang            = array("fr"=>"Langue préférée","en"=>"Prefered language");
            $lblenglish         = array("fr"=>"Anglais","en"=>"English");
            $lblfrench          = array("fr"=>"Français","en"=>"French");
            $lblgender          = array("fr"=>"Sexe","en"=>"Gender");
            $lblmale            = array("fr"=>"Homme","en"=>"Male");
            $lblfemale          = array("fr"=>"Femme","en"=>"Female");
            $lblphone           = array("fr"=>"Téléphone","en"=>"Phone number");
            $lblcity            = array("fr"=>"Ville","en"=>"City");
            $lblregion          = array("fr"=>"Région/Province/État","en"=>"Province/State");
            $lblregionCA        = array("fr"=>"PROVINCES DU CANADA","en"=>"CANADA PROVINCES");
            $lblregionUS        = array("fr"=>"ÉTATS-UNIS","en"=>"UNITED STATES");
            $lbldatebirth       = array("fr"=>"Date de naissance","en"=>"Date of birth");
            $lblday             = array("fr"=>"Jour","en"=>"Day");
            $lblmonth           = array("fr"=>"Mois","en"=>"Month");
            $lblnever           = array("fr"=>"Jamais","en"=>"Never");
            $lblyear            = array("fr"=>"Année","en"=>"Year");
            $lblhours           = array("fr"=>"Heures","en"=>"Hours");
            $lblminutes         = array("fr"=>"Minutes","en"=>"Minutes");
            $lblback   		    = array("fr"=>"Retour","en"=>"Back to list");
            $lblbtncancel       = array("fr"=>"Annuler","en"=>"Cancel");
            $lblbtnupdate       = array("fr"=>"Sauvegarder","en"=>"Update contact");
            
            $lblinvitetitle     = array("fr"=>"Invitation à un survol d'entreprise","en"=>"Invite to a business overview");
            $lblmodalinvite     = array("fr"=>"Envoyez une invitation...","en"=>"Send an invitation");
            $lblinviteleft      = array("fr"=>"Nombre d'envoi:","en"=>"# invites:");
            $lblexpirein        = array("fr"=>"Expire dans","en"=>"Expire in");
            $lblinviteimg       = array("fr"=>"Voici un exemple du courriel qui sera envoyé à votre prospect","en"=>"This is an exemple of the mail that will be sent to your prospect");
            $lblinvitewait      = array("fr"=>"ATTENDEZ UNE SECONDE !","en"=>"WAIT A SECOND !");
            $lblinvitedesc      = array("fr"=>"Avant d'envoyer une invitation, assurez-vous de bien lire les règles suivantes :","en"=>"Before sending your invitation, please make sure you understand those simple rules :");
            $lblinvitedesc1     = array("fr"=>"Vous ne pouvez envoyer plus de 3 invitations par prospect.","en"=>"You cannot send more than 3 invitations to a prospect.");
            $lblinvitedesc2     = array("fr"=>"Assurez-vous de contacter votre prospect AVANT d'envoyer votre invitation.","en"=>"Make sure to contact your prospect BEFORE sending the invite.");
            $lblinvitedesc3     = array("fr"=>"Les prospects peuvent maintenant se désabonner de vos courriels. Vous ne serez plus en mesure d'envoyer aucun courriel aux prospects qui sont désabonnés!","en"=>"The prospect will be able to unsubscribe from your mail at any time. You will NOT be able to send any mails to the prospect after that.");
            $lblbtninviteconfirm = array("fr"=>"ENVOYER MON INVITATION","en"=>"SEND MY INVITE NOW");
            
            $lblarchivedtitle      = array("fr"=>"n'est pas intéressé ?","en"=>"is not interested ?");
            $lblarchiveddesc        = array("fr"=>"Ne vous laissez pas abattre, cela ne veut pas dire que votre contact ne sera intéressé dans un avenir rapproché !","en"=>"Don't be discouraged, it does not mean that your contact will not be interested in the near future!");
            $lblbtnarchived         = array("fr"=>"ARCHIVER CE CONTACT","en"=>"MOVE TO ARCHIVE");
            
            
            $lblclientarchivedtitle      = array("fr"=>"n'est plus client(e) de l'entreprise ?","en"=>"is not a client anymore ?");
            $lblclientarchiveddesc        = array("fr"=>"Ce n'est pas grave, archiver votre client et recontactez le dans quelques mois si vous le désirez.","en"=>"This is the client choice, we have to respect that. Archive your client and call him back in a few months.");
            $lblbtnclientarchived         = array("fr"=>"ARCHIVER CE CLIENT","en"=>"MOVE TO ARCHIVE");
            
            $expired    = false;
            $iswatching = ($prospect->dtwatch != null && ((strtotime($prospect->dtwatch)+10) >= (time()))) ? '<i class="fa fa-eye" aria-hidden="true"></i>' : '';
            $progress   = ($prospect->position > 0) ? "(".number_format(($prospect->position / 1200)*100,0)."%)" : "";
            $progress   = ($prospect->position >= 1200) ? "(100%)" : $progress;
            
                
            if($prospect->status==1) {
				$status         = "<span class=\"label label-primary\" style=\"width:100%;display:block;border-radius:0;line-height:50px;\">$iswatching ".mb_strtoupper($lblmeeting[$lang])." $progress</span>";
			}elseif($prospect->status==0) {
				if($prospect->tokexpire && strtotime($prospect->tokexpire) < time()) {
					$status 	= "<span class=\"label label-danger\" style=\"width:100%;line-height:50px;display:block;border-radius:0;\">".mb_strtoupper($lblexpired[$lang])."</span>";
					$expired 	= true;
				}elseif($prospect->tokexpire && strtotime($prospect->tokexpire) > time() && $prospect->token) {
					    $status  = "<span class=\"label label-warning\" style=\"width:100%;display:block;display:block;line-height:50px;\">".mb_strtoupper($lblinvited[$lang])."</span>";
				}else  $status  = "<span class=\"label label-default\" style=\"width:100%;display:block;display:block;border-radius:0;line-height:50px;\">".mb_strtoupper($lblpending[$lang])."</span>";
			}elseif($prospect->status==2) {
				$status         = "<span class=\"label label-success\" style=\"width:100%;display:block;display:block;border-radius:0;line-height:50px;\">".mb_strtoupper($lblinterest[$lang])." $progress</span>";
			}
			
			if($prospect->category==1){
			    $status         = "<span class=\"label\" style=\"width:100%;display:block;display:block;border-radius:0;line-height:46px;color:#7aa668;border:solid 1px #7aa668;font-size:1.2em;\">".mb_strtoupper("CLIENT")."</span>";
			}elseif($prospect->category==2){
			    $status         = "<span class=\"label\" style=\"width:100%;display:block;display:block;border-radius:0;line-height:46px;color:#e22c2c;border:solid 1px #e22c2c;font-size:1.2em;\">".mb_strtoupper($lblbuilder[$lang])."</span>";
			}
			
			if($prospect->archived==1) {
			    $status = "<span class=\"label\" style=\"width:100%;display:block;display:block;line-height:40px;color:#333;border:solid 1px #eee;font-size:1.2em;\">".mb_strtoupper($lblarchived[$lang])."</span>";
			}
			
			
			if($prospect->tokexpire){
                $now = new DateTime();
                $future_date = new DateTime($prospect->tokexpire);
                $interval = $future_date->diff($now);
            }
            
            
            $msgbox     = "";
            if($iserror && $msg != ""){
                $msgbox = '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-danger" role="alert">'.$msg.'</div></div></div>';
            }elseif($msg != ""){
                $msgbox = '<div class="row"><div class="col-md-12 text-center"><div class="alert alert-success" role="alert">'.$msg.'</div></div></div>';
            }
            
            
            $btninvite   = ($prospect->nbrinvite < 3 && $prospect->status <= 0) ? '<button class="btn btn-block btn-warning" data-toggle="modal" data-target="#sendInvite"><i class="fa fa-envelope" aria-hidden="true"></i> '.$lblmodalinvite[$lang].'</button>' : '<strong class="text-danger">Non disponible</strong>';
            
            $summaries   = '<div class="row">';
            $summaries  .= '<div class="col-md-5">';
            $summaries  .= '<table class="table">
            <tr>
                <td colspan="2"><h4>'.$lblinvitetitle[$lang].'</h4></td>
            </tr>
            <tr>
                <td>'.$lblinviteleft[$lang].'</td>
                <td><strong>'.$prospect->nbrinvite.'</strong> / 3</td>
            </tr>
            <tr>
                <td>'.$lblexpirein[$lang].'</td>
                <td><strong>'.(isset($interval)?$interval->format("%h ".$lblhours[$lang].", %I ".$lblminutes[$lang].""):'n/a').'</strong></td>
            </tr>
            <tr>
                <td class="text-center" colspan="2">'.$btninvite.'</td>
            </tr>
            </table>';
            $summaries  .= '</div>';
            $summaries  .= '<div class="col-md-2">';
            $summaries  .= '</div>';
            $summaries  .= '<div class="col-md-5">';
            $summaries  .= ($prospect->category == 0) ? '<table class="table">
            <tr>
                <td><h4>Conversion du prospect</h4></td>
            </tr>
            <tr>
                <td><button class="btn btn-block btn-lg btn-danger">Convertir en client</button></td>
            </tr>
            <tr>
                <td><button class="btn btn-block btn-sm btn-default" data-toggle="modal" data-target="#modalArchived">Pas intéressé(e)</button></td>
            </tr>
            
            </table>' : 
            
            '<table class="table">
            <tr>
                <td><h4>Statut et Cancellation</h4></td>
            </tr>
            <tr>
                <td><button class="btn btn-block btn-lg btn-danger" data-toggle="modal" data-target="#modalClientArchived">Archiver le client</button></td>
            </tr>
            <tr>
                <td><button class="btn btn-block btn-sm btn-default" data-toggle="modal" data-target="#modalClientStatus">Modifier le statut</button></td>
            </tr>
            
            </table>';
            $summaries  .= '</div>';
            $summaries  .= '</div>';
            
            
            $modalinvite = '<!-- Modal -->
                        <div class="modal fade" id="sendInvite" tabindex="-1" role="dialog" aria-labelledby="sendInviteLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong style="font-size: 2em;">'.$lblinvitewait[$lang].'</strong> <p>'.$lblinvitedesc[$lang].'</p>
                              </div>
                              <div class="modal-body text-left">
                                <div class="row">
                                    <div class="col-md-5">
                                        <table class="table table-striped table-bordered">
                                            <tr><td>'.$lblinvitedesc1[$lang].'</td></tr>
                                            <tr><td>'.$lblinvitedesc2[$lang].'</td></tr>
                                            <tr><td>'.$lblinvitedesc3[$lang].'</td></tr>
                                            <tr><td><button class="btn btn-block btn-lg btn-primary" data-dismiss="modal" aria-label="Close" id="btnSendinvite" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-contactid="'.$prospect->id.'" data-token="'.sha1($leaderid.$prospect->id.$salt).'"><i class="fa fa-envelope" aria-hidden="true"></i> '.$lblbtninviteconfirm[$lang].'</button></td></tr>
                                        </table>
                                        
                                    </div>
                                    <div class="col-md-7"><img src="//media.xpressleader.com/images/ssInvite_'.$lang.'.png" alt="" class="img img-responsive"/><small>'.$lblinviteimg[$lang].'</small></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="modal fade" id="modalArchived" tabindex="-1" role="dialog" aria-labelledby="modalArchivedLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3>'.$prospect->name.' '.$lblarchivedtitle[$lang].'</h3><p>'.$lblarchiveddesc[$lang].'</p>
                              </div>
                              <div class="modal-body text-left">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            <label for="ctc_callback">Réactiver le contact dans</label>
                    		                <select class="form-control" name="ctc_callback" id="ctc_callback">
                            					<option value="">'.$lblnever[$lang].'</option>
                            					<option value="1">1 '.$lblmonth[$lang].'</option>
                            					<option value="3">3 '.$lblmonth[$lang].'</option>
                            					<option value="12">12 '.$lblmonth[$lang].'</option>
                            				</select>
                        				</p>
                                        <button class="btn btn-block btn-lg btn-danger" data-dismiss="modal" aria-label="Close" id="btnArchiveContact" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-contactid="'.$prospect->id.'" data-token="'.sha1($leaderid.$prospect->id.$salt).'">'.$lblbtnarchived[$lang].'</button>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        
                        <!-- Modal -->
                        <div class="modal fade" id="sendInvite" tabindex="-1" role="dialog" aria-labelledby="sendInviteLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong style="font-size: 2em;">'.$lblinvitewait[$lang].'</strong> <p>'.$lblinvitedesc[$lang].'</p>
                              </div>
                              <div class="modal-body text-left">
                                <div class="row">
                                    <div class="col-md-5">
                                        <table class="table table-striped table-bordered">
                                            <tr><td>'.$lblinvitedesc1[$lang].'</td></tr>
                                            <tr><td>'.$lblinvitedesc2[$lang].'</td></tr>
                                            <tr><td>'.$lblinvitedesc3[$lang].'</td></tr>
                                            <tr><td><button class="btn btn-block btn-lg btn-primary" data-dismiss="modal" aria-label="Close" id="btnSendinvite" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-contactid="'.$prospect->id.'" data-token="'.sha1($leaderid.$prospect->id.$salt).'"><i class="fa fa-envelope" aria-hidden="true"></i> '.$lblbtninviteconfirm[$lang].'</button></td></tr>
                                        </table>
                                        
                                    </div>
                                    <div class="col-md-7"><img src="//media.xpressleader.com/images/ssInvite_'.$lang.'.png" alt="" class="img img-responsive"/><small>'.$lblinviteimg[$lang].'</small></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="modal fade" id="modalClientArchived" tabindex="-1" role="dialog" aria-labelledby="modalClientArchivedLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header text-left">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3>'.$prospect->name.' '.$lblclientarchivedtitle[$lang].'</h3><p>'.$lblclientarchiveddesc[$lang].'</p>
                              </div>
                              <div class="modal-body text-left">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            <label for="ctc_callback">Réactiver le contact dans</label>
                    		                <select class="form-control" name="ctc_callback" id="ctc_callback">
                            					<option value="">'.$lblnever[$lang].'</option>
                            					<option value="1">1 '.$lblmonth[$lang].'</option>
                            					<option value="3">3 '.$lblmonth[$lang].'</option>
                            					<option value="12">12 '.$lblmonth[$lang].'</option>
                            				</select>
                        				</p>
                                        <button class="btn btn-block btn-lg btn-danger" data-dismiss="modal" aria-label="Close" id="btnArchiveContact" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-contactid="'.$prospect->id.'" data-token="'.sha1($leaderid.$prospect->id.$salt).'">'.$lblbtnclientarchived[$lang].'</button>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>';
			
			
			
			$profile = '<div class="row">';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_name">'.$lblname[$lang].'*</label>
    		                <input type="text" name="ctc_name" id="ctc_name" title="" value="'.$prospect->name.'" maxlength="50" class="form-control" />
    		            </div>';
			$profile .= '</div>';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_type">'.$lbltype[$lang].'</label>
    		                <select class="form-control" name="ctc_type" id="ctc_type">
            					<option value="0" '.(($prospect->category==0)?'selected="selected"':'').'>Prospect</option>
            					<option value="1" '.(($prospect->category==1)?'selected="selected"':'').'>Client</option>
            					<option value="2" '.(($prospect->category==2)?'selected="selected"':'').'>'.$lblbuilder[$lang].'</option>
            				</select>
    		            </div>';
			$profile .= '</div>';
			$profile .= '</div>';
			
			$profile .= '<div class="row">';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_email">'.$lblemail[$lang].'*</label>
    		                <input type="text" name="ctc_email" id="ctc_email" title="" value="'.$prospect->mail.'" maxlength="50" class="form-control" />
    		            </div>';
			$profile .= '</div>';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_lang">'.$lbllang[$lang].'</label>
    		                <select class="form-control" name="ctc_lang" id="ctc_lang">
            					<option value=""></option>
            					<option value="en" '.(($prospect->lang=="en")?'selected="selected"':'').'>'.$lblenglish[$lang].'</option>
            					<option value="fr" '.(($prospect->lang=="fr")?'selected="selected"':'').'>'.$lblfrench[$lang].'</option>
            				</select>
    		            </div>';
			$profile .= '</div>';
			$profile .= '</div>';
			
			$profile .= '<div class="row">';
			$profile .= '<div class="col-md-12">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_phone">'.$lblphone[$lang].'</label>
    		                <input type="text" name="ctc_phone" id="ctc_phone" title="" value="'.$prospect->phone.'" maxlength="50" class="form-control" />
    		            </div>';
			$profile .= '</div>';
			$profile .= '</div>';
			
			
			$profile .= '<div class="row">';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_city">'.$lblcity[$lang].'</label>
    		                <input type="text" name="ctc_city" id="ctc_city" title="" value="'.$prospect->city.'" maxlength="50" class="form-control" />
    		            </div>';
			$profile .= '</div>';
			$profile .= '<div class="col-md-6">';
			$profile .= '<div class="form-group">
    		                <label for="ctc_region">'.$lblregion[$lang].'</label>
    		                <select class="form-control" name="ctc_region" id="ctc_region">
            					<option value=""></option>
            					<option value="">'.$lblregionCA[$lang].'</option>
            					<option value="AB" '.(($prospect->region=='AB')?'selected="selected"':'').'> &nbsp; Alberta</option>
            					<option value="BC" '.(($prospect->region=='BC')?'selected="selected"':'').'> &nbsp; British Columbia</option>
            					<option value="MB" '.(($prospect->region=='MB')?'selected="selected"':'').'> &nbsp; Manitoba</option>
            					<option value="NB" '.(($prospect->region=='NB')?'selected="selected"':'').'> &nbsp; New Brunswick</option>
            					<option value="NL" '.(($prospect->region=='NL')?'selected="selected"':'').'> &nbsp; Newfoundland and Labrador</option>
            					<option value="NS" '.(($prospect->region=='NS')?'selected="selected"':'').'> &nbsp; Nova Scotia</option>
            					<option value="ON" '.(($prospect->region=='ON')?'selected="selected"':'').'> &nbsp; Ontario</option>
            					<option value="PE" '.(($prospect->region=='PE')?'selected="selected"':'').'> &nbsp; Prince Edward Island</option>
            					<option value="QC" '.(($prospect->region=='QC')?'selected="selected"':'').'> &nbsp; Quebec</option>
            					<option value="SK" '.(($prospect->region=='SK')?'selected="selected"':'').'> &nbsp; Saskatchewan</option>
            					<option value="NT" '.(($prospect->region=='NT')?'selected="selected"':'').'> &nbsp; Northwest Territories</option>
            					<option value="NU" '.(($prospect->region=='NU')?'selected="selected"':'').'> &nbsp; Nunavut</option>
            					<option value="YT" '.(($prospect->region=='YT')?'selected="selected"':'').'> &nbsp; Yukon</option>
            					<option value="">'.$lblregionUS[$lang].'</option>
            					<option value="AL" '.(($prospect->region=='AL')?'selected="selected"':'').'> &nbsp; Alabama</option> 
            					<option value="AK" '.(($prospect->region=='AK')?'selected="selected"':'').'> &nbsp; Alaska</option> 
            					<option value="AZ" '.(($prospect->region=='AZ')?'selected="selected"':'').'> &nbsp; Arizona</option> 
            					<option value="AR" '.(($prospect->region=='AR')?'selected="selected"':'').'> &nbsp; Arkansas</option> 
            					<option value="CA" '.(($prospect->region=='CA')?'selected="selected"':'').'> &nbsp; California</option> 
            					<option value="CO" '.(($prospect->region=='CO')?'selected="selected"':'').'> &nbsp; Colorado</option> 
            					<option value="CT" '.(($prospect->region=='CT')?'selected="selected"':'').'> &nbsp; Connecticut</option> 
            					<option value="DE" '.(($prospect->region=='DE')?'selected="selected"':'').'> &nbsp; Delaware</option> 
            					<option value="DC" '.(($prospect->region=='DC')?'selected="selected"':'').'> &nbsp; District Of Columbia</option> 
            					<option value="FL" '.(($prospect->region=='FL')?'selected="selected"':'').'> &nbsp; Florida</option> 
            					<option value="GA" '.(($prospect->region=='GA')?'selected="selected"':'').'> &nbsp; Georgia</option> 
            					<option value="HI" '.(($prospect->region=='HI')?'selected="selected"':'').'> &nbsp; Hawaii</option> 
            					<option value="ID" '.(($prospect->region=='ID')?'selected="selected"':'').'> &nbsp; Idaho</option> 
            					<option value="IL" '.(($prospect->region=='IL')?'selected="selected"':'').'> &nbsp; Illinois</option> 
            					<option value="IN" '.(($prospect->region=='IN')?'selected="selected"':'').'> &nbsp; Indiana</option> 
            					<option value="IA" '.(($prospect->region=='IA')?'selected="selected"':'').'> &nbsp; Iowa</option> 
            					<option value="KS" '.(($prospect->region=='KS')?'selected="selected"':'').'> &nbsp; Kansas</option> 
            					<option value="KY" '.(($prospect->region=='KY')?'selected="selected"':'').'> &nbsp; Kentucky</option> 
            					<option value="LA" '.(($prospect->region=='LA')?'selected="selected"':'').'> &nbsp; Louisiana</option> 
            					<option value="ME" '.(($prospect->region=='ME')?'selected="selected"':'').'> &nbsp; Maine</option> 
            					<option value="MD" '.(($prospect->region=='MD')?'selected="selected"':'').'> &nbsp; Maryland</option> 
            					<option value="MA" '.(($prospect->region=='MA')?'selected="selected"':'').'> &nbsp; Massachusetts</option> 
            					<option value="MI" '.(($prospect->region=='MI')?'selected="selected"':'').'> &nbsp; Michigan</option> 
            					<option value="MN" '.(($prospect->region=='MN')?'selected="selected"':'').'> &nbsp; Minnesota</option> 
            					<option value="MS" '.(($prospect->region=='MS')?'selected="selected"':'').'> &nbsp; Mississippi</option> 
            					<option value="MO" '.(($prospect->region=='MO')?'selected="selected"':'').'> &nbsp; Missouri</option> 
            					<option value="MT" '.(($prospect->region=='MT')?'selected="selected"':'').'> &nbsp; Montana</option> 
            					<option value="NE" '.(($prospect->region=='NE')?'selected="selected"':'').'> &nbsp; Nebraska</option> 
            					<option value="NV" '.(($prospect->region=='NV')?'selected="selected"':'').'> &nbsp; Nevada</option> 
            					<option value="NH" '.(($prospect->region=='NH')?'selected="selected"':'').'> &nbsp; New Hampshire</option> 
            					<option value="NJ" '.(($prospect->region=='NJ')?'selected="selected"':'').'> &nbsp; New Jersey</option> 
            					<option value="NM" '.(($prospect->region=='NM')?'selected="selected"':'').'> &nbsp; New Mexico</option> 
            					<option value="NY" '.(($prospect->region=='NY')?'selected="selected"':'').'> &nbsp; New York</option> 
            					<option value="NC" '.(($prospect->region=='NC')?'selected="selected"':'').'> &nbsp; North Carolina</option> 
            					<option value="ND" '.(($prospect->region=='ND')?'selected="selected"':'').'> &nbsp; North Dakota</option> 
            					<option value="OH" '.(($prospect->region=='OH')?'selected="selected"':'').'> &nbsp; Ohio</option> 
            					<option value="OK" '.(($prospect->region=='OK')?'selected="selected"':'').'> &nbsp; Oklahoma</option> 
            					<option value="OR" '.(($prospect->region=='OR')?'selected="selected"':'').'> &nbsp; Oregon</option> 
            					<option value="PA" '.(($prospect->region=='PA')?'selected="selected"':'').'> &nbsp; Pennsylvania</option> 
            					<option value="RI" '.(($prospect->region=='RI')?'selected="selected"':'').'> &nbsp; Rhode Island</option> 
            					<option value="SC" '.(($prospect->region=='SC')?'selected="selected"':'').'> &nbsp; South Carolina</option> 
            					<option value="SD" '.(($prospect->region=='SD')?'selected="selected"':'').'> &nbsp; South Dakota</option> 
            					<option value="TN" '.(($prospect->region=='TN')?'selected="selected"':'').'> &nbsp; Tennessee</option> 
            					<option value="TX" '.(($prospect->region=='TX')?'selected="selected"':'').'> &nbsp; Texas</option> 
            					<option value="UT" '.(($prospect->region=='UT')?'selected="selected"':'').'> &nbsp; Utah</option> 
            					<option value="VT" '.(($prospect->region=='VT')?'selected="selected"':'').'> &nbsp; Vermont</option> 
            					<option value="VA" '.(($prospect->region=='VA')?'selected="selected"':'').'> &nbsp; Virginia</option> 
            					<option value="WA" '.(($prospect->region=='WA')?'selected="selected"':'').'> &nbsp; Washington</option> 
            					<option value="WV" '.(($prospect->region=='WV')?'selected="selected"':'').'> &nbsp; West Virginia</option> 
            					<option value="WI" '.(($prospect->region=='WI')?'selected="selected"':'').'> &nbsp; Wisconsin</option> 
            					<option value="WY" '.(($prospect->region=='WY')?'selected="selected"':'').'> &nbsp; Wyoming</option>
            				</select>
    		            </div>';
			$profile .= '</div>';
			$profile .= '</div>';

			
			$result .= $modalinvite;
            $result .= '<div class="msgbox">'.$msgbox.'</div>';
            $result .= $status;
            $result .= '<div class="panel-heading panel-heading-black text-left" style="line-height: 80px;"><h3 class="modal-title" style="line-height: 70px;"><button type="button" class="btn btn-danger backContact" aria-label="Close"><i class="fa fa-chevron-left" aria-hidden="true"></i> '.$lblback[$lang].'</button> '.$prospect->name.' <small style="color:#8c8c8c;">'.$prospect->mail.'</small> </h3></div>';
            $result .= '<div class="panel-body text-left">';
            $result .= $summaries;
            $result .= '<div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                            </ul>
                            
                            <div class="tab-content text-left" style="padding:10px;min-height: 350px;">
                                <div role="tabpanel" class="tab-pane fade in active" id="profile">'.$profile.'</div>
                            </div>
                        </div>';
            $result .= '<div class="row">';
			$result .= '<div class="col-md-6"></div>';
			$result .= '<div class="col-md-3"><button type="button" id="btngetcancel" class="btn btn-block btn-default backContact">'.$lblbtncancel[$lang].'</button></div>';
			$result .= '<div class="col-md-3"><button type="button" id="btngetupdate" class="btn btn-block btn-danger" data-leaderid="'.$leaderid.'" data-lang="'.$lang.'" data-contactid="'.$prospect->id.'" data-token="'.sha1($leaderid.$prospect->id.$salt).'">'.$lblbtnupdate[$lang].'</button></div>';
			$result .= '</div>';            
            $result .= '</div>';
            
            
            
            echo json_encode(array("result"=>true,"html"=>$result));
            die();
                
        }else{
            $lblstatus    		= array("fr"=>"Aucun prospect n'a été trouvé.","en"=>"No prospect has been found.");
            $result             = '<h4 style="line-height:200px;">'.$lblstatus[$lang].' <i class="fa fa-frown-o" aria-hidden="true"></i></h4>';
        }
        
        echo json_encode(array("result"=>true,"html"=>$result));
        die();
        
    }
}


