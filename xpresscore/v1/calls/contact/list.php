<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid   = $_POST['leaderid'];
    $salt       = "g3Tl1stPr0sp3cts";
    
    if($_POST['token'] == sha1($leaderid.$salt)){
        
        $result = "";
        $lang           = (isset($_POST['lang'])) ? $_POST['lang'] : "en";
        $key            = (isset($_POST['key'])) ? $_POST['key'] : "";
        $orderby        = (isset($_POST['orderby'])) ? $_POST['orderby'] : "name";
        $sort           = (isset($_POST['sort'])) ? $_POST['sort'] : "ASC";
        $page           = (isset($_POST['page'])) ? $_POST['page'] : 1;
        $perpage        = (isset($_POST['perpage'])) ? $_POST['perpage'] : 100;
        $statusid       = (isset($_POST['statusid'])) ? $_POST['statusid'] : "ALL";
        
        //Save perpage in session
        $db->query("UPDATE `leaders` SET ctcperpage=:perpage WHERE `code`=:leaderid");
        $db->bind(":leaderid", $leaderid);
        $db->bind(":perpage", $perpage);
        $db->execute();
        
        try{
            $prospects 	= $contact->search($leaderid, $statusid, $key, $page, $perpage, $orderby, $sort);
        }catch(exception $e){
            mail("info@softnox.net","error",print_r($e,true));
        }
        if($prospects){
            $ctc_total      = $contact->statusCount($leaderid, $statusid);
            $totalpages     = ceil($ctc_total / $perpage);
           
            $linkpages      = "";
            for ($k = 1 ; $k <= $totalpages; $k++){  $linkpages      .= '<li><a href="#" class="chpage" data-page="'.$k.'">'.$k.'</a></li>'; }
            
            $lblstatus    		= array("fr"=>"Statut","en"=>"Status");
            $lblmeeting    		= array("fr"=>"Survol","en"=>"Meeting");
            $lblinterest    	= array("fr"=>"Intérêt","en"=>"Interest");
            $lblinvited    	    = array("fr"=>"Invité(e)","en"=>"Invited");
            $lblarchived    	= array("fr"=>"Archive","en"=>"Archived");
            $lblpending   		= array("fr"=>"À contacter","en"=>"To contact");
            $lblexpired   		= array("fr"=>"Invitation expirée","en"=>"Expired invite");
            $lblbtnview   		= array("fr"=>"Détails","en"=>"View");
            $lblemail   		= array("fr"=>"COURRIEL","en"=>"EMAIL");
            $lblbuilder   		= array("fr"=>"Bâtisseur","en"=>"Builder");
            
            $result = '<table class="table table-condensed table-hover xpress-table">';
            $result .= '<tr>';
            $result .= '<th>STATUS</th>';
            $result .= '<th>CONTACT</th>';
            $result .= '<th>'.$lblemail[$lang].'</th>';
            $result .= '<th></th>';
            $result .= '</tr>';
            foreach($prospects as $pro){
                $expired    = false;
                $iswatching = ($pro->dtwatch != null && ((strtotime($pro->dtwatch)+10) >= (time()))) ? '<i class="fa fa-eye" aria-hidden="true"></i>' : '';
                $progress   = ($pro->position > 0) ? "(".number_format(($pro->position / 1200)*100,0)."%)" : "";
                $progress   = ($pro->position >= 1200) ? "(100%)" : $progress;
                
                if($pro->status==1) {
					$status         = "<span class=\"label label-primary\" style=\"width:100%;display:block;line-height:40px;\">$iswatching ".mb_strtoupper($lblmeeting[$lang])." $progress</span>";
				}elseif($pro->status==0) {
					if($pro->tokexpire && strtotime($pro->tokexpire) < time()) {
						$status 	= "<span class=\"label label-danger\" style=\"width:100%;line-height:40px;display:block;\">".mb_strtoupper($lblexpired[$lang])."</span>";
						$expired 	= true;
					}elseif($pro->tokexpire && strtotime($pro->tokexpire) > time() && $pro->token) {
					    $status  = "<span class=\"label label-warning\" style=\"width:100%;display:block;display:block;line-height:40px;\">".mb_strtoupper($lblinvited[$lang])."</span>";
					
					}else  $status  = "<span class=\"label label-default\" style=\"width:100%;display:block;display:block;line-height:40px;\">".mb_strtoupper($lblpending[$lang])."</span>";
				}elseif($pro->status==2) {
					$status         = "<span class=\"label label-success\" style=\"width:100%;display:block;display:block;line-height:40px;\">".mb_strtoupper($lblinterest[$lang])." $progress</span>";
				}
				
				if($pro->category==1){
				    $status         = "<span class=\"label\" style=\"width:100%;display:block;display:block;line-height:40px;color:#7aa668;border:solid 1px #7aa668;font-size:1.2em;\">".mb_strtoupper("CLIENT")."</span>";
				}elseif($pro->category==2){
				    $status         = "<span class=\"label\" style=\"width:100%;display:block;display:block;line-height:40px;color:#e22c2c;border:solid 1px #e22c2c;font-size:1.2em;\">".mb_strtoupper($lblbuilder[$lang])."</span>";
				}
				
				if($pro->archived==1) {
				    $status = "<span class=\"label\" style=\"width:100%;display:block;display:block;line-height:40px;color:#333;border:solid 1px #eee;font-size:1.2em;\">".mb_strtoupper($lblarchived[$lang])."</span>";
				}
				
				$result .= '<tr>';
				$result .= '<td class="text-left" style="max-width:80px;">'.$status.'</td>';
				$result .= '<td class="text-left"><a href="#" class="loadContact" data-prospectid="'.$pro->id.'" data-token="'.sha1($leaderid.$pro->id.$salt).'">'.$pro->name.'</a><br><span style="color:#8a8a8a;">'.$pro->city.''.(($pro->region!="")?', '.$pro->region:'').'</span></td>';
				$result .= '<td class="text-left">'.$pro->mail.'</td>';
				$result .= '<td class="text-right"></td>';
				$result .= '</tr>';
            }
            $result .= '<tr>';
            $result .= '<th class="text-left">
                            <div class="btn-group dropup">
                              <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                '.$perpage.' per page
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </a>
                            
                              <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="#" class="perpage" data-perpage="50">50</a></li>
                                <li><a href="#" class="perpage" data-perpage="100">100</a></li>
                                <li><a href="#" class="perpage" data-perpage="150">150</a></li>
                                <li><a href="#" class="perpage" data-perpage="200">200</a></li>
                              </ul>
                            </div>
                        </th>
                        <th colspan="2" class="text-center"> 
                            <div class="btn-group dropup">
                              <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                PAGE '.$page.'
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </a>
                            
                              <ul class="dropdown-menu" aria-labelledby="dLabel">
                                '.$linkpages.'
                              </ul>
                              / '.$totalpages.'
                            </div>
                        
                        
                         </th>
                        <th class="text-right">'.$ctc_total.' CONTACTS</th>';
            $result .= '</tr>';
            $result .= '</table>';
                
        }else{
            
            $lblstatus  = array("fr"=>"Aucun contact n'a été trouvé.","en"=>"No contact has been found.");
            $result     = '<h4 style="line-height:200px;">'.$lblstatus[$lang].' <i class="fa fa-frown-o" aria-hidden="true"></i></h4>';
        }
        
        echo json_encode(array("result"=>true,"html"=>$result));
        die();
        
    }
}