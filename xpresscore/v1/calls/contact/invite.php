<?php
header("content-type:application/json");
if(isset($_POST['token']) && isset($_POST['leaderid'])){
    
    $leaderid       = $_POST['leaderid'];
    $contactid      = $_POST['contactid'];
    $salt           = "g3Tl1stPr0sp3cts";
    
    if($_POST['token'] == sha1($leaderid.$contactid.$salt)){
        
        $lang           = (isset($_POST['lang'])) ? $_POST['lang'] : "en";
        
        $db->query("UPDATE dbclients SET token=:token, tokexpire=DATE_ADD(NOW(), INTERVAL 1 DAY), nbrinvite=(nbrinvite+1), archived=0 WHERE id=:contactid;");
        $db->bind(":token", sha1($leaderid.$contactid.$salt.time()));
        $db->bind(":contactid",$contactid);
        if($db->execute()){
            $lblmsg    		= array("fr"=>"Votre invitation a été envoyé avec succès.","en"=>"The invitation has been sent successfully.");
            echo json_encode(array("message"=>$lblmsg[$lang],"iserror"=>"false"));
        }else{
            $lblmsg    		= array("fr"=>"Impossible d'envoyer une invitation à ce prospect.","en"=>"We cannot send an invitation to this prospect.");
            echo json_encode(array("message"=>$lblmsg[$lang],"iserror"=>"true"));
        }
        
    }
}
die();