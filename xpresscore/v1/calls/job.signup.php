<?php
header("content-type:application/json");
if($_POST)    {
    $leaderid     = ($_POST['leaderid']=="") ? "" : trim($_POST['leaderid']);
    $token        = ($_POST['token']=="") ? "" : trim($_POST['token']);

if($token == sha1($leaderid."signupMynewAccount!")) { 
    
    $firstname    = ($_POST['firstname']=="") ? "" : trim($_POST['firstname']);
    $lastname     = ($_POST['lastname']=="") ? "" : trim($_POST['lastname']);
    $email1       = ($_POST['email1']=="") ? "" : trim($_POST['email1']);
    $email2       = ($_POST['email2']=="") ? "" : trim($_POST['email2']);
    
    
      if($firstname==""||$lastname==""||$email1==""||$email2==""){
          echo json_encode(array("result"=>false,"message"=>array(
                "fr"=>"Tous les champs sont obligatoires.", 
                "en"=>"Missing informations, please try again."
            )));
      }elseif($email1!=$email2){
          echo json_encode(array("result"=>false,"message"=>array(
                "fr"=>"Le courriel de confirmation n'est doit être identique.", 
                "en"=>"Emails does not matches, please try again."
            )));
      }elseif(!filter_var($email1, FILTER_VALIDATE_EMAIL)) {
          echo json_encode(array("result"=>false,"message"=>
          array(
                "fr"=>"Votre courriel n'est pas valide.", 
                "en"=>"The email address is not valid, please try again."
            )));
      }else{
            if($account->signup($firstname, $lastname, $email1, $leaderid, $_POST['lang'])){
                echo json_encode(array("result"=>true,"message"=>
                                array(
                                    "fr"=>"<h3>Merci de votre confiance!</h3> <p>Un courriel d'activation vous sera envoyé dans les prochaines minutes afin de confirmer vos informations.</p> <button type=\"button\" class=\"btn btn-block btn-danger\" data-dismiss=\"modal\">Fermer cette fenêtre</button>", 
                                    "en"=>"<h3>Thank you!</h3> <p>Check your mailbox, in a few minutes, you will receive an email to confirm your informations and complete your subscribtion. </p> <button type=\"button\" class=\"btn btn-block btn-danger\" data-dismiss=\"modal\">Close this window</button>"
                                )));
            }else echo json_encode(array("result"=>false,"message"=>
            array(
                "fr"=>"Impossible de créer votre compte, votre courriel est peut-être déjà utilisé ?", 
                "en"=>"We cannot create your account at the moment, maybe your email address is already in use ?"
            )));
      }
      
} else echo json_encode(array("result"=>false,"message"=>
            array(
                "fr"=>"Impossible de créer votre compte, réessayez plus tard.", 
                "en"=>"We cannot create your account at the moment, please try again later."
            )));
}