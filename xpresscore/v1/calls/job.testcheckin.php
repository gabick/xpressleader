<?php 
if(isset($_GET['key']) && $_GET['key']<>""){
    $db->query("SELECT prospectid FROM invitations WHERE token=:token;");
    $db->bind(":token", $_GET['key']);
    $event = $db->single();
    
    if($event){
        $db->query("UPDATE dbclients SET position=:position WHERE id=:prospectid AND position<:position;");
        $db->bind(":position", $_GET['position']);
        $db->bind(":prospectid", $event->prospectid);
        $db->execute();
        
        $db->query("UPDATE dbclients SET dtwatch=NOW() WHERE id=:prospectid;");
        $db->bind(":prospectid", $event->prospectid);
        $db->execute();
    }
}