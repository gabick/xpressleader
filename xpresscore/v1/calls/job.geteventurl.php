<?php
header("content-type:application/json");
if($_POST)    {
    $eventid      = ($_POST['eventid']=="") ? "" : trim($_POST['eventid']);
    $token        = ($_POST['token']=="") ? "" : trim($_POST['token']);
    if($token == sha1($eventid."AttendT3HSuper3V3nt!")) {
        
        $name           = ($_POST['name']=="") ? "" : trim($_POST['name']);
        $email          = ($_POST['email']=="") ? "" : trim($_POST['email']);
        $leaderid       = ($_POST['leaderid']=="") ? "" : trim($_POST['leaderid']);
        
        $leader = $account->getleader($leaderid);
        $name = $name.' ['.$leader->firstname.' '.$leader->lastname.']';
        
          if($name==""||$email==""){
              echo json_encode(array("result"=>false,"message"=>array(
                    "fr"=>"Tous les champs sont obligatoires.", 
                    "en"=>"Missing informations, please try again."
                )));
    
          }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              echo json_encode(array("result"=>false,"message"=>
              array(
                    "fr"=>"Votre courriel n'est pas valide.", 
                    "en"=>"The email address is not valid, please try again."
                )));

          }else{
                $metadata = json_encode(array("leaderid"=>$leaderid, "email"=>$email));
                
                //Get Event information
                $curevent   = $event->getEventInfo($eventid);
                
                if($_POST['password'] != $curevent->password){
                   echo json_encode(array("result"=>false,"message"=>
                      array(
                            "fr"=>"Votre mot de passe n'est pas valide.", 
                            "en"=>"Password is not valid, please try again."
                        ))); 
                }else{
                    if($curevent){
                        if($bigbluebutton->create($curevent->token, $curevent->name, $curevent->evmod, $curevent->evusr, $metadata)){
                            $joinUrl =  $bigbluebutton->joinUrl($curevent->token, $name, $curevent->evusr, "nouser");
                            echo json_encode(array("result"=>true,"message"=>array("fr"=>$joinUrl,"en"=>$joinUrl)));
                        }else $joinUrl = '';
                    }else $joinUrl = '';
                }
        
          }
          
    } else echo json_encode(array("result"=>false,"message"=>
                array(
                    "fr"=>"Impossible de créer votre compte, réessayez plus tard.", 
                    "en"=>"We cannot create your account at the moment, please try again later."
                )));
}