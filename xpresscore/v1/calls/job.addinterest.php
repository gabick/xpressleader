<?php
header("content-type:application/json");

if($_POST)    {
    $eventid      = ($_POST['key']=="") ? "" : trim($_POST['key']);
    $token        = ($_POST['token']=="") ? "" : trim($_POST['token']);
    
    if($token == sha1($eventid."AddT3HSuperPr0sp3ct!")) { 
        
        $name           = ($_POST['name']=="") ? "" : trim($_POST['name']);
        $email          = ($_POST['email']=="") ? "" : trim($_POST['email']);
        $phone          = ($_POST['phone']=="") ? "" : trim($_POST['phone']);
        $lang           = ($_POST['lang']=="") ? "" : trim($_POST['lang']);
        $leaderid       = ($_POST['leaderid']=="") ? "" : trim($_POST['leaderid']);
        
        if($name==""||$email==""||$phone==""){
            echo json_encode(array("result"=>false,"message"=>array(
                "fr"=>"Tous les champs sont obligatoires. Votre numéro de téléphone est manquant.",
                "en"=>"Missing informations, please enter your phone number."
            )));
        
        }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo json_encode(array("result"=>false,"message"=>
            array(
                "fr"=>"Votre courriel n'est pas valide.", 
                "en"=>"The email address is not valid, please try again."
            )));

        }else{
            
            $prospect->interest($leaderid, $eventid, $phone, $lang,  $name);
            
            echo json_encode(array("result"=>true,"message"=>array(
                    "fr"=>"Merci de votre intérêt ! Vous recevrez par courriel un document explicatif qui résume ce que vous avez visionné.", 
                    "en"=>"Thanks for your interest ! You will be emailed an explanatory document that summarizes this overview."
                )));
        
        }
          
    } else echo json_encode(array("result"=>false,"message"=>
            array(
                "fr"=>"Impossible de créer votre compte, réessayez plus tard.", 
                "en"=>"We cannot create your account at the moment, please try again later."
            )));
}