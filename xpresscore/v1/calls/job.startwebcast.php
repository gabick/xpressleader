<?php
header("content-type:application/json");

if($_POST)    {
    $eventid      = ($_POST['key']=="") ? "" : trim($_POST['key']);
    $token        = ($_POST['token']=="") ? "" : trim($_POST['token']);
    
    if($token == sha1($eventid."AttendT3HSuper3V3nt!")) { 
        
        $name           = ($_POST['name']=="") ? "" : trim($_POST['name']);
        $email          = ($_POST['email']=="") ? "" : trim($_POST['email']);
        $leaderid       = ($_POST['leaderid']=="") ? "" : trim($_POST['leaderid']);
        
          if(($name==""||$email=="")){
              echo json_encode(array("result"=>false,"message"=>array(
                    "fr"=>"Tous les champs sont obligatoires.", 
                    "en"=>"Missing informations, please try again."
                )));
    
          }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              echo json_encode(array("result"=>false,"message"=>
              array(
                    "fr"=>"Votre courriel n'est pas valide.", 
                    "en"=>"The email address is not valid, please try again."
                )));

          }else{
              $db->query("SELECT id,name,mail,phone FROM dbclients WHERE leaderid=:leaderid AND mail=:email;");
              $db->bind(":leaderid", $leaderid);
              $db->bind(":email", $email);
              $prospect = $db->single();
              
              if($prospect){
              
                    $db->query("UPDATE dbclients SET `status`=1 WHERE leaderid=:leaderid AND mail=:email AND `status`<=1;");
                    $db->bind(":leaderid", $leaderid);
                    $db->bind(":email", $email);
                    $db->execute();
                    
                    $bot = new xpressbot($db);
                    
                    $buttons = array("fr"=> array(
    								"type"=>"postback",
    								"title"=>"VOIR STATUS",
    								"payload"=>"STATS"),
    								
    								"en"=> array(
    								"type"=>"postback",
    								"title"=>"GET STATUS",
    								"payload"=>"STATS")
    							);
    							
    							
					$message = array(
                            "fr"=>$prospect->name." (".$email.") regarde présentement un vidéo de survol.", 
                            "en"=>$prospect->name." (".$email.") is currently watching a business overview."
                        );
                        
                    
                    $bot->Notifytemplate($fbuser,$message, $buttons);

                    
                    echo json_encode(array("result"=>true,"message"=>array(
                            "fr"=>"", 
                            "en"=>""
                        )));
              }else{
                  echo json_encode(array("result"=>false,"message"=>
                        array(
                            "fr"=>"Votre courriel n'est pas valide. Entrez le même courriel que votre invitation.", 
                            "en"=>"Email address is not valid. Please use the same email as your invitation."
                        )));
              }
        
          }
          
    } else echo json_encode(array("result"=>false,"message"=>
                array(
                    "fr"=>"Impossible de créer votre compte, réessayez plus tard.", 
                    "en"=>"We cannot create your account at the moment, please try again later."
                )));
}