<?xml version="1.0" ?>
<config>
    <localeversion suppressWarning="true">0.8</localeversion>
    <version>4357-2014-02-06</version>
    <help url="http://%BBBHOST%/help.html"/>
    <javaTest url="http://%BBBHOST%/testjava.html"/>
    <porttest host="%BBBHOST%" application="video/portTest" timeout="10000"/>
    <bwMon server="198.50.158.30" application="video/bwTest"/>
    <application uri="rtmp://%BBBHOST%/bigbluebutton" host="http://%BBBHOST%/bigbluebutton/api/enter" />
    <language userSelectionEnabled="true" />
    <skinning enabled="true" url="http://%BBBHOST%/client/branding/css/BBBDefault.css.swf" />
    <shortcutKeys showButton="true" />
    <layout showLogButton="false" showVideoLayout="false" showResetLayout="true" defaultLayout="NoChat"
            showToolbar="true" showFooter="false" showMeetingName="true" showHelpButton="true" 
            showLogoutWindow="false" showLayoutTools="false" showNetworkMonitor="false" confirmLogout="false"/>
                <modules>
                <module name="ChatModule" url="http://%BBBHOST%/client/ChatModule.swf?v=4357" 
                        uri="rtmp://%BBBHOST%/bigbluebutton" 
                        dependsOn="UsersModule" 
                        translationOn="false"
                        translationEnabled="false"      
                        privateEnabled="true"  
                        position="top-right"
                        baseTabIndex="701"
                />
                <module name="UsersModule" url="http://%BBBHOST%/client/UsersModule.swf?v=4357" 
                        uri="rtmp://%BBBHOST%/bigbluebutton" 
                        allowKickUser="true"
                        enableRaiseHand="true"
                        enableSettingsButton="true"
                        baseTabIndex="301"
                />
                <module name="DeskShareModule" 
                        url="http://%BBBHOST%/client/DeskShareModule.swf?v=4105" 
                        uri="rtmp://%BBBHOST%/deskShare"
                        showButton="true"
                        autoStart="false"
                        autoFullScreen="false"
                        baseTabIndex="201"
                />
                <module name="PhoneModule" url="http://%BBBHOST%/client/PhoneModule.swf?v=4357" 
                        uri="rtmp://%BBBHOST%/sip" 
                        autoJoin="true"
                        skipCheck="false"
                        showButton="true"
                        enabledEchoCancel="true"
                        dependsOn="UsersModule"
                />
                
                <module name="VideoconfModule" url="http://%BBBHOST%/client/VideoconfModule.swf?v=4357" 
                        uri="rtmp://%BBBHOST%/video"
                        dependson = "UsersModule"
                        videoQuality = "90"
                        presenterShareOnly = "true"
                        controlsForPresenter = "true"
                        resolutions = "320x240,640x480,1280x720"
                        autoStart = "false"
                        skipCamSettingsCheck="false"
                        showButton = "true"
                        showCloseButton = "true"
                        publishWindowVisible = "true"
                        viewerWindowMaxed = "false"
                        viewerWindowLocation = "top"
                        camKeyFrameInterval = "30"
                        camModeFps = "10"
                        camQualityBandwidth = "0"
                        camQualityPicture = "90"
                        smoothVideo="false"
                        applyConvolutionFilter="false"
                        convolutionFilter="-1, 0, -1, 0, 6, 0, -1, 0, -1"
                        filterBias="0"
                        filterDivisor="4"
                        enableH264 = "true"
                        h264Level = "2.1"
                        h264Profile = "main"            
                        displayAvatar = "false"
                        focusTalking = "false"
                        glowColor = "0x4A931D"
                        glowBlurSize = "30.0"   
                />
                <module name="WhiteboardModule" url="http://%BBBHOST%/client/WhiteboardModule.swf?v=4105" 
                        uri="rtmp://%BBBHOST%/bigbluebutton" 
                        dependsOn="PresentModule"
                        baseTabIndex="601"
                        whiteboardAccess="presenter"
                        keepToolbarVisible="false"
                />
                
                <module name="PresentModule" url="http://%BBBHOST%/client/PresentModule.swf?v=4357" 
                        uri="rtmp://%BBBHOST%/bigbluebutton" 
                        host="http://%BBBHOST%" 
                        showPresentWindow="true"
                        showWindowControls="true"
                        openExternalFileUploadDialog="false"
                        dependsOn="UsersModule"
                        baseTabIndex="501"
                        maxFileSize="30"
                />
                <module name="VideodockModule" url="http://%BBBHOST%/client/VideodockModule.swf?v=4357"
                        uri="rtmp://%BBBHOST%/bigbluebutton" 
                        dependsOn="VideoconfModule, UsersModule"
                        autoDock="true"
                        showControls="true"
                        maximizeWindow="false"
                        position="bottom-right"
                        width="172"
                        height="179"
                        layout="smart"
                        oneAlwaysBigger="false"
                        baseTabIndex="401"
                />
                <module name="LayoutModule" url="http://%BBBHOST%/client/LayoutModule.swf?v=4357"
                        uri="rtmp://%BBBHOST%/bigbluebutton"
                        layoutConfig="http://%BBBHOST%/client/conf/layout.xml"
                        enableEdit="false"
                /> 
        </modules>
</config>